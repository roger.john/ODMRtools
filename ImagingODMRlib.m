ClearAll[SetCameraTriggeringMode, GetCameraTriggeringMode, 
  ImagingODMR, MaximizeImagingContrast, CaptureImage];
Options[SetCameraTriggeringMode] = {NumberOfTries -> 3};
SetCameraTriggeringMode[triggerMode_String, OptionsPattern[]] := 
  Module[{currentTriggerMode = 
     AndorSDK3GetEnumString[$ODMRcamera, "TriggerMode"]}, 
   If[currentTriggerMode != triggerMode,
    Module[{acquisitionRunning = 
       AndorSDK3LiveViewAcquisitionRunning[], 
      timeout = AndorSDK3GetLiveViewAcquisitionTimeout[], n = 0}, 
     If[acquisitionRunning, AndorSDK3StopLiveViewAcquisition[]; 
      Pause[2*timeout]];
     While[
      n++ < OptionValue[NumberOfTries] && 
       triggerMode != currentTriggerMode,
      AndorSDK3SetEnumString[$ODMRcamera, "TriggerMode", 
       triggerMode];
      Pause[timeout];
      currentTriggerMode = 
       AndorSDK3GetEnumString[$ODMRcamera, "TriggerMode"]];
     If[acquisitionRunning, AndorSDK3StartLiveViewAcquisition[]; 
      Pause[2*timeout]];
     If[triggerMode != currentTriggerMode, 
      Print["could not set triggerMode to " <> triggerMode <> 
        ", still is " <> currentTriggerMode <> " (acquisition was " <>
         If[acquisitionRunning, "", "not "] <> "running)"]; $Failed]
     ]]
   ];
GetCameraTriggeringMode[] := 
  AndorSDK3GetEnumString[$ODMRcamera, "TriggerMode"];
Options[ImagingODMR] = {Autorun -> False, ExposureTime -> 0.1, 
   TrackerSamples -> 3, HistoryLength -> 500, 
   FixedAxes -> {True, True, False}, TrackerTime -> 10, 
   TrackerIntervall -> 120, OutputData -> False, 
   MaxScans -> \[Infinity], MaxScanTime -> \[Infinity], 
   MicrowaveSettlingTime -> 0.011, CustomDisplayFunction -> None, 
   SnapshotIntervall -> \[Infinity], 
   SnapshotFilePrefix -> "ImagingODMR", 
   MicrowaveSwitchInstalled -> True, UsePulseModulation -> True, 
   SaveIntervall -> 600, PreviousDataFile -> None, 
   PreviousNumberOfScans -> 0, TrackerEnabled -> False, 
   TrackerStepSize -> 0.5, TrackerMinCountsToTrack -> 1000, 
   TrackerDiscriminationFactors -> {1.03, 1.03, 1.03}, 
   ShowAverageSpectrum -> True, WaitForCustomDisplayFunction -> True, 
   AbortOnMissedTriggers -> False};
ImagingODMR[xyz0 : {_?NumericQ, _?NumericQ, _?NumericQ} | Null, 
   poi : {{_Integer, _Integer} ...} | Null, 
   frequencies : _?NumericQ | {__?NumericQ}, 
   power : _?NumericQ | {__?NumericQ}, rawImagesFile : _String | Null,
    options : OptionsPattern[]] := 
  Module[{historyLength = OptionValue[HistoryLength], 
     outputData = (#1 === True &)[OptionValue[OutputData]],
     scan = OptionValue[Autorun] === True,
     et = OptionValue[ExposureTime],
     mst = OptionValue[MicrowaveSettlingTime], 
     maxScans = (If[NumericQ[#1] || #1 === \[Infinity], #1, 0] &)[
       OptionValue[MaxScans]], 
     maxScanTime = (If[NumericQ[#1] || #1 === \[Infinity], #1, 0] &)[
       OptionValue[MaxScanTime]],
     track = (OptionValue[TrackerEnabled] === True),
     minCountsToTrack = 
      OptionValue[TrackerMinCountsToTrack]*OptionValue[ExposureTime],
     numTrackingSamples = OptionValue[TrackerSamples],
     trackerStepSize = OptionValue[TrackerStepSize], 
     trackerDiscriminationFactors = 
      OptionValue[TrackerDiscriminationFactors],
     fixedAxes = 
      Switch[#, 
         All, {True, True, 
          True}, {_?BooleanQ, _?BooleanQ, _?BooleanQ}, #, _, {False, 
          False, False}] &@OptionValue[FixedAxes],
     trackerIntervall = OptionValue[TrackerIntervall],
     trackerTime = OptionValue[TrackerTime],
     microwaveSwitchInstalled = 
      OptionValue[MicrowaveSwitchInstalled] === True, 
     usePulseModulation = OptionValue[UsePulseModulation] === True, 
     snapshotIntervall = OptionValue[SnapshotIntervall], 
     snapshotFilePrefix = OptionValue[SnapshotFilePrefix],
     saveIntervall = OptionValue[SaveIntervall],
     customDisplayFunction = OptionValue[CustomDisplayFunction],
     showAverageSpectrum = OptionValue[ShowAverageSpectrum], 
     waitForCustomDisplayFunction = 
      OptionValue[WaitForCustomDisplayFunction],
     abortOnMissedTriggers = OptionValue[AbortOnMissedTriggers]},
    Module[{quit = False, abort = False, showHistory = False, 
      showLastSweep = False, x, y, z, t, \[Delta], c = 0, n, 
      scans = 0, history = {}, 
      historyIndices = Range[-historyLength, -1], 
      lastScanStartTime = -\[Infinity], accumulatedScanTime = 0, 
      lastTrackingStartTime = -\[Infinity], 
      lastTrackingEndTime = -\[Infinity], 
      lastSnapshotTime = -\[Infinity], lastSaveTime = -\[Infinity], 
      scanReversed = False, microwaveActive, microwaveEnabled, 
      stageRange, lastExport, et0, poiData, paddedFrequencies, 
      paddedPower, activateMicrowave, deactivateMicrowave, 
      enableMicrowave, disableMicrowave, programMicrowaveScanList, 
      reverseMicrowaveScanList, generateOutput, 
      customDisplayEvaluationFunction, 
      customDisplayFunctionResult = None, 
      imageWidth = AndorSDK3GetInt[$ODMRcamera, "AOIWidth"], 
      imageHeight = AndorSDK3GetInt[$ODMRcamera, "AOIHeight"], 
      rawPoiCoordinates, dumpRawImagesToFile, 
      odmrAccumulationBuffers = {}, samplingAccumulationBuffers = {}, 
      availableSamplingAccumulationBuffers = {}, cleanup, 
      trackingContext, tracking = False, mode, manualSave = False, 
      samplingFunction, oldTimeout, timeout = 2 et, extractPOIdata, 
      clear = False},
     rawPoiCoordinates = {1, imageWidth}.Round[# - 1] & /@ 
       If[! showAverageSpectrum && (poi === Null || 
           Length[poi] == 0), {Round[{imageWidth, imageHeight}/2]}, 
        If[poi === Null, {}, poi]];
     customDisplayEvaluationFunction = 
      If[Head[customDisplayFunction] === Function, 
        customDisplayFunctionResult = ((Switch[#1,
                {$Aborted, _}, quit = True; #1[[2]],
                {$Failed, _}, quit = True; abort = True; #1[[2]],
                {$Clear, _}, clear = True; #1[[2]],
                _, #1] &)[If[MatchQ[#1, Null], {#1, None}, #1]] &)[
          customDisplayFunction[poiData]]] &;
     programMicrowaveScanList = 
      Function[{freq, powers}, 
       Module[{action = "programming microwave"}, 
         Monitor[SCPIcmd[$ODMRsmiq, 
           ":LIST:FREQ " <> 
            Riffle[(ToString[#1, CForm] &) /@ freq, ","]]; 
          SCPIcmd[$ODMRsmiq, 
           ":LIST:POWER " <> 
            Riffle[(ToString[#1, CForm] &) /@ powers, ","]];, 
          action]];];
     reverseMicrowaveScanList = 
      Module[{action = "reversing microwave scan list"}, 
        Monitor[scanReversed = ! scanReversed; 
         If[microwaveActive, SCPIcmd[$ODMRsmiq, ":FREQ:MODE FIX"]; 
          SCPIcmd[$ODMRsmiq, 
           ":POWER:MODE FIX"]]; (programMicrowaveScanList @@ 
             If[scanReversed, 
              Reverse /@ #1, #1] &)[{paddedFrequencies, paddedPower}];
          If[microwaveActive, SCPIcmd[$ODMRsmiq, ":LIST:LEARN"]; 
          SCPIcmd[$ODMRsmiq, ":FREQ:MODE LIST"]; 
          SCPIcmd[$ODMRsmiq, "*WAI"]; 
          While[SCPIqueryNumber[$ODMRsmiq, ":FREQ"] == Null && ! quit,
            action = action <> "."]; 
          Pause[1]];, {Button["quit", quit = True], action}]] &;
     activateMicrowave = 
      If[! microwaveActive || (Initialize /. {##1}) === True, 
        Module[{action = "switching on microwave"}, 
         Monitor[SCPIcmd[$ODMRsmiq, ":OUTP:STATE 1"]; 
          SCPIcmd[$ODMRsmiq, ":LIST:LEARN"]; 
          SCPIcmd[$ODMRsmiq, ":FREQ:MODE LIST"]; 
          SCPIcmd[$ODMRsmiq, "*WAI"]; Pause[0.5]; 
          While[SCPIqueryNumber[$ODMRsmiq, ":FREQ"] == Null && ! quit,
            action = action <> "."]; 
          Pause[1];, {Button["quit", quit = True], action}]]; 
        microwaveActive = True;] &;
     deactivateMicrowave = 
      If[microwaveActive || (Initialize /. {##1}) === True, 
        Module[{action = "switching off microwave"}, 
         Monitor[SCPIcmd[$ODMRsmiq, ":FREQ:MODE FIX"]; 
          SCPIcmd[$ODMRsmiq, ":POWER:MODE FIX"]; 
          SCPIcmd[$ODMRsmiq, ":OUTP:STATE 0"]; 
          SCPIcmd[$ODMRsmiq, "*WAI"]; 
          While[SCPIqueryNumber[$ODMRsmiq, ":FREQ"] == Null && ! quit,
            action = action <> "."];, {Button["quit", quit = True], 
           action}]]; microwaveActive = False;] &;
     enableMicrowave = 
      If[! microwaveEnabled || (Initialize /. {##1}) === True, 
        EnableAOM[MicrowaveSwitchEnabled -> True]; 
        microwaveEnabled = True;] &;
     disableMicrowave = 
      If[microwaveEnabled || (Initialize /. {##1}) === True, 
        EnableAOM[MicrowaveSwitchEnabled -> False]; 
        microwaveEnabled = False;] &;
     cleanup = Function[
       If[microwaveSwitchInstalled, disableMicrowave[]];
       deactivateMicrowave[];
       AndorSDK3StopLiveViewAcquisition[];
       Pause[timeout];
       AndorSDK3GetWaitingLiveViewAccumulationBuffers[True]; 
       AndorSDK3GetFinishedLiveViewAccumulationBuffers[True]; 
       AndorSDK3DestroyLiveViewAccumulationBuffers[
        Join[samplingAccumulationBuffers, odmrAccumulationBuffers]];
       AndorSDK3SetEnumString[$ODMRcamera, "TriggerMode", "Internal"];
        AndorSDK3SetFloat[$ODMRcamera, "ExposureTime", N@et0];
       AndorSDK3SetLiveViewAcquisitionTimeout[oldTimeout];
       AndorSDK3StartLiveViewAcquisition[];
       ];
     samplingFunction = 
      Function[
       poiSamplingPoints, (history = 
           Join[If[Length[history] > historyLength - Length[#], 
             Take[history, -(historyLength - Length[#])], history], #/
             et];
          (c = #/et; #) &@Mean[#]) &@poiSamplingPoints;
       If[
        track \[And] (! scan || tracking), (MoveStage[#1]; 
           trackingContext = #2; {x, y, z} = #2[[2]]) & @@ 
         TrackAdvisor[Mean /@ poiSamplingPoints, {x, y, z}, 
          trackingContext, 
          TrackerDiscriminationFactors -> 
           trackerDiscriminationFactors, 
          TrackAdvisorMinCountsToTrack -> minCountsToTrack, 
          TrackAdvisorMinSamplesPerAxis -> numTrackingSamples, 
          TrackerSteps -> trackerStepSize, ActualPosition -> False, 
          FixedAxes -> fixedAxes], MoveStage[{x, y, z}]]; 
       poiSamplingPoints];
     extractPOIdata = 
      Function[buffers, 
       If[showAverageSpectrum, 
          Transpose@
           Append[Transpose[#], 
            First@Transpose[
              AndorSDK3ComputeStatisticsOfLiveViewAccumulationBuffers[
               buffers]]], #] &@
        AndorSDK3ExtractPOIformLiveViewAccumulationBuffers[
         rawPoiCoordinates, buffers]];
     stageRange = GetStageRange[];
     deactivateMicrowave[Initialize -> True];
     {paddedFrequencies, paddedPower} = (Switch[{#1, #2},
          {_List, _List}, {#1, #2},
          {_List, _}, {#1, ConstantArray[#2, Length[#1]]},
          {_, _List}, {ConstantArray[#1, Length[#2]], #2},
          _, {{#1}, {#2}}] &)[frequencies, power];
     SCPIcmd[$ODMRsmiq, 
      ":FREQ " <> ToString[Mean[paddedFrequencies], CForm]]; 
     SCPIcmd[$ODMRsmiq, 
      ":POWER " <> ToString[Mean[paddedPower], CForm]]; 
     SCPIcmd[$ODMRsmiq, ":LIST:DELETE:ALL"];
     SCPIcmd[$ODMRsmiq, ":LIST:SELECT 'ODMR'"]; 
     SCPIcmd[$ODMRsmiq, ":TRIG1:LIST:SOURCE SINGLE"];
     SCPIcmd[$ODMRsmiq, ":LIST:MODE AUTO"]; 
     SCPIcmd[$ODMRsmiq, ":LIST:DWELL " <> ToString[et + mst, CForm]]; 
     programMicrowaveScanList[paddedFrequencies, paddedPower]; 
     If[usePulseModulation, 
      SCPIcmd[$ODMRsmiq, ":PULM:POLARITY NORMAL"]; 
      SCPIcmd[$ODMRsmiq, ":PULM:STATE ON"];, 
      SCPIcmd[$ODMRsmiq, ":PULM:STATE OFF"];];
     AndorSDK3StopLiveViewAcquisition[];
     et0 = AndorSDK3GetFloat[$ODMRcamera, "ExposureTime"];
     AndorSDK3SetFloat[$ODMRcamera, "ExposureTime", N@et];
     oldTimeout = AndorSDK3GetLiveViewAcquisitionTimeout[];
     AndorSDK3SetLiveViewAcquisitionTimeout[N@timeout];
     AndorSDK3SetEnumString[$ODMRcamera, "IOSelector", 
      "External Trigger"];
     AndorSDK3SetBool[$ODMRcamera, "IOInvert", True];
     SetCameraTriggeringMode[$ODMRcamera, "TriggerMode", "Internal"];
     samplingAccumulationBuffers = 
      AndorSDK3CreateLiveViewAccumulationBuffers[
       1 & /@ Range[numTrackingSamples]];
     If[Length[samplingAccumulationBuffers] < numTrackingSamples || ! 
        MatchQ[samplingAccumulationBuffers, {___Integer}], cleanup[]; 
      Return[$Failed["could not alloc tracking buffers"]]];
     availableSamplingAccumulationBuffers = 
      samplingAccumulationBuffers;
     {scans, odmrAccumulationBuffers} = 
      If[StringQ[#], 
         Print[
          "trying to restore from " <> #]; {OptionValue[
           PreviousNumberOfScans], 
          AndorSDK3CreateLiveViewAccumulationBuffersFromFile[#]}, {0, 
          AndorSDK3CreateLiveViewAccumulationBuffers[
           1 & /@ paddedFrequencies]}] &@OptionValue[PreviousDataFile];
     If[Length[odmrAccumulationBuffers] < 
        Length[paddedFrequencies] || ! 
        MatchQ[odmrAccumulationBuffers, {___Integer}], cleanup[]; 
      Return[$Failed["could not alloc odmr buffers"]]];
     AndorSDK3DestroyLiveViewAccumulationBuffers@
      AndorSDK3GetWaitingLiveViewAccumulationBuffers[True];
     AndorSDK3StartLiveViewAcquisition[];
     poiData = extractPOIdata[odmrAccumulationBuffers];
     {x, y, z} = If[xyz0 =!= Null, xyz0, ReadStage[]];
     trackingContext = TrackAdvisor[{}, {x, y, z}][[2]];
     generateOutput = 
      Function[
       Module[{scans1 = Max[scans, 1]}, {If[outputData, 
          Transpose[{paddedFrequencies, poiData}], 
          ListLinePlot[(SortBy[#1, First] &)[
              Transpose[{paddedFrequencies, #/(scans1*et)}]] & /@ 
            Transpose[poiData], PlotRange -> All, Axes -> False, 
           Frame -> True]], {x, y, z}, poi, power, rawImagesFile, 
         scans}]];
     If[microwaveSwitchInstalled, disableMicrowave[Initialize -> True];
      activateMicrowave[];];
     lastSaveTime = lastSnapshotTime = SessionTime[];
     Monitor[Monitor[
       While[! quit && ! abort && scans < maxScans && 
         accumulatedScanTime < maxScanTime,
        If[scan && ! tracking,
         mode = "scanning";
         lastScanStartTime = SessionTime[];
         SetCameraTriggeringMode["External"];
         availableSamplingAccumulationBuffers = 
          Join[availableSamplingAccumulationBuffers, 
           AndorSDK3GetWaitingLiveViewAccumulationBuffers[True], 
           AndorSDK3GetFinishedLiveViewAccumulationBuffers[True]];
         If[
          scan && track && 
           lastScanStartTime - lastTrackingEndTime >= 
            trackerIntervall, lastTrackingStartTime = t;
          MaximizeImagingContrast[Accumulations -> numTrackingSamples];
          z = ReadStage[][[3]];
          lastTrackingEndTime = SessionTime[];
          lastScanStartTime = lastTrackingEndTime;
          ];
         If[clear, 
          AndorSDK3ResetLiveViewAccumulationBuffers[
           odmrAccumulationBuffers];
          poiData = Map[0 &, poiData, {2}]; scans = 0; 
          clear = False];
         AndorSDK3QueueLiveViewAccumulationBuffers[
          odmrAccumulationBuffers, 1];
         If[microwaveSwitchInstalled, enableMicrowave[], 
          activateMicrowave[]];
         SCPIcmd[$ODMRsmiq, ":TRIG:LIST"];
         t = SessionTime[];
         If[! waitForCustomDisplayFunction, 
          customDisplayEvaluationFunction[]];
         (*\[Delta]={};
         Module[{finishedODMRaccumulationBuffers={}},
         While[!abort&&Length[finishedODMRaccumulationBuffers]<Length[
         paddedFrequencies],If[IntegerQ[#],
         finishedODMRaccumulationBuffers=Append[
         finishedODMRaccumulationBuffers,#];
         \[Delta]=Join[\[Delta],#-Take[poiData,Length[\[Delta]]+{1,
         Length[#]}]]&@
         AndorSDK3ExtractPOIformLiveViewAccumulationBuffers[
         rawPoiCoordinates,{#}],cleanup[];Return[$Failed["timeout("<>
         ToString@Length[finishedODMRaccumulationBuffers]<>"), got "<>
         ToString[#]]]]&@
         AndorSDK3WaitForFinishedLiveViewAccumulationBuffer[2.*et]];
         If[
         finishedODMRaccumulationBuffers\[NotEqual]\
odmrAccumulationBuffers,cleanup[];Return[$Failed[
         "fetched back odmr buffers differ"]]];
         ];*)
         
         Pause[Max[Length[paddedFrequencies]*(et + mst) - #, 0]] &[
          SessionTime[] - t];
         n = 0;
         While[! 
            AndorSDK3LiveViewAccumulationBuffersFinished[] \[And] ! 
            abort \[And] (n++ < Length[paddedFrequencies]), 
          Pause[et + mst];];
         If[! AndorSDK3LiveViewAccumulationBuffersFinished[] \[And] ! 
            abort, n = 
           Length@AndorSDK3GetWaitingLiveViewAccumulationBuffers[];
          
          Print["missed " <> ToString[n] <> " trigger event" <> 
            If[n != 1, "s", ""] <> " at scan " <> ToString[scans + 1]];
          
          If[abortOnMissedTriggers,
		cleanup[]; 
          	Return[$Failed["missed triggers", n, scans + 1]]
		,
		SetCameraTriggeringMode["Internal"];
        	Pause[(n+1)*et];
		SetCameraTriggeringMode["External"];
           ];
          ];
         If[
          AndorSDK3GetFinishedLiveViewAccumulationBuffers[True] != 
           odmrAccumulationBuffers, cleanup[]; 
          Return[$Failed["fetched back odmr buffers differ"]]];
         \[Delta] = 
          extractPOIdata[odmrAccumulationBuffers] - poiData;
         poiData += \[Delta];
         scans++;
         samplingFunction[\[Delta]];
         If[waitForCustomDisplayFunction, 
          customDisplayEvaluationFunction[]];
         t = SessionTime[]; 
         accumulatedScanTime += t - lastScanStartTime;
         If[
          StringQ[snapshotFilePrefix] && 
           snapshotIntervall < \[Infinity] && 
           t - lastSnapshotTime >= snapshotIntervall, 
          Monitor[lastSnapshotTime = t; 
           If[! IntegerQ[#], Print[#]] &@
            AndorSDK3DumpLiveViewAccumulationBuffersToFile[
             odmrAccumulationBuffers, 
             snapshotFilePrefix <> "_" <> ToString[scans]]; 
           Export[snapshotFilePrefix <> "_poi_" <> ToString[scans] <> 
             ".m", generateOutput[]], 
           "saving snapshot to " <> snapshotFilePrefix <> "_" <> 
            ToString[scans]]];
         ,
         mode = If[scan, "tracking", "idle"];
         SetCameraTriggeringMode["Internal"];
         If[microwaveSwitchInstalled, disableMicrowave[], 
          deactivateMicrowave[]];
         AndorSDK3ResetLiveViewAccumulationBuffers[
          availableSamplingAccumulationBuffers];
         AndorSDK3QueueLiveViewAccumulationBuffers[
          availableSamplingAccumulationBuffers];
         t = SessionTime[];
         customDisplayEvaluationFunction[];
         Pause[Max[et - #, 0]] &[SessionTime[] - t];
         availableSamplingAccumulationBuffers = \
{AndorSDK3WaitForFinishedLiveViewAccumulationBuffer[N@et]};
         samplingFunction@
          extractPOIdata[availableSamplingAccumulationBuffers];
         t = SessionTime[];
         If[scan && t - lastTrackingStartTime >= trackerTime, 
          tracking = False; lastTrackingEndTime = t];
         ];
        If[
         StringQ[rawImagesFile] && (manualSave || (saveIntervall < \
\[Infinity] && t - lastSaveTime >= saveIntervall)), 
         Monitor[lastSaveTime = t; 
          If[! IntegerQ[#], Print[#]] &@
           AndorSDK3DumpLiveViewAccumulationBuffersToFile[
            odmrAccumulationBuffers, rawImagesFile]; 
          manualSave = False;, "saving to " <> rawImagesFile]];
        ], 
       Column[{{x, y, z, c, scans, 
          If[abort, "aborting", 
           If[quit, "quitting", 
            If[manualSave, "saving", If[clear, "clearing", mode]]]]}, 
         If[showHistory, 
          ListPlot[
           Transpose[{et Take[historyIndices, -Length[#]], #}] & /@ 
            Transpose[history], PlotRange -> All, 
           ImageSize -> 180*96/25.4, Axes -> False, Frame -> True], 
          Sequence[]], 
         If[lastExport =!= #1, 
            Export[FileNameJoin[{$RootDirectory, "var", "www", 
               "odmrActivity.svg"}], lastExport = #1]; #1, #1] &[
          
          ListLinePlot[(SortBy[#1, First] &)[
              Transpose[{(*Take[paddedFrequencies,Length[#]]*)
                paddedFrequencies, #}]] & /@ 
            Transpose@
             If[! showLastSweep, 
              poiData/(Max[scans, 1] et), \[Delta]/et], 
           PlotRange -> {{Min[paddedFrequencies], 
              Max[paddedFrequencies]}, All}, 
           ImageSize -> (180 96)/25.4`, Axes -> False, 
           Frame -> True]], 
         Dynamic@If[customDisplayFunctionResult =!= None, 
           customDisplayFunctionResult, Sequence[]]}]], 
      Panel[Column[{Row[{Row[{"x", 
             InputField[Dynamic@x, Number, FieldSize -> {6, 1}]}], , 
           Row[{"fixX", Checkbox[Dynamic[fixedAxes[[1]]]]}], , 
           Row[{"y", 
             InputField[Dynamic@y, Number, FieldSize -> {6, 1}]}], , 
           Row[{"fixY", Checkbox[Dynamic[fixedAxes[[2]]]]}], , 
           Row[{"z", 
             InputField[Dynamic@z, Number, FieldSize -> {6, 1}]}], , 
           Row[{"fixZ", Checkbox[Dynamic[fixedAxes[[3]]]]}]}], 
         Row[{Row[{"scan", Checkbox[Dynamic[scan]]}], , 
           Row[{"track", Checkbox[Dynamic[track]]}], ,
           Row[{"showHistory", Checkbox[Dynamic[showHistory]]}], , 
           Row[{"showLastSweep", Checkbox[Dynamic[showLastSweep]]}], ,
            Button["clear", clear = True], , 
           Button["save", manualSave = True], , 
           Button["quit", quit = True], , 
           Button["abort", abort = True]}],}]]]//
		If[MatchQ[#,$Failed|$Failed[___]],Return[#]]&;
     If[StringQ[rawImagesFile], 
      Monitor[AndorSDK3DumpLiveViewAccumulationBuffersToFile[
        odmrAccumulationBuffers, rawImagesFile], 
       "saving to " <> rawImagesFile]];
     cleanup[];
     generateOutput[]]] /; ((#1 == #2 || #1 == 1 || #2 == 1 &) @@ (If[
          ListQ[#1], Length[#1], 1] &) /@ {frequencies, power}) && 
    AndorSDK3LiveViewRunning[];
Options[MaximizeImagingContrast] = {ZRange -> 4, ZStep -> 0.1, 
   Accumulations -> 3, 
   ContrastEvaluationFunction -> 
    Function[{constrastSum, contrastSquareSum, sum}, 
     Sqrt[contrastSquareSum]/sum]};
MaximizeImagingContrast[o : OptionsPattern[]] := 
  Module[{quit = False, z0 = ReadStage[][[3]], 
    zRange = OptionValue@ZRange, zStep = OptionValue@ZStep, data, 
    buffer, accumulations = OptionValue@Accumulations, 
    contrastFunction = OptionValue@ContrastEvaluationFunction, 
    exposureTime = AndorSDK3GetFloat[$ODMRcamera, "ExposureTime"], z, 
    stride = AndorSDK3GetInt[camera, "AOIWidth"], lastImage = Null, 
    lastBuffer = Null, t, imageData, imageContrasts = {}, 
    triggerMode = GetCameraTriggeringMode[], 
    acquisitionRunning = AndorSDK3LiveViewAcquisitionRunning[]},
   If[SetCameraTriggeringMode["Internal"] === $Failed, 
    Return[$Failed]];
   If[! acquisitionRunning, AndorSDK3StartLiveViewAcquisition[]];
   buffer = 
    AndorSDK3CreateLiveViewAccumulationBuffers[
     accumulations & /@ Range[z0 - zRange/2, z0 + zRange/2, zStep]]; 
   imageData = If[Length[#] > 0, #[[1]], {}] &@Reap[Monitor[
        (z = #[[1]]; If[! quit,
            MoveStage[3 -> z];
            
            AndorSDK3QueueLiveViewAccumulationBuffers[{#[[2]]}, 
             accumulations];
            t = SessionTime[];
            If[lastBuffer =!= Null,
             
             lastImage = 
              ImageAdjust@
               
               Image[Partition[
                 Sow@AndorSDK3GetLiveViewAccumulationBufferData[
                   lastBuffer], stride]]; 
             imageContrasts = 
              Append[imageContrasts, 
               contrastFunction @@ 
                First@AndorSDK3ComputeContrastsOfLiveViewAccumulationB\
uffers[{lastBuffer}]]];
            
            If[# > 0, Pause[#]] &[
             accumulations*exposureTime - (SessionTime[] - t)]; 
            While[! quit && ! 
               AndorSDK3LiveViewAccumulationBuffersFinished[], 
             Pause[exposureTime]];
            lastBuffer = #[[2]];
            AndorSDK3GetFinishedLiveViewAccumulationBuffers[True];
            ]) & /@ 
         Transpose[{Range[z0 - zRange/2, z0 + zRange/2, zStep], 
           buffer}], {Button["quit", quit = True], 
         ProgressIndicator[(z - (z0 - zRange/2))/zRange], z, 
         ListLinePlot[
          Transpose[{Take[Range[z0 - zRange/2, z0 + zRange/2, zStep], 
               Length[#]], #}] &@imageContrasts, PlotRange -> All, 
          Axes -> False, Frame -> True], lastImage}]][[2]]; 
   MoveStage[3 -> z0]; 
   AndorSDK3GetWaitingLiveViewAccumulationBuffers[True]; 
   AndorSDK3GetFinishedLiveViewAccumulationBuffers[True];
   If[lastBuffer =!= Null, 
    imageData = 
     Append[imageData, 
      AndorSDK3GetLiveViewAccumulationBufferData[lastBuffer]];
    imageContrasts = 
     Append[imageContrasts, 
      contrastFunction @@ 
       First@AndorSDK3ComputeContrastsOfLiveViewAccumulationBuffers[{\
lastBuffer}]]];
   data = 
    Transpose[{Range[z0 - zRange/2, z0 + zRange/2, 
         zStep], #}] & /@ {imageData, imageContrasts};
   AndorSDK3DestroyLiveViewAccumulationBuffers[buffer];
   If[! acquisitionRunning, AndorSDK3StopLiveViewAcquisition[]];
   If[SetCameraTriggeringMode[triggerMode] === $Failed, 
    Return[$Failed]];
   If[! quit, 
    If[#[[1]] > 0, MoveStage[3 -> #[[2, 1]]]] &@
     ListMax[data[[2]], #[[2]] &]]; 
   MapAt[ListLinePlot[#, PlotRange -> All, Axes -> False, 
      Frame -> True] &, data, 2]];
Options[CaptureImage] = {Accumulations -> 1, 
   OutputFunction -> Function[ImageAdjust[Image[#]]]};
CaptureImage[o : OptionsPattern[]] := 
  Module[{quit = False, buffer, 
    accumulations = OptionValue[Accumulations], 
    exposureTime = AndorSDK3GetFloat[$ODMRcamera, "ExposureTime"], 
    stride = AndorSDK3GetInt[camera, "AOIWidth"], imageData, 
    triggerMode = GetCameraTriggeringMode[], 
    acquisitionRunning = AndorSDK3LiveViewAcquisitionRunning[]}, 
   If[SetCameraTriggeringMode["Internal"] === $Failed, 
    Return[$Failed]]; 
   If[! acquisitionRunning, AndorSDK3StartLiveViewAcquisition[]]; 
   buffer = 
    AndorSDK3CreateLiveViewAccumulationBuffers[{accumulations}];
   AndorSDK3GetWaitingLiveViewAccumulationBuffers[True];
   AndorSDK3QueueLiveViewAccumulationBuffers[buffer, accumulations];
   Monitor[(If[#1 > 0, Pause[#1]] &)[
     accumulations exposureTime - SessionTime[]]; 
    While[! quit && ! AndorSDK3LiveViewAccumulationBuffersFinished[], 
     Pause[exposureTime]], {Button["quit", quit = True], 
     "retrieving image"}]; 
   imageData = 
    Partition[AndorSDK3GetLiveViewAccumulationBufferData[buffer[[1]]],
      stride];
   AndorSDK3GetFinishedLiveViewAccumulationBuffers[True]; 
   AndorSDK3DestroyLiveViewAccumulationBuffers[buffer]; 
   If[! acquisitionRunning, AndorSDK3StopLiveViewAcquisition[]]; 
   If[SetCameraTriggeringMode[triggerMode] === $Failed, 
    Return[$Failed]];
   If[Head[#2] === Function, #2[#1], ImageAdjust[Image[#1]]] &[
    imageData, OptionValue[OutputFunction]]
   ];
