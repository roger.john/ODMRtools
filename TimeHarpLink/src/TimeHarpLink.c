/*
 ============================================================================
 Name        : TimeHarpLink.c
 Author      : Roger John
 Version     :
 Copyright   : 
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <thdefin.h>
#include <thlib.h>
#include <mathlink.h>
#include "TimeHarpLink.tmc"

int sargc;
char **sargv;

void abortWithMsg(MLINK ml, const char *msgId) {
	char err_msg[100];
	sprintf(err_msg, "Message[%s,\"%.76s\"]", msgId, MLErrorMessage(ml));
	MLClearError(ml);
	MLNewPacket(ml);
	MLEvaluate(ml, err_msg);
	MLNextPacket(ml);
	MLNewPacket(ml);
	MLPutSymbol(ml, "$Failed");
}

void failWithMsg(MLINK ml, const char *msg) {
	MLClearError(ml);
	MLNextPacket(ml);
	MLNewPacket(ml);
	if (msg) {
		MLPutFunction(ml, "$Failed", 1);
		MLPutString(ml, msg);
	} else
		MLPutSymbol(ml, "$Failed");
}

void TimeHarpGetErrorString(int errorCode) {
	char s[64];
	int r = TH_GetErrorString(s, errorCode);
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		sprintf(s, "error: %d", -r);
		MLPutString(stdlink, s);
	} else
		MLPutString(stdlink, s);
}

void TimeHarpGetLibraryVersion(void) {
	char s[64];
	int r = TH_GetLibraryVersion(s);
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutString(stdlink, s);
}

void TimeHarpInitialize(int mode) {
	int r = TH_Initialize(mode);
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutSymbol(stdlink, "Null");
}

void TimeHarpGetHardwareVersion(void) {
	char s[64];
	int r = TH_GetHardwareVersion(s);
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutString(stdlink, s);
}

void TimeHarpGetSerialNumber(void) {
	char s[64];
	int r = TH_GetSerialNumber(s);
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutString(stdlink, s);
}

void TimeHarpGetBaseResolution(void) {
	int r = TH_GetBaseResolution();
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutInteger(stdlink, r);
}

void TimeHarpCalibrate() {
	int r = TH_Calibrate();
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutSymbol(stdlink, "Null");
}

#define clip(x,min,max) x<min?min:x>max?max:x

void TimeHarpSetCFDdiscriminatorLevel(double level) {
	int intLevel = clip(round(level * 1000), DISCRMIN, DISCRMAX);
	int r = TH_SetCFDDiscrMin(intLevel);
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutReal(stdlink, intLevel / 1000.);
}

void TimeHarpSetCFDzeroCrossLevel(double level) {
	int intLevel = clip(round(level * 1000), ZCMIN, ZCMAX);
	int r = TH_SetCFDZeroCross(intLevel);
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutReal(stdlink, intLevel / 1000.);
}

void TimeHarpSetSyncLevel(double level) {
	int intLevel = clip(round(level * 1000), SYNCMIN, SYNCMAX);
	int r = TH_SetSyncLevel(intLevel);
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutReal(stdlink, intLevel / 1000.);
}

void TimeHarpSetStopOnOverflow(int stopLevel) {
	int r = TH_SetStopOverflow(stopLevel > 0 ? 1 : 0, stopLevel);
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutSymbol(stdlink, "Null");
}

void TimeHarpSetRange(int range) {
	int r = TH_SetRange(range);
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutSymbol(stdlink, "Null");

}

void TimeHarpSetOffset(double offset) {
	int intOffset = clip(round(offset * 1e9), OFFSETMIN, OFFSETMAX);
	int r = TH_SetOffset(intOffset);
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutReal(stdlink, r / 1.e9);
}

void TimeHarpNextOffset(int direction) {
	int r = TH_NextOffset(direction);
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutReal(stdlink, r / 1.e9);
}

void TimeHarpClearHistogramMemory(int block) {
	int r = TH_ClearHistMem(block);
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutSymbol(stdlink, "Null");
}

void TimeHarpSetMeasurementMode(int mode, double aquisitionTime) {
	int intAquisitionTime = mode == 1 && aquisitionTime == 0 ? 0 :
							clip(round(aquisitionTime * 1000), ACQTMIN, ACQTMAX)
							;
	int r = TH_SetMMode(mode, intAquisitionTime);
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutReal(stdlink, intAquisitionTime / 1000.);
}

void TimeHarpStartMeasurement() {
	int r = TH_StartMeas();
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutSymbol(stdlink, "Null");
}

void TimeHarpStopMeasurement() {
	int r = TH_StopMeas();
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutSymbol(stdlink, "Null");
}

void TimeHarpMeasurementRunning() {
	int r = TH_CTCStatus();
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else if (r > 0)
		MLPutSymbol(stdlink, "False");
	else
		MLPutSymbol(stdlink, "True");
}

void TimeHarpSetSyncMode() {
	int r = TH_SetSyncMode();
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutSymbol(stdlink, "Null");
}

void TimeHarpGetHistogramBlock(int block) {
	unsigned int buffer[BLOCKSIZE];
	int r = TH_GetBlock(buffer, block);
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else {
		MLPutFunction(stdlink, "List", BLOCKSIZE);
		int i;
		for (i = 0; i < BLOCKSIZE; i++)
			MLPutInteger(stdlink, buffer[i]);
	}
}

void TimeHarpGetResolution(void) {
	float r = TH_GetResolution();
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutReal(stdlink, r / 1e9);
}

void TimeHarpGetCountRate(void) {
	int r = TH_GetCountRate();
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutInteger(stdlink, r);
}

void TimeHarpGetFlags(void) {
	int flags = TH_GetFlags();
	int err;
	MLINK loop = MLLoopbackOpen(stdenv, &err);
	if (err != MLEOK) {
		MLPutFunction(stdlink, "$Failed", 1);
		MLPutString(stdlink, MLErrorString(stdenv, err));
	} else {
		int c = 0;
		if ((flags & FLAG_SYSERR) != 0) {
			MLPutString(loop, "SYSERR");
			c++;
		}
		if ((flags & FLAG_OVERFLOW) != 0) {
			MLPutString(loop, "OVERFLOW");
			c++;
		}
		if ((flags & FLAG_RAMREADY) != 0) {
			MLPutString(loop, "RAMREADY");
			c++;
		}
		if ((flags & FLAG_FIFOFULL) != 0) {
			MLPutString(loop, "FIFOFULL");
			c++;
		}
		if ((flags & FLAG_FIFOHALFFULL) != 0) {
			MLPutString(loop, "FIFOHALFFULL");
			c++;
		}
		if ((flags & FLAG_FIFOEMPTY) != 0) {
			MLPutString(loop, "FIFOEMPTY");
			c++;
		}
		if ((flags & FLAG_SCANACTIVE) != 0) {
			MLPutString(loop, "SCANACTIVE");
			c++;
		}
		MLEndPacket(loop);
		MLPutFunction(stdlink, "List", c);
		MLTransferToEndOfLoopbackLink(stdlink, loop);
		MLClose(loop);
	}
}

void TimeHarpGetElapsedMeasurementTime(void) {
	int r = TH_GetElapsedMeasTime();
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutReal(stdlink, r / 1000.);
}

void TimeHarpShutdown(void) {
	int r = TH_Shutdown();
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutSymbol(stdlink, "Null");
}

void TimeHarpGetHistogramBank(int chanFrom, int chanTo) {
	unsigned short buffer[BLOCKSIZE];
	int r = TH_GetBank(buffer, chanFrom, chanTo);
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else {
		int i, j, l = chanTo - chanFrom + 1;
		MLPutFunction(stdlink, "List", CURVES);
		for (i = 0; i < CURVES; i++) {
			MLPutFunction(stdlink, "List", l);
			for (j = 0; j < l; j++)
				MLPutInteger(stdlink, buffer[i * l + j]);
		}
	}
}

void TimeHarpGetLostBlocks(void) {
	int r = TH_GetLostBlocks();
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		TimeHarpGetErrorString(r);
	} else
		MLPutInteger(stdlink, r);
}

int main(int argc, char **argv) {
	sargc = argc;
	sargv = argv;
	return MLMain(argc, argv);
}

