void TimeHarpGetErrorString(int);

:Begin:
:Function:       TimeHarpGetErrorString
:Pattern:        TimeHarpGetErrorString[errcode_Integer]
:Arguments:      { errcode }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void TimeHarpGetLibraryVersion(void);

:Begin:
:Function:       TimeHarpGetLibraryVersion
:Pattern:        TimeHarpGetLibraryVersion[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

void TimeHarpInitialize(int);

:Begin:
:Function:       TimeHarpInitialize
:Pattern:        TimeHarpInitialize[mode:(0|1)]
:Arguments:      { mode }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void TimeHarpGetHardwareVersion(void);

:Begin:
:Function:       TimeHarpGetHardwareVersion
:Pattern:        TimeHarpGetHardwareVersion[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

void TimeHarpGetSerialNumber(void);

:Begin:
:Function:       TimeHarpGetSerialNumber
:Pattern:        TimeHarpGetSerialNumber[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

void TimeHarpGetBaseResolution(void);

:Begin:
:Function:       TimeHarpGetBaseResolution
:Pattern:        TimeHarpGetBaseResolution[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

void TimeHarpCalibrate(void);

:Begin:
:Function:       TimeHarpCalibrate
:Pattern:        TimeHarpCalibrate[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

void TimeHarpSetCFDdiscriminatorLevel(double);

:Begin:
:Function:       TimeHarpSetCFDdiscriminatorLevel
:Pattern:        TimeHarpSetCFDdiscriminatorLevel[level_Real]
:Arguments:      { level }
:ArgumentTypes:  { Real }
:ReturnType:     Manual
:End:

void TimeHarpSetCFDzeroCrossLevel(double);

:Begin:
:Function:       TimeHarpSetCFDzeroCrossLevel
:Pattern:        TimeHarpSetCFDzeroCrossLevel[level_Real]
:Arguments:      { level }
:ArgumentTypes:  { Real }
:ReturnType:     Manual
:End:

void TimeHarpSetSyncLevel(double);

:Begin:
:Function:       TimeHarpSetSyncLevel
:Pattern:        TimeHarpSetSyncLevel[level_Real]
:Arguments:      { level }
:ArgumentTypes:  { Real }
:ReturnType:     Manual
:End:

void TimeHarpSetStopOnOverflow(int);

:Begin:
:Function:       TimeHarpSetStopOnOverflow
:Pattern:        TimeHarpSetStopOnOverflow[
					stopLevel_Integer?NonNegative]
:Arguments:      { stopLevel }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void TimeHarpSetRange(int);

:Begin:
:Function:       TimeHarpSetRange
:Pattern:        TimeHarpSetRange[range_Integer?NonNegative]
:Arguments:      { range }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void TimeHarpSetOffset(double);

:Begin:
:Function:       TimeHarpSetOffset
:Pattern:        TimeHarpSetOffset[offset_Real]
:Arguments:      { offset }
:ArgumentTypes:  { Real }
:ReturnType:     Manual
:End:

void TimeHarpNextOffset(int);

:Begin:
:Function:       TimeHarpNextOffset
:Pattern:        TimeHarpNextOffset[direction:(-1|0|1)]
:Arguments:      { direction }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void TimeHarpClearHistogramMemory(int);

:Begin:
:Function:       TimeHarpClearHistogramMemory
:Pattern:        TimeHarpClearHistogramMemory[block_Integer]
:Arguments:      { block }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void TimeHarpSetMeasurementMode(int,double);

:Begin:
:Function:       TimeHarpSetMeasurementMode
:Pattern:        TimeHarpSetMeasurementMode[
					mode:(0|1),aquisitionTime_Real]
:Arguments:      { mode, aquisitionTime }
:ArgumentTypes:  { Integer, Real }
:ReturnType:     Manual
:End:

void TimeHarpStartMeasurement(void);

:Begin:
:Function:       TimeHarpStartMeasurement
:Pattern:        TimeHarpStartMeasurement[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

void TimeHarpStopMeasurement(void);

:Begin:
:Function:       TimeHarpStopMeasurement
:Pattern:        TimeHarpStopMeasurement[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

void TimeHarpMeasurementRunning(void);

:Begin:
:Function:       TimeHarpMeasurementRunning
:Pattern:        TimeHarpMeasurementRunning[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

void TimeHarpSetSyncMode(void);

:Begin:
:Function:       TimeHarpSetSyncMode
:Pattern:        TimeHarpSetSyncMode[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

void TimeHarpGetHistogramBlock(int);

:Begin:
:Function:       TimeHarpGetHistogramBlock
:Pattern:        TimeHarpGetHistogramBlock[block_Integer]
:Arguments:      { block }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void TimeHarpGetResolution(void);

:Begin:
:Function:       TimeHarpGetResolution
:Pattern:        TimeHarpGetResolution[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

void TimeHarpGetCountRate(void);

:Begin:
:Function:       TimeHarpGetCountRate
:Pattern:        TimeHarpGetCountRate[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

void TimeHarpGetFlags(void);

:Begin:
:Function:       TimeHarpGetFlags
:Pattern:        TimeHarpGetFlags[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

void TimeHarpGetElapsedMeasurementTime(void);

:Begin:
:Function:       TimeHarpGetElapsedMeasurementTime
:Pattern:        TimeHarpGetElapsedMeasurementTime[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

void TimeHarpShutdown(void);

:Begin:
:Function:       TimeHarpShutdown
:Pattern:        TimeHarpShutdown[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

void TimeHarpGetHistogramBank(int,int);

:Begin:
:Function:       TimeHarpGetHistogramBank
:Pattern:        TimeHarpGetHistogramBank[chanFrom_Integer,chanFrom_Integer]
:Arguments:      { chanFrom, chanTo }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Manual
:End:

void TimeHarpGetLostBlocks(void);

:Begin:
:Function:       TimeHarpGetLostBlocks
:Pattern:        TimeHarpGetLostBlocks[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:
