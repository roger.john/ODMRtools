/*
 * main.c
 * 6-axis stepper motor controller firmware for the Tiva Connected Launchpad
 *  Created on: Dec 6, 2017
 *      Author: Roger John <roger.john@uni-leipzig.de>
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include "inc/hw_gpio.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_timer.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/timer.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"

#ifndef max
#define max(a,b) ((a) >= (b) ? (a) : (b))
#endif
#ifndef min
#define min(a,b) ((a) < (b) ? (a) : (b))
#endif

#define ACCELERATION_APPROXIMATION_BITS 24

typedef enum {
	setDirection,
	preaccelerate,
	accelerate,
	cruise,
	deccelerate,
	cruiseLastSteps,
	disable,
	stopped
} stepperState_type;

typedef struct {
	int32_t position;
	int32_t encoderValue;
	uint32_t minVelocity;
	uint32_t maxVelocity;
	uint32_t acceleration;
	uint32_t homeSearchVelocity;
	uint32_t homeApproachVelocity;
	uint32_t maxStepPeriod;
	uint32_t minStepPeriod;
	uint32_t accelerationApproximationData[ACCELERATION_APPROXIMATION_BITS];
	uint8_t accelerationApproximationDataSign[ACCELERATION_APPROXIMATION_BITS];
	uint8_t enableStopSwitches;
	uint8_t ignoreError;
	uint8_t keepEnabled;
	uint32_t enableToDirectionSetDelay;
	uint32_t directionSetToFirstStepPulseDelay;
	uint32_t lastStepPulseToDisableDelay;
	stepperState_type state;
	int8_t direction;
	uint32_t absoluteOffset;
	uint32_t halfwaySteps;
	uint8_t oddSteps;
	uint32_t accelerationSteps;
	uint32_t cruiseSteps;
	uint32_t interStepChangeCounter;
	uint32_t stepPeriod;
	uint32_t stepPeriodChange;
	uint8_t stepPeriodChangeSign;
	uint8_t currentAccelerationApproximationIndex;
	uint32_t nextAccelerationApproximationBitMask;
	uint8_t stopSignal;
	uint8_t hasStopped;
	uint8_t counterStopSignal;
	uint8_t jog;
	int32_t targetVelocity;
	int8_t targetDirection;
	uint32_t targetStepPeriod;
} stepperProperty;

#define NUM_STEPPER_PORTS 6
#define SERIAL_INPUT_BUFFER_SIZE 32
#define CR 0x0D
#define CLOCK_FREQUENCY HSE_VALUE
#define CLOCK_CYCLES_PER_US CLOCK_FREQUENCY/1000000L
#define PRESCALER_CLOCK_CYCLES 1L
#define PRESCALED_CLOCK_CYCLES_PER_US CLOCK_CYCLES_PER_US/PRESCALER_CLOCK_CYCLES
#define PRESCALER_FREQUENCY (CLOCK_FREQUENCY/PRESCALER_CLOCK_CYCLES)
#define STEPPER_PRESCALER_SYNC 3
#define US_TO_TC(a) ((uint32_t)((a)*(PRESCALED_CLOCK_CYCLES_PER_US)))
#define TC_TO_US(a) ((uint32_t)((a)/(PRESCALED_CLOCK_CYCLES_PER_US)))
#define STEPPER_ENABLE_TO_DIRECTION_SET_DELAY US_TO_TC(5)
#define STEPPER_DIRECTION_SET_TO_STEP_PULSE_DELAY US_TO_TC(5)
#define STEPPER_PULSE_HIGH_WIDTH US_TO_TC(3.5)
#define STEPPER_PULSE_MIN_LOW_WIDTH US_TO_TC(2.5)
#define STEPPER_LAST_STEP_PULSE_TO_DISABLE_DELAY US_TO_TC(5)
#define STEPPER_MAX_PERIOD (1L<<24)
#define STEPPER_MAX_STEPS ((1L<<24)-1)
#define MAX_VELOCITY 100000L
#define STEP_PERIOD_TO_VELOCITY(t) (CLOCK_FREQUENCY/(PRESCALER_CLOCK_CYCLES*(t)))
#define STEPPER_MIN_PERIOD (max(STEPPER_PULSE_HIGH_WIDTH+STEPPER_PULSE_MIN_LOW_WIDTH,\
		STEP_PERIOD_TO_VELOCITY(MAX_VELOCITY)))
#define VELOCITY_TO_STEP_PERIOD_M1(t) \
			((t) < 32 ? \
					STEPPER_MAX_PERIOD - 1 : \
					(t)*STEPPER_MIN_PERIOD*PRESCALER_CLOCK_CYCLES > CLOCK_FREQUENCY ? \
							STEPPER_MIN_PERIOD - 1 : \
							STEP_PERIOD_TO_VELOCITY(t) - 1)
#define VELOCITY_TO_STEP_PERIOD(t) (VELOCITY_TO_STEP_PERIOD_M1(t) + 1)
#define STEPPER_ACCELERATION 10000	// steps per second increase per second
#define INVALID_VALUE 0x80000000
#define MIN_VELOCITY STEP_PERIOD_TO_VELOCITY(STEPPER_MAX_PERIOD)
#define MIN_ACCELERATION 0
#define MAX_ACCELERATION ((1l<<16)-1)
#define HOME_SET_BACK_STEPS 500
#define PROPERTY_POSITION 0
#define PROPERTY_MIN_VELOCITY 1
#define PROPERTY_MAX_VELOCITY 2
#define PROPERTY_ACCELERATION 3
#define PROPERTY_HOME_SEARCH_VELOCITY 4
#define PROPERTY_HOME_APPROACH_VELOCITY 5
#define PROPERTY_MAX_STEP_PERIOED 6
#define PROPERTY_MIN_STEP_PERIOED 7
#define PROPERTY_AA_DATA 8
#define PROPERTY_CURRENT_STEP_PERIOD 9
#define PROPERTY_CURRENT_STEP_PERIOD_CHANGE 10
#define PROPERTY_IGNORE_STOPPERS 11
#define PROPERTY_STEP_COUNTER 12
#define PROPERTY_IGNORE_ERROR 13
#define PROPERTY_KEEP_ENABLED 14
#define PROPERTY_ENABLE_TO_DIRECTION_SET_DELAY 15
#define PROPERTY_DIRECTION_SET_TO_FIRST_STEP_PULSE_DELAY 16
#define PROPERTY_LAST_STEP_PULSE_TO_DISABLE_DELAY 17
#define PROPERTY_ENCODER 18
#define PROPERTY_JOG_VELOCITY 19
#define PROPERTY_VELOCITY 20

#define GPIOWRITE(base,pin,val) (HWREG((base) + (GPIO_O_DATA + ((pin) << 2))) = (val) ? (pin) : 0)
#define GPIOREAD(base,pin) (HWREG((base) + (GPIO_O_DATA + ((pin) << 2))))

#include "config.h"

#if DEBUG & 1
#define DEBUG_OUT(val) (HWREG(DEBUGSIGNALS_PORT + GPIO_O_DATA + (0xFF<<2)) = (val))
#else
#define DEBUG_OUT(val)
#endif

uint32_t sysClockFrequency;
uint8_t stopStepping;
stepperProperty stepperProperties[NUM_STEPPER_PORTS];

static inline uint32_t getTimerBaseForAxis(int axis) {
	switch (axis) {
	case 0:
		return TIMER0_BASE;
	case 1:
		return TIMER1_BASE;
	case 2:
		return TIMER2_BASE;
	case 3:
		return TIMER3_BASE;
	case 4:
		return TIMER4_BASE;
	case 5:
		return TIMER5_BASE;
	default:
		return 0;
	}
}

static inline uint32_t getStopperStatus(int axis, int direction) {
	uint32_t stopperStatus =
			axis < 4 ?
					GPIOREAD(STOPSIGNALS_PORT,
							(direction > 0) ^ invert_stopper_directions[axis] ?
									1 << (2 * axis + 1) : 1 << (2 * axis)) :
					GPIOREAD(STOPSIGNALS2_PORT,
							(direction > 0) ^ invert_stopper_directions[axis] ?
									1 << (2 * (axis - 4) + 1) :
									1 << (2 * (axis - 4)));
	return stopperStatus ? INVERT_STOPSIGNALS ? 0 : 1
	: INVERT_STOPSIGNALS ? 1 : 0;
}

void StopSignalISR(void) {
	uint32_t interruptStatus = HWREG(STOPSIGNALS_PORT+GPIO_O_MIS);
	HWREG(STOPSIGNALS_PORT+GPIO_O_ICR) = interruptStatus;
	for (int i = 0; i < min(4, NUM_STEPPER_PORTS); i++) {
		stepperProperty *sp = &stepperProperties[i];
		if (sp->enableStopSwitches
				&& (interruptStatus
						& ((sp->direction > 0) ^ invert_stopper_directions[i] ?
								1 << (2 * i + 1) : 1 << (2 * i))))
			sp->stopSignal = 1;
	}
}

void StopSignal2ISR(void) {
	uint32_t interruptStatus = HWREG(STOPSIGNALS2_PORT+GPIO_O_MIS);
	HWREG(STOPSIGNALS2_PORT+GPIO_O_ICR) = interruptStatus;
	for (int i = 4; i < NUM_STEPPER_PORTS; i++) {
		stepperProperty *sp = &stepperProperties[i];
		if (sp->enableStopSwitches
				&& (interruptStatus
						& ((sp->direction > 0) ^ invert_stopper_directions[i] ?
								1 << (2 * (i - 4) + 1) : 1 << (2 * (i - 4)))))
			sp->stopSignal = 1;
	}
}

void ErrorSignalISR(void) {
	uint32_t interruptStatus = HWREG(ERRORSIGNALS_PORT+GPIO_O_MIS);
	HWREG(ERRORSIGNALS_PORT+GPIO_O_ICR) = interruptStatus;
	for (int i = 0; i < NUM_STEPPER_PORTS; i++) {
		stepperProperty *sp = &stepperProperties[i];
		if (!sp->ignoreError && (interruptStatus & (1 << i)))
			sp->stopSignal = 1;
	}
}

void EncoderI1signalISR(void) {
	uint32_t interruptStatus = HWREG(ENCODERSIGNALS_I1_PORT+GPIO_O_MIS);
	HWREG(ENCODERSIGNALS_I1_PORT+GPIO_O_ICR) = interruptStatus;
	uint32_t i1v = GPIOREAD(ENCODERSIGNALS_I1_PORT, ENCODERSIGNALS_I1_PINS);
	uint32_t q1v = GPIOREAD(ENCODERSIGNALS_Q1_PORT, ENCODERSIGNALS_Q1_PINS);
	uint32_t q2v = GPIOREAD(ENCODERSIGNALS_Q2_PORT, ENCODERSIGNALS_Q2_PINS);
	for (int pin = ENCODERSIGNALS_I1_FIRST_PIN;
			pin < ENCODERSIGNALS_I1_FIRST_PIN + ENCODERSIGNALS_I1_NUMPINS;
			pin++) {
		int axis = pin - ENCODERSIGNALS_I1_FIRST_PIN;
		stepperProperty *sp = &stepperProperties[axis];
		if (interruptStatus & (1 << pin)) {
			uint8_t i = (i1v & (1 << pin)) != 0, q;
			if (axis < ENCODERSIGNALS_Q1_NUMPINS)
				q = (q1v & (1 << (axis + ENCODERSIGNALS_Q1_FIRST_PIN))) != 0;
			else
				q = (q2v
						& (1
								<< (axis - ENCODERSIGNALS_Q1_NUMPINS
										+ ENCODERSIGNALS_Q2_FIRST_PIN))) != 0;
			sp->encoderValue +=
					i ^ q ? encoder_directions[axis] : -encoder_directions[axis];
		}
	}
}

void EncoderI2signalISR(void) {
	uint32_t interruptStatus = HWREG(ENCODERSIGNALS_I2_PORT+GPIO_O_MIS);
	HWREG(ENCODERSIGNALS_I2_PORT+GPIO_O_ICR) = interruptStatus;
	uint32_t i2v = GPIOREAD(ENCODERSIGNALS_I2_PORT, ENCODERSIGNALS_I2_PINS);
	uint32_t q1v = GPIOREAD(ENCODERSIGNALS_Q1_PORT, ENCODERSIGNALS_Q1_PINS);
	uint32_t q2v = GPIOREAD(ENCODERSIGNALS_Q2_PORT, ENCODERSIGNALS_Q2_PINS);
	for (int pin = ENCODERSIGNALS_I2_FIRST_PIN;
			pin < ENCODERSIGNALS_I2_FIRST_PIN + ENCODERSIGNALS_I2_NUMPINS;
			pin++) {
		int axis = pin - ENCODERSIGNALS_I2_FIRST_PIN + ENCODERSIGNALS_I1_NUMPINS;
		stepperProperty *sp = &stepperProperties[axis];
		if (interruptStatus & (1 << pin)) {
			uint8_t i = (i2v & (1 << pin)) != 0, q;
			if (axis < ENCODERSIGNALS_Q1_NUMPINS)
				q = (q1v & (1 << (axis + ENCODERSIGNALS_Q1_FIRST_PIN))) != 0;
			else
				q = (q2v
						& (1
								<< (axis - ENCODERSIGNALS_Q1_NUMPINS
										+ ENCODERSIGNALS_Q2_FIRST_PIN))) != 0;
			sp->encoderValue +=
					i ^ q ? encoder_directions[axis] : -encoder_directions[axis];
		}
	}
}

void EncoderQ1signalISR(void) {
	uint32_t interruptStatus = HWREG(ENCODERSIGNALS_Q1_PORT+GPIO_O_MIS);
	HWREG(ENCODERSIGNALS_Q1_PORT+GPIO_O_ICR) = interruptStatus;
	uint32_t q1v = GPIOREAD(ENCODERSIGNALS_Q1_PORT, ENCODERSIGNALS_Q1_PINS);
	uint32_t i1v = GPIOREAD(ENCODERSIGNALS_I1_PORT, ENCODERSIGNALS_I2_PINS);
	uint32_t i2v = GPIOREAD(ENCODERSIGNALS_I2_PORT, ENCODERSIGNALS_I2_PINS);
	for (int pin = ENCODERSIGNALS_Q1_FIRST_PIN;
			pin < ENCODERSIGNALS_Q1_FIRST_PIN + ENCODERSIGNALS_Q1_NUMPINS;
			pin++) {
		int axis = pin - ENCODERSIGNALS_Q1_FIRST_PIN;
		stepperProperty *sp = &stepperProperties[axis];
		if (interruptStatus & (1 << pin)) {
			uint8_t q = (q1v & (1 << pin)) != 0, i;
			if (axis < ENCODERSIGNALS_I1_NUMPINS)
				i = (i1v & (1 << (axis + ENCODERSIGNALS_I1_FIRST_PIN))) != 0;
			else
				i = (i2v
						& (1
								<< (axis - ENCODERSIGNALS_I1_NUMPINS
										+ ENCODERSIGNALS_I2_FIRST_PIN))) != 0;
			sp->encoderValue +=
					i ^ q ? -encoder_directions[axis] : encoder_directions[axis];
		}
	}
}

void EncoderQ2signalISR(void) {
	uint32_t interruptStatus = HWREG(ENCODERSIGNALS_Q2_PORT+GPIO_O_MIS);
	HWREG(ENCODERSIGNALS_Q2_PORT+GPIO_O_ICR) = interruptStatus;
	uint32_t q2v = GPIOREAD(ENCODERSIGNALS_Q2_PORT, ENCODERSIGNALS_Q2_PINS);
	uint32_t i1v = GPIOREAD(ENCODERSIGNALS_I1_PORT, ENCODERSIGNALS_I2_PINS);
	uint32_t i2v = GPIOREAD(ENCODERSIGNALS_I2_PORT, ENCODERSIGNALS_I2_PINS);
	for (int pin = ENCODERSIGNALS_Q2_FIRST_PIN;
			pin < ENCODERSIGNALS_Q2_FIRST_PIN + ENCODERSIGNALS_Q2_NUMPINS;
			pin++) {
		int axis = pin - ENCODERSIGNALS_Q1_FIRST_PIN + ENCODERSIGNALS_Q2_NUMPINS;
		stepperProperty *sp = &stepperProperties[axis];
		if (interruptStatus & (1 << pin)) {
			uint8_t q = (q2v & (1 << pin)) != 0, i;
			if (axis < ENCODERSIGNALS_I1_NUMPINS)
				i = (i1v & (1 << (axis + ENCODERSIGNALS_I1_FIRST_PIN))) != 0;
			else
				i = (i2v
						& (1
								<< (axis - ENCODERSIGNALS_I1_NUMPINS
										+ ENCODERSIGNALS_I2_FIRST_PIN))) != 0;
			sp->encoderValue +=
					i ^ q ? -encoder_directions[axis] : encoder_directions[axis];
		}
	}
}

//static inline uint8_t __builtin_add_overflow(uint16_t a, uint16_t b, uint16_t *c) {
//	uint8_t r;
//	*c = a + b;
//	asm ( "mov %[r], 0; \n\t"
//		  "adc %[r], 0; \n\t"
//			/*"sts %[r], __tmp_reg__; \n\t"*/: [r] "=r" (r) : );
//	return r;
//}

static inline float __hw_sqrtf(float x) {
	float r;
	asm ( "vsqrt.f32 %[r], %[x]" : [r]"=w"(r) : [x]"w"(x) );
	return r;
}

static inline int32_t __hw_roundf(float x) {
	return roundf(x);
	int32_t r;
	asm ( "vcvtr.f32.s32 %[r], %[x]" : [r]"=w"(r) : [x]"w"(x) );
	return r;
}

#define SET_TIMER_LOAD_REGISTER(timerBase,value) \
	(HWREG(timerBase + TIMER_O_TAILR) = (value) & 0xFFFF); \
	(HWREG(timerBase + TIMER_O_TAPR) = ((value)>>16) & 0xFF) /* note that prescaler gives upper 8 bits */

#define SET_TIMER_MATCH_REGISTER(timerBase,value) \
	(HWREG(timerBase + TIMER_O_TAMATCHR) = (value) & 0xFFFF); \
	(HWREG(timerBase + TIMER_O_TAPMR) = ((value)>>16) & 0xFF) /* note that prescaler gives upper 8 bits */

#define SET_COUNTER_LOAD_REGISTER(timerBase,value) \
	(HWREG(timerBase + TIMER_O_TBILR) = (value) & 0xFFFF); \
	(HWREG(timerBase + TIMER_O_TBPR) = ((value)>>16) & 0xFF) /* note that prescaler gives upper 8 bits */

#define SET_COUNTER_MATCH_REGISTER(timerBase,value) \
	(HWREG(timerBase + TIMER_O_TBMATCHR) = (value) & 0xFFFF); \
	(HWREG(timerBase + TIMER_O_TBPMR) = ((value)>>16) & 0xFF) /* note that prescaler gives upper 8 bits */

static inline void initiateSteppingStop(uint32_t timerBase, stepperProperty *sp,
		uint32_t stepPinPort, uint8_t stepPinBit) {
	HWREG(timerBase+TIMER_O_CTL) &= ~(TIMER_CTL_TAEN | TIMER_CTL_TBEN); /* stop timers */
	HWREG(timerBase + TIMER_O_TAMR) = TIMER_TAMR_TAMRSU | TIMER_TAMR_TAILD
			| TIMER_TAMR_TAMR_1_SHOT; /* configure timer A as single shot counter */
	SET_TIMER_LOAD_REGISTER(timerBase, sp->lastStepPulseToDisableDelay - 1);
	/* configure sufficient delay before disabling stepper */
	HWREG(timerBase+TIMER_O_CTL) |= TIMER_CTL_TAEN; /* start timer A for waiting before disabling stepper */
////	uint32_t stepsDone = HWREG(timerBase + TIMER_O_TBR);
//	uint32_t stepsDone = 0;
////	if (stepsDone == 0)
////		stepsDone = absoluteOffset;
//	switch (sp->state) {
//	case setDirection:
//	case preaccelerate:
//		stepsDone = 0;
//		break;
//	case accelerate:
//		stepsDone = sp->accelerationSteps;
//		break;
//	case cruise:
//	case cruiseLastSteps:
//		stepsDone = sp->absoluteOffset - sp->accelerationSteps
//				- sp->cruiseSteps;
//		break;
//	case deccelerate:
//		stepsDone = sp->absoluteOffset - sp->accelerationSteps;
//		break;
//	default:
//		stepsDone = sp->absoluteOffset;
//	}
	sp->state = disable;
	HWREG(stepPinPort+GPIO_O_AFSEL) &= ~stepPinBit; // deactivate pwm pin
//	if (sp->direction < 0)
//		sp->position -= stepsDone;
//	else
//		sp->position += stepsDone;
}

static inline void generalPWMTimerISR(uint32_t timerBase, stepperProperty *sp,
		uint32_t stepPinPort, uint8_t stepPinBit, uint32_t dirPinPort,
		uint8_t dirPinBit, uint32_t enaPinPort, uint8_t enaPinBit) {
	HWREG(timerBase + TIMER_O_ICR) = TIMER_ICR_TATOCINT | TIMER_ICR_CAECINT; /* clear interrupt */
	DEBUG_OUT(0);
	if (sp->stopSignal || stopStepping) {
		DEBUG_OUT(1);
		switch (sp->state) {
		case accelerate:
			(sp->accelerationSteps)++;
			sp->position += sp->direction;
			break;
		case cruise:
		case cruiseLastSteps:
			(sp->cruiseSteps)--;
			sp->position += sp->direction;
			break;
		case deccelerate:
			(sp->accelerationSteps)--;
			sp->position += sp->direction;
			break;
		default:
			break;
		}
		sp->hasStopped = 1;
		if (sp->state != disable) {
			initiateSteppingStop(timerBase, sp, stepPinPort, stepPinBit);
			return;
		}
	}
	switch (sp->state) {
	case setDirection:
		DEBUG_OUT(0x10);
		GPIOWRITE(dirPinPort, dirPinBit,
				sp->direction < 0 ? !INVERT_DIRECTIONSIGNALS : INVERT_DIRECTIONSIGNALS);
		HWREG(timerBase + TIMER_O_CTL) &= ~TIMER_CTL_TAEN; /* stop timer A (should already be stopped) */
		SET_TIMER_LOAD_REGISTER(timerBase,
				sp->directionSetToFirstStepPulseDelay - 1)
		; /* configure direction-set-to-stop-delay before first step */
		HWREG(timerBase + TIMER_O_TAMR) = TIMER_TAMR_TAPLO | TIMER_TAMR_TAMRSU
				| TIMER_TAMR_TAPWMIE | TIMER_TAMR_TAILD | TIMER_TAMR_TAAMS
				| TIMER_TAMR_TAMR_PERIOD; /* configure timer A as PWM with delayed value load */
		DEBUG_OUT(0x11);
		HWREG(timerBase + TIMER_O_CTL) |= TIMER_CTL_TAEN; /* start timer */
		HWREG(stepPinPort+GPIO_O_AFSEL) |= stepPinBit; // activate pwm pin
		DEBUG_OUT(0x12);
		sp->state = preaccelerate;
		DEBUG_OUT(0x13);
		break;
	case preaccelerate:
		DEBUG_OUT(0x80);
		sp->nextAccelerationApproximationBitMask = 2;
		sp->currentAccelerationApproximationIndex = 0;
		sp->stepPeriodChange = sp->accelerationApproximationData[0];
		sp->stepPeriodChangeSign = sp->accelerationApproximationDataSign[0];
		sp->interStepChangeCounter = 0;
		sp->stepPeriod = sp->maxStepPeriod;
		SET_TIMER_LOAD_REGISTER(timerBase, sp->maxStepPeriod)
		; /* set first step time after first step due to TAILD */
//		uint32_t currentCounterValue = HWREG(timerBase + TIMER_O_TBR);
//		SET_COUNTER_MATCH_REGISTER(timerBase,
//				(currentCounterValue + sp->absoluteOffset) & ((1 << 24 - 1)))
//		; /* use counter in wrap-around? */
		if (sp->halfwaySteps > 0 || sp->jog) {
			sp->accelerationSteps = 0;
			sp->state = accelerate;
		} else {
			sp->cruiseSteps = sp->oddSteps;
			sp->state = cruiseLastSteps;
		}
		DEBUG_OUT(0x81);
		break;
	case accelerate:
		DEBUG_OUT(0x20);
		(sp->accelerationSteps)++;
//		(sp->absoluteOffset)--;
		sp->position += sp->direction;
//		uint32_t currentAccelerationSteps = HWREG(timerBase + TIMER_O_TBR);
		uint32_t currentAccelerationSteps = sp->accelerationSteps;
		DEBUG_OUT(0x21);
		if ((currentAccelerationSteps & sp->nextAccelerationApproximationBitMask)
				!= 0
				&& sp->currentAccelerationApproximationIndex
						< ACCELERATION_APPROXIMATION_BITS - 1) {
			(sp->currentAccelerationApproximationIndex)++;
			sp->nextAccelerationApproximationBitMask <<= 1;
			sp->stepPeriodChange =
					sp->accelerationApproximationData[sp->currentAccelerationApproximationIndex];
			sp->stepPeriodChangeSign =
					sp->accelerationApproximationDataSign[sp->currentAccelerationApproximationIndex];
			DEBUG_OUT(0x22);
		}
		if (!sp->jog && currentAccelerationSteps == sp->halfwaySteps) {
			sp->accelerationSteps = currentAccelerationSteps;
			sp->cruiseSteps = (sp->absoluteOffset) - currentAccelerationSteps;
			if (sp->oddSteps) {
				sp->state = cruise;
//				sp->cruiseSteps = 1;
			} else
				sp->state = deccelerate;
			sp->interStepChangeCounter = 0;
			if ((currentAccelerationSteps
					& sp->nextAccelerationApproximationBitMask) == 0) {
//				(sp->currentAAI)--;
				sp->nextAccelerationApproximationBitMask >>= 1;
//				sp->stepTimeChange = sp->accelerationApproximationData[sp->currentAccelerationApproximationIndex];
//				sp->stepTimeChangeSign = sp->accelerationApproximationDataSign[sp->currentAccelerationApproximationIndex];
			}DEBUG_OUT(0x23);
		} else if (
				sp->jog ?
						sp->direction == sp->targetDirection
								&& sp->stepPeriod == sp->targetStepPeriod :
						sp->stepPeriod == sp->minStepPeriod) {
			sp->state = cruise;
//			sp->accelerationSteps = currentAccelerationSteps;
//			sp->cruiseSteps = (sp->absoluteOffset) - currentAccelerationSteps;
			if (!sp->jog)
				sp->cruiseSteps = (sp->absoluteOffset)
						- 2 * currentAccelerationSteps;
			sp->interStepChangeCounter = 0;
			if ((currentAccelerationSteps
					& sp->nextAccelerationApproximationBitMask) == 0) {
//				(sp->currentAAI)--;
				sp->nextAccelerationApproximationBitMask >>= 1;
//				sp->stepTimeChange = sp->accelerationApproximationData[sp->currentAccelerationApproximationIndex];
//				sp->stepTimeChangeSign = sp->accelerationApproximationDataSign[sp->currentAccelerationApproximationIndex];
			}DEBUG_OUT(0x24);
		} else if (sp->jog
				&& (sp->direction != sp->targetDirection
						|| sp->stepPeriod < sp->targetStepPeriod)) {
			sp->state = deccelerate;
			if ((currentAccelerationSteps
					& sp->nextAccelerationApproximationBitMask) == 0) {
//				(sp->currentAAI)--;
				sp->nextAccelerationApproximationBitMask >>= 1;
			}
		} else {
			if (sp->interStepChangeCounter > 0) {
				(sp->interStepChangeCounter)--;
			} else {
				uint32_t minStepPeriod =
						sp->jog ?
								max(sp->targetStepPeriod, sp->minStepPeriod) :
								sp->minStepPeriod;
				if (sp->stepPeriodChangeSign) {
					sp->interStepChangeCounter = sp->stepPeriodChange - 1;
					sp->stepPeriod = max(minStepPeriod, sp->stepPeriod - 1);
				} else {
					sp->stepPeriod = max(minStepPeriod,
							sp->stepPeriod - sp->stepPeriodChange);
				}
				SET_TIMER_LOAD_REGISTER(timerBase, sp->stepPeriod);
			}DEBUG_OUT(0x25);
		}
		break;
	case cruise:
		DEBUG_OUT(0x30);
		sp->position += sp->direction;
		if (sp->jog) {
			if (sp->direction != sp->targetDirection
					|| sp->targetStepPeriod > sp->stepPeriod)
				sp->state = deccelerate;
			else if (sp->targetStepPeriod < sp->stepPeriod) {
				if ((sp->accelerationSteps
						& (2 * sp->nextAccelerationApproximationBitMask)) == 0)
					sp->nextAccelerationApproximationBitMask <<= 1;
				sp->state = accelerate;
			}
		} else {
			(sp->cruiseSteps)--;
//			(sp->absoluteOffset)--;
//			uint32_t currentStepsDone = HWREG(timerBase + TIMER_O_TBR);
			if (sp->cruiseSteps == 0/*currentStepsDone*/)
				sp->state = deccelerate;
		}
		DEBUG_OUT(0x31);
		break;
	case deccelerate:
		DEBUG_OUT(0x40);
		(sp->accelerationSteps)--;
//		(sp->absoluteOffset)--;
		sp->position += sp->direction;
//		uint32_t deccelerationStepsLeft = sp->absoluteOffset
//				- HWREG(timerBase + TIMER_O_TBR);
		uint32_t deccelerationStepsLeft = sp->accelerationSteps;
		if (deccelerationStepsLeft > 0) {
			if ((deccelerationStepsLeft
					& sp->nextAccelerationApproximationBitMask) == 0
					&& sp->currentAccelerationApproximationIndex > 0) {
				(sp->currentAccelerationApproximationIndex)--;
				sp->nextAccelerationApproximationBitMask >>= 1;
				sp->stepPeriodChange =
						sp->accelerationApproximationData[sp->currentAccelerationApproximationIndex];
				sp->stepPeriodChangeSign =
						sp->accelerationApproximationDataSign[sp->currentAccelerationApproximationIndex];
				DEBUG_OUT(0x41);
			}
			if (sp->interStepChangeCounter > 0) {
				(sp->interStepChangeCounter)--;
			} else {
				uint32_t maxStepPeriod =
						sp->jog && sp->direction == sp->targetDirection ?
								min(sp->targetStepPeriod, sp->maxStepPeriod) :
								sp->maxStepPeriod;
				if (sp->stepPeriodChangeSign) {
					sp->interStepChangeCounter = sp->stepPeriodChange - 1;
					sp->stepPeriod = min(maxStepPeriod, sp->stepPeriod + 1);
				} else {
					sp->stepPeriod = min(maxStepPeriod,
							sp->stepPeriod + sp->stepPeriodChange);
				}
				SET_TIMER_LOAD_REGISTER(timerBase, sp->stepPeriod);
			}DEBUG_OUT(0x42);
			if (sp->jog && sp->direction == sp->targetDirection) {
				if (sp->targetStepPeriod == sp->stepPeriod)
					sp->state = cruise;
				else if (sp->targetStepPeriod < sp->stepPeriod) {
					if ((sp->accelerationSteps
							& (2 * sp->nextAccelerationApproximationBitMask))
							== 0)
						sp->nextAccelerationApproximationBitMask <<= 1;
					sp->state = accelerate;
				}
			}
		} else {
//			if (sp->absoluteOffset > 0) {
//				SET_TIMER_LOAD_REGISTER(timerBase, sp->maxStepPeriod);
//				sp->state = cruiseLastSteps;
//				DEBUG_OUT(0x43);
//			} else {
			if (sp->jog && sp->direction != sp->targetDirection
					&& sp->targetDirection != 0) {
				HWREG(stepPinPort+GPIO_O_AFSEL) &= ~stepPinBit; // deactivate pwm pin
				sp->direction = sp->targetDirection;
				sp->state = setDirection;
			} else
				initiateSteppingStop(timerBase, sp, stepPinPort, stepPinBit);
			DEBUG_OUT(0x44);
//			}
		}
		break;
	case cruiseLastSteps:
		DEBUG_OUT(0x50);
//		(sp->absoluteOffset)--;
		sp->position += sp->direction;
		(sp->cruiseSteps)--;
		if (sp->cruiseSteps == 0) {
			DEBUG_OUT(0x51);
			initiateSteppingStop(timerBase, sp, stepPinPort, stepPinBit);
		}
		DEBUG_OUT(0x52);
		break;
	case disable:
		DEBUG_OUT(0x60);
		if (!sp->keepEnabled)
			GPIOWRITE(enaPinPort, enaPinBit, INVERT_ENABLESIGNALS); // disable stepper driver
		HWREG(timerBase + TIMER_O_CTL) &= ~(TIMER_CTL_TAEN | TIMER_CTL_TBEN); /* stop timers */
		sp->state = stopped;
		DEBUG_OUT(0x61);
		break;
	default:
		DEBUG_OUT(0x70);
		break;
	}
}

static inline void generalCounterTimerISR(uint32_t timerBase,
		stepperProperty *sp) {
	HWREG(timerBase + TIMER_O_ICR) = TIMER_ICR_CBMCINT; /* clear interrupt */
	if (sp->state != setDirection) {
		sp->stopSignal = 1;
		sp->counterStopSignal = 1;
	}
}

#define TIMER_ISR_IMPL(timer,i,stepPinPort,stepPinBit,dirPinPort,dirPinBit,enaPinPort,enaPinBit) \
void Timer##timer##Aisr(void) { \
	generalPWMTimerISR(TIMER##timer##_BASE, &stepperProperties[i], \
		stepPinPort, stepPinBit, dirPinPort, dirPinBit, enaPinPort, enaPinBit); \
} \
void Timer##timer##Bisr(void) { \
	generalCounterTimerISR(TIMER##timer##_BASE, absoluteOffsets[i], &stopSignals[i], &counterStopSignals[i]); \
}
/*
 TIMER_ISR_IMPL(0, 0, GPIO_PORTD_BASE, GPIO_PIN_0, GPIO_PORTE_BASE, GPIO_PIN_0, GPIO_PORTL_BASE, GPIO_PIN_0)
 TIMER_ISR_IMPL(1, 1, GPIO_PORTD_BASE, GPIO_PIN_2, GPIO_PORTE_BASE, GPIO_PIN_1, GPIO_PORTL_BASE, GPIO_PIN_1)
 TIMER_ISR_IMPL(2, 2, GPIO_PORTA_BASE, GPIO_PIN_4, GPIO_PORTE_BASE, GPIO_PIN_2, GPIO_PORTL_BASE, GPIO_PIN_2)
 TIMER_ISR_IMPL(3, 3, GPIO_PORTA_BASE, GPIO_PIN_6, GPIO_PORTE_BASE, GPIO_PIN_3, GPIO_PORTL_BASE, GPIO_PIN_3)
 TIMER_ISR_IMPL(4, 4, GPIO_PORTM_BASE, GPIO_PIN_4, GPIO_PORTE_BASE, GPIO_PIN_2, GPIO_PORTL_BASE, GPIO_PIN_2)
 TIMER_ISR_IMPL(5, 5, GPIO_PORTM_BASE, GPIO_PIN_6, GPIO_PORTE_BASE, GPIO_PIN_3, GPIO_PORTL_BASE, GPIO_PIN_3)
 */

void Timer0Aisr(void) {
	generalPWMTimerISR(getTimerBaseForAxis(0), &stepperProperties[0],
	GPIO_PORTD_BASE, GPIO_PIN_0, DIRECTIONSIGNALS_PORT, GPIO_PIN_0,
	ENABLESIGNALS_PORT, GPIO_PIN_0);
}

void Timer0Bisr(void) {
	generalCounterTimerISR(getTimerBaseForAxis(0), &stepperProperties[0]);
}

void Timer1Aisr(void) {
	generalPWMTimerISR(getTimerBaseForAxis(1), &stepperProperties[1],
	GPIO_PORTD_BASE, GPIO_PIN_2, DIRECTIONSIGNALS_PORT, GPIO_PIN_1,
	ENABLESIGNALS_PORT, GPIO_PIN_1);
}

void Timer1Bisr(void) {
	generalCounterTimerISR(getTimerBaseForAxis(1), &stepperProperties[1]);
}

void Timer2Aisr(void) {
	generalPWMTimerISR(getTimerBaseForAxis(2), &stepperProperties[2],
	GPIO_PORTA_BASE, GPIO_PIN_4, DIRECTIONSIGNALS_PORT, GPIO_PIN_2,
	ENABLESIGNALS_PORT, GPIO_PIN_2);
}

void Timer2Bisr(void) {
	generalCounterTimerISR(getTimerBaseForAxis(2), &stepperProperties[2]);
}

void Timer3Aisr(void) {
	generalPWMTimerISR(getTimerBaseForAxis(3), &stepperProperties[3],
	GPIO_PORTA_BASE, GPIO_PIN_6, DIRECTIONSIGNALS_PORT, GPIO_PIN_3,
	ENABLESIGNALS_PORT, GPIO_PIN_3);
}

void Timer3Bisr(void) {
	generalCounterTimerISR(getTimerBaseForAxis(3), &stepperProperties[3]);
}

void Timer4Aisr(void) {
	generalPWMTimerISR(getTimerBaseForAxis(4), &stepperProperties[4],
	GPIO_PORTM_BASE, GPIO_PIN_4, DIRECTIONSIGNALS_PORT, GPIO_PIN_4,
	ENABLESIGNALS_PORT, GPIO_PIN_4);
}

void Timer4Bisr(void) {
	generalCounterTimerISR(getTimerBaseForAxis(4), &stepperProperties[4]);
}

void Timer5Aisr(void) {
	generalPWMTimerISR(getTimerBaseForAxis(5), &stepperProperties[5],
	GPIO_PORTM_BASE, GPIO_PIN_6, DIRECTIONSIGNALS_PORT, GPIO_PIN_5,
	ENABLESIGNALS_PORT, GPIO_PIN_5);
}

void Timer5Bisr(void) {
	generalCounterTimerISR(getTimerBaseForAxis(5), &stepperProperties[5]);
}

uint32_t readStepCounter(int axis) {
	return HWREG(getTimerBaseForAxis(axis) + TIMER_O_TBR);
}

int axisHasError(int axis) {
	return invert_errors[axis] ^ (GPIOREAD(ERRORSIGNALS_PORT,1<<axis) != 0);
}

//*****************************************************************************
//
// Configure the UART and its pins.  This must be called before UARTprintf().
//
//*****************************************************************************
void ConfigureUART(uint32_t sysClock) {
//
// Enable the GPIO Peripheral used by the UART.
//
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

//
// Enable UART0.
//
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);

//
// Configure GPIO Pins for UART mode.
//
	ROM_GPIOPinConfigure(GPIO_PA0_U0RX);
	ROM_GPIOPinConfigure(GPIO_PA1_U0TX);
	ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

//
// Initialize the UART for console I/O.
//
	UARTStdioConfig(0, 115200, sysClock);
	UARTEchoSet(ECHO_UART);
}

void calculateAccelerationApproximationData(int axis) {
	if (axis < 0 || axis >= NUM_STEPPER_PORTS)
		return;
	stepperProperty *sp = &stepperProperties[axis];
	sp->maxStepPeriod = VELOCITY_TO_STEP_PERIOD_M1(sp->minVelocity);
	sp->minStepPeriod = VELOCITY_TO_STEP_PERIOD_M1(sp->maxVelocity);
	float inv_v0 = ((float) sp->maxStepPeriod) + 1;
	float inv_v0_prescaled = inv_v0 / PRESCALER_FREQUENCY;
	float twoAoverV0sq = (sp->acceleration * inv_v0_prescaled)
			* (inv_v0_prescaled * 2);
	uint32_t x = 1, next_x;
	bool reciprocal = false;
	float y = inv_v0_prescaled, next_y;
	for (int i = 0; i < ACCELERATION_APPROXIMATION_BITS; i++) {
		next_x = 2 * x;
		next_y =
				reciprocal ?
						__hw_sqrtf(1 + twoAoverV0sq * (next_x - 1))
								/ inv_v0_prescaled :
						inv_v0_prescaled
								/ __hw_sqrtf(1 + twoAoverV0sq * (next_x - 1));
		float t =
				reciprocal ?
						(y * next_y) / (next_y - y) * x / PRESCALER_FREQUENCY :
						(y - next_y) / x * PRESCALER_FREQUENCY;
		int32_t it = __hw_roundf(t);
		if (!reciprocal) {
			if (it >= 1) {
				sp->accelerationApproximationDataSign[i] = 0;
				sp->accelerationApproximationData[i] = min(it,
						STEPPER_MAX_PERIOD-1);
			} else {
				reciprocal = true;
				sp->accelerationApproximationDataSign[i] = 1;
				y = 1 / y;
				next_y = __hw_sqrtf(1 + twoAoverV0sq * (next_x - 1))
						/ inv_v0_prescaled;
				t = (y * next_y) / (next_y - y) * x / PRESCALER_FREQUENCY;
				it = __hw_roundf(t);
				sp->accelerationApproximationData[i] = min(it,
						STEPPER_MAX_PERIOD-1);
			}
		} else {
			sp->accelerationApproximationDataSign[i] = 1;
			sp->accelerationApproximationData[i] = min(it,
					STEPPER_MAX_PERIOD-1);
		}
		x = next_x;
		y = next_y;
	}
}

inline bool stepperRunning(int axis) {
	return stepperProperties[axis].state != stopped;
}

inline bool steppersRunning() {
	for (int axis = 0; axis < NUM_STEPPER_PORTS; axis++)
		if (stepperRunning(axis))
			return true;
	return false;
}

int parseNumberFromDigit(char c) {
	if (c >= '0' && c <= '9')
		return c - '0';
	if (c >= 'A' && c <= 'Z')
		return c - 'A' + 10;
	if (c >= 'a' && c <= 'z')
		return c - 'a' + 10 + 'Z' - 'A';
	return -1;
}

int32_t parseValueFromString(char *str) {
	char *endPtr;
	int32_t val = strtol(str, &endPtr, 10);
	if (endPtr == str || *endPtr != 0) {
		UARTprintf("%d", (int) (*endPtr));
		return INVALID_VALUE;
	} else
		return val;
}

void sendProperty(int axis, int property) {
	if (axis < 0 || axis >= NUM_STEPPER_PORTS)
		UARTprintf("INVALID_AXIS");
	else {
		stepperProperty *sp = &stepperProperties[axis];
		switch (property) {
		case PROPERTY_POSITION:
			UARTprintf("%d", sp->position);
			break;
		case PROPERTY_MIN_VELOCITY:
			UARTprintf("%d", sp->minVelocity);
			break;
		case PROPERTY_MAX_VELOCITY:
			UARTprintf("%d", sp->maxVelocity);
			break;
		case PROPERTY_ACCELERATION:
			UARTprintf("%d", sp->acceleration);
			break;
		case PROPERTY_HOME_SEARCH_VELOCITY:
			UARTprintf("%d", sp->homeSearchVelocity);
			break;
		case PROPERTY_HOME_APPROACH_VELOCITY:
			UARTprintf("%d", sp->homeApproachVelocity);
			break;
		case PROPERTY_MAX_STEP_PERIOED:
			UARTprintf("%d", sp->maxStepPeriod);
			break;
		case PROPERTY_MIN_STEP_PERIOED:
			UARTprintf("%d", sp->minStepPeriod);
			break;
		case PROPERTY_AA_DATA:
			for (int i = 0; i < ACCELERATION_APPROXIMATION_BITS; i++) {
				if (i > 0)
					UARTprintf(",");
				if (sp->accelerationApproximationDataSign[i])
					UARTprintf("-");
				UARTprintf("%d", sp->accelerationApproximationData[i]);
			}
			break;
		case PROPERTY_CURRENT_STEP_PERIOD:
			UARTprintf("%d", sp->stepPeriod);
			break;
		case PROPERTY_CURRENT_STEP_PERIOD_CHANGE:
			if (sp->stepPeriodChangeSign)
				UARTprintf("-");
			UARTprintf("%d", sp->stepPeriodChange);
			break;
		case PROPERTY_IGNORE_STOPPERS:
			UARTprintf("%d,%d,%d", !sp->enableStopSwitches,
					getStopperStatus(axis, -1), getStopperStatus(axis, 1));
			break;
		case PROPERTY_STEP_COUNTER:
			UARTprintf("%d", readStepCounter(axis));
			break;
		case PROPERTY_IGNORE_ERROR:
			UARTprintf("%d,%d", sp->ignoreError, axisHasError(axis));
			break;
		case PROPERTY_KEEP_ENABLED:
			UARTprintf("%d", sp->keepEnabled);
			break;
		case PROPERTY_ENABLE_TO_DIRECTION_SET_DELAY:
			UARTprintf("%d", TC_TO_US(sp->enableToDirectionSetDelay));
			break;
		case PROPERTY_DIRECTION_SET_TO_FIRST_STEP_PULSE_DELAY:
			UARTprintf("%d", TC_TO_US(sp->directionSetToFirstStepPulseDelay));
			break;
		case PROPERTY_LAST_STEP_PULSE_TO_DISABLE_DELAY:
			UARTprintf("%d", TC_TO_US(sp->lastStepPulseToDisableDelay));
			break;
		case PROPERTY_ENCODER:
			UARTprintf("%d", sp->encoderValue);
			break;
		case PROPERTY_JOG_VELOCITY:
			UARTprintf("%d", sp->targetVelocity);
			break;
		case PROPERTY_VELOCITY:
			UARTprintf("%d",
					(int32_t) (sp->direction
							* STEP_PERIOD_TO_VELOCITY(sp->stepPeriod)));
			break;
		default:
			UARTprintf("INVALID_PROPERTY");
			return;
		}
	}
	UARTprintf("\n");
}

void sendCurrentPosition(int axis) {
	if (axis < 0 || axis >= NUM_STEPPER_PORTS)
		UARTprintf("INVALID_AXIS");
	else {
		stepperProperty *sp = &stepperProperties[axis];
//		if (stepperRunning(axis))
//			UARTprintf("%d",
//					sp->direction > 0 ?
//							sp->position + sp->absoluteOffset
//									- readStepCounter(axis) :
//							sp->position - sp->absoluteOffset
//									+ readStepCounter(axis));
//		else
		UARTprintf("%d", sp->position);
	}
	UARTprintf("\n");
}

inline void setVelocity(uint32_t *var, int32_t val) {
	if (val < MIN_VELOCITY) {
		*var = MIN_VELOCITY;
		UARTprintf("MIN_VELOCITY_SET");
	} else if (val > MAX_VELOCITY) {
		*var = MAX_VELOCITY;
		UARTprintf("MAX_VELOCITY_SET");
	} else
		*var = val;
}

inline void setStepperDelay(uint32_t *var, int32_t val) {
	int32_t valInClockCycles = US_TO_TC(val);
	if (valInClockCycles > (1L << 24))
		valInClockCycles = 1L << 24;
	else if (valInClockCycles < 1)
		valInClockCycles = 0;
	*var = valInClockCycles;
}

void setProperty(int axis, int property, char *param) {
	if (axis < 0 || axis >= NUM_STEPPER_PORTS)
		UARTprintf("INVALID_AXIS");
	else {
		stepperProperty *sp = &stepperProperties[axis];
		int32_t val = parseValueFromString(param);
		if (val == INVALID_VALUE)
			UARTprintf("INVALID_VALUE");
		else
			switch (property) {
			case PROPERTY_POSITION:
				sp->position = val;
				break;
			case PROPERTY_MIN_VELOCITY:
				setVelocity(&sp->minVelocity, val);
				calculateAccelerationApproximationData(axis);
				break;
			case PROPERTY_MAX_VELOCITY:
				setVelocity(&sp->maxVelocity, val);
				calculateAccelerationApproximationData(axis);
				break;
			case PROPERTY_ACCELERATION:
				if (val >= MIN_ACCELERATION && val <= MAX_ACCELERATION) {
					sp->acceleration = val;
					calculateAccelerationApproximationData(axis);
				} else
					UARTprintf("INVALID_ACCELERATION");
				break;
			case PROPERTY_HOME_SEARCH_VELOCITY:
				setVelocity(&sp->homeSearchVelocity, val);
				break;
			case PROPERTY_HOME_APPROACH_VELOCITY:
				setVelocity(&sp->homeApproachVelocity, val);
				break;
			case PROPERTY_IGNORE_STOPPERS:
				sp->enableStopSwitches = val == 0;
				break;
			case PROPERTY_IGNORE_ERROR:
				sp->ignoreError = val != 0;
				break;
			case PROPERTY_KEEP_ENABLED:
				sp->keepEnabled = val != 0;
				if (!stepperRunning(axis))
					GPIOWRITE(ENABLESIGNALS_PORT, 1 << axis,
							sp->keepEnabled ^ INVERT_ENABLESIGNALS);
				break;
			case PROPERTY_ENABLE_TO_DIRECTION_SET_DELAY:
				setStepperDelay(&sp->enableToDirectionSetDelay, val);
				break;
			case PROPERTY_DIRECTION_SET_TO_FIRST_STEP_PULSE_DELAY:
				setStepperDelay(&sp->directionSetToFirstStepPulseDelay, val);
				break;
			case PROPERTY_LAST_STEP_PULSE_TO_DISABLE_DELAY:
				setStepperDelay(&sp->lastStepPulseToDisableDelay, val);
				break;
			case PROPERTY_ENCODER:
				sp->encoderValue = val;
				break;
			case PROPERTY_JOG_VELOCITY:
				if (abs(val) > MAX_VELOCITY) {
					sp->targetVelocity = val > 0 ? MAX_VELOCITY : -MAX_VELOCITY;
					UARTprintf("MAX_VELOCITY_SET");
				} else
					sp->targetVelocity = val;
				sp->targetDirection = sp->targetVelocity > 0 ? 1 :
										sp->targetVelocity < 0 ? -1 : 0;
				sp->targetStepPeriod = VELOCITY_TO_STEP_PERIOD_M1(
						abs(sp->targetVelocity));
				break;
			default:
				UARTprintf("INVALID_PROPERTY");
				return;
			}
	}
	UARTprintf("\n");
}

void printStatus() {
	for (int axis = 0; axis < NUM_STEPPER_PORTS; axis++) {
		stepperProperty *sp = &stepperProperties[axis];
		char c = '?';
		switch (sp->state) {
		case setDirection:
			c = 'E';
			break;
		case preaccelerate:
			c = 'P';
			break;
		case accelerate:
			c = 'A';
			break;
		case cruise:
			c = 'C';
			break;
		case deccelerate:
			c = 'D';
			break;
		case cruiseLastSteps:
			c = 'c';
			break;
		case disable:
			c = 'd';
			break;
		case stopped:
			c = 's';
			break;
		}
		UARTprintf("%c%c%u", c, sp->direction >= 0 ? '+' : '-',
		/*absoluteOffsets[axis]*/
		sp->counterStopSignal ? 0 : readStepCounter(axis));
		if (sp->stopSignal && !sp->counterStopSignal) {
			UARTprintf("!");
			if (!stepperRunning(axis))
				sp->stopSignal = 0;
		}
		if (axisHasError(axis))
			UARTprintf("#");
		if (axis != NUM_STEPPER_PORTS - 1)
			UARTprintf(",");
	}
	UARTprintf("\n");
}

void stopAxis(int axis) {
	if (axis < 0 || axis >= NUM_STEPPER_PORTS) {
		UARTprintf("INVALID_AXIS\n");
		return;
	}
	if (stepperRunning(axis))
		stepperProperties[axis].stopSignal = 1;
	UARTprintf("\n");
}

void moveAxis(int axis, int32_t offset) {
	if (axis < 0 || axis >= NUM_STEPPER_PORTS) {
		UARTprintf("INVALID_AXIS\n");
		return;
	}
	stepperProperty *sp = &stepperProperties[axis];
	if (stepperRunning(axis) || (!sp->ignoreError && axisHasError(axis))) {
		printStatus();
		return;
	}
	if (offset == 0)
		return;
	if (abs(offset) > (STEPPER_MAX_STEPS)) {
		UARTprintf("TOO_MANY_STEPS\n");
		return;
	}
	sp->jog = 0;
	sp->direction = offset > 0 ? 1 : -1;
	sp->absoluteOffset = abs(offset);
	sp->halfwaySteps = sp->absoluteOffset >> 1;
	sp->oddSteps = sp->absoluteOffset & 1;
	sp->state = setDirection;
	stopStepping = 0;
	sp->stopSignal = 0;
	sp->counterStopSignal = 0;
	sp->accelerationSteps = 0;
	uint32_t timerBase = getTimerBaseForAxis(axis);
	HWREG(timerBase + TIMER_O_CTL) &= ~(TIMER_CTL_TAEN | TIMER_CTL_TBEN); /* stop timer */
	HWREG(timerBase + TIMER_O_TAMR) = TIMER_TAMR_TAMRSU | TIMER_TAMR_TAILD
			| TIMER_TAMR_TAMR_1_SHOT; /* configure timer A for later use as PWM with delayed value load */
	SET_COUNTER_LOAD_REGISTER(timerBase, sp->absoluteOffset);
	SET_COUNTER_MATCH_REGISTER(timerBase, 0);
	if (sp->enableStopSwitches && getStopperStatus(axis, sp->direction)) {
		sp->stopSignal = 1;
		sp->counterStopSignal = 1;
		sp->state = stopped;
		return;
	}
	if (!sp->keepEnabled) {
		SET_TIMER_LOAD_REGISTER(timerBase, sp->enableToDirectionSetDelay - 1); /* enable-to-direction-set-delay on start */
		GPIOWRITE(ENABLESIGNALS_PORT, 1 << axis, !INVERT_ENABLESIGNALS); /* set enable pin high */
		HWREG(timerBase + TIMER_O_CTL) |= TIMER_CTL_TAEN | TIMER_CTL_TBEN; /* start timer */
	} else {
		SET_TIMER_LOAD_REGISTER(timerBase, 0); /* no enable-to-direction-set-delay on start */
		HWREG(timerBase + TIMER_O_CTL) |= TIMER_CTL_TAEN | TIMER_CTL_TBEN; /* start timer */
	}
	UARTprintf("\n");
// return asynchronously
}

void jogAxis(int axis) {
	if (axis < 0 || axis >= NUM_STEPPER_PORTS) {
		UARTprintf("INVALID_AXIS\n");
		return;
	}
	stepperProperty *sp = &stepperProperties[axis];
	if (!sp->ignoreError && axisHasError(axis)) {
		printStatus();
		return;
	}
	sp->jog = 1;
	if (stepperRunning(axis)) {
		UARTprintf("\n");
		return;
	}
	sp->direction = sp->targetDirection;
	sp->absoluteOffset = 0;
	sp->halfwaySteps = 0;
	sp->oddSteps = 0;
	sp->state = setDirection;
	stopStepping = 0;
	sp->stopSignal = 0;
	sp->counterStopSignal = 0;
	sp->accelerationSteps = 0;
	uint32_t timerBase = getTimerBaseForAxis(axis);
	HWREG(timerBase + TIMER_O_CTL) &= ~(TIMER_CTL_TAEN | TIMER_CTL_TBEN); /* stop timer */
	HWREG(timerBase + TIMER_O_TAMR) = TIMER_TAMR_TAMRSU | TIMER_TAMR_TAILD
			| TIMER_TAMR_TAMR_1_SHOT; /* configure timer A for later use as PWM with delayed value load */
	SET_COUNTER_LOAD_REGISTER(timerBase, STEPPER_MAX_STEPS);
	SET_COUNTER_MATCH_REGISTER(timerBase, 0);
	if (sp->enableStopSwitches && getStopperStatus(axis, sp->direction)) {
		sp->stopSignal = 1;
		sp->counterStopSignal = 1;
		sp->state = stopped;
		return;
	}
	if (!sp->keepEnabled) {
		SET_TIMER_LOAD_REGISTER(timerBase, sp->enableToDirectionSetDelay - 1); /* enable-to-direction-set-delay on start */
		GPIOWRITE(ENABLESIGNALS_PORT, 1 << axis, !INVERT_ENABLESIGNALS); /* set enable pin high */
		HWREG(timerBase + TIMER_O_CTL) |= TIMER_CTL_TAEN | TIMER_CTL_TBEN; /* start timer */
	} else {
		SET_TIMER_LOAD_REGISTER(timerBase, 0); /* no enable-to-direction-set-delay on start */
		HWREG(timerBase + TIMER_O_CTL) |= TIMER_CTL_TAEN | TIMER_CTL_TBEN; /* start timer */
	}
	UARTprintf("\n");
// return asynchronously
}

bool joinStepper(int axis) {
	bool forcedStop = 0;
	while (stepperRunning(axis)) {
		int c = 0;
		if (UARTRxBytesAvail())
			c = UARTgetc();
		switch (c) {
		case '!':
			stepperProperties[axis].stopSignal = 1;
			forcedStop = 1;
			break;
		case 't':
			printStatus();
			break;
		case 'P':
			sendCurrentPosition(axis);
			break;
		default:
			break;
		}
	}
	return forcedStop;
}

void homeAxis(int axis, int direction, bool moveBack) {
	if (axis < 0 || axis >= NUM_STEPPER_PORTS) {
		UARTprintf("INVALID_AXIS\n");
		return;
	}
	if (stepperRunning(axis)) {
		printStatus();
		return;
	}
	stepperProperty *sp = &stepperProperties[axis];
	uint32_t savedVelocity = sp->maxVelocity, savedPosition = sp->position;
	sp->maxVelocity = sp->homeSearchVelocity;
	calculateAccelerationApproximationData(axis);
	moveAxis(axis, direction < 0 ? -(1 << 23) : (1 << 23) - 1);
	if (joinStepper(axis)) {
		sp->maxVelocity = savedVelocity;
		calculateAccelerationApproximationData(axis);
		UARTprintf("HOMING_ABORTED_APPROACH\n");
		return;
	}
//	sp->maxVelocity =
//			sp->homeApproachVelocity;
//	calculateAccelerationApproximationData(axis);
	moveAxis(axis, direction < 0 ? HOME_SET_BACK_STEPS : -HOME_SET_BACK_STEPS);
	if (joinStepper(axis)) {
		sp->maxVelocity = savedVelocity;
		calculateAccelerationApproximationData(axis);
		UARTprintf("HOMING_ABORTED_SETBACK\n");
		return;
	}
	sp->maxVelocity = sp->homeApproachVelocity;
	calculateAccelerationApproximationData(axis);
	moveAxis(axis,
			direction < 0 ? -2 * HOME_SET_BACK_STEPS : 2 * HOME_SET_BACK_STEPS);
	if (!joinStepper(axis)) {
		if (sp->stopSignal) {
			int32_t delta = savedPosition - sp->position;
			sp->position = 0;
			if (moveBack) {
				sp->maxVelocity = savedVelocity;
				calculateAccelerationApproximationData(axis);
				moveAxis(axis, delta);
				if (joinStepper(axis)) {
					UARTprintf("HOMING_ABORTED\n");
					return;
				}
			}
			UARTprintf("HOMING_DELTA: %d\n", delta - savedPosition);
		} else
			UARTprintf("HOMING_FAILED\n");
	} else
		UARTprintf("HOMING_ABORTED_SLOWAPPROACH\n");
	sp->maxVelocity = savedVelocity;
	calculateAccelerationApproximationData(axis);
}

void loop() {
	char rawSerialInputBuffer[SERIAL_INPUT_BUFFER_SIZE + 1];
	char *serialInputBuffer = rawSerialInputBuffer;
	size_t n = UARTgets(serialInputBuffer,
	SERIAL_INPUT_BUFFER_SIZE);
	if (n >= SERIAL_INPUT_BUFFER_SIZE || n < 1) // discard on overflow or timeout
		return;
	while (n > 0 && serialInputBuffer[0] == 'P') { // discard homing position polling command
		serialInputBuffer++;
		n--;
	}
	if (n == 0)
		return;
	if (n == 1) {
		switch (serialInputBuffer[0]) {
		case '!':
			stopStepping = 1;
			break;
		case 't':
			printStatus();
			break;
		case 'R':
			ROM_SysCtlReset();
			break;
		default:
			UARTprintf("UNKNOWN_CMD\n");
			break;
		}
	} else {
		serialInputBuffer[n] = 0;
		int axis = parseNumberFromDigit(serialInputBuffer[1]);
		if (axis < 0 || axis >= NUM_STEPPER_PORTS)
			UARTprintf("INVALID_AXIS\n");
		else
			switch (serialInputBuffer[0]) {
			case 'g':
				if (n == 3)
					sendProperty(axis,
							parseNumberFromDigit(serialInputBuffer[2]));
				else
					UARTprintf("PARAM_EXPECTED\n");
				break;
			case 's':
				if (n > 3)
					setProperty(axis,
							parseNumberFromDigit(serialInputBuffer[2]),
							serialInputBuffer + 3);
				else if (n == 3)
					UARTprintf("VALUE_EXPECTED\n");
				else
					UARTprintf("PARAM_EXPECTED\n");
				break;
			case 'p':
				sendCurrentPosition(axis);
				break;
			case 'a':
				if (n >= 3) {
					int32_t newPos = parseValueFromString(
							serialInputBuffer + 2);
					if (newPos == INVALID_VALUE)
						UARTprintf("INVALID_VALUE\n");
					else
						moveAxis(axis,
								newPos - stepperProperties[axis].position);
				} else
					UARTprintf("VALUE_EXPECTED\n");
				break;
			case 'r':
				if (n >= 3) {
					int32_t offset = parseValueFromString(
							serialInputBuffer + 2);
					if (offset == INVALID_VALUE)
						UARTprintf("INVALID_VALUE\n");
					else
						moveAxis(axis, offset);
				} else
					UARTprintf("VALUE_EXPECTED\n");
				break;
			case 'h':
				homeAxis(axis, n >= 3 && serialInputBuffer[2] == '+' ? 1 : -1,
						parseValueFromString(serialInputBuffer + 2) != 0);
				break;
			case 'j':
				jogAxis(axis);
				break;
			case 'S':
				stopAxis(axis);
				break;
			default:
				UARTprintf("UNKNOWN_CMD\n");
				break;
			}
	}
}

void __error__(char *pcFilename, uint32_t ui32Line) {
	UARTprintf("\n!error in file %s line %u!\n", pcFilename, ui32Line);
}

int main(void) {
// Set the clocking to run directly from the crystal at 120MHz.
	sysClockFrequency = SysCtlClockFreqSet(
			(SYSCTL_XTAL_25MHZ | SYSCTL_OSC_MAIN | SYSCTL_USE_PLL
					| SYSCTL_CFG_VCO_480), 120000000);

// Initialize the UART and write status.
	ConfigureUART(sysClockFrequency);

	UARTprintf("Tiva Stepper controller starting...");

// Enable the GPIO port that is used for the on-board LEDs.
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);

// Enable the GPIO pins for the LEDs (PN0 & PN1).
	ROM_GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE, GPIO_PIN_0 | GPIO_PIN_1);

// Configure timer pins
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOM);
	ROM_GPIOPinConfigure(GPIO_PD0_T0CCP0);
	ROM_GPIOPinConfigure(GPIO_PD1_T0CCP1);
	ROM_GPIOPinConfigure(GPIO_PD2_T1CCP0);
	ROM_GPIOPinConfigure(GPIO_PD3_T1CCP1);
	ROM_GPIOPinConfigure(GPIO_PA4_T2CCP0);
	ROM_GPIOPinConfigure(GPIO_PA5_T2CCP1);
	ROM_GPIOPinConfigure(GPIO_PA6_T3CCP0);
	ROM_GPIOPinConfigure(GPIO_PA7_T3CCP1);
	ROM_GPIOPinConfigure(GPIO_PM4_T4CCP0);
	ROM_GPIOPinConfigure(GPIO_PM5_T4CCP1);
	ROM_GPIOPinConfigure(GPIO_PM6_T5CCP0);
	ROM_GPIOPinConfigure(GPIO_PM7_T5CCP1);
	ROM_GPIOPinTypeTimer(GPIO_PORTD_BASE,
	GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
	ROM_GPIOPinTypeTimer(GPIO_PORTA_BASE,
	GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);
	ROM_GPIOPinTypeTimer(GPIO_PORTM_BASE,
	GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);
	ROM_GPIODirModeSet(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_2,
	GPIO_DIR_MODE_OUT); // deactivate pwm pins
	ROM_GPIODirModeSet(GPIO_PORTA_BASE, GPIO_PIN_4 | GPIO_PIN_6,
	GPIO_DIR_MODE_OUT); // deactivate pwm pins
	ROM_GPIODirModeSet(GPIO_PORTM_BASE, GPIO_PIN_4 | GPIO_PIN_6,
	GPIO_DIR_MODE_OUT); // deactivate pwm pins
	GPIOWRITE(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_2, INVERT_STEPSIGNALS); // deactivate pwm pins
	GPIOWRITE(GPIO_PORTA_BASE, GPIO_PIN_4 | GPIO_PIN_6, INVERT_STEPSIGNALS); // deactivate pwm pins
	GPIOWRITE(GPIO_PORTM_BASE, GPIO_PIN_4 | GPIO_PIN_6, INVERT_STEPSIGNALS); // deactivate pwm pins

// Configure the timers.
	for (int axis = 0; axis < NUM_STEPPER_PORTS; axis++) {
		uint32_t timerBase = getTimerBaseForAxis(axis);
		ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0 + axis); /* activate axis */
		HWREG(timerBase+ TIMER_O_CTL) &= ~(TIMER_CTL_TAEN | TIMER_CTL_TBEN); /* disable axis */
		HWREG(timerBase+ TIMER_O_CC) = TIMER_CLOCK_SYSTEM; /* run axis from main clock at 120MHz */
		HWREG(timerBase+ TIMER_O_CFG) = TIMER_CFG_16_BIT; /* split into two (16+8)bit axis */
		HWREG(timerBase+ TIMER_O_TAMR) = TIMER_TAMR_TAMRSU | TIMER_TAMR_TAILD
				| TIMER_TAMR_TAMR_1_SHOT; /* configure axis A for later use as PWM with delayed value load */
		HWREG(timerBase+ TIMER_O_TBMR) =
		/*TIMER_TBMR_TBCDIR |*/TIMER_TBMR_TBMR_CAP; /* configure axis B for edge counting */
		HWREG(timerBase+ TIMER_O_CTL) = TIMER_CTL_TASTALL
				| (INVERT_STEPSIGNALS ?
						TIMER_CTL_TAEVENT_POS | TIMER_CTL_TBEVENT_POS :
						TIMER_CTL_TAPWML | TIMER_CTL_TAEVENT_NEG
								| TIMER_CTL_TBEVENT_NEG)
				/*| TIMER_CTL_TBSTALL*/; /* enable timer stalling and configure counting edge and pwm level/trigger edge */
		HWREG(timerBase+ TIMER_O_IMR) = TIMER_IMR_TATOIM | TIMER_IMR_CAEIM
				| TIMER_IMR_CBMIM; /* enable timeout and capture event interrupt for pwm (init cycle) and match interrupt for counter */
		HWREG(timerBase+ TIMER_O_ICR) = TIMER_ICR_TATOCINT | TIMER_ICR_CAECINT
				| TIMER_ICR_CBMCINT;/*`clear pending interrupts */
//		SET_COUNTER_LOAD_REGISTER(timerBase,(1<<24)-1);/* set load register of counter to maximum as this limits the number of counted edges */
		SET_COUNTER_LOAD_REGISTER(timerBase, 0);	// reset counter to zero
		SET_COUNTER_MATCH_REGISTER(timerBase, 0);	// reset counter to zero
		SET_TIMER_MATCH_REGISTER(timerBase, STEPPER_PULSE_HIGH_WIDTH-1); // set pwm high pulse width
	}

	ROM_IntEnable(INT_TIMER0A);
	ROM_IntEnable(INT_TIMER0B);
	ROM_IntEnable(INT_TIMER1A);
	ROM_IntEnable(INT_TIMER1B);
	ROM_IntEnable(INT_TIMER2A);
	ROM_IntEnable(INT_TIMER2B);
	ROM_IntEnable(INT_TIMER3A);
	ROM_IntEnable(INT_TIMER3B);
	ROM_IntEnable(INT_TIMER4A);
	ROM_IntEnable(INT_TIMER4B);
	ROM_IntEnable(INT_TIMER5A);
	ROM_IntEnable(INT_TIMER5B);

// Configure other pins
	ROM_SysCtlPeripheralEnable(DIRECTIONSIGNALS_PERIPH);
	ROM_SysCtlPeripheralEnable(ENABLESIGNALS_PERIPH);
	ROM_SysCtlPeripheralEnable(STOPSIGNALS_PERIPH);
	ROM_SysCtlPeripheralEnable(STOPSIGNALS2_PERIPH);
	ROM_SysCtlPeripheralEnable(ERRORSIGNALS_PERIPH);
	ROM_SysCtlPeripheralEnable(ENCODERSIGNALS_I1_PERIPH);
	ROM_SysCtlPeripheralEnable(ENCODERSIGNALS_I2_PERIPH);
	ROM_SysCtlPeripheralEnable(ENCODERSIGNALS_Q1_PERIPH);
	ROM_SysCtlPeripheralEnable(ENCODERSIGNALS_Q2_PERIPH);
	ROM_GPIOPinTypeGPIOOutput(DIRECTIONSIGNALS_PORT,
			GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4
					| GPIO_PIN_5);
	ROM_GPIOPinTypeGPIOOutput(ENABLESIGNALS_PORT,
			GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4
					| GPIO_PIN_5);
	ROM_GPIOPinTypeGPIOInput(STOPSIGNALS_PORT,
			GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4
					| GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);
	ROM_GPIOPinTypeGPIOInput(STOPSIGNALS2_PORT,
	GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
	HWREG(STOPSIGNALS_PORT+GPIO_O_IS) = 0;
	HWREG(STOPSIGNALS2_PORT+GPIO_O_IS) = 0;
	HWREG(STOPSIGNALS_PORT+GPIO_O_IM) = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2
			| GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7;
	HWREG(STOPSIGNALS2_PORT+GPIO_O_IM) = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2
			| GPIO_PIN_3;
#if PULLUP_STOPSIGNALS
	HWREG(STOPSIGNALS_PORT+GPIO_O_PUR) = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2
			| GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7;
	HWREG(STOPSIGNALS2_PORT+GPIO_O_PUR) = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2
			| GPIO_PIN_3;
#elif PULLDOWN_STOPSIGNALS
	HWREG(STOPSIGNALS_PORT+GPIO_O_PDR) = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2
	| GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7;
	HWREG(STOPSIGNALS2_PORT+GPIO_O_PDR) = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2
	| GPIO_PIN_3;
#endif
#if INVERT_STOPSIGNALS
	HWREG(STOPSIGNALS_PORT+GPIO_O_IEV) = 0;
	HWREG(STOPSIGNALS2_PORT+GPIO_O_IEV) = 0;
#else
	HWREG(STOPSIGNALS_PORT+GPIO_O_IEV) = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2
	| GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7;
	HWREG(STOPSIGNALS2_PORT+GPIO_O_IEV) = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2
	| GPIO_PIN_3;
#endif
	ROM_IntEnable(STOPSIGNALS_INTERRUPT);
	ROM_IntEnable(STOPSIGNALS2_INTERRUPT);

	ROM_GPIOPinTypeGPIOInput(ERRORSIGNALS_PORT,
			GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4
					| GPIO_PIN_5);
	HWREG(ERRORSIGNALS_PORT+GPIO_O_IS) = 0;
	HWREG(ERRORSIGNALS_PORT+GPIO_O_IEV) = 0;
	for (int axis = 0; axis < NUM_STEPPER_PORTS; axis++)
		if (!invert_errors[axis])
			HWREG(ERRORSIGNALS_PORT+GPIO_O_IEV) |= 1 << axis;
	HWREG(ERRORSIGNALS_PORT+GPIO_O_IEV) = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2
			| GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5;
	HWREG(ERRORSIGNALS_PORT+GPIO_O_IM) = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2
			| GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5;
#if PULLDOWN_ERRORSIGNALS
	HWREG(ERRORSIGNALS_PORT+GPIO_O_PDR) = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2
	| GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5;
#elif PULLUP_ERRORSIGNALS
	HWREG(ERRORSIGNALS_PORT+GPIO_O_PUR) = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2
	| GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5;
#endif
	ROM_IntEnable(ERRORSIGNALS_INTERRUPT);

	ROM_GPIOPinTypeGPIOInput(ENCODERSIGNALS_I1_PORT, ENCODERSIGNALS_I1_PINS);
	HWREG(ENCODERSIGNALS_I1_PORT+GPIO_O_IS) = 0;
	HWREG(ENCODERSIGNALS_I1_PORT+GPIO_O_IBE) = ENCODERSIGNALS_I1_PINS;
	HWREG(ENCODERSIGNALS_I1_PORT+GPIO_O_IM) = ENCODERSIGNALS_I1_PINS;
	ROM_GPIOPinTypeGPIOInput(ENCODERSIGNALS_I2_PORT, ENCODERSIGNALS_I2_PINS);
	HWREG(ENCODERSIGNALS_I2_PORT+GPIO_O_IS) = 0;
	HWREG(ENCODERSIGNALS_I2_PORT+GPIO_O_IBE) = ENCODERSIGNALS_I2_PINS;
	HWREG(ENCODERSIGNALS_I2_PORT+GPIO_O_IM) = ENCODERSIGNALS_I2_PINS;
	ROM_GPIOPinTypeGPIOInput(ENCODERSIGNALS_Q1_PORT, ENCODERSIGNALS_Q1_PINS);
	HWREG(ENCODERSIGNALS_Q1_PORT+GPIO_O_IS) = 0;
	HWREG(ENCODERSIGNALS_Q1_PORT+GPIO_O_IBE) = ENCODERSIGNALS_Q1_PINS;
	HWREG(ENCODERSIGNALS_Q1_PORT+GPIO_O_IM) = ENCODERSIGNALS_Q1_PINS;
	ROM_GPIOPinTypeGPIOInput(ENCODERSIGNALS_Q2_PORT, ENCODERSIGNALS_Q2_PINS);
	HWREG(ENCODERSIGNALS_Q2_PORT+GPIO_O_IS) = 0;
	HWREG(ENCODERSIGNALS_Q2_PORT+GPIO_O_IBE) = ENCODERSIGNALS_Q2_PINS;
	HWREG(ENCODERSIGNALS_Q2_PORT+GPIO_O_IM) = ENCODERSIGNALS_Q2_PINS;
#if PULLDOWN_ENCODERSIGNALS
	HWREG(ENCODERSIGNALS_I1_PORT+GPIO_O_PDR) = ENCODERSIGNALS_I1_PINS;
	HWREG(ENCODERSIGNALS_I2_PORT+GPIO_O_PDR) = ENCODERSIGNALS_I2_PINS;
	HWREG(ENCODERSIGNALS_Q1_PORT+GPIO_O_PDR) = ENCODERSIGNALS_Q1_PINS;
	HWREG(ENCODERSIGNALS_Q2_PORT+GPIO_O_PDR) = ENCODERSIGNALS_Q2_PINS;
#elif PULLUP_ENCODERSIGNALS
	HWREG(ENCODERSIGNALS_I1_PORT+GPIO_O_PUR) = ENCODERSIGNALS_I1_PINS;
	HWREG(ENCODERSIGNALS_I2_PORT+GPIO_O_PUR) = ENCODERSIGNALS_I1_PINS;
	HWREG(ENCODERSIGNALS_Q1_PORT+GPIO_O_PUR) = ENCODERSIGNALS_Q1_PINS;
	HWREG(ENCODERSIGNALS_Q2_PORT+GPIO_O_PUR) = ENCODERSIGNALS_Q2_PINS;
#endif

	ROM_IntEnable(ENCODERSIGNALS_I1_INTERRUPT);
	ROM_IntEnable(ENCODERSIGNALS_I2_INTERRUPT);
	ROM_IntEnable(ENCODERSIGNALS_Q1_INTERRUPT);
	ROM_IntEnable(ENCODERSIGNALS_Q2_INTERRUPT);

#if OPEN_DRAIN_OUTPUTS
	HWREG(DIRECTIONSIGNALS_PORT+GPIO_O_ODR) = GPIO_PIN_0 | GPIO_PIN_1
	| GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5;
	HWREG(ENABLESIGNALS_PORT+GPIO_O_ODR) = GPIO_PIN_0 | GPIO_PIN_1
	| GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5;
	HWREG(GPIO_PORTD_BASE+GPIO_O_ODR) = GPIO_PIN_0 | GPIO_PIN_2;
	HWREG(GPIO_PORTA_BASE+GPIO_O_ODR) = GPIO_PIN_4 | GPIO_PIN_6;
	HWREG(GPIO_PORTM_BASE+GPIO_O_ODR) = GPIO_PIN_4 | GPIO_PIN_6;
#endif

	for (int axis = 0; axis < NUM_STEPPER_PORTS; axis++)
		GPIOWRITE(ENABLESIGNALS_PORT, 1 << axis, INVERT_ENABLESIGNALS); /* set enable pin low */

#if DEBUG & 1
	ROM_SysCtlPeripheralEnable(DEBUGSIGNALS_PERIPH);
	ROM_GPIOPinTypeGPIOOutput(DEBUGSIGNALS_PORT,
			GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4
			| GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);
#endif

	ROM_IntMasterEnable(); // Enable processor interrupts.

// configure default stepper parameters
	for (int axis = 0; axis < NUM_STEPPER_PORTS; axis++) {
		stepperProperty *sp = &stepperProperties[axis];
		sp->state = stopped;
		sp->position = 0;
		sp->minVelocity = MIN_VELOCITY;
		sp->maxVelocity = MAX_VELOCITY;
		sp->acceleration = STEPPER_ACCELERATION;
		calculateAccelerationApproximationData(axis);
		sp->enableStopSwitches = 1;
		sp->keepEnabled = 0;
		sp->ignoreError = 0;
		sp->enableToDirectionSetDelay = STEPPER_ENABLE_TO_DIRECTION_SET_DELAY;
		sp->directionSetToFirstStepPulseDelay =
		STEPPER_DIRECTION_SET_TO_STEP_PULSE_DELAY;
		sp->lastStepPulseToDisableDelay =
		STEPPER_LAST_STEP_PULSE_TO_DISABLE_DELAY;
		sp->encoderValue = 0;
	}

	UARTprintf("done\n");

	while (1)
		loop(); // start reading commands and performing corresponding actions

	return 0;
}

