//*****************************************************************************
//
// startup_gcc.c - Startup code for use with GNU tools.
//
// Copyright (c) 2013-2017 Texas Instruments Incorporated.  All rights reserved.
// Software License Agreement
// 
// Texas Instruments (TI) is supplying this software for use solely and
// exclusively on TI's microcontroller products. The software is owned by
// TI and/or its suppliers, and is protected under applicable copyright
// laws. You may not combine this software with "viral" open-source
// software in order to form a larger program.
// 
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY
// CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
// DAMAGES, FOR ANY REASON WHATSOEVER.
// 
// This is part of revision 2.1.4.178 of the EK-TM4C1294XL Firmware Package.
//
//*****************************************************************************

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_nvic.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/tm4c1294ncpdt.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"

//*****************************************************************************
//
// Forward declaration of the default fault handlers.
//
//*****************************************************************************
void ResetISR(void);
static void NmiSR(void);
static void HardFaultISR(void);
static void MPUFaultISR(void);
static void BusFaultISR(void);
static void UsageFaultISR(void);
static void IntDefaultHandler(void);

//*****************************************************************************
//
// External declarations for the interrupt handlers used by the application.
//
//*****************************************************************************
extern void Timer0Aisr(void);
extern void Timer0Bisr(void);
extern void Timer1Aisr(void);
extern void Timer1Bisr(void);
extern void Timer2Aisr(void);
extern void Timer2Bisr(void);
extern void Timer3Aisr(void);
extern void Timer3Bisr(void);
extern void Timer4Aisr(void);
extern void Timer4Bisr(void);
extern void Timer5Aisr(void);
extern void Timer5Bisr(void);
extern void StopSignalISR(void);
extern void StopSignal2ISR(void);
extern void ErrorSignalISR(void);
extern void EncoderI1signalISR(void);
extern void EncoderI2signalISR(void);
extern void EncoderQ1signalISR(void);
extern void EncoderQ2signalISR(void);
extern void UARTStdioIntHandler(void);

//*****************************************************************************
//
// The entry point for the application.
//
//*****************************************************************************
extern int main(void);

//*****************************************************************************
//
// Reserve space for the system stack.
//
//*****************************************************************************
static uint32_t pui32Stack[64];

//*****************************************************************************
//
// The vector table.  Note that the proper constructs must be placed on this to
// ensure that it ends up at physical address 0x0000.0000.
//
//*****************************************************************************
__attribute__ ((section(".isr_vector")))
void (* const g_pfnVectors[])(void) =
{
	(void (*)(void))((uint32_t)pui32Stack + sizeof(pui32Stack)),
	// The initial stack pointer
		ResetISR,// The reset handler
		NmiSR,// The NMI handler
		HardFaultISR,// The hard fault handler
		MPUFaultISR,// The MPU fault handler
		BusFaultISR,// The bus fault handler
		UsageFaultISR,// The usage fault handler
		0,// Reserved
		0,// Reserved
		0,// Reserved
		0,// Reserved
		IntDefaultHandler,// SVCall handler
		IntDefaultHandler,// Debug monitor handler
		0,// Reserved
		IntDefaultHandler,// The PendSV handler
		IntDefaultHandler,// The SysTick handler
		IntDefaultHandler,// GPIO Port A
		EncoderI2signalISR,// GPIO Port B
		EncoderI1signalISR,// GPIO Port C
		IntDefaultHandler,// GPIO Port D
		IntDefaultHandler,// GPIO Port E
		UARTStdioIntHandler,// UART0 Rx and Tx
		IntDefaultHandler,// UART1 Rx and Tx
		IntDefaultHandler,// SSI0 Rx and Tx
		IntDefaultHandler,// I2C0 Master and Slave
		IntDefaultHandler,// PWM Fault
		IntDefaultHandler,// PWM Generator 0
		IntDefaultHandler,// PWM Generator 1
		IntDefaultHandler,// PWM Generator 2
		IntDefaultHandler,// Quadrature Encoder 0
		IntDefaultHandler,// ADC Sequence 0
		IntDefaultHandler,// ADC Sequence 1
		IntDefaultHandler,// ADC Sequence 2
		IntDefaultHandler,// ADC Sequence 3
		IntDefaultHandler,// Watchdog timer
		Timer0Aisr,// Timer 0 subtimer A
		Timer0Bisr,// Timer 0 subtimer B
		Timer1Aisr,// Timer 1 subtimer A
		Timer1Bisr,// Timer 1 subtimer B
		Timer2Aisr,// Timer 2 subtimer A
		Timer2Bisr,// Timer 2 subtimer B
		IntDefaultHandler,// Analog Comparator 0
		IntDefaultHandler,// Analog Comparator 1
		IntDefaultHandler,// Analog Comparator 2
		IntDefaultHandler,// System Control (PLL, OSC, BO)
		IntDefaultHandler,// FLASH Control
		IntDefaultHandler,// GPIO Port F
		EncoderQ2signalISR,// GPIO Port G
		EncoderQ1signalISR,// GPIO Port H
		IntDefaultHandler,// UART2 Rx and Tx
		IntDefaultHandler,// SSI1 Rx and Tx
		Timer3Aisr,// Timer 3 subtimer A
		Timer3Bisr,// Timer 3 subtimer B
		IntDefaultHandler,// I2C1 Master and Slave
		IntDefaultHandler,// CAN0
		IntDefaultHandler,// CAN1
		IntDefaultHandler,// Ethernet
		IntDefaultHandler,// Hibernate
		IntDefaultHandler,// USB0
		IntDefaultHandler,// PWM Generator 3
		IntDefaultHandler,// uDMA Software Transfer
		IntDefaultHandler,// uDMA Error
		IntDefaultHandler,// ADC1 Sequence 0
		IntDefaultHandler,// ADC1 Sequence 1
		IntDefaultHandler,// ADC1 Sequence 2
		IntDefaultHandler,// ADC1 Sequence 3
		IntDefaultHandler,// External Bus Interface 0
		IntDefaultHandler,// GPIO Port J
		StopSignalISR,// GPIO Port K
		IntDefaultHandler,// GPIO Port L
		IntDefaultHandler,// SSI2 Rx and Tx
		IntDefaultHandler,// SSI3 Rx and Tx
		IntDefaultHandler,// UART3 Rx and Tx
		IntDefaultHandler,// UART4 Rx and Tx
		IntDefaultHandler,// UART5 Rx and Tx
		IntDefaultHandler,// UART6 Rx and Tx
		IntDefaultHandler,// UART7 Rx and Tx
		IntDefaultHandler,// I2C2 Master and Slave
		IntDefaultHandler,// I2C3 Master and Slave
		Timer4Aisr,// Timer 4 subtimer A
		Timer4Bisr,// Timer 4 subtimer B
		Timer5Aisr,// Timer 5 subtimer A
		Timer5Bisr,// Timer 5 subtimer B
		IntDefaultHandler,// FPU
		0,// Reserved
		0,// Reserved
		IntDefaultHandler,// I2C4 Master and Slave
		IntDefaultHandler,// I2C5 Master and Slave
		StopSignal2ISR,// GPIO Port M
		IntDefaultHandler,// GPIO Port N
		0,// Reserved
		IntDefaultHandler,// Tamper
		ErrorSignalISR,// GPIO Port P (Summary or P0)
		IntDefaultHandler,// GPIO Port P1
		IntDefaultHandler,// GPIO Port P2
		IntDefaultHandler,// GPIO Port P3
		IntDefaultHandler,// GPIO Port P4
		IntDefaultHandler,// GPIO Port P5
		IntDefaultHandler,// GPIO Port P6
		IntDefaultHandler,// GPIO Port P7
		IntDefaultHandler,// GPIO Port Q (Summary or Q0)
		IntDefaultHandler,// GPIO Port Q1
		IntDefaultHandler,// GPIO Port Q2
		IntDefaultHandler,// GPIO Port Q3
		IntDefaultHandler,// GPIO Port Q4
		IntDefaultHandler,// GPIO Port Q5
		IntDefaultHandler,// GPIO Port Q6
		IntDefaultHandler,// GPIO Port Q7
		IntDefaultHandler,// GPIO Port R
		IntDefaultHandler,// GPIO Port S
		IntDefaultHandler,// SHA/MD5 0
		IntDefaultHandler,// AES 0
		IntDefaultHandler,// DES3DES 0
		IntDefaultHandler,// LCD Controller 0
		IntDefaultHandler,// Timer 6 subtimer A
		IntDefaultHandler,// Timer 6 subtimer B
		IntDefaultHandler,// Timer 7 subtimer A
		IntDefaultHandler,// Timer 7 subtimer B
		IntDefaultHandler,// I2C6 Master and Slave
		IntDefaultHandler,// I2C7 Master and Slave
		IntDefaultHandler,// HIM Scan Matrix Keyboard 0
		IntDefaultHandler,// One Wire 0
		IntDefaultHandler,// HIM PS/2 0
		IntDefaultHandler,// HIM LED Sequencer 0
		IntDefaultHandler,// HIM Consumer IR 0
		IntDefaultHandler,// I2C8 Master and Slave
		IntDefaultHandler,// I2C9 Master and Slave
		IntDefaultHandler// GPIO Port T
};

//*****************************************************************************
//
// The following are constructs created by the linker, indicating where the
// the "data" and "bss" segments reside in memory.  The initializers for the
// for the "data" segment resides immediately following the "text" segment.
//
//*****************************************************************************
extern uint32_t _ldata;
extern uint32_t _data;
extern uint32_t _edata;
extern uint32_t _bss;
extern uint32_t _ebss;

//*****************************************************************************
//
// This is the code that gets called when the processor first starts execution
// following a reset event.  Only the absolutely necessary set is performed,
// after which the application supplied entry() routine is called.  Any fancy
// actions (such as making decisions based on the reset cause register, and
// resetting the bits in that register) are left solely in the hands of the
// application.
//
//*****************************************************************************
void ResetISR(void) {
	uint32_t *pui32Src, *pui32Dest;

	//
	// Copy the data segment initializers from flash to SRAM.
	//
	pui32Src = &_ldata;
	for (pui32Dest = &_data; pui32Dest < &_edata;) {
		*pui32Dest++ = *pui32Src++;
	}

	//
	// Zero fill the bss segment.
	//
	__asm("    ldr     r0, =_bss\n"
			"    ldr     r1, =_ebss\n"
			"    mov     r2, #0\n"
			"    .thumb_func\n"
			"zero_loop:\n"
			"        cmp     r0, r1\n"
			"        it      lt\n"
			"        strlt   r2, [r0], #4\n"
			"        blt     zero_loop");

	//
	// Enable the floating-point unit.  This must be done here to handle the
	// case where main() uses floating-point and the function prologue saves
	// floating-point registers (which will fault if floating-point is not
	// enabled).  Any configuration of the floating-point unit using DriverLib
	// APIs must be done here prior to the floating-point unit being enabled.
	//
	// Note that this does not use DriverLib since it might not be included in
	// this project.
	//
	HWREG(NVIC_CPAC) = ((HWREG(NVIC_CPAC)
			& ~(NVIC_CPAC_CP10_M | NVIC_CPAC_CP11_M)) |
	NVIC_CPAC_CP10_FULL | NVIC_CPAC_CP11_FULL);

	//
	// Call the application's entry point.
	//
	main();
}

static inline uint32_t xPSR(void) {
	uint32_t xPSR;
	asm("mrs %[r], PSR": [r] "=r" (xPSR) : );
	return xPSR;
}

//*****************************************************************************
//
// This is the code that gets called when the processor receives a NMI.  This
// simply enters an infinite loop, preserving the system state for examination
// by a debugger.
//
//*****************************************************************************
static void NmiSR(void) {
	//
	// Enter an infinite loop.
	//
	while (1) {
	}
}

//*****************************************************************************
//
// This is the code that gets called when the processor receives a fault
// interrupt.  This simply enters an infinite loop, preserving the system state
// for examination by a debugger.
//
//*****************************************************************************
static void HardFaultISR(void) {
	UARTprintf("hardfault: 0x%X\n", NVIC_FAULT_STAT_R);
	//
	// Enter an infinite loop.
	//
	while (1) {
	}
}

//*****************************************************************************
//
// This is the code that gets called when the processor receives a mpu fault
// interrupt.  This simply enters an infinite loop, preserving the system state
// for examination by a debugger.
//
//*****************************************************************************
static void MPUFaultISR(void) {
	UARTprintf("mpufault: 0x%X\n", NVIC_FAULT_STAT_R);
	//
	// Enter an infinite loop.
	//
//	while (1) {
//	}
}

//*****************************************************************************
//
// This is the code that gets called when the processor receives a bus fault
// interrupt.  This simply enters an infinite loop, preserving the system state
// for examination by a debugger.
//
//*****************************************************************************
static void BusFaultISR(void) {
	UARTprintf("busfault: 0x%X\n", NVIC_FAULT_STAT_R);
	//
	// Enter an infinite loop.
	//
	while (1) {
	}
}

//*****************************************************************************
//
// This is the code that gets called when the processor receives a bus fault
// interrupt.  This simply enters an infinite loop, preserving the system state
// for examination by a debugger.
//
//*****************************************************************************
static void UsageFaultISR(void) {
	UARTprintf("usagefault: 0x%X\n", NVIC_FAULT_STAT_R);
	//
	// Enter an infinite loop.
	//
	while (1) {
	}
}
//*****************************************************************************
//
// This is the code that gets called when the processor receives an unexpected
// interrupt.  This simply enters an infinite loop, preserving the system state
// for examination by a debugger.
//
//*****************************************************************************
static void IntDefaultHandler(void) {
	UARTprintf("interrupt: 0x%X\n", xPSR());
	// Go into an infinite loop.
	//
	while (1) {
	}
}
