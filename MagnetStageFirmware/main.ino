/*
 * main.ino
 *
 *  Created on: 10.05.2017
 *      Author: john
 */
#ifndef __AVR_ATmega2560__
#define __AVR_ATmega2560__
#endif

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <math.h>

#define NOP __asm__ __volatile__ ("nop\n\t")

#ifndef F_CPU
#define F_CPU 16000000
#endif


__attribute__((optimize("unroll-loops"))) inline void delayClockCycles(int n) {
	for (int i = 0; i < n; i++)
		NOP;
}

#ifndef max
#define max(a,b) ((a) >= (b) ? (a) : (b))
#endif
#ifndef min
#define min(a,b) ((a) < (b) ? (a) : (b))
#endif

#define STEPPER_PINS_PER_AXIS 5
#define STEPPER_OUT_PINS_PER_AXIS 3

typedef struct {
	char name;
	union {
		uint8_t numbered[5];
		struct {
			uint8_t stepPin;
			uint8_t directionPin;
			uint8_t enablePin;
			uint8_t startPin;
			uint8_t endPin;
		} named;
	} pins;
} stepper_pins;

typedef struct {
	int32_t position;
	uint16_t minVelocity;
	uint16_t maxVelocity;
	uint16_t acceleration;
	uint16_t homeSearchVelocity;
	uint16_t homeApproachVelocity;
	uint16_t maxStepPeriod;
	uint16_t minStepPeriod;
	uint16_t accelerationApproximationData[16];
	uint8_t accelerationApproximationDataSign[16];
} stepperProperty;



#define NUM_STEPPER_PORTS 4
#define SERIAL_INPUT_BUFFER_SIZE 32
#define CR 0x0D
#define CLOCK_FREQUENCY F_CPU
#define CLOCK_CYCLES_PER_US CLOCK_FREQUENCY/1000000L
#define PRESCALER_CLOCK_CYCLES 8L
#define PRESCALED_CLOCK_CYCLES_PER_US CLOCK_CYCLES_PER_US/PRESCALER_CLOCK_CYCLES
#define PRESCALER_FREQUENCY (CLOCK_FREQUENCY/PRESCALER_CLOCK_CYCLES)
#define STEPPER_PRESCALER_SYNC 3
#define US_TO_TC(a) ((int)((a)*PRESCALED_CLOCK_CYCLES_PER_US))
#define STEPPER_ENABLE_TO_DIRECTION_SET_DELAY US_TO_TC(5)
#define STEPPER_DIRECTION_SET_TO_STEP_PULSE_DELAY US_TO_TC(5)
#define STEPPER_PULSE_HIGH_WIDTH US_TO_TC(2.5)
#define STEPPER_PULSE_MIN_LOW_WIDTH US_TO_TC(2.5)
#define STEPPER_MAX_PERIOD (1L<<16)
#define MAX_VELOCITY 60000L
#define STEP_PERIOD_TO_VELOCITY(t) (CLOCK_FREQUENCY/(PRESCALER_CLOCK_CYCLES*(t)))
#define STEPPER_MIN_PERIOD (max(STEPPER_PULSE_HIGH_WIDTH+STEPPER_PULSE_MIN_LOW_WIDTH,\
		STEP_PERIOD_TO_VELOCITY(MAX_VELOCITY)))
#define VELOCITY_TO_STEP_PERIOD_M1(t) \
			((t) < 32 ? \
					STEPPER_MAX_PERIOD - 1 : \
					(t)*STEPPER_MIN_PERIOD*PRESCALER_CLOCK_CYCLES > CLOCK_FREQUENCY ? \
							STEPPER_MIN_PERIOD - 1 : \
							STEP_PERIOD_TO_VELOCITY(t) - 1)
#define VELOCITY_TO_STEP_PERIOD(t) (VELOCITY_TO_STEP_PERIOD_M1(t) + 1)
#define STEPPER_ACCELERATION 10000	// steps per second increase per second
#define INVALID_VALUE 0x80000000
#define MIN_VELOCITY STEP_PERIOD_TO_VELOCITY(STEPPER_MAX_PERIOD)
#define MIN_ACCELERATION 0
#define MAX_ACCELERATION ((1l<<16)-1)
#define HOME_SET_BACK_STEPS 1000
#define PROPERTY_POSITION 0
#define PROPERTY_MIN_VELOCITY 1
#define PROPERTY_MAX_VELOCITY 2
#define PROPERTY_ACCELERATION 3
#define PROPERTY_HOME_SEARCH_VELOCITY 4
#define PROPERTY_HOME_APPROACH_VELOCITY 5
#define PROPERTY_MAX_STEP_PERIOED 6
#define PROPERTY_MIN_STEP_PERIOED 7
#define PROPERTY_AA_DATA 8
#define PROPERTY_CURRENT_STEP_PERIOD 9
#define PROPERTY_CURRENT_STEP_PERIOD_CHANGE 10
#define STOPSIGNALS_READ_PORT PINK
#define STOPSIGNALS_WRITE_PORT PINK
#define STOPSIGNALS_DIRECTION_PORT DDRK
#define STOPSIGNALS_PCI_NUMBER 2
#define STOPSIGNALS_MASK_REG PCMSK2
#define STOPSIGNALS_INT_VECT PCINT2_vect
#define DEBUG 0
#define DEBUG_DIRECTION_PORT DDRF
#define DEBUG_TOGGLE_PORT PINF
#define DEBUG_PORT PORTF
#if DEBUG&1
#define DEBUG_OUT(axis,value) (DEBUG_PORT = (axis<<6) + value)
#else
#define DEBUG_OUT(axis,value)
#endif
#if DEBUG&2
#define DEBUG_PRINT(x) (Serial.print(x))
#define DEBUG_PRINTLN(x) (Serial.println(x))
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTLN(x)
#endif
#if DEBUG==0
#define NO_DEBUG_DELAY NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP
#else
#define NO_DEBUG_DELAY
#endif

stepperProperty stepperProperties[NUM_STEPPER_PORTS];

typedef enum {
	enable,
	setDirection,
	accelerate,
	cruise,
	deccelerate,
	cruiseLastSteps,
	stop,
	disable,
	disabled
} stepperState_type;
//typedef uint8_t[3] uint24_t;
stepperState_type stepperStates[NUM_STEPPER_PORTS];
int8_t currentDirections[NUM_STEPPER_PORTS];
uint32_t absoluteOffsets[NUM_STEPPER_PORTS], halfwaySteps[NUM_STEPPER_PORTS];
int32_t currentOffsets[NUM_STEPPER_PORTS];
uint16_t accelerationSteps[NUM_STEPPER_PORTS], cruiseSteps[NUM_STEPPER_PORTS];
uint16_t interStepChangeCounter[NUM_STEPPER_PORTS];
uint16_t stepTime[NUM_STEPPER_PORTS];
uint16_t stepTimeChange[NUM_STEPPER_PORTS];
uint8_t stepTimeChangeSign[NUM_STEPPER_PORTS];
uint8_t currentAAI[NUM_STEPPER_PORTS], oddSteps[NUM_STEPPER_PORTS];
uint16_t nextAABM[NUM_STEPPER_PORTS];
uint8_t stopStepping, stopSignals[NUM_STEPPER_PORTS],stopped[NUM_STEPPER_PORTS];

ISR(STOPSIGNALS_INT_VECT) {
	uint8_t x = STOPSIGNALS_READ_PORT;
	for (int i = 0; i < NUM_STEPPER_PORTS; i++)
		if (x & (currentDirections[i] > 0 ?	1<<(2*i+1):	1<<(2*i)))
			stopSignals[i] = 1;
}

inline uint8_t __builtin_add_overflow(uint16_t a, uint16_t b, uint16_t *c) {
	uint8_t r;
	*c = a + b;
	asm (	"ldi %[r], 0; \n\t"
			"adc %[r], %[r]; \n\t"
			/*"sts %[r], __tmp_reg__; \n\t"*/: [r] "=r" (r) : );
	return r;
}

#define TIMER_ISR(timer,axis,stepperState,accelerationSteps,cruiseSteps,absoluteOffset,\
		dirPinPort,dirPinBit,enaPinPort,enaPinBit,interStepChangeCounter,halfwaySteps,oddSteps,\
		nextAABM,currentAAI,currentAAD,currentAADS,stepTimeChange,stepTimeChangeSign,stepTime,\
		minStepTime,maxStepTime,stopped,stopSignal,position,direction,currentOffset) \
ISR(TIMER##timer##_OVF_vect) { \
	if (stopSignal||stopStepping) { \
		DEBUG_OUT(axis,1); \
		TCCR##timer##B = (1 << WGM13) | (1 << WGM12); \
		TCCR##timer##A = (1 << WGM11) | (1 << WGM10); \
		enaPinPort &= ~(1<<enaPinBit); \
		TIMSK##timer = 0; /* deactivate overflow interrupt */ \
		position += currentOffset - direction*absoluteOffset; \
		switch (stepperState) { \
		case accelerate: \
		case cruise: \
		case deccelerate: \
			position += direction; \
			break; \
		default: \
			break; \
		} \
		stopped = 1; \
		stepperState = disabled; \
		return; \
	} \
	switch (stepperState) { \
	case enable: \
		DEBUG_OUT(axis,2); \
		enaPinPort |= (1<<enaPinBit); \
		OCR##timer##A = STEPPER_DIRECTION_SET_TO_STEP_PULSE_DELAY - 1; \
		stepperState = setDirection; \
		break; \
	case setDirection: \
		DEBUG_OUT(axis,3); \
		if (direction < 0) \
			dirPinPort |= (1<<dirPinBit); \
		else \
			dirPinPort &= ~(1<<dirPinBit); \
		if (halfwaySteps > 0) { \
			stepperState = accelerate; \
			nextAABM = 2; \
			currentAAI = 0; \
			stepTimeChange = currentAAD[0]; \
			stepTimeChangeSign = currentAADS[0]; \
		} else \
			stepperState = cruiseLastSteps; \
		stepTime = maxStepTime; \
		DEBUG_OUT(axis,3+16); \
		OCR##timer##A = stepTime; /* start with initial maximum step time */ \
		OCR##timer##B = STEPPER_PULSE_HIGH_WIDTH - 1; /* use OCnB to produce step pulse of correct length */ \
		NO_DEBUG_DELAY; \
		TCCR##timer##A = (1 << COM1B1) | (1 << WGM11) | (1 << WGM10); /* connect OCnB nonInverted to produce PWM */ \
		DEBUG_PRINT("s,"); \
		DEBUG_PRINTLN(stepTime); \
		DEBUG_OUT(axis,3+32); \
		break; \
	case accelerate: \
		DEBUG_OUT(axis,4); \
		accelerationSteps++; \
		absoluteOffset--; \
		DEBUG_PRINT("a,"); \
		DEBUG_PRINT(accelerationSteps); \
		DEBUG_PRINT(','); \
		if ((accelerationSteps & nextAABM) != 0 && currentAAI < 15) { \
			currentAAI++; \
			nextAABM <<= 1; \
			stepTimeChange = currentAAD[currentAAI]; \
			stepTimeChangeSign = currentAADS[currentAAI]; \
			DEBUG_OUT(axis,4+32); \
			DEBUG_PRINT(currentAAI); \
			DEBUG_PRINT(','); \
			DEBUG_PRINT(nextAABM); \
			DEBUG_PRINT(','); \
		} \
		DEBUG_OUT(axis,4+16); \
		/* position += direction; */ \
		if (accelerationSteps == halfwaySteps) { \
			if (oddSteps) { \
				stepperState = cruise; \
				cruiseSteps = 1; \
			} else \
				stepperState = deccelerate; \
			interStepChangeCounter = 0; \
			if((accelerationSteps & nextAABM) == 0) { \
				/*currentAAI--;*/ \
				nextAABM >>= 1; \
				/*stepTimeChange = currentAAD[currentAAI];*/ \
				/*stepTimeChangeSign = currentAADS[currentAAI];*/ \
				DEBUG_OUT(axis,4+32); \
				DEBUG_PRINT(currentAAI); \
				DEBUG_PRINT(','); \
				DEBUG_PRINT(nextAABM); \
				DEBUG_PRINT(','); \
			} \
		} else if (accelerationSteps==0) { /* wrap-around! acceleration too slow, cruise on with current speed */ \
			interStepChangeCounter = 0; \
			cruiseSteps = absoluteOffset - (1l<<16); \
			if(cruiseSteps>0) \
				stepperState = cruise; \
			else \
				stepperState = deccelerate; \
		} else if (stepTime == minStepTime) { \
			stepperState = cruise; \
			cruiseSteps = absoluteOffset - accelerationSteps; \
			interStepChangeCounter = 0; \
			if((accelerationSteps & nextAABM) == 0) { \
				/*currentAAI--;*/ \
				nextAABM >>= 1; \
				/*stepTimeChange = currentAAD[currentAAI];*/ \
				/*stepTimeChangeSign = currentAADS[currentAAI];*/ \
				DEBUG_OUT(axis,4+32); \
				DEBUG_PRINT(currentAAI); \
				DEBUG_PRINT(','); \
				DEBUG_PRINT(nextAABM); \
				DEBUG_PRINT(','); \
			} \
		} else { \
			if (interStepChangeCounter > 0) { \
				interStepChangeCounter--; \
			} else { \
				if (stepTimeChangeSign) { \
					interStepChangeCounter = stepTimeChange - 1; \
					stepTime = max(minStepTime, stepTime - 1); \
				} else { \
					stepTime = max(minStepTime, stepTime - stepTimeChange); \
				} \
				OCR##timer##A = stepTime; \
			} \
		} \
		DEBUG_PRINTLN(stepTime); \
		DEBUG_OUT(axis,4+48); \
		break; \
	case cruise: \
		DEBUG_OUT(axis,5); \
		/* position += direction; */ \
		cruiseSteps--; \
		absoluteOffset--; \
		DEBUG_PRINT("c,"); \
		DEBUG_PRINT(absoluteOffset); \
		DEBUG_PRINT(','); \
		if (cruiseSteps == 0) \
			stepperState = deccelerate; \
		DEBUG_PRINTLN(stepTime); \
		break; \
	case deccelerate: \
		DEBUG_OUT(axis,6); \
		accelerationSteps--; \
		absoluteOffset--; \
		DEBUG_PRINT("d,"); \
		DEBUG_PRINT(accelerationSteps); \
		DEBUG_PRINT(','); \
		if (accelerationSteps > 0) { \
			if ((accelerationSteps & nextAABM) == 0 && currentAAI > 0) { \
				currentAAI--; \
				nextAABM >>= 1; \
				stepTimeChange = currentAAD[currentAAI]; \
				stepTimeChangeSign = currentAADS[currentAAI]; \
				DEBUG_OUT(axis,6+16); \
				DEBUG_PRINT(currentAAI); \
				DEBUG_PRINT(','); \
				DEBUG_PRINT(nextAABM); \
				DEBUG_PRINT(','); \
			} \
			if (interStepChangeCounter > 0) { \
				interStepChangeCounter--; \
			} else { \
				if (stepTimeChangeSign) { \
					interStepChangeCounter = stepTimeChange - 1; \
					stepTime = min(maxStepTime, stepTime + 1); \
				} else { \
					if(__builtin_add_overflow(stepTime,stepTimeChange,&stepTime)) \
						stepTime = maxStepTime; \
					else if(stepTime>maxStepTime) \
						stepTime = maxStepTime; \
					/*stepTime = min(maxStepTime, stepTime + stepTimeChange);*/ \
				} \
				OCR##timer##A = stepTime; \
			} \
		} else { \
			if (absoluteOffset > 0) { \
				OCR##timer##A = maxStepTime; \
				stepperState = cruiseLastSteps; \
			} else { \
				DEBUG_OUT(axis,7); \
				TCCR##timer##A = (1 << WGM11) | (1 << WGM10); /* disconnect OCnB nonInverted to stop PWM */ \
				OCR##timer##A = STEPPER_DIRECTION_SET_TO_STEP_PULSE_DELAY + STEPPER_ENABLE_TO_DIRECTION_SET_DELAY - 1; \
				stepperState = stop; \
			} \
		} \
		/* position += direction; */ \
		DEBUG_PRINTLN(stepTime); \
		DEBUG_OUT(axis,6+32); \
		break; \
	case cruiseLastSteps: \
		DEBUG_OUT(axis,7); \
		absoluteOffset--; \
		DEBUG_PRINT("l,"); \
		DEBUG_PRINT(absoluteOffset); \
		DEBUG_PRINT(','); \
		/* position += direction; */ \
		if (absoluteOffset == 0) { \
			TCCR##timer##A = (1 << WGM11) | (1 << WGM10); /* disconnect OCnB nonInverted to stop PWM */ \
			OCR##timer##A = STEPPER_DIRECTION_SET_TO_STEP_PULSE_DELAY + STEPPER_ENABLE_TO_DIRECTION_SET_DELAY - 1; \
			stepperState = stop; \
			DEBUG_OUT(axis,16+7); \
		} \
		DEBUG_PRINTLN(stepTime); \
		break; \
	case stop: \
		DEBUG_OUT(axis,8); \
		stepperState = disable; \
		break; \
	case disable: \
		DEBUG_OUT(axis,9); \
		enaPinPort &= ~(1<<enaPinBit); \
		TIMSK##timer = 0; /* deactivate overflow interrupt */ \
		TCCR##timer##B = 0;	/* deactivate timer */ \
		position += currentOffset - direction*absoluteOffset; \
		switch (stepperState) { \
		case accelerate: \
		case cruise: \
		case deccelerate: \
			position += direction; \
			break; \
		default: \
			break; \
		} \
		stepperState = disabled; \
		break; \
	default: \
		DEBUG_OUT(axis,10); \
		break; \
	} \
}

#define TIMER_ISR_IMPL(timer,i,dirPort,dirBit,enaPort,enaBit) \
	TIMER_ISR(timer,i,stepperStates[i],accelerationSteps[i],cruiseSteps[i],absoluteOffsets[i], \
		dirPort, dirBit, enaPort, enaBit, \
		interStepChangeCounter[i],halfwaySteps[i],oddSteps[i],nextAABM[i],currentAAI[i], \
		stepperProperties[i].accelerationApproximationData, \
		stepperProperties[i].accelerationApproximationDataSign, \
		stepTimeChange[i],stepTimeChangeSign[i],stepTime[i],stepperProperties[i].minStepPeriod, \
		stepperProperties[i].maxStepPeriod, stopped[i],stopSignals[i], \
		stepperProperties[i].position,currentDirections[i],currentOffsets[i])

const stepper_pins stepperPins[] = {
	{ 'X', { .numbered = { 12/*OC1B/PB6*/, 38/*PD7*/, 40/*PG1*/, A8/*PCINT16/PK0*/, A9/*PCINT17/PK1*/ } } },
	{ 'Y', { .numbered = { 2/*OC3B/PE4*/, 32/*PC5*/, 34/*PC3*/, A10/*PCINT18/PK2*/, A11/*PCINT19/PK3*/ } } },
	{ 'Z', { .numbered = { 7/*OC4B/PH4*/, 26/*PA4*/, 28/*PA6*/, A12/*PCINT20/PK4*/, A13/*PCINT21/PK5*/ } } },
	{ 'T', { .numbered = { 45/*OC5B/PL4*/, 44/*PL5*/, 46/*PL3*/, A14/*PCINT22/PK6*/, A15/*PCINT23/PK7*/ } } } };

TIMER_ISR_IMPL(1,0,PORTD,7,PORTG,1)
TIMER_ISR_IMPL(3,1,PORTC,5,PORTC,3)
TIMER_ISR_IMPL(4,2,PORTA,4,PORTA,6)
TIMER_ISR_IMPL(5,3,PORTL,5,PORTL,3)

#if DEBUG&4
#define LOGPRINT1(axis,name,value) \
	Serial.print(axis); \
	Serial.print(": "); \
	Serial.print(name); \
	Serial.print(": "); \
	Serial.print(value); \
	Serial.println()
#define LOGPRINT2(axis,i,name,value) \
	Serial.print(axis); \
	Serial.print(": "); \
	Serial.print(i); \
	Serial.print(": "); \
	Serial.print(name); \
	Serial.print(": "); \
	Serial.print(value); \
	Serial.println()
#else
#define LOGPRINT1(axis,name,value)
#define LOGPRINT2(axis,i,name,value)
#endif

void calculateAccelerationApproximationData(int axis) {
	if (axis < 0 || axis >= NUM_STEPPER_PORTS)
		return;
	stepperProperty *sp = &stepperProperties[axis];
	sp->maxStepPeriod = VELOCITY_TO_STEP_PERIOD_M1(sp->minVelocity);
	sp->minStepPeriod = VELOCITY_TO_STEP_PERIOD_M1(sp->maxVelocity);
	LOGPRINT1(axis,"minVelocity",sp->minVelocity);
	LOGPRINT1(axis,"maxVelocity",sp->maxVelocity);
	LOGPRINT1(axis,"maxStepPeriod",sp->maxStepPeriod);
	LOGPRINT1(axis,"minStepPeriod",sp->minStepPeriod);
	LOGPRINT1(axis,"acceleration",sp->acceleration);
	float inv_v0 = ((float) sp->maxStepPeriod) + 1;
	float inv_v0_prescaled = inv_v0 / PRESCALER_FREQUENCY;
	float twoAoverV0sq = (sp->acceleration * inv_v0_prescaled) * (inv_v0_prescaled * 2);
	LOGPRINT1(axis,"inv_v0",inv_v0);
	LOGPRINT1(axis,"twoAoverV0sq",twoAoverV0sq);
	LOGPRINT1(axis,"inv_v0_prescaled",inv_v0_prescaled);
	uint32_t x = 1, next_x;
	bool reciprocal = false;
	float y = inv_v0_prescaled, next_y;
	for (int i = 0; i < 16; i++) {
		next_x = 2 * x;
		next_y =
				reciprocal ?
						sqrt(1 + twoAoverV0sq * (next_x - 1))
								/ inv_v0_prescaled :
						inv_v0_prescaled
								/ sqrt(1 + twoAoverV0sq * (next_x - 1));
		float t =
				reciprocal ?
						(y * next_y) / (next_y - y) * x / PRESCALER_FREQUENCY :
						(y - next_y) / x * PRESCALER_FREQUENCY;
		int32_t it = round(t);
		if(!reciprocal){
			if(it >= 1) {
				sp->accelerationApproximationDataSign[i] = 0;
				sp->accelerationApproximationData[i] = min(it,UINT16_MAX);
			} else {
				reciprocal = true;
				sp->accelerationApproximationDataSign[i] = 1;
				y = 1 / y;
				next_y = sqrt(1 + twoAoverV0sq * (next_x - 1))
						/ inv_v0_prescaled;
				t = (y * next_y) / (next_y - y) * x / PRESCALER_FREQUENCY;
				it = round(t);
				sp->accelerationApproximationData[i] = min(it,UINT16_MAX);
			}
		} else {
			sp->accelerationApproximationDataSign[i] = 1;
			sp->accelerationApproximationData[i] = min(it,UINT16_MAX);
		}
		x = next_x;
		y = next_y;
		LOGPRINT2(axis,i,"x",x);
		LOGPRINT2(axis,i,"y",y);
		LOGPRINT2(axis,i,"next_x",next_x);
		LOGPRINT2(axis,i,"next_y",next_y);
		LOGPRINT2(axis,i,"t",t);
		LOGPRINT2(axis,i,"it",it);
		LOGPRINT2(axis,i,"aad",sp->accelerationApproximationData[i]);
		LOGPRINT2(axis,i,"aads",sp->accelerationApproximationDataSign[i]);
	}
}

void setup() {
	EIMSK = 0; // disable external interrupts
	TCCR1B = 0; // disable timer1 (X)
	TCCR3B = 0; // disable timer3 (Y)
	TCCR4B = 0; // disable timer4 (Z)
	TCCR5B = 0; // disable timer5 (T)
	Serial.begin(115200);
	for (int axis = 0; axis < NUM_STEPPER_PORTS; axis++) {
		stepperStates[axis] = disabled;
		for (int j = 0; j < STEPPER_PINS_PER_AXIS; j++)
			pinMode(stepperPins[axis].pins.numbered[j],
					j < STEPPER_OUT_PINS_PER_AXIS ? OUTPUT : INPUT);
		stepperProperties[axis].position = 0;
		stepperProperties[axis].minVelocity = MIN_VELOCITY;
		stepperProperties[axis].maxVelocity = MAX_VELOCITY;
		stepperProperties[axis].acceleration = STEPPER_ACCELERATION;
		calculateAccelerationApproximationData(axis);
	}
	STOPSIGNALS_DIRECTION_PORT = 0;
	PCICR = 1<<STOPSIGNALS_PCI_NUMBER;
	STOPSIGNALS_MASK_REG = 0xFF;
#if DEBUG&1
	DEBUG_DIRECTION_PORT = 0xFF;
	DEBUG_PORT = 0;
#endif
}

inline bool stepperRunning(int axis) {
	return stepperStates[axis] != disabled;
}

inline bool steppersRunning() {
	for(int axis=0;axis<NUM_STEPPER_PORTS;axis++)
		if(stepperRunning(axis))
			return true;
	return false;
}


int parseNumberFromDigit(char c) {
	if (c >= '0' && c <= '9')
		return c - '0';
	if (c >= 'A' && c <= 'Z')
		return c - 'A' + 10;
	if (c >= 'a' && c <= 'z')
		return c - 'a' + 10 + 'Z' - 'A';
	return -1;
}

int32_t parseValueFromString(char *str) {
	char *endPtr;
	int32_t val = strtol(str, &endPtr, 10);
	if (endPtr == str || *endPtr != 0) {
		Serial.print((int)(*endPtr));
		return INVALID_VALUE;
	} else
		return val;
}

void sendProperty(int axis, int property) {
	if (axis < 0 || axis >= NUM_STEPPER_PORTS)
		Serial.print("INVALID_AXIS");
	else
		switch (property) {
		case PROPERTY_POSITION:
			Serial.print(stepperProperties[axis].position);
			break;
		case PROPERTY_MIN_VELOCITY:
			Serial.print(stepperProperties[axis].minVelocity);
			break;
		case PROPERTY_MAX_VELOCITY:
			Serial.print(stepperProperties[axis].maxVelocity);
			break;
		case PROPERTY_ACCELERATION:
			Serial.print(stepperProperties[axis].acceleration);
			break;
		case PROPERTY_HOME_SEARCH_VELOCITY:
			Serial.print(stepperProperties[axis].homeSearchVelocity);
			break;
		case PROPERTY_HOME_APPROACH_VELOCITY:
			Serial.print(stepperProperties[axis].homeApproachVelocity);
			break;
		case PROPERTY_MAX_STEP_PERIOED:
			Serial.print(stepperProperties[axis].maxStepPeriod);
			break;
		case PROPERTY_MIN_STEP_PERIOED:
			Serial.print(stepperProperties[axis].minStepPeriod);
			break;
		case PROPERTY_AA_DATA:
			for(int i=0;i<16;i++) {
				if(i>0)
					Serial.print(',');
				if(stepperProperties[axis].accelerationApproximationDataSign[i])
					Serial.print('-');
				Serial.print(stepperProperties[axis].accelerationApproximationData[i]);
			}
			break;
		case PROPERTY_CURRENT_STEP_PERIOD:
			Serial.print(stepTime[axis]);
			break;
		case PROPERTY_CURRENT_STEP_PERIOD_CHANGE:
			if(stepTimeChangeSign[axis])
				Serial.print('-');
			Serial.print(stepTimeChange[axis]);
			break;
		default:
			Serial.print("INVALID_PROPERTY");
			return;
		}
	Serial.println();
}

inline void setVelocity(uint16_t *var, int32_t val) {
	if (val < MIN_VELOCITY) {
		*var = MIN_VELOCITY;
		Serial.print("MIN_VELOCITY_SET");
	} else if (val > MAX_VELOCITY) {
		*var = MAX_VELOCITY;
		Serial.print("MAX_VELOCITY_SET");
	} else
		*var = val;
}

void setProperty(int axis, int property, char *param) {
	if (axis < 0 || axis >= NUM_STEPPER_PORTS)
		Serial.print("INVALID_AXIS");
	else {
		int32_t val = parseValueFromString(param);
		if (val == INVALID_VALUE)
			Serial.print("INVALID_VALUE");
		else
			switch (property) {
			case PROPERTY_POSITION:
				stepperProperties[axis].position = val;
				break;
			case PROPERTY_MIN_VELOCITY:
				setVelocity(&stepperProperties[axis].minVelocity, val);
				calculateAccelerationApproximationData(axis);
				break;
			case PROPERTY_MAX_VELOCITY:
				setVelocity(&stepperProperties[axis].maxVelocity, val);
				calculateAccelerationApproximationData(axis);
				break;
			case PROPERTY_ACCELERATION:
				if (val >= MIN_ACCELERATION && val <= MAX_ACCELERATION) {
					stepperProperties[axis].acceleration = val;
					calculateAccelerationApproximationData(axis);
				} else
					Serial.print("INVALID_ACCELERATION");
				break;
			case PROPERTY_HOME_SEARCH_VELOCITY:
				setVelocity(&stepperProperties[axis].homeSearchVelocity, val);
				break;
			case PROPERTY_HOME_APPROACH_VELOCITY:
				setVelocity(&stepperProperties[axis].homeApproachVelocity, val);
				break;
			default:
				Serial.print("INVALID_PROPERTY");
				return;
			}
	}
	Serial.println();
}

void printStatus() {
	for(int axis=0;axis<NUM_STEPPER_PORTS;axis++) {
		char c = '?';
		switch (stepperStates[axis]) {
		case enable:
			c = 'e';
			break;
		case setDirection:
			c = 'E';
			break;
		case accelerate:
			c = 'A';
			break;
		case cruise:
			c = 'C';
			break;
		case deccelerate:
			c = 'D';
			break;
		case cruiseLastSteps:
			c = 'c';
			break;
		case stop:
			c = 's';
			break;
		case disable:
			c = 'd';
			break;
		case disabled:
			c = 'i';
			break;
		}
		Serial.print(c);
		Serial.print(currentDirections[axis] >= 0 ? '+' : '-');
		Serial.print(absoluteOffsets[axis]);
		if (stopSignals[axis]) {
			Serial.print('!');
			if (!stepperRunning(axis))
				stopSignals[axis] = 0;
		}
		if(axis!=NUM_STEPPER_PORTS-1)
			Serial.print(',');
	}
	Serial.println();
}

#define START_STEPPING_CMD(timer) { \
		TCCR##timer##B = 0;								/* stop timer */ \
		TCNT##timer = 0;								/* start counter at 0 */ \
		OCR##timer##A = STEPPER_PRESCALER_SYNC;			/* some counts for initialization in COMPB */ \
		OCR##timer##B = 0;								/* 0 to safely enable PWM after direction set*/ \
		TIMSK##timer = (1 << TOIE1);					/* generate interrupt for overflow */ \
		TCCR##timer##A = (1 << WGM11) | (1 << WGM10);	/* FAST_PWM mode */ \
		TCCR##timer##B = (1 << WGM13) | (1 << WGM12) | (1 << CS11);	/* enable timer with 1/8 prescaler => 0.5us per count */ \
		OCR##timer##A = STEPPER_ENABLE_TO_DIRECTION_SET_DELAY;	/* enable-to-direction-set-delay for next timer cycle*/ \
	}

void moveAxis(int axis, int32_t offset) {
	if (stepperRunning(axis)) {
		printStatus();
		return;
	}
	if(offset==0)
		return;
	currentOffsets[axis] = offset;
	currentDirections[axis] = offset > 0 ? 1 : -1;
	absoluteOffsets[axis]= abs(offset);
	halfwaySteps[axis] = absoluteOffsets[axis] >> 1;
	oddSteps[axis] = absoluteOffsets[axis] & 1;
	stepperStates[axis] = enable;
	stopStepping = 0;
	stopSignals[axis] = 0;
	DEBUG_OUT(axis,0);
	if(axis==0)
		START_STEPPING_CMD(1)
	else if(axis==1)
		START_STEPPING_CMD(3)
	else if(axis==2)
		START_STEPPING_CMD(4)
	else if(axis==3)
		START_STEPPING_CMD(5)
	else
		Serial.println("INVALID_AXIS");
// return asynchronously
}

bool joinStepper(int axis) {
	bool forcedStop = 0;
	while (stepperRunning(axis)) {
		int c = Serial.read();
		switch (c) {
		case '!':
			stopSignals[axis] = 1;
			forcedStop = 1;
			break;
		case 't':
			printStatus();
			break;
		default:
			break;
		}
	}
	return forcedStop;
}

void homeAxis(int axis, int direction) {
	if (stepperRunning(axis)) {
		printStatus();
		return;
	}
	uint32_t savedVelocity = stepperProperties[axis].maxVelocity;
	stepperProperties[axis].maxVelocity =
			stepperProperties[axis].homeSearchVelocity;
	calculateAccelerationApproximationData(axis);
	moveAxis(axis, direction < 0 ? -(1ll << 31) : (1ll << 31) - 1);
	if (joinStepper(axis)) {
		stepperProperties[axis].maxVelocity = savedVelocity;
		calculateAccelerationApproximationData(axis);
		Serial.println("HOMING_ABORTED1");
		return;
	}
	stepperProperties[axis].maxVelocity =
			stepperProperties[axis].homeApproachVelocity;
	calculateAccelerationApproximationData(axis);
	moveAxis(axis, direction < 0 ? HOME_SET_BACK_STEPS : -HOME_SET_BACK_STEPS);
	if (joinStepper(axis)) {
		stepperProperties[axis].maxVelocity = savedVelocity;
		calculateAccelerationApproximationData(axis);
		Serial.println("HOMING_ABORTED2");
		return;
	}
	moveAxis(axis, direction < 0 ? -HOME_SET_BACK_STEPS : HOME_SET_BACK_STEPS);
	if (!joinStepper(axis)) {
		if (stopStepping)
			stepperProperties[axis].position = 0;
		else
			Serial.println("HOMING_FAILED");
	} else
		Serial.println("HOMING_ABORTED3");
	stepperProperties[axis].maxVelocity = savedVelocity;
	calculateAccelerationApproximationData(axis);
}

void loop() {
	char serialInputBuffer[SERIAL_INPUT_BUFFER_SIZE+1];
	size_t n = Serial.readBytesUntil(CR, serialInputBuffer,	SERIAL_INPUT_BUFFER_SIZE);
	if (n >= SERIAL_INPUT_BUFFER_SIZE || n < 1) // discard on overflow or timeout
		return;
	if (n == 1) {
		switch (serialInputBuffer[0]) {
		case '!':
			stopStepping = 1;
			break;
		case 't':
			printStatus();
			break;
		case 'R':
			setup();
			break;
		default:
			Serial.println("UNKNOWN_CMD");
			break;
		}
	} else {
		serialInputBuffer[n] = 0;
		int axis = parseNumberFromDigit(serialInputBuffer[1]);
		if (axis < 0 || axis >= NUM_STEPPER_PORTS)
			Serial.println("INVALID_AXIS");
		else
			switch (serialInputBuffer[0]) {
			case 'g':
				if (n == 3)
					sendProperty(axis,
							parseNumberFromDigit(serialInputBuffer[2]));
				else
					Serial.println("PARAM_EXPECTED");
				break;
			case 's':
				if (n > 3)
					setProperty(axis,
							parseNumberFromDigit(serialInputBuffer[2]),
							serialInputBuffer + 3);
				else if (n == 3)
					Serial.println("VALUE_EXPECTED");
				else
					Serial.println("PARAM_EXPECTED");
				break;
			case 'p':
				sendProperty(axis, 0);
				break;
			case 'a':
				if (n >= 3) {
					int32_t newPos = parseValueFromString(serialInputBuffer + 2);
					if (newPos == INVALID_VALUE)
						Serial.print("INVALID_VALUE");
					else
						moveAxis(axis,
								newPos - stepperProperties[axis].position);
				} else
					Serial.println("VALUE_EXPECTED");
				break;
			case 'r':
				if (n >= 3) {
					int32_t offset = parseValueFromString(serialInputBuffer + 2);
					if (offset == INVALID_VALUE)
						Serial.print("INVALID_VALUE");
					else
						moveAxis(axis, offset);
				} else
					Serial.println("VALUE_EXPECTED");
				break;
			case 'h':
				homeAxis(axis,
						n >= 3 ? serialInputBuffer[2] == '-' ? -1 : 1:-1);
				break;
			default:
				Serial.println("UNKNOWN_CMD");
				break;
			}
	}
}
