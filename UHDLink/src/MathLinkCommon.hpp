#ifndef MATHLINKCOMMON_H

#define MATHLINKCOMMON_H

#include <mathlink.h>

void abortWithMsg(MLINK ml, const char *msgId);
void failWithMsg(MLINK ml, const char *msg);
void failWithClearAndMsg(MLINK ml, const char *msg);
void failWithMsgAndError(MLINK ml, const char *msg);

#endif
