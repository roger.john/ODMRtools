#include <stddef.h>
#include <stdint.h>

class UHDtxBuffer {
public:
	void **buffers;
	int numBuffers;
	size_t numSamplesPerBuffer;
	bool freeAfterUse;
	UHDtxBuffer(void);
	~UHDtxBuffer(void);
};

typedef struct __attribute__((__packed__)) UHDtimeSpec {
	int32_t fullSeconds;
	int32_t fullSeconds_pad;
	double fractionalSeconds;
} UHDtimeSpec_t;

const UHDtimeSpec_t ZERO_TIME_SPEC = { fullSeconds:0, fullSeconds_pad:0,
		fractionalSeconds:0. };

class UHDtxBufferChainEntry {
public:
	UHDtxBuffer *txBuffer;
	UHDtxBufferChainEntry(void);
	UHDtxBufferChainEntry(UHDtxBuffer *txBuffer);
	virtual ~UHDtxBufferChainEntry(void);
	virtual UHDtxBufferChainEntry *getNext(void) = 0;
	virtual void setNext(UHDtxBufferChainEntry *next) = 0;
	virtual bool hasNext(void) = 0;
	virtual void deleteNext(void) = 0;
	virtual bool mayDeleteAfterUse(void) {
		return true;
	}
	virtual bool hasTimeSpec(void) {
		return false;
	}
	virtual UHDtimeSpec_t getTimeSpec(void) {
		return ZERO_TIME_SPEC;
	}
	virtual uint64_t getEntryType(void) = 0;
};

class UHDtxBufferListEntry: public UHDtxBufferChainEntry {
public:
	static const uint64_t ENTRY_TYPE = 1;
	UHDtxBufferChainEntry *next;
	bool _hasTimeSpec;
	UHDtimeSpec_t timeSpec;
	uint64_t numRepetitions, repetitionCounter;
	UHDtxBufferListEntry(UHDtxBuffer *txBuffer);
	UHDtxBufferListEntry(UHDtxBuffer *txBuffer, uint64_t numRepetitions);
	UHDtxBufferListEntry(UHDtxBuffer *txBuffer, uint64_t numRepetitions,
			UHDtimeSpec_t timeSpec);
	virtual ~UHDtxBufferListEntry(void);
	virtual UHDtxBufferChainEntry *getNext(void);
	virtual void setNext(UHDtxBufferChainEntry *next);
	virtual bool hasNext(void);
	virtual void deleteNext(void);
	virtual bool mayDeleteAfterUse(void);
	virtual bool hasTimeSpec(void) {
		return _hasTimeSpec;
	}
	virtual UHDtimeSpec_t getTimeSpec(void) {
		return timeSpec;
	}
	virtual uint64_t getEntryType(void) {
		return ENTRY_TYPE;
	}
};
