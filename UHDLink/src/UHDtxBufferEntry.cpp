#include "UHDtxBufferEntry.hpp"

#include <stdlib.h>


UHDtxBuffer::UHDtxBuffer(void) :
		buffers(NULL), numBuffers(0), numSamplesPerBuffer(0), freeAfterUse(true) {
}
UHDtxBuffer::~UHDtxBuffer(void) {
	if (buffers) {
		for (int i = 0; i < numBuffers; i++)
			free(buffers[i]);
		free(buffers);
	}
	buffers = NULL;
}

UHDtxBufferChainEntry::UHDtxBufferChainEntry(void) :
		txBuffer(NULL) {
}
UHDtxBufferChainEntry::UHDtxBufferChainEntry(UHDtxBuffer *txBuffer) :
		txBuffer(txBuffer) {
}
UHDtxBufferChainEntry::~UHDtxBufferChainEntry(void) {
	if (txBuffer && txBuffer->freeAfterUse)
		delete txBuffer;
	txBuffer = NULL;
}
UHDtxBufferListEntry::UHDtxBufferListEntry(UHDtxBuffer *txBuffer) :
		UHDtxBufferChainEntry(txBuffer), next(NULL), numRepetitions(0), repetitionCounter(
				0), _hasTimeSpec(false) {
}
UHDtxBufferListEntry::UHDtxBufferListEntry(UHDtxBuffer *txBuffer,
		uint64_t numRepetitions) :
		UHDtxBufferChainEntry(txBuffer), next(NULL), numRepetitions(
				numRepetitions), repetitionCounter(0), _hasTimeSpec(false) {
}
UHDtxBufferListEntry::UHDtxBufferListEntry(UHDtxBuffer *txBuffer,
		uint64_t numRepetitions, UHDtimeSpec_t timeSpec) :
		UHDtxBufferChainEntry(txBuffer), next(NULL), numRepetitions(
				numRepetitions), repetitionCounter(0), _hasTimeSpec(true), timeSpec(
				timeSpec) {
}
UHDtxBufferListEntry::~UHDtxBufferListEntry(void) {
	next = NULL;
}
void UHDtxBufferListEntry::deleteNext(void) {
	if (next) {
		UHDtxBufferChainEntry *oldNext = next; // to break graph circles
		next = NULL;
		oldNext->deleteNext();
		delete oldNext;
	}
}
UHDtxBufferChainEntry* UHDtxBufferListEntry::getNext(void) {
	repetitionCounter++;
	if (repetitionCounter <= numRepetitions) {
		return this;
	} else
		return next;
}
void UHDtxBufferListEntry::setNext(UHDtxBufferChainEntry *next) {
	this->next = next;
}
bool UHDtxBufferListEntry::hasNext(void) {
	return repetitionCounter < numRepetitions || next != NULL;
}
bool UHDtxBufferListEntry::mayDeleteAfterUse(void) {
	return numRepetitions <= repetitionCounter;
}
