void UHDfind(const char*);

:Begin:
:Function:		UHDfind
:Pattern:		UHDfind[args_String]
:Arguments:		{ args }
:ArgumentTypes:	{ String }
:ReturnType:	Manual
:End:

void UHDopen(const char*);

:Begin:
:Function:		UHDopen
:Pattern:		UHDopen[deviceArgs_String]
:Arguments:		{ deviceArgs }
:ArgumentTypes:	{ String }
:ReturnType:	Manual
:End:

void UHDclose(mlint64);

:Begin:
:Function:		UHDclose
:Pattern:		UHDclose[usrp_Integer]
:Arguments:		{ usrp }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDgetPPstring(mlint64);

:Begin:
:Function:		UHDgetPPstring
:Pattern:		UHDgetPPstring[usrp_Integer]
:Arguments:		{ usrp }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDgetNumMboards(mlint64);

:Begin:
:Function:		UHDgetNumMboards
:Pattern:		UHDgetNumMboards[usrp_Integer]
:Arguments:		{ usrp }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDgetMboardName(mlint64, int);

:Begin:
:Function:		UHDgetMboardName
:Pattern:		UHDgetMboardName[usrp_Integer, mBoard_Integer:0]
:Arguments:		{ usrp, mBoard }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDenumerateRegisters(mlint64, int);

:Begin:
:Function:		UHDenumerateRegisters
:Pattern:		UHDenumerateRegisters[usrp_Integer, mBoard_Integer:0]
:Arguments:		{ usrp, mBoard }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDgetRegisterInfo(mlint64, int, const char*);

:Begin:
:Function:		UHDgetRegisterInfo
:Pattern:		UHDgetRegisterInfo[usrp_Integer, mBoard_Integer:0, registerPath_String]
:Arguments:		{ usrp, mBoard, registerPath }
:ArgumentTypes:	{ Integer64, Integer, String }
:ReturnType:	Manual
:End:

void UHDreadRegister(mlint64, int, const char*, int);

:Begin:
:Function:		UHDreadRegister
:Pattern:		UHDreadRegister[usrp_Integer, mBoard_Integer:0, registerPath_String, field_Integer]
:Arguments:		{ usrp, mBoard, registerPath, field }
:ArgumentTypes:	{ Integer64, Integer, String, Integer }
:ReturnType:	Manual
:End:

void UHDwriteRegister(mlint64, int, const char*, int, mlint64);

:Begin:
:Function:		UHDwriteRegister
:Pattern:		UHDwriteRegister[usrp_Integer, mBoard_Integer:0, registerPath_String,
					field_Integer, value_Integer]
:Arguments:		{ usrp, mBoard, registerPath, field, value }
:ArgumentTypes:	{ Integer64, Integer, String, Integer, Integer64 }
:ReturnType:	Manual
:End:

void UHDsetUserRegister(mlint64, int, int, int);

:Begin:
:Function:		UHDsetUserRegister
:Pattern:		UHDsetUserRegister[usrp_Integer, channel_Integer:0, addr_Integer, value_Integer]
:Arguments:		{ usrp, channel, addr, value }
:ArgumentTypes:	{ Integer64, Integer, Integer, Integer }
:ReturnType:	Manual
:End:

void UHDgetUserRegister(mlint64, int, int);

:Begin:
:Function:		UHDgetUserRegister
:Pattern:		UHDgetUserRegister[usrp_Integer, channel_Integer:0, addr_Integer]
:Arguments:		{ usrp, channel, addr }
:ArgumentTypes:	{ Integer64, Integer, Integer }
:ReturnType:	Manual
:End:

void UHDgetGPIObanks(mlint64, int);

:Begin:
:Function:		UHDgetGPIObanks
:Pattern:		UHDgetGPIObanks[usrp_Integer, mBoard_Integer:0]
:Arguments:		{ usrp, mBoard }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDgetGPIOattr(mlint64, int, const char*, const char*);

:Begin:
:Function:		UHDgetGPIOattr
:Pattern:		UHDgetGPIOattr[usrp_Integer, mBoard_Integer:0, bank_String,
					attr_String]
:Arguments:		{ usrp, mBoard, bank, attr }
:ArgumentTypes:	{ Integer64, Integer, String, String }
:ReturnType:	Manual
:End:

void UHDsetGPIOattr(mlint64, int, const char*, const char*, mlint64, mlint64);

:Begin:
:Function:		UHDsetGPIOattr
:Pattern:		UHDsetGPIOattr[usrp_Integer, mBoard_Integer:0, bank_String,
					attr_String, value_Integer, mask_Integer:16^^FFFFFFFF]
:Arguments:		{ usrp, mBoard, bank, attr, value, mask }
:ArgumentTypes:	{ Integer64, Integer, String, String, Integer64, Integer64 }
:ReturnType:	Manual
:End:

void UHDgetMboardSensorNames(mlint64, int);

:Begin:
:Function:		UHDgetMboardSensorNames
:Pattern:		UHDgetMboardSensorNames[usrp_Integer, mBoard_Integer:0]
:Arguments:		{ usrp, mBoard }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDgetMboardSensor(mlint64, int, const char*);

:Begin:
:Function:		UHDgetMboardSensor
:Pattern:		UHDgetMboardSensor[usrp_Integer, mBoard_Integer:0, sensorName_String]
:Arguments:		{ usrp, mBoard, sensorName }
:ArgumentTypes:	{ Integer64, Integer, String }
:ReturnType:	Manual
:End:


void UHDgetClockSources(mlint64, int);

:Begin:
:Function:		UHDgetClockSources
:Pattern:		UHDgetClockSources[usrp_Integer, mBoard_Integer:0]
:Arguments:		{ usrp, mBoard }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDgetClockSource(mlint64, int);

:Begin:
:Function:		UHDgetClockSource
:Pattern:		UHDgetClockSource[usrp_Integer, mBoard_Integer:0]
:Arguments:		{ usrp, mBoard }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDsetClockSource(mlint64, int, const char*);

:Begin:
:Function:		UHDsetClockSource
:Pattern:		UHDsetClockSource[usrp_Integer, mBoard_Integer:0, clockSource_String]
:Arguments:		{ usrp, mBoard, clockSource }
:ArgumentTypes:	{ Integer64, Integer, String }
:ReturnType:	Manual
:End:

void UHDsetClockSourceOut(mlint64, int, const char *);

:Begin:
:Function:		UHDsetClockSourceOut
:Pattern:		UHDsetClockSourceOut[usrp_Integer, mBoard_Integer:0, enabled:(True|False)]
:Arguments:		{ usrp, mBoard, enabled }
:ArgumentTypes:	{ Integer64, Integer, Symbol }
:ReturnType:	Manual
:End:

void UHDgetMasterClockRate(mlint64, int);

:Begin:
:Function:		UHDgetMasterClockRate
:Pattern:		UHDgetMasterClockRate[usrp_Integer, channel_Integer:0]
:Arguments:		{ usrp, channel }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDsetMasterClockRate(mlint64, int, double);

:Begin:
:Function:		UHDsetMasterClockRate
:Pattern:		UHDsetMasterClockRate[usrp_Integer, channel_Integer:0, rate_Real]
:Arguments:		{ usrp, channel, rate }
:ArgumentTypes:	{ Integer64, Integer, Real64 }
:ReturnType:	Manual
:End:

void UHDgetTimeSources(mlint64, int);

:Begin:
:Function:		UHDgetTimeSources
:Pattern:		UHDgetTimeSources[usrp_Integer, mBoard_Integer:0]
:Arguments:		{ usrp, mBoard }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDgetTimeSource(mlint64, int);

:Begin:
:Function:		UHDgetTimeSource
:Pattern:		UHDgetTimeSource[usrp_Integer, mBoard_Integer:0]
:Arguments:		{ usrp, mBoard }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDsetTimeSource(mlint64, int, const char*);

:Begin:
:Function:		UHDsetTimeSource
:Pattern:		UHDsetTimeSource[usrp_Integer, mBoard_Integer:0, timeSource_String]
:Arguments:		{ usrp, mBoard, timeSource }
:ArgumentTypes:	{ Integer64, Integer, String }
:ReturnType:	Manual
:End:

void UHDgetTimeNow(mlint64, int);

:Begin:
:Function:		UHDgetTimeNow
:Pattern:		UHDgetTimeNow[usrp_Integer, mBoard_Integer:0]
:Arguments:		{ usrp, mBoard }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDsetTimeNow(mlint64, int, mlint64, double);

:Begin:
:Function:		UHDsetTimeNow
:Pattern:		UHDsetTimeNow[usrp_Integer, mBoard_Integer:0,
					seconds_Integer, fractionalSeconds_Real]
:Arguments:		{ usrp, mBoard, seconds, fractionalSeconds }
:ArgumentTypes:	{ Integer64, Integer, Integer64, Real64 }
:ReturnType:	Manual
:End:

void UHDgetTimeLastPPS(mlint64, int);

:Begin:
:Function:		UHDgetTimeLastPPS
:Pattern:		UHDgetTimeLastPPS[usrp_Integer, mBoard_Integer:0]
:Arguments:		{ usrp, mBoard }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDsetTimeNextPPS(mlint64, int, mlint64, double);

:Begin:
:Function:		UHDsetTimeNextPPS
:Pattern:		UHDsetTimeNextPPS[usrp_Integer, mBoard_Integer:0,
					seconds_Integer, fractionalSeconds_Real]
:Arguments:		{ usrp, mBoard, seconds, fractionalSeconds }
:ArgumentTypes:	{ Integer64, Integer, Integer64, Real64 }
:ReturnType:	Manual
:End:

void UHDsetTimeUnknownPPS(mlint64, mlint64, double);

:Begin:
:Function:		UHDsetTimeUnknownPPS
:Pattern:		UHDsetTimeUnknownPPS[usrp_Integer, seconds_Integer,
					fractionalSeconds_Real]
:Arguments:		{ usrp, seconds, fractionalSeconds }
:ArgumentTypes:	{ Integer64, Integer64, Real64 }
:ReturnType:	Manual
:End:

void UHDgetTimeSynchronized(mlint64);

:Begin:
:Function:		UHDgetTimeSynchronized
:Pattern:		UHDgetTimeSynchronized[usrp_Integer]
:Arguments:		{ usrp }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDsetCommandTime(mlint64, int, mlint64, double);

:Begin:
:Function:		UHDsetCommandTime
:Pattern:		UHDsetCommandTime[usrp_Integer, mBoard_Integer:0,
					seconds_Integer, fractionalSeconds_Real]
:Arguments:		{ usrp, mBoard, seconds, fractionalSeconds }
:ArgumentTypes:	{ Integer64, Integer, Integer64, Real64 }
:ReturnType:	Manual
:End:

void UHDclearCommandTime(mlint64, int);

:Begin:
:Function:		UHDclearCommandTime
:Pattern:		UHDclearCommandTime[usrp_Integer, mBoard_Integer:0]
:Arguments:		{ usrp, mBoard }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:


void UHDgetTxNumChannels(mlint64);

:Begin:
:Function:		UHDgetTxNumChannels
:Pattern:		UHDgetTxNumChannels[usrp_Integer]
:Arguments:		{ usrp }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDgetTxInfo(mlint64, int);

:Begin:
:Function:		UHDgetTxInfo
:Pattern:		UHDgetTxInfo[usrp_Integer, channel_Integer:0]
:Arguments:		{ usrp, channel }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDgetTxSubdevName(mlint64, int);

:Begin:
:Function:		UHDgetTxSubdevName
:Pattern:		UHDgetTxSubdevName[usrp_Integer, channel_Integer:0]
:Arguments:		{ usrp, channel }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDgetTxSubdevSpec(mlint64, int);

:Begin:
:Function:		UHDgetTxSubdevSpec
:Pattern:		UHDgetTxSubdevSpec[usrp_Integer, mBoard_Integer:0]
:Arguments:		{ usrp, mBoard }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:


void UHDgetTxSensorNames(mlint64, int);

:Begin:
:Function:		UHDgetTxSensorNames
:Pattern:		UHDgetTxSensorNames[usrp_Integer, channel_Integer:0]
:Arguments:		{ usrp, channel }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDgetTxSensor(mlint64, int, const char*);

:Begin:
:Function:		UHDgetTxSensor
:Pattern:		UHDgetTxSensor[usrp_Integer, channel_Integer:0, sensorName_String]
:Arguments:		{ usrp, channel, sensorName }
:ArgumentTypes:	{ Integer64, Integer, String }
:ReturnType:	Manual
:End:

void UHDgetTxRates(mlint64, int);

:Begin:
:Function:		UHDgetTxRates
:Pattern:		UHDgetTxRates[usrp_Integer, channel_Integer:0]
:Arguments:		{ usrp, channel }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDgetTxRate(mlint64, int);

:Begin:
:Function:		UHDgetTxRate
:Pattern:		UHDgetTxRate[usrp_Integer, channel_Integer:0]
:Arguments:		{ usrp, channel }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDsetTxRate(mlint64, int, double);

:Begin:
:Function:		UHDsetTxRate
:Pattern:		UHDsetTxRate[usrp_Integer, channel_Integer:0, rate_Real]
:Arguments:		{ usrp, channel, rate }
:ArgumentTypes:	{ Integer64, Integer, Real64 }
:ReturnType:	Manual
:End:

void UHDgetTxAntennas(mlint64, int);

:Begin:
:Function:		UHDgetTxAntennas
:Pattern:		UHDgetTxAntennas[usrp_Integer, channel_Integer:0]
:Arguments:		{ usrp, channel }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDgetTxAntenna(mlint64, int);

:Begin:
:Function:		UHDgetTxAntenna
:Pattern:		UHDgetTxAntenna[usrp_Integer, channel_Integer:0]
:Arguments:		{ usrp, channel }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDsetTxAntenna(mlint64, int, const char *);

:Begin:
:Function:		UHDsetTxAntenna
:Pattern:		UHDsetTxAntenna[usrp_Integer, channel_Integer:0, antenna_String]
:Arguments:		{ usrp, channel, antenna }
:ArgumentTypes:	{ Integer64, Integer, String }
:ReturnType:	Manual
:End:

void UHDgetTxGainNames(mlint64, int);

:Begin:
:Function:		UHDgetTxGainNames
:Pattern:		UHDgetTxGainNames[usrp_Integer, channel_Integer:0]
:Arguments:		{ usrp, channel }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDgetTxGainRange(mlint64, int, const char *);

:Begin:
:Function:		UHDgetTxGainRange
:Pattern:		UHDgetTxGainRange[usrp_Integer, channel_Integer:0, gainName_String:""]
:Arguments:		{ usrp, channel, gainName }
:ArgumentTypes:	{ Integer64, Integer, String }
:ReturnType:	Manual
:End:

void UHDgetTxGain(mlint64, int, const char *);

:Begin:
:Function:		UHDgetTxGain
:Pattern:		UHDgetTxGain[usrp_Integer, channel_Integer:0, gainName_String:""]
:Arguments:		{ usrp, channel, gainName }
:ArgumentTypes:	{ Integer64, Integer, String }
:ReturnType:	Manual
:End:

void UHDsetTxGain(mlint64, int, const char *, double);

:Begin:
:Function:		UHDsetTxGain
:Pattern:		UHDsetTxGain[usrp_Integer, channel_Integer:0, gainName_String:"", gain_Real]
:Arguments:		{ usrp, channel, gainName, gain }
:ArgumentTypes:	{ Integer64, Integer, String, Real64 }
:ReturnType:	Manual
:End:

void UHDgetNormalizedTxGain(mlint64, int);

:Begin:
:Function:		UHDgetNormalizedTxGain
:Pattern:		UHDgetNormalizedTxGain[usrp_Integer, channel_Integer:0]
:Arguments:		{ usrp, channel }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDsetNormalizedTxGain(mlint64, int, double);

:Begin:
:Function:		UHDsetNormalizedTxGain
:Pattern:		UHDsetNormalizedTxGain[usrp_Integer, channel_Integer:0, gain_Real]
:Arguments:		{ usrp, channel, gain }
:ArgumentTypes:	{ Integer64, Integer, Real64 }
:ReturnType:	Manual
:End:

void UHDgetTxBandwidthRange(mlint64, int);

:Begin:
:Function:		UHDgetTxBandwidthRange
:Pattern:		UHDgetTxBandwidthRange[usrp_Integer, channel_Integer:0]
:Arguments:		{ usrp, channel }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDgetTxBandwidth(mlint64, int);

:Begin:
:Function:		UHDgetTxBandwidth
:Pattern:		UHDgetTxBandwidth[usrp_Integer, channel_Integer:0]
:Arguments:		{ usrp, channel }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDsetTxBandwidth(mlint64, int, double);

:Begin:
:Function:		UHDsetTxBandwidth
:Pattern:		UHDsetTxBandwidth[usrp_Integer, channel_Integer:0, bandwidth_Real]
:Arguments:		{ usrp, channel, bandwidth }
:ArgumentTypes:	{ Integer64, Integer, Real64 }
:ReturnType:	Manual
:End:

void UHDgetTxFrequencyRange(mlint64, int);

:Begin:
:Function:		UHDgetTxFrequencyRange
:Pattern:		UHDgetTxFrequencyRange[usrp_Integer, channel_Integer:0]
:Arguments:		{ usrp, channel }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDgetTxFrequency(mlint64, int);

:Begin:
:Function:		UHDgetTxFrequency
:Pattern:		UHDgetTxFrequency[usrp_Integer, channel_Integer:0]
:Arguments:		{ usrp, channel }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDsetTxFrequency(mlint64, int, double);

:Begin:
:Function:		UHDsetTxFrequency
:Pattern:		UHDsetTxFrequency[usrp_Integer, channel_Integer:0, targetFrequency_Real]
:Arguments:		{ usrp, channel, targetFrequency }
:ArgumentTypes:	{ Integer64, Integer, Real64 }
:ReturnType:	Manual
:End:

void UHDsetTxFrequencyEx(mlint64, int, double, double, const char*, double, const char *);

:Begin:
:Function:		UHDsetTxFrequencyEx
:Pattern:		UHDsetTxFrequencyEx[usrp_Integer, channel_Integer:0, targetFrequency_Real,
					rfFrequency_Real:0, rfFrequencyPolicy_String:"auto",
					dspFrequency_Real:0, dspFrequencyPolicy_String:"auto"]
:Arguments:		{ usrp, channel, targetFrequency, rfFrequency, rfFrequencyPolicy,
					dspFrequency, dspFrequencyPolicy }
:ArgumentTypes:	{ Integer64, Integer, Real64, Real64, String, Real64, String }
:ReturnType:	Manual
:End:

void UHDgetFEtxFrequencyRange(mlint64, int);

:Begin:
:Function:		UHDgetFEtxFrequencyRange
:Pattern:		UHDgetFEtxFrequencyRange[usrp_Integer, channel_Integer:0]
:Arguments:		{ usrp, channel }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDsetTxIdle(mlint64, int, int, int);

:Begin:
:Function:		UHDsetTxIdle
:Pattern:		UHDsetTxIdle[usrp_Integer, channel_Integer:0, {txi_Integer, txq_Integer}]
:Arguments:		{ usrp, channel, txi, txq}
:ArgumentTypes:	{ Integer64, Integer, Integer, Integer }
:ReturnType:	Manual
:End:

void UHDgetTxIdle(mlint64, int);

:Begin:
:Function:		UHDgetTxIdle
:Pattern:		UHDgetTxIdle[usrp_Integer, channel_Integer:0]
:Arguments:		{ usrp, channel}
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void UHDstartTxStreamer(mlint64, double, const char*,const char*,const char*,
	int *,long,const char*);

:Begin:
:Function:		UHDstartTxStreamer
:Pattern:		UHDstartTxStreamer[usrp_Integer, timeout_Real, args_String,
					cpuFormat_String, otwFormat_String, channels:{__Integer}:{0},
					repeatLastBuffer:(True|False):False]
:Arguments:		{ usrp, timeout, args, cpuFormat, otwFormat, channels, repeatLastBuffer }
:ArgumentTypes:	{ Integer64, Real64, String, String, String, IntegerList, Symbol }
:ReturnType:	Manual
:End:

void UHDstopTxStreamer(mlint64);
 
:Begin:
:Function:		UHDstopTxStreamer
:Pattern:		UHDstopTxStreamer[streamerRef_Integer]
:Arguments:		{ streamerRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDtxStreamerRunning(mlint64);
 
:Begin:
:Function:		UHDtxStreamerRunning
:Pattern:		UHDtxStreamerRunning[streamerRef_Integer]
:Arguments:		{ streamerRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDgetTxStreamerTimeout(mlint64);
 
:Begin:
:Function:		UHDgetTxStreamerTimeout
:Pattern:		UHDgetTxStreamerTimeout[streamerRef_Integer]
:Arguments:		{ streamerRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDsetTxStreamerTimeout(mlint64, double);
 
:Begin:
:Function:		UHDsetTxStreamerTimeout
:Pattern:		UHDsetTxStreamerTimeout[streamerRef_Integer, timeout_Real]
:Arguments:		{ streamerRef, timeout }
:ArgumentTypes:	{ Integer64, Real64 }
:ReturnType:	Manual
:End:

void UHDgetTxStreamerRepeatLastBuffer(mlint64);
 
:Begin:
:Function:		UHDgetTxStreamerRepeatLastBuffer
:Pattern:		UHDgetTxStreamerRepeatLastBuffer[streamerRef_Integer]
:Arguments:		{ streamerRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDsetTxStreamerRepeatLastBuffer(mlint64, const char*);
 
:Begin:
:Function:		UHDsetTxStreamerRepeatLastBuffer
:Pattern:		UHDsetTxStreamerRepeatLastBuffer[streamerRef_Integer, repeatLastBuffer:(True|False)]
:Arguments:		{ streamerRef, repeatLastBuffer }
:ArgumentTypes:	{ Integer64, Symbol }
:ReturnType:	Manual
:End:

void UHDcreateIQbuffer(void);

:Begin:
:Function:		UHDcreateIQbuffer
:Pattern:		UHDcreateIQbuffer[iqBuffer:{__Integer}?(EvenQ[Length[#]]&)|UHDiqBuffer[_Integer,_Integer]]
:Arguments:		{ iqBuffer }
:ArgumentTypes:	{ Manual }
:ReturnType:	Manual
:End:

void UHDgetIQbuffer(void);

:Begin:
:Function:		UHDgetIQbuffer
:Pattern:		UHDgetIQbuffer[iqBufferRef:UHDiqBuffer[_Integer,_Integer]]
:Arguments:		{ iqBufferRef }
:ArgumentTypes:	{ Manual }
:ReturnType:	Manual
:End:

void UHDdestroyIQbuffer(void);

:Begin:
:Function:		UHDdestroyIQbuffer
:Pattern:		UHDdestroyIQbuffer[iqBufferRef:_Integer|UHDiqBuffer[_Integer,_Integer]]
:Arguments:		{ iqBufferRef }
:ArgumentTypes:	{ Manual }
:ReturnType:	Manual
:End:

mlint64 UHDcreateTxStreamerBuffer(void);

:Begin:
:Function:		UHDcreateTxStreamerBuffer
:Pattern:		UHDcreateTxStreamerBuffer[
					data:{({__Integer}?(EvenQ[Length[#]]&)|UHDiqBuffer[_Integer,_Integer])..}?
						(Equal@@(If[ListQ[#],Length[#]/2,#[[2]]]&/@#)&)]
:Arguments:		{ data }
:ArgumentTypes:	{ Manual }
:ReturnType:	Integer64
:End:

void UHDgetTxStreamerBuffer(mlint64);

:Begin:
:Function:		UHDgetTxStreamerBuffer
:Pattern:		UHDgetTxStreamerBuffer[txBuffersRef_Integer]
:Arguments:		{ txBuffersRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDsetTxStreamerBufferFreeAfterUse(mlint64,const char *);

:Begin:
:Function:		UHDsetTxStreamerBufferFreeAfterUse
:Pattern:		UHDsetTxStreamerBufferFreeAfterUse[txBuffersRef_Integer, freeAfterUse:(True|False|Null)]
:Arguments:		{ txBuffersRef, freeAfterUse }
:ArgumentTypes:	{ Integer64, Symbol }
:ReturnType:	Manual
:End:

void UHDsetTxStreamerBuffer(mlint64,const char *);

:Begin:
:Function:		UHDsetTxStreamerBuffer
:Pattern:		UHDsetTxStreamerBuffer[txBuffersRef_Integer, freeAfterUse:(True|False|Null),
										data:{({__Integer}?(EvenQ[Length[#]]&)|_Integer)..}?
											(Length[#]<2||MatrixQ[#]&@Cases[#,_List]&)]
:Arguments:		{ txBuffersRef, freeAfterUse, data }
:ArgumentTypes:	{ Integer64, Symbol, Manual }
:ReturnType:	Manual
:End:

void UHDdestroyTxStreamerBuffer(mlint64);

:Begin:
:Function:		UHDdestroyTxStreamerBuffer
:Pattern:		UHDdestroyTxStreamerBuffer[bufferRef_Integer]
:Arguments:		{ bufferRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDdestroyTxStreamerBufferButKeepIQbuffers(mlint64);

:Begin:
:Function:		UHDdestroyTxStreamerBufferButKeepIQbuffers
:Pattern:		UHDdestroyTxStreamerBufferButKeepIQbuffers[bufferRef_Integer]
:Arguments:		{ bufferRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDqueueTxStreamerBuffer(mlint64);

:Begin:
:Function:		UHDqueueTxStreamerBuffer
:Pattern:		UHDqueueTxStreamerBuffer[streamerRef_Integer,
					data:({({__Integer}?(EvenQ[Length[#]]&)|UHDiqBuffer[_Integer,_Integer])..}?
						(Equal@@(If[ListQ[#],Length[#],#[[2]]]&/@#)&)|_Integer),
					numRepetitions_Integer:0,timeSpec:{_Integer,_Real}|Null:Null]
:Arguments:		{ streamerRef, data, numRepetitions, timeSpec }
:ArgumentTypes:	{ Integer64, Manual }
:ReturnType:	Manual
:End:

void UHDclearTxStreamerQueue(mlint64);

:Begin:
:Function:		UHDclearTxStreamerQueue
:Pattern:		UHDclearTxStreamerQueue[streamerRef_Integer]
:Arguments:		{ streamerRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDgetTxStreamerQueue(mlint64);

:Begin:
:Function:		UHDgetTxStreamerQueue
:Pattern:		UHDgetTxStreamerQueue[streamerRef_Integer]
:Arguments:		{ streamerRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDgetTxStreamerQueueTopAndClearQueue(mlint64);

:Begin:
:Function:		UHDgetTxStreamerQueueTopAndClearQueue
:Pattern:		UHDgetTxStreamerQueueTopAndClearQueue[streamerRef_Integer]
:Arguments:		{ streamerRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDgetTxStreamDeviceAndChannelsAndMaxBufferSize(mlint64);

:Begin:
:Function:		UHDgetTxStreamDeviceAndChannelsAndMaxBufferSize
:Pattern:		UHDgetTxStreamDeviceAndChannelsAndMaxBufferSize[streamerRef_Integer]
:Arguments:		{ streamerRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDtestTxMetadata(const char*, mlint64, double, const char*,const char*);

:Begin:
:Function:		UHDtestTxMetadata
:Pattern:		UHDtestTxMetadata[hasTimeSpec:(True|False),timeSpecSeconds_Integer,
					timeSpecFractionalSeconds_Real, beginOfBurst:(True|False),
					endOfBurst:(True|False)]
:Arguments:		{ hasTimeSpec, timeSpecSeconds, timeSpecFractionalSeconds, beginOfBurst, endOfBurst }
:ArgumentTypes:	{ Symbol, Integer64, Real64, Symbol, Symbol }
:ReturnType:	Manual
:End:

void UHDenableTxListMode(mlint64 usrpRef, double dwellTime,
		const char *includeSettingInDwellTime, const char *listModeStepBool,
		const char *triggerModeSingleBool,
		double blankerWaitTime, int blankerGPIOmainboard, 
		const char *blankerGPIObank, int blankerGPIOmask, int blankerGPIOvalue,
		int *channels, long numChannels);
		
:Begin:
:Function:		UHDenableTxListMode
:Pattern:		UHDenableTxListMode[usrp_Integer,
					dwellTime_Real,
					includeSettingInDwellTime:(True|False):False,
					listModeStep:(True|False):False,
					triggerModeSingle:(True|False):True,
					blankerWaitTime_Real:0.,
					blankerGPIOmainboard_Integer:0,
					blankerGPIObank_String:"",
					blankerGPIOmask_Integer:0,
					blankerGPIOvalue_Integer:0,
					channels:{___Integer}:{0},
					frequencies:{{___Real}...}?(Length[DeleteCases[Union[Length/@#],0]]<=1&):{},
					gains:{{___Real}...}?(Length[DeleteCases[Union[Length/@#],0]]<=1&):{},
					gainNames:_String|{___String}:{}]/;
				With[{numChannels=Length[channels],numGainNames=If[StringQ[#],1,Length[#]]&@gainNames,
						dimFunction=Function[If[#==={},{0,0},Dimensions[#]]&@DeleteCases[#,{}]]},
					Function[{fDims,gDims},(fDims==={0,0}||gDims==={0,0}||fDims[[2]]==gDims[[2]])&&
						(fDims[[1]]<=1||Length[frequencies]==numChannels)&&
						(gDims[[1]]<=1||Length[gains]==numChannels)&&
						(numGainNames<=1||numGainNames==numChannels)]@@(
						dimFunction/@{frequencies,gains})]
:Arguments:		{ usrp, dwellTime, includeSettingInDwellTime, listModeStep, triggerModeSingle,
					blankerWaitTime, blankerGPIOmainboard, blankerGPIObank,	blankerGPIOmask,
					blankerGPIOvalue, channels, frequencies, gains, gainNames }
:ArgumentTypes:	{ Integer64, Real, Symbol, Symbol, Symbol, Real, Integer, String, Integer,
					Integer, IntegerList, Manual }
:ReturnType:	Manual
:End:

void UHDdisableTxListMode(mlint64);

:Begin:
:Function:		UHDdisableTxListMode
:Pattern:		UHDdisableTxListMode[listModeParamsRef_Integer]
:Arguments:		{ listModeParamsRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDtxListModeRunning(mlint64);

:Begin:
:Function:		UHDtxListModeRunning
:Pattern:		UHDtxListModeRunning[listModeParamsRef_Integer]
:Arguments:		{ listModeParamsRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDtriggerTxListModeRun(mlint64);

:Begin:
:Function:		UHDtriggerTxListModeRun
:Pattern:		UHDtriggerTxListModeRun[listModeParamsRef_Integer]
:Arguments:		{ listModeParamsRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDgetTxListModeCurrentIndex(mlint64);

:Begin:
:Function:		UHDgetTxListModeCurrentIndex
:Pattern:		UHDgetTxListModeCurrentIndex[listModeParamsRef_Integer]
:Arguments:		{ listModeParamsRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDgetTxListModeChannels(mlint64);

:Begin:
:Function:		UHDgetTxListModeChannels
:Pattern:		UHDgetTxListModeChannels[listModeParamsRef_Integer]
:Arguments:		{ listModeParamsRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void UHDsetMIMO(mlint64 usrpRef, int mBoard, const char *mimo);

:Begin:
:Function:		UHDsetMIMO
:Pattern:		UHDsetMIMO[usrp_Integer,mBoard_Integer:0, mimo:(True|False)]
:Arguments:		{ usrp, mBoard, mimo }
:ArgumentTypes:	{ Integer64, Integer, Symbol }
:ReturnType:	Manual
:End:

void UHDgetMIMO(mlint64 usrpRef, int mBoard);

:Begin:
:Function:		UHDgetMIMO
:Pattern:		UHDgetMIMO[usrp_Integer,mBoard_Integer:0]
:Arguments:		{ usrp, mBoard }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:
