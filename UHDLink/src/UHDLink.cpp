#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>
#include <math.h>
#include <mathlink.h>
#include "uhd.h"
#include "UHDLink.tmc"
#include "MathLinkCommon.hpp"
#include <unordered_set>
#include <forward_list>
#include <utility>

#include "UHDtxBufferEntry.hpp"

#define UHD_STRING_BUFFER_SIZE 2048
int sargc;
char **sargv;

void UHDfailWithError(uhd_usrp_handle usrp) {
	char errorString[UHD_STRING_BUFFER_SIZE];
	if (usrp)
		uhd_usrp_last_error(usrp, errorString, sizeof(errorString));
	else
		uhd_get_last_error(errorString, sizeof(errorString));
	if (!MLPutFunction(stdlink, "$Failed", 1)
			|| !MLPutString(stdlink, errorString))
		failWithMsgAndError(stdlink, "UHDfailWithError:result:error: ");
}

int UHDputNamedBool(const char *name, int value) {
	return !(!MLPutFunction(stdlink, "Rule", 2) || !MLPutString(stdlink, name)
			|| !MLPutSymbol(stdlink, value ? "True" : "False"));
}

int UHDputNamedInteger(const char *name, int value) {
	return !(!MLPutFunction(stdlink, "Rule", 2) || !MLPutString(stdlink, name)
			|| !MLPutInteger(stdlink, value));
}

int UHDputNamedReal(const char *name, double value) {
	return !(!MLPutFunction(stdlink, "Rule", 2) || !MLPutString(stdlink, name)
			|| !MLPutReal(stdlink, value));
}

int UHDputNamedString(const char *name, const char *value) {
	return !(!MLPutFunction(stdlink, "Rule", 2) || !MLPutString(stdlink, name)
			|| !MLPutString(stdlink, value));
}

int UHDputStringVector(uhd_usrp_handle usrp, uhd_string_vector_handle strings) {
	size_t numStrings;
	if (uhd_string_vector_size(strings, &numStrings) != UHD_ERROR_NONE) {
		UHDfailWithError(usrp);
		return 1;
	}
	if (!MLPutFunction(stdlink, "List", numStrings)) {
		failWithMsgAndError(stdlink, "UHDputStringVector:size:error: ");
		return 1;
	}
	for (size_t i = 0; i < numStrings; i++) {
		char string[UHD_STRING_BUFFER_SIZE];
		if (uhd_string_vector_at(strings, i, string, sizeof(string))
				== UHD_ERROR_NONE) {
			if (!MLPutString(stdlink, string))
				failWithMsgAndError(stdlink,
						"UHDputStringVector:entry:error: ");
		} else
			UHDfailWithError(usrp);
	}
	return 0;
}

int UHDputMetaRange(uhd_usrp_handle usrp, uhd_meta_range_handle metaRange) {
	size_t numRanges;
	if (uhd_meta_range_size(metaRange, &numRanges) != UHD_ERROR_NONE) {
		UHDfailWithError(usrp);
		return 1;
	}
	if (!MLPutFunction(stdlink, "List", numRanges)) {
		failWithMsgAndError(stdlink, "UHDputMetaRange:size:error: ");
		return 1;
	}
	for (size_t i = 0; i < numRanges; i++) {
		uhd_range_t range;
		if (uhd_meta_range_at(metaRange, i, &range) == UHD_ERROR_NONE) {
			if (!MLPutFunction(stdlink, "List", 3)
					|| !MLPutReal(stdlink, range.start)
					|| !MLPutReal(stdlink, range.stop)
					|| !MLPutReal(stdlink, range.step))
				failWithMsgAndError(stdlink, "UHDputMetaRange:entry:error: ");
		} else
			UHDfailWithError(usrp);
	}
	return 0;
}

int UHDputSubdevSpec(uhd_usrp_handle usrp, uhd_subdev_spec_handle subdevSpec) {
	size_t numSpecs;
	if (uhd_subdev_spec_size(subdevSpec, &numSpecs) != UHD_ERROR_NONE) {
		UHDfailWithError(usrp);
		return 1;
	}
	if (!MLPutFunction(stdlink, "List", numSpecs)) {
		failWithMsgAndError(stdlink, "UHDputSubdevSpec:size:error: ");
		return 1;
	}
	for (size_t i = 0; i < numSpecs; i++) {
		uhd_subdev_spec_pair_t spec;
		if (uhd_subdev_spec_at(subdevSpec, i, &spec) == UHD_ERROR_NONE) {
			if (!MLPutFunction(stdlink, "List", 2)
					|| !MLPutString(stdlink, spec.db_name)
					|| !MLPutString(stdlink, spec.sd_name))
				failWithMsgAndError(stdlink, "UHDputSubdevSpec:entry:error: ");
		} else
			UHDfailWithError(usrp);
	}
	return 0;
}

int UHDputSensorValue(uhd_usrp_handle usrp,
		uhd_sensor_value_handle sensorValue) {
	uhd_sensor_value_data_type_t sensorValueDataType;
	if (uhd_sensor_value_data_type(sensorValue, &sensorValueDataType)
			!= UHD_ERROR_NONE) {
		UHDfailWithError(usrp);
		return 1;
	}
	_Bool sensorBoolValue;
	char sensorStringValue[UHD_STRING_BUFFER_SIZE];
	int sensorIntegerValue;
	double sensorRealValue;
	switch (sensorValueDataType) {
	case UHD_SENSOR_VALUE_BOOLEAN:
		if (uhd_sensor_value_to_bool(sensorValue, &sensorBoolValue)
				!= UHD_ERROR_NONE) {
			UHDfailWithError(usrp);
			return 1;
		}
		if (!MLPutSymbol(stdlink, sensorBoolValue ? "True" : "False"))
			failWithMsgAndError(stdlink,
					"UHDgetMboardSensor:boolResult:error: ");
		break;
	case UHD_SENSOR_VALUE_INTEGER:
		if (uhd_sensor_value_to_int(sensorValue, &sensorIntegerValue)
				!= UHD_ERROR_NONE) {
			UHDfailWithError(usrp);
			return 1;
		}
		if (!MLPutInteger(stdlink, sensorIntegerValue))
			failWithMsgAndError(stdlink,
					"UHDgetMboardSensor:integerResult:error: ");
		break;
	case UHD_SENSOR_VALUE_REALNUM:
		if (uhd_sensor_value_to_realnum(sensorValue, &sensorRealValue)
				!= UHD_ERROR_NONE) {
			UHDfailWithError(usrp);
			return 1;
		}
		if (!MLPutReal(stdlink, sensorRealValue))
			failWithMsgAndError(stdlink,
					"UHDgetMboardSensor:RealResult:error: ");
		break;
	case UHD_SENSOR_VALUE_STRING:
		if (uhd_sensor_value_to_pp_string(sensorValue, sensorStringValue,
				sizeof(sensorStringValue)) != UHD_ERROR_NONE) {
			UHDfailWithError(usrp);
			return 1;
		}
		if (!MLPutString(stdlink, sensorStringValue))
			failWithMsgAndError(stdlink,
					"UHDgetMboardSensor:StringResult:error: ");
		break;
	default:
		if (!MLPutFunction(stdlink, "$Failed", 2)
				|| !MLPutString(stdlink, "unknownSensorDataType")
				|| !MLPutInteger(stdlink, sensorValueDataType))
			failWithMsgAndError(stdlink,
					"UHDgetMboardSensor:unknownResult:error: ");
		break;
	}
	return 0;
}

void UHDfind(const char *args) {
	uhd_string_vector_handle deviceStrings;
	if (uhd_string_vector_make(&deviceStrings) != UHD_ERROR_NONE) {
		UHDfailWithError(0);
		return;
	}
	if (uhd_usrp_find(args, &deviceStrings) == UHD_ERROR_NONE)
		UHDputStringVector(0, deviceStrings);
	else
		UHDfailWithError(0);
	uhd_string_vector_free(&deviceStrings);
}

void UHDopen(const char *deviceArgs) {
	uhd_usrp_handle usrp;
	if (uhd_usrp_make(&usrp, deviceArgs) == UHD_ERROR_NONE) {
		if (!MLPutInteger64(stdlink, (mlint64) usrp))
			failWithMsgAndError(stdlink, "UHDopen:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDclose(mlint64 usrpRef) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	if (uhd_usrp_free(&usrp) == UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "UHDclose:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDgetPPstring(mlint64 usrpRef) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	char ppString[UHD_STRING_BUFFER_SIZE];
	if (uhd_usrp_get_pp_string(usrp, ppString, sizeof(ppString))
			== UHD_ERROR_NONE) {
		if (!MLPutString(stdlink, ppString))
			failWithMsgAndError(stdlink, "UHDgetPPstring:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDgetNumMboards(mlint64 usrpRef) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	size_t numMboards;
	if (uhd_usrp_get_num_mboards(usrp, &numMboards) == UHD_ERROR_NONE) {
		if (!MLPutInteger(stdlink, numMboards))
			failWithMsgAndError(stdlink, "UHDgetNumMboards:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDgetMboardName(mlint64 usrpRef, int mBoard) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	char mBoardName[UHD_STRING_BUFFER_SIZE];
	if (uhd_usrp_get_mboard_name(usrp, mBoard, mBoardName, sizeof(mBoardName))
			== UHD_ERROR_NONE) {
		if (!MLPutString(stdlink, mBoardName))
			failWithMsgAndError(stdlink, "UHDgetMboardName:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDenumerateRegisters(mlint64 usrpRef, int mBoard) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uhd_string_vector_handle registerPaths;
	if (uhd_string_vector_make(&registerPaths) != UHD_ERROR_NONE) {
		UHDfailWithError(usrp);
		return;
	}
	if (uhd_usrp_enumerate_registers(usrp, mBoard, &registerPaths)
			== UHD_ERROR_NONE)
		UHDputStringVector(usrp, registerPaths);
	else
		UHDfailWithError(usrp);
	uhd_string_vector_free(&registerPaths);
}

void UHDgetRegisterInfo(mlint64 usrpRef, int mBoard, const char *registerPath) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uhd_usrp_register_info_t regInfo;
	if (uhd_usrp_get_register_info(usrp, registerPath, mBoard, &regInfo)
			== UHD_ERROR_NONE) {
		if (!MLPutFunction(stdlink, "List", 3)
				|| !UHDputNamedInteger("bitwidth", regInfo.bitwidth)
				|| !UHDputNamedBool("readable", regInfo.readable)
				|| !UHDputNamedBool("writeable", regInfo.writable))
			failWithMsgAndError(stdlink, "UHDgetRegisterInfo:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDreadRegister(mlint64 usrpRef, int mBoard, const char *registerPath,
		int field) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uint64_t value;
	if (uhd_usrp_read_register(usrp, registerPath, field, mBoard, &value)
			== UHD_ERROR_NONE) {
		if (!MLPutInteger64(stdlink, value))
			failWithMsgAndError(stdlink, "UHDreadRegister:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDwriteRegister(mlint64 usrpRef, int mBoard, const char *registerPath,
		int field, mlint64 value) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	if (uhd_usrp_write_register(usrp, registerPath, field, value, mBoard)
			== UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "UHDwriteRegister:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDsetUserRegister(mlint64 usrpRef, int channel, int addr, int value) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	if (uhd_usrp_set_user_settings_register(usrp, addr, value, channel)
			== UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "UHDsetUserRegister:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDgetUserRegister(mlint64 usrpRef, int channel, int addr) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uint64_t data;
	if (uhd_usrp_get_user_settings_register(usrp, addr, &data, channel)
			== UHD_ERROR_NONE) {
		if (!MLPutInteger64(stdlink, data))
			failWithMsgAndError(stdlink, "UHDgetUserRegister:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDgetGPIObanks(mlint64 usrpRef, int mBoard) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uhd_string_vector_handle banks;
	if (uhd_string_vector_make(&banks) != UHD_ERROR_NONE) {
		UHDfailWithError(usrp);
		return;
	}
	if (uhd_usrp_get_gpio_banks(usrp, mBoard, &banks) == UHD_ERROR_NONE) {
		UHDputStringVector(usrp, banks);
	} else
		UHDfailWithError(usrp);
}

void UHDgetGPIOattr(mlint64 usrpRef, int mBoard, const char *bank,
		const char *attr) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uint32_t value;
	if (uhd_usrp_get_gpio_attr(usrp, bank, attr, mBoard, &value)
			== UHD_ERROR_NONE) {
		if (!MLPutInteger64(stdlink, value))
			failWithMsgAndError(stdlink, "UHDgetGPIOattr:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDsetGPIOattr(mlint64 usrpRef, int mBoard, const char *bank,
		const char *attr, mlint64 value, mlint64 mask) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	if (uhd_usrp_set_gpio_attr(usrp, bank, attr, value, mask, mBoard)
			== UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "UHDsetGPIOattr:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDgetMboardSensorNames(mlint64 usrpRef, int mBoard) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uhd_string_vector_handle sensorNames;
	if (uhd_string_vector_make(&sensorNames) != UHD_ERROR_NONE) {
		UHDfailWithError(usrp);
		return;
	}
	if (uhd_usrp_get_mboard_sensor_names(usrp, mBoard, &sensorNames)
			== UHD_ERROR_NONE)
		UHDputStringVector(usrp, sensorNames);
	else
		UHDfailWithError(usrp);
	uhd_string_vector_free(&sensorNames);
}

void UHDgetMboardSensor(mlint64 usrpRef, int mBoard, const char *sensorName) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uhd_sensor_value_handle sensorValue;
	if (uhd_sensor_value_make(&sensorValue) != UHD_ERROR_NONE) {
		UHDfailWithError(usrp);
		return;
	}
	if (uhd_usrp_get_mboard_sensor(usrp, sensorName, mBoard, &sensorValue)
			== UHD_ERROR_NONE)
		UHDputSensorValue(usrp, sensorValue);
	else
		UHDfailWithError(usrp);
	uhd_sensor_value_free(&sensorValue);
}

void UHDgetClockSources(mlint64 usrpRef, int mBoard) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uhd_string_vector_handle clockSources;
	if (uhd_string_vector_make(&clockSources) != UHD_ERROR_NONE) {
		UHDfailWithError(usrp);
		return;
	}
	if (uhd_usrp_get_clock_sources(usrp, mBoard, &clockSources)
			== UHD_ERROR_NONE)
		UHDputStringVector(usrp, clockSources);
	else
		UHDfailWithError(usrp);
	uhd_string_vector_free(&clockSources);
}

void UHDgetClockSource(mlint64 usrpRef, int mBoard) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	char clockSource[UHD_STRING_BUFFER_SIZE];
	if (uhd_usrp_get_clock_source(usrp, mBoard, clockSource,
			sizeof(clockSource)) == UHD_ERROR_NONE) {
		if (!MLPutString(stdlink, clockSource))
			failWithMsgAndError(stdlink, "UHDgetClockSource:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDsetClockSource(mlint64 usrpRef, int mBoard, const char *clockSource) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	if (uhd_usrp_set_clock_source(usrp, clockSource, mBoard)
			== UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "UHDsetClockSource:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDsetClockSourceOut(mlint64 usrpRef, int mBoard, const char *enabled) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	if (uhd_usrp_set_clock_source_out(usrp, strcmp("True", enabled) == 0,
			mBoard) == UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "UHDsetClockSourceOut:result:error: ");
	} else
		UHDfailWithError(usrp);
	MLReleaseSymbol(stdlink, enabled);
}

void UHDgetMasterClockRate(mlint64 usrpRef, int mBoard) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	double rate;
	if (uhd_usrp_get_master_clock_rate(usrp, mBoard, &rate) == UHD_ERROR_NONE) {
		if (!MLPutReal(stdlink, rate))
			failWithMsgAndError(stdlink,
					"UHDgetMasterClockRate:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDsetMasterClockRate(mlint64 usrpRef, int mBoard, double rate) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	if (uhd_usrp_set_master_clock_rate(usrp, rate, mBoard) == UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"UHDsetMasterClockRate:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDgetTimeSources(mlint64 usrpRef, int mBoard) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uhd_string_vector_handle timeSources;
	if (uhd_string_vector_make(&timeSources) != UHD_ERROR_NONE) {
		UHDfailWithError(usrp);
		return;
	}
	if (uhd_usrp_get_time_sources(usrp, mBoard, &timeSources) == UHD_ERROR_NONE)
		UHDputStringVector(usrp, timeSources);
	else
		UHDfailWithError(usrp);
	uhd_string_vector_free(&timeSources);
}

void UHDgetTimeSource(mlint64 usrpRef, int mBoard) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	char timeSource[UHD_STRING_BUFFER_SIZE];
	if (uhd_usrp_get_time_source(usrp, mBoard, timeSource, sizeof(timeSource))
			== UHD_ERROR_NONE) {
		if (!MLPutString(stdlink, timeSource))
			failWithMsgAndError(stdlink, "UHDgetTimeSource:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDsetTimeSource(mlint64 usrpRef, int mBoard, const char *timeSource) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	if (uhd_usrp_set_time_source(usrp, timeSource, mBoard) == UHD_ERROR_NONE) {
		if (!MLPutString(stdlink, timeSource))
			failWithMsgAndError(stdlink, "UHDsetTimeSource:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDgetTimeNow(mlint64 usrpRef, int mBoard) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	mlint64 seconds;
	double fractionalSecondes;
	if (uhd_usrp_get_time_now(usrp, mBoard, &seconds, &fractionalSecondes)
			== UHD_ERROR_NONE) {
		if (!MLPutFunction(stdlink, "List", 2)
				|| !MLPutInteger(stdlink, seconds)
				|| !MLPutReal(stdlink, fractionalSecondes))
			failWithMsgAndError(stdlink, "UHDgetTimeNow:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDsetTimeNow(mlint64 usrpRef, int mBoard, mlint64 seconds,
		double fractionalSeconds) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	if (uhd_usrp_set_time_now(usrp, seconds, fractionalSeconds, mBoard)
			== UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "UHDsetTimeNow:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDgetTimeLastPPS(mlint64 usrpRef, int mBoard) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	mlint64 seconds;
	double fractionalSecondes;
	if (uhd_usrp_get_time_last_pps(usrp, mBoard, &seconds, &fractionalSecondes)
			== UHD_ERROR_NONE) {
		if (!MLPutFunction(stdlink, "List", 2)
				|| !MLPutInteger(stdlink, seconds)
				|| !MLPutReal(stdlink, fractionalSecondes))
			failWithMsgAndError(stdlink, "UHDgetTimeLastPPS:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDsetTimeNextPPS(mlint64 usrpRef, int mBoard, mlint64 seconds,
		double fractionalSeconds) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	if (uhd_usrp_set_time_next_pps(usrp, seconds, fractionalSeconds, mBoard)
			== UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "UHDsetTimeNextPPS:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDsetTimeUnknownPPS(mlint64 usrpRef, mlint64 seconds,
		double fractionalSeconds) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	if (uhd_usrp_set_time_unknown_pps(usrp, seconds, fractionalSeconds)
			== UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "UHDsetTimeUnknownPPS:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDgetTimeSynchronized(mlint64 usrpRef) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	_Bool result;
	if (uhd_usrp_get_time_synchronized(usrp, &result) == UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, result ? "True" : "False"))
			failWithMsgAndError(stdlink,
					"UHDgetTimeSynchronized:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDsetCommandTime(mlint64 usrpRef, int mBoard, mlint64 seconds,
		double fractionalSeconds) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	if (uhd_usrp_set_command_time(usrp, seconds, fractionalSeconds, mBoard)
			== UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "UHDsetCommandTime:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDclearCommandTime(mlint64 usrpRef, int mBoard) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	if (uhd_usrp_clear_command_time(usrp, mBoard) == UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "UHDclearCommandTime:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDgetTxNumChannels(mlint64 usrpRef) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	size_t numChannels;
	if (uhd_usrp_get_tx_num_channels(usrp, &numChannels) == UHD_ERROR_NONE) {
		if (!MLPutInteger(stdlink, numChannels))
			failWithMsgAndError(stdlink, "UHDgetTxNumChannels:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDgetTxInfo(mlint64 usrpRef, int channel) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uhd_usrp_tx_info_t info;
	if (uhd_usrp_get_tx_info(usrp, channel, &info) == UHD_ERROR_NONE) {
		if (!MLPutFunction(stdlink, "List", 8)
				|| !UHDputNamedString("mBoard_id", info.mboard_id)
				|| !UHDputNamedString("mBoard_name", info.mboard_name)
				|| !UHDputNamedString("mBoard_serial", info.mboard_serial)
				|| !UHDputNamedString("tx_antenna", info.tx_antenna)
				|| !UHDputNamedString("tx_id", info.tx_id)
				|| !UHDputNamedString("tx_serial", info.tx_serial)
				|| !UHDputNamedString("tx_subdev_name", info.tx_subdev_name)
				|| !UHDputNamedString("tx_subdev_spec", info.tx_subdev_spec))
			failWithMsgAndError(stdlink, "UHDgetTxNumChannels:result:error: ");
	} else
		UHDfailWithError(usrp);
	uhd_usrp_tx_info_free(&info);
}

void UHDgetTxSubdevName(mlint64 usrpRef, int channel) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	char subdevName[UHD_STRING_BUFFER_SIZE];
	if (uhd_usrp_get_tx_subdev_name(usrp, channel, subdevName,
			sizeof(subdevName)) == UHD_ERROR_NONE) {
		if (!MLPutString(stdlink, subdevName))
			failWithMsgAndError(stdlink, "UHDgetTxSubdevName:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDgetTxSubdevSpec(mlint64 usrpRef, int mBoard) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uhd_subdev_spec_handle subdevSpec;
	if (uhd_subdev_spec_make(&subdevSpec, "") != UHD_ERROR_NONE) {
		UHDfailWithError(usrp);
		return;
	}
	if (uhd_usrp_get_tx_subdev_spec(usrp, mBoard, subdevSpec) == UHD_ERROR_NONE)
		UHDputSubdevSpec(usrp, subdevSpec);
	else
		UHDfailWithError(usrp);
	uhd_subdev_spec_free(&subdevSpec);
}

void UHDgetTxSensorNames(mlint64 usrpRef, int channel) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uhd_string_vector_handle sensorNames;
	if (uhd_string_vector_make(&sensorNames) != UHD_ERROR_NONE) {
		UHDfailWithError(usrp);
		return;
	}
	if (uhd_usrp_get_tx_sensor_names(usrp, channel, &sensorNames)
			== UHD_ERROR_NONE)
		UHDputStringVector(usrp, sensorNames);
	else
		UHDfailWithError(usrp);
	uhd_string_vector_free(&sensorNames);
}

void UHDgetTxSensor(mlint64 usrpRef, int channel, const char *sensorName) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uhd_sensor_value_handle sensorValue;
	if (uhd_sensor_value_make(&sensorValue) != UHD_ERROR_NONE) {
		UHDfailWithError(usrp);
		return;
	}
	if (uhd_usrp_get_tx_sensor(usrp, sensorName, channel, &sensorValue)
			== UHD_ERROR_NONE)
		UHDputSensorValue(usrp, sensorValue);
	else
		UHDfailWithError(usrp);
	uhd_sensor_value_free(&sensorValue);
}

void UHDgetTxRates(mlint64 usrpRef, int channel) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uhd_meta_range_handle rateRanges;
	if (uhd_meta_range_make(&rateRanges) != UHD_ERROR_NONE) {
		UHDfailWithError(usrp);
		return;
	}
	if (uhd_usrp_get_tx_rates(usrp, channel, rateRanges) == UHD_ERROR_NONE)
		UHDputMetaRange(usrp, rateRanges);
	else
		UHDfailWithError(usrp);
	uhd_meta_range_free(&rateRanges);
}

void UHDgetTxRate(mlint64 usrpRef, int channel) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	double rate;
	if (uhd_usrp_get_tx_rate(usrp, channel, &rate) == UHD_ERROR_NONE) {
		if (!MLPutReal(stdlink, rate))
			failWithMsgAndError(stdlink, "UHDgetTxRate:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDsetTxRate(mlint64 usrpRef, int channel, double rate) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	if (uhd_usrp_set_tx_rate(usrp, rate, channel) == UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "UHDsetTxRate:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDgetTxAntennas(mlint64 usrpRef, int channel) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uhd_string_vector_handle antennas;
	if (uhd_string_vector_make(&antennas) != UHD_ERROR_NONE) {
		UHDfailWithError(usrp);
		return;
	}
	if (uhd_usrp_get_tx_antennas(usrp, channel, &antennas) == UHD_ERROR_NONE)
		UHDputStringVector(usrp, antennas);
	else
		UHDfailWithError(usrp);
	uhd_string_vector_free(&antennas);
}

void UHDgetTxAntenna(mlint64 usrpRef, int channel) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	char antenna[UHD_STRING_BUFFER_SIZE];
	if (uhd_usrp_get_tx_antenna(usrp, channel, antenna, sizeof(antenna))
			== UHD_ERROR_NONE) {
		if (!MLPutString(stdlink, antenna))
			failWithMsgAndError(stdlink, "UHDgetTxAntenna:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDsetTxAntenna(mlint64 usrpRef, int channel, const char *antenna) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	if (uhd_usrp_set_tx_antenna(usrp, antenna, channel) == UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "UHDsetTxAntenna:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDgetTxGainNames(mlint64 usrpRef, int channel) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uhd_string_vector_handle gainNames;
	if (uhd_string_vector_make(&gainNames) != UHD_ERROR_NONE) {
		UHDfailWithError(usrp);
		return;
	}
	if (uhd_usrp_get_tx_gain_names(usrp, channel, &gainNames) == UHD_ERROR_NONE)
		UHDputStringVector(usrp, gainNames);
	else
		UHDfailWithError(usrp);
	uhd_string_vector_free(&gainNames);
}

void UHDgetTxGainRange(mlint64 usrpRef, int channel, const char *gainName) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uhd_meta_range_handle gainRanges;
	if (uhd_meta_range_make(&gainRanges) != UHD_ERROR_NONE) {
		UHDfailWithError(usrp);
		return;
	}
	if (uhd_usrp_get_tx_gain_range(usrp, gainName, channel, gainRanges)
			== UHD_ERROR_NONE)
		UHDputMetaRange(usrp, gainRanges);
	else
		UHDfailWithError(usrp);
	uhd_meta_range_free(&gainRanges);
}

void UHDgetTxGain(mlint64 usrpRef, int channel, const char *gainName) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	double gain;
	if (uhd_usrp_get_tx_gain(usrp, channel, gainName, &gain)
			== UHD_ERROR_NONE) {
		if (!MLPutReal(stdlink, gain))
			failWithMsgAndError(stdlink, "UHDgetTxGain:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDsetTxGain(mlint64 usrpRef, int channel, const char *gainName,
		double gain) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	if (uhd_usrp_set_tx_gain(usrp, gain, channel, gainName) == UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "UHDsetTxGain:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDgetNormalizedTxGain(mlint64 usrpRef, int channel) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	double normalizedGain;
	if (uhd_usrp_get_normalized_tx_gain(usrp, channel, &normalizedGain)
			== UHD_ERROR_NONE) {
		if (!MLPutReal(stdlink, normalizedGain))
			failWithMsgAndError(stdlink,
					"UHDgetNormalizedTxGain:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDsetNormalizedTxGain(mlint64 usrpRef, int channel,
		double normalizedGain) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	if (uhd_usrp_set_normalized_tx_gain(usrp, normalizedGain, channel)
			== UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"UHDsetNormalizedTxGain:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDgetTxBandwidthRange(mlint64 usrpRef, int channel) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uhd_meta_range_handle bandwidthRanges;
	if (uhd_meta_range_make(&bandwidthRanges) != UHD_ERROR_NONE) {
		UHDfailWithError(usrp);
		return;
	}
	if (uhd_usrp_get_tx_bandwidth_range(usrp, channel, bandwidthRanges)
			== UHD_ERROR_NONE)
		UHDputMetaRange(usrp, bandwidthRanges);
	else
		UHDfailWithError(usrp);
	uhd_meta_range_free(&bandwidthRanges);
}

void UHDgetTxBandwidth(mlint64 usrpRef, int channel) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	double bandwidth;
	if (uhd_usrp_get_tx_bandwidth(usrp, channel, &bandwidth)
			== UHD_ERROR_NONE) {
		if (!MLPutReal(stdlink, bandwidth))
			failWithMsgAndError(stdlink, "UHDgetTxBandwidth:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDsetTxBandwidth(mlint64 usrpRef, int channel, double bandwidth) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	if (uhd_usrp_set_tx_bandwidth(usrp, bandwidth, channel) == UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "UHDsetTxBandwidth:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDgetTxFrequencyRange(mlint64 usrpRef, int channel) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uhd_meta_range_handle frequencyRanges;
	if (uhd_meta_range_make(&frequencyRanges) != UHD_ERROR_NONE) {
		UHDfailWithError(usrp);
		return;
	}
	if (uhd_usrp_get_tx_freq_range(usrp, channel, frequencyRanges)
			== UHD_ERROR_NONE)
		UHDputMetaRange(usrp, frequencyRanges);
	else
		UHDfailWithError(usrp);
	uhd_meta_range_free(&frequencyRanges);
}

void UHDgetTxFrequency(mlint64 usrpRef, int channel) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	double frequency;
	if (uhd_usrp_get_tx_freq(usrp, channel, &frequency) == UHD_ERROR_NONE) {
		if (!MLPutReal(stdlink, frequency))
			failWithMsgAndError(stdlink, "UHDgetTxFrequency:result:error: ");
	} else
		UHDfailWithError(usrp);
}

uhd_tune_request_policy_t UHDparseTuneRequestPolicy(const char *policy) {
	if (strcasecmp(policy, "auto") == 0)
		return UHD_TUNE_REQUEST_POLICY_AUTO;
	else if (strcasecmp(policy, "manual") == 0)
		return UHD_TUNE_REQUEST_POLICY_MANUAL;
	else
		return UHD_TUNE_REQUEST_POLICY_NONE;
}

void UHDsetTxFrequency(mlint64 usrpRef, int channel, double targetFrequency) {
	UHDsetTxFrequencyEx(usrpRef, channel, targetFrequency, 0., "auto", 0.,
			"auto");
}

void UHDsetTxFrequencyEx(mlint64 usrpRef, int channel, double targetFrequency,
		double rfFrequency, const char *rfFrequencyPolicy, double dspFrequency,
		const char *dspFrequencyPolicy) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uhd_tune_request_t tuneReq = { target_freq : targetFrequency, rf_freq_policy
			: UHDparseTuneRequestPolicy(rfFrequencyPolicy), rf_freq
			: rfFrequency, dsp_freq_policy : UHDparseTuneRequestPolicy(
			dspFrequencyPolicy), dsp_freq : dspFrequency };
//	tuneReq.rf_freq_policy = UHDparseTuneRequestPolicy(rfFrequencyPolicy);
//	tuneReq.dsp_freq_policy = UHDparseTuneRequestPolicy(dspFrequencyPolicy);
	uhd_tune_result_t tuneRes;
	if (uhd_usrp_set_tx_freq(usrp, &tuneReq, channel, &tuneRes)
			== UHD_ERROR_NONE) {
		if (!MLPutFunction(stdlink, "List", 5)
				|| !UHDputNamedReal("target_rf_freq", tuneRes.target_rf_freq)
				|| !UHDputNamedReal("clipped_rf_freq", tuneRes.clipped_rf_freq)
				|| !UHDputNamedReal("actual_rf_freq", tuneRes.actual_rf_freq)
				|| !UHDputNamedReal("target_dsp_freq", tuneRes.target_dsp_freq)
				|| !UHDputNamedReal("actual_dsp_freq", tuneRes.actual_dsp_freq))
			failWithMsgAndError(stdlink, "UHDsetTxFrequency:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDgetFEtxFrequencyRange(mlint64 usrpRef, int channel) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	uhd_meta_range_handle frequencyRanges;
	if (uhd_meta_range_make(&frequencyRanges) != UHD_ERROR_NONE) {
		UHDfailWithError(usrp);
		return;
	}
	if (uhd_usrp_get_fe_tx_freq_range(usrp, channel, frequencyRanges)
			== UHD_ERROR_NONE)
		UHDputMetaRange(usrp, frequencyRanges);
	else
		UHDfailWithError(usrp);
	uhd_meta_range_free(&frequencyRanges);
}

void UHDgetTxIdle(mlint64 usrpRef, int channel) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	pair16_t tx;
	if (uhd_usrp_get_tx_idle(usrp, channel, &tx) == UHD_ERROR_NONE) {
		if (!MLPutFunction(stdlink, "List", 2)
				|| !MLPutInteger(stdlink, tx.first)
				|| !MLPutInteger(stdlink, tx.second))
			failWithMsgAndError(stdlink, "UHDgetTxIdle:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDsetTxIdle(mlint64 usrpRef, int channel, int tx_i, int tx_q) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	pair16_t tx = { (int16_t) tx_i, (int16_t) tx_q };
	if (uhd_usrp_set_tx_idle(usrp, tx, channel) == UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "UHDsetTxIdle:result:error: ");
	} else
		UHDfailWithError(usrp);
}

typedef struct UHDtxStreamerThreadParams {
	pthread_t txStreamerThread;
	bool volatile mayRun;
	bool volatile isRunning;
	double timeout;
	uhd_usrp_handle usrp;
	uhd_stream_args_t streamArguments;
	uhd_tx_streamer_handle txStreamer;
	bool repeatLastBuffer;
	uint64_t totalSamplesTransmitted;
	UHDtxBufferChainEntry volatile *volatile txBuffersListHead;
	UHDtxBufferChainEntry volatile *volatile txBuffersListTail;
	pthread_mutex_t txBuffersListMutex;
	pthread_cond_t txBuffersListEmptyCondition;
} UHDtxStreamerThreadParams_t;

#define exitTxStreamerThreadOnError(r,call) (r) = (call); \
	if((r) != UHD_ERROR_NONE) { \
		char errorString[UHD_STRING_BUFFER_SIZE]; \
		uhd_get_last_error(errorString, sizeof(errorString)); \
		fprintf(stderr, "error %d in %s line %d: %s, exiting transmitter thread\n", \
				r, __FILE__, __LINE__, errorString); \
		if(params->txStreamer) uhd_tx_streamer_free(&params->txStreamer); \
		if(metaData) uhd_tx_metadata_free(&metaData); \
		params->isRunning = 0; \
		return NULL;}

typedef struct __attribute__((__packed__)) UHDtxMetaData {
	int8_t has_time_spec;
	int8_t has_time_spec_pad[7];
	UHDtimeSpec_t time_spec;
	int8_t start_of_burst;
	int8_t end_of_burst;
} UHDtxMetaData_t;

void UHDtestTxMetadata(const char *hasTimeSpecBool, mlint64 timeSpecSeconds,
		double timeSpecFractionalSeconds, const char *beginOfBurstBool,
		const char *endOfBurstBool) {
	uhd_tx_metadata_handle metaData = NULL;
	uhd_tx_metadata_make(&metaData, strcmp("True", hasTimeSpecBool) == 0,
			timeSpecSeconds, timeSpecFractionalSeconds,
			strcmp("True", beginOfBurstBool) == 0,
			strcmp("True", endOfBurstBool) == 0);
	UHDtxMetaData_t *cMetaData = (UHDtxMetaData_t*) metaData;
	if (!MLPutFunction(stdlink, "List", 5)
			|| !UHDputNamedInteger("hasTimeSpec", cMetaData->has_time_spec)
			|| !UHDputNamedInteger("timeSpecSeconds",
					cMetaData->time_spec.fullSeconds)
			|| !UHDputNamedReal("timeSpecFractionalSeconds",
					cMetaData->time_spec.fractionalSeconds)
			|| !UHDputNamedInteger("startOfBurst", cMetaData->start_of_burst)
			|| !UHDputNamedInteger("endOfBurst", cMetaData->end_of_burst))
		failWithMsgAndError(stdlink, "UHDsetTxFrequency:result:error: ");
	uhd_tx_metadata_free(&metaData);
}

void* UHDtxStreamerThread(void *vParams) {
	UHDtxStreamerThreadParams_t *params = (UHDtxStreamerThreadParams_t*) vParams;
	params->isRunning = 1;
	uhd_error r;
	uhd_tx_metadata_handle metaData = NULL;
	if (uhd_set_thread_priority(uhd_default_thread_priority, true)
			!= UHD_ERROR_NONE) {
		char errorString[UHD_STRING_BUFFER_SIZE];
		uhd_get_last_error(errorString, sizeof(errorString));
		fprintf(stderr,
				"Unable to set thread priority, continuing anyway: %s\n",
				errorString);
	}
	if (!params->txStreamer) {
		exitTxStreamerThreadOnError(r,
				uhd_tx_streamer_make(&params->txStreamer));
		exitTxStreamerThreadOnError(r,
				uhd_usrp_get_tx_stream(params->usrp, &params->streamArguments,
						params->txStreamer));
	}
	size_t numSamplesSent = 0;
	bool beginOfBurst = true, endOfBurst = false;
	exitTxStreamerThreadOnError(r,
			uhd_tx_metadata_make(&metaData, false, 0, 0, beginOfBurst,
					endOfBurst));
	UHDtxBufferChainEntry *currentTxBufferChainEntry = NULL;
	pthread_mutex_lock(&params->txBuffersListMutex);
	currentTxBufferChainEntry =
			(UHDtxBufferChainEntry*) params->txBuffersListHead;
	pthread_mutex_unlock(&params->txBuffersListMutex);
	while (params->mayRun) {
		if (currentTxBufferChainEntry) {
			if (currentTxBufferChainEntry->txBuffer) {
				UHDtxMetaData_t *cMetaData = (UHDtxMetaData_t*) metaData;
				cMetaData->has_time_spec =
						currentTxBufferChainEntry->hasTimeSpec();
				cMetaData->time_spec = currentTxBufferChainEntry->getTimeSpec();
				cMetaData->start_of_burst = beginOfBurst;
				cMetaData->end_of_burst = endOfBurst;
				exitTxStreamerThreadOnError(r,
						uhd_tx_streamer_send(params->txStreamer,
								(const void** )currentTxBufferChainEntry->txBuffer->buffers,
								currentTxBufferChainEntry->txBuffer->numSamplesPerBuffer,
								&metaData, params->timeout, &numSamplesSent));
				params->totalSamplesTransmitted += numSamplesSent;
			}
			pthread_mutex_lock(&params->txBuffersListMutex);
			if (currentTxBufferChainEntry->hasNext()) {
				endOfBurst = false;
				UHDtxBufferChainEntry *nextTxBufferChainEntry =
						currentTxBufferChainEntry->getNext();
				if (nextTxBufferChainEntry != currentTxBufferChainEntry) {
					if (currentTxBufferChainEntry->mayDeleteAfterUse())
						delete currentTxBufferChainEntry;
					params->txBuffersListHead = nextTxBufferChainEntry;
					currentTxBufferChainEntry = nextTxBufferChainEntry;
				}
				pthread_mutex_unlock(&params->txBuffersListMutex);
			} else if (params->repeatLastBuffer) {
				pthread_mutex_unlock(&params->txBuffersListMutex);
				beginOfBurst = false;
				endOfBurst = false;
			} else {
				if (currentTxBufferChainEntry->mayDeleteAfterUse())
					delete currentTxBufferChainEntry;
				currentTxBufferChainEntry = NULL;
				params->txBuffersListHead = NULL;
				params->txBuffersListTail = NULL;
				pthread_mutex_unlock(&params->txBuffersListMutex);
				pthread_cond_signal(&params->txBuffersListEmptyCondition);
				beginOfBurst = true;
				endOfBurst = false;
			}
		} else {
			usleep(1000000 * params->timeout);
			pthread_mutex_lock(&params->txBuffersListMutex);
			currentTxBufferChainEntry =
					(UHDtxBufferChainEntry*) params->txBuffersListHead;
			pthread_mutex_unlock(&params->txBuffersListMutex);
		}
	}
	if (params->txStreamer)
		uhd_tx_streamer_free(&params->txStreamer);
	if (metaData)
		uhd_tx_metadata_free(&metaData);
	params->isRunning = 0;
	return NULL;
}

void UHDstartTxStreamer(mlint64 usrpRef, double timeout, const char *args,
		const char *cpuFormat, const char *otwFormat, int *channels,
		long numChannels, const char *repeatLastBufferBool) {
	UHDtxStreamerThreadParams_t *params = (UHDtxStreamerThreadParams_t*) malloc(
			sizeof(UHDtxStreamerThreadParams_t));
	params->usrp = (uhd_usrp_handle) usrpRef;
	params->timeout = timeout;
	params->isRunning = false;
	params->mayRun = true;
	params->txStreamer = NULL;
	if (uhd_tx_streamer_make(&params->txStreamer) != UHD_ERROR_NONE) {
		UHDfailWithError(params->usrp);
		free(params);
		return;
	}
	params->streamArguments.args = strdup(args);
	params->streamArguments.cpu_format = strdup(cpuFormat);
	params->streamArguments.otw_format = strdup(otwFormat);
	params->streamArguments.n_channels = numChannels;
	params->streamArguments.channel_list = (size_t*) calloc(numChannels,
			sizeof(size_t));
	for (int i = 0; i < numChannels; i++)
		params->streamArguments.channel_list[i] = channels[i];
	if (uhd_usrp_get_tx_stream(params->usrp, &params->streamArguments,
			params->txStreamer) != UHD_ERROR_NONE) {
		UHDfailWithError(params->usrp);
		uhd_tx_streamer_free(&params->txStreamer);
		free(params->streamArguments.args);
		free(params->streamArguments.cpu_format);
		free(params->streamArguments.otw_format);
		free(params->streamArguments.channel_list);
		free(params);
		return;
	}
	params->txBuffersListMutex = (pthread_mutex_t ) PTHREAD_MUTEX_INITIALIZER;
	params->txBuffersListEmptyCondition = (pthread_cond_t )
			PTHREAD_COND_INITIALIZER;
	params->txBuffersListHead = NULL;
	params->txBuffersListTail = NULL;
	params->totalSamplesTransmitted = 0;
	params->repeatLastBuffer = strcmp("True", repeatLastBufferBool) == 0;
	int r = pthread_create(&params->txStreamerThread, NULL, UHDtxStreamerThread,
			(void*) params);
	if (r) {
		fprintf(stderr, "streamer thread creation failed: %d\n", r);
		uhd_tx_streamer_free(&params->txStreamer);
		free(params->streamArguments.args);
		free(params->streamArguments.cpu_format);
		free(params->streamArguments.otw_format);
		free(params->streamArguments.channel_list);
		pthread_mutex_destroy(&params->txBuffersListMutex);
		pthread_cond_destroy(&params->txBuffersListEmptyCondition);
		free(params);
		failWithMsg(stdlink,
				"UHDstartTxStreamer:could_not_create_streamer_thread");
		return;
	}
	if (!MLPutInteger64(stdlink, (mlint64) params)) {
		failWithMsgAndError(stdlink, "UHDstartTxStreamer:result:error: ");
		return;
	}
}

void UHDstopTxStreamer(mlint64 paramsRef) {
	UHDtxStreamerThreadParams_t *params =
			(UHDtxStreamerThreadParams_t*) paramsRef;
	if (!params) {
		failWithMsg(stdlink, "UHDstopTxStreamer:param:invalidAddress");
		return;
	}
	if (params->isRunning) {
		params->mayRun = 0;
		int r = pthread_join(params->txStreamerThread, NULL);
		if (r) {
			fprintf(stderr, "streamer thread join failed: %d\n", r);
			exit(r);
		}
	} else {
		params->mayRun = 0;
		fprintf(stderr, "streamer thread already dead!\n");
	}
	if (params->txBuffersListHead) {
		((UHDtxBufferChainEntry*) params->txBuffersListHead)->deleteNext();
		delete params->txBuffersListHead;
		params->txBuffersListHead = NULL;
	}
	params->txBuffersListTail = NULL;
	free(params->streamArguments.args);
	free(params->streamArguments.cpu_format);
	free(params->streamArguments.otw_format);
	free(params->streamArguments.channel_list);
	pthread_mutex_destroy((pthread_mutex_t*) &params->txBuffersListMutex);
	pthread_cond_destroy(
			(pthread_cond_t*) &params->txBuffersListEmptyCondition);
	free((void*) params);
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink, "UHDstopTxStreamer:result:error: ");
}

void UHDtxStreamerRunning(mlint64 paramsRef) {
	UHDtxStreamerThreadParams_t *params =
			(UHDtxStreamerThreadParams_t*) paramsRef;
	if (!params) {
		failWithMsg(stdlink, "UHDtxStreamerRunning:param:invalidAddress");
		return;
	}
	if (!MLPutSymbol(stdlink, params->isRunning ? "True" : "False"))
		failWithMsgAndError(stdlink, "UHDtxStreamerRunning:result:error: ");
}

void UHDgetTxStreamerTimeout(mlint64 paramsRef) {
	UHDtxStreamerThreadParams_t *params =
			(UHDtxStreamerThreadParams_t*) paramsRef;
	if (!params) {
		failWithMsg(stdlink, "UHDgetTxStreamerTimeout:param:invalidAddress");
		return;
	}
	if (!MLPutReal(stdlink, params->timeout))
		failWithMsgAndError(stdlink, "UHDgetTxStreamerTimeout:result:error: ");
}

void UHDsetTxStreamerTimeout(mlint64 paramsRef, double timeout) {
	UHDtxStreamerThreadParams_t *params =
			(UHDtxStreamerThreadParams_t*) paramsRef;
	if (!params) {
		failWithMsg(stdlink, "UHDsetTxStreamerTimeout:param:invalidAddress");
		return;
	}
	params->timeout = timeout;
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink, "UHDsetTxStreamerTimeout:result:error: ");
}

void UHDgetTxStreamerRepeatLastBuffer(mlint64 paramsRef) {
	UHDtxStreamerThreadParams_t *params =
			(UHDtxStreamerThreadParams_t*) paramsRef;
	if (!params) {
		failWithMsg(stdlink,
				"UHDgetTxStreamerRepeatLastBuffer:param:invalidAddress");
		return;
	}
	if (!MLPutSymbol(stdlink, params->repeatLastBuffer ? "True" : "False"))
		failWithMsgAndError(stdlink,
				"UHDgetTxStreamerRepeatLastBuffer:result:error: ");
}

void UHDsetTxStreamerRepeatLastBuffer(mlint64 paramsRef,
		const char *repeatLastBuffer) {
	volatile UHDtxStreamerThreadParams_t *params =
			(UHDtxStreamerThreadParams_t*) paramsRef;
	if (!params) {
		failWithMsg(stdlink,
				"UHDsetTxStreamerRepeatLastBuffer:param:invalidAddress");
		return;
	}
	params->repeatLastBuffer = strcmp("True", repeatLastBuffer) == 0;
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink,
				"UHDsetTxStreamerRepeatLastBuffer:result:error: ");
}

void* UHDcreateIQbufferEx(mlint64 *numSamples) {
	const char *head;
	int argCount;
	short int *buffer;
	if (!MLGetFunction(stdlink, &head, &argCount))
		return 0;
	if (strcmp(head, "List") == 0) {
		MLReleaseSymbol(stdlink, head);
		*numSamples = argCount >> 1;
		buffer = (short int*) calloc(*numSamples * 2,
				sizeof(short int));
		for (int i = 0; i < argCount; i++)
			if (!MLGetInteger16(stdlink, &buffer[i])) {
				free(buffer);
				return 0;
			}
	} else if (strcmp(head, "UHDiqBuffer") == 0) {
		MLReleaseSymbol(stdlink, head);
		if (!MLGetInteger64(stdlink, (mlint64*) &buffer)
				|| !MLGetInteger64(stdlink, numSamples))
			return 0;
	} else {
		MLReleaseSymbol(stdlink, head);
		return 0;
	}
	return buffer;
}

void UHDcreateIQbuffer(void) {
	mlint64 numSamples;
	void *buffer = UHDcreateIQbufferEx(&numSamples);
	if (buffer) {
		if (!MLPutFunction(stdlink, "UHDiqBuffer", 2)
				|| !MLPutInteger64(stdlink, (mlint64) buffer)
				|| !MLPutInteger(stdlink, numSamples))
			failWithMsgAndError(stdlink, "UHDcreateIQbuffer:return:error: ");
	} else
		failWithMsg(stdlink, "null");
}

void UHDgetIQbuffer(void) {
	short int *buffer = 0;
	int numSamples;
	int shouldBeTwo;
	const char *head;
	if (!MLGetFunction(stdlink, &head, &shouldBeTwo)) {
		failWithMsgAndError(stdlink, "UHDgetIQbuffer:head:error: ");
		return;
	}
	if (strcmp(head, "UHDiqBuffer") != 0 || shouldBeTwo != 2)
		failWithClearAndMsg(stdlink, "invalid parameter");
	else if (!MLGetInteger64(stdlink, (mlint64*) &buffer)
			|| !MLGetInteger(stdlink, &numSamples)
			|| !MLPutInteger16List(stdlink, buffer, numSamples * 2))
		failWithMsgAndError(stdlink, "UHDgetIQbuffer:error: ");
}

void UHDdestroyIQbuffer(void) {
	void *buffer = 0;
	int numSamples;
	int shouldBeTwo;
	const char *head;
	switch (MLGetType(stdlink)) {
	case MLTKINT:
		if (!MLGetInteger64(stdlink, (mlint64*) &buffer))
			failWithMsgAndError(stdlink, "UHDdestroyIQbuffer:error: ");
		break;
	case MLTKFUNC:
		if (!MLGetFunction(stdlink, &head, &shouldBeTwo)) {
			failWithMsgAndError(stdlink, "UHDdestroyIQbuffer:error: ");
			return;
		}
		if (shouldBeTwo != 2 || strcmp(head, "UHDiqBuffer") != 0) {
			failWithClearAndMsg(stdlink, "invalid input");
			return;
		}
		if (!MLGetInteger64(stdlink, (mlint64*) &buffer)
				|| !MLGetInteger(stdlink, &numSamples))
			failWithMsgAndError(stdlink, "UHDdestroyIQbuffer:error: ");
		break;
	default:
		failWithClearAndMsg(stdlink, "invalid input");
	}
	free(buffer);
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink,
				"UHDdestroyIQbuffer:result:error: ");
}

mlint64 UHDcreateTxStreamerBufferEx(bool freeAfterUse) {
	long numBuffers;
	mlint64 numSamples;
	if (!MLCheckFunction(stdlink, "List", &numBuffers) || numBuffers <= 0) {
		failWithMsgAndError(stdlink, "UHDqueueTxStreamerBuffer:data:error: ");
		return 0;
	}
	UHDtxBuffer *txBuffers = new UHDtxBuffer();
	txBuffers->numBuffers = numBuffers;
	txBuffers->buffers = (void**) calloc(numBuffers, sizeof(void*));
	for (int i = 0; i < numBuffers; i++) {
		txBuffers->buffers[i] = UHDcreateIQbufferEx(&numSamples);
		if (!txBuffers->buffers[i]
				|| (i != 0 && numSamples != txBuffers->numSamplesPerBuffer)) {
			delete txBuffers;
			failWithMsgAndError(stdlink,
					"UHDqueueTxStreamerBuffer:data:error: ");
			return 0;
		}
		if (i == 0)
			txBuffers->numSamplesPerBuffer = numSamples;
	}
	txBuffers->freeAfterUse = freeAfterUse;
	return (mlint64) txBuffers;
}

mlint64 UHDcreateTxStreamerBuffer(void) {
	return UHDcreateTxStreamerBufferEx(false);
}

int UHDputTxStreamerBuffer(UHDtxBuffer *txBuffer) {
	return txBuffer ?
			MLPutFunction(stdlink, "UHDtxBuffer", 4)
					&& MLPutInteger64(stdlink, (mlint64) txBuffer)
					&& MLPutInteger64List(stdlink,
							(mlint64*) (txBuffer->buffers),
							txBuffer->numBuffers)
					&& MLPutInteger64(stdlink, txBuffer->numSamplesPerBuffer)
					&& MLPutSymbol(stdlink,
							txBuffer->freeAfterUse ? "True" : "False") :
			MLPutSymbol(stdlink, "Null");
}

void UHDgetTxStreamerBuffer(mlint64 bufferRef) {
	if(!UHDputTxStreamerBuffer((UHDtxBuffer*) bufferRef))
		failWithMsgAndError(stdlink, "UHDgetTxStreamerBuffer:return:error: ");
}

void UHDsetTxStreamerBufferFreeAfterUse(mlint64 bufferRef,
		const char *freeAfterUse) {
	UHDtxBuffer *txBuffers = (UHDtxBuffer*) bufferRef;
	if (txBuffers) {
		if (strcmp(freeAfterUse, "True") == 0)
			txBuffers->freeAfterUse = true;
		else if (strcmp(freeAfterUse, "False"))
			txBuffers->freeAfterUse = false;
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"UHDsetTxStreamerBufferFreeAfterUse:return:error: ");
	} else
		failWithClearAndMsg(stdlink, "null reference");
}

void UHDsetTxStreamerBuffer(mlint64 bufferRef, const char *freeAfterUse) {
	UHDtxBuffer *txBuffers = (UHDtxBuffer*) bufferRef;
	void *buffer;
	long int numBuffers;
	mlint64 numSamples, newNumSamples = -1;
	if (txBuffers) {
		if (!MLCheckFunction(stdlink, "List", &numBuffers)) {
			failWithMsgAndError(stdlink,
					"UHDsetTxStreamerBuffer:listHead:error: ");
		}
		if (numBuffers != txBuffers->numBuffers) {
			free(txBuffers->buffers);
			txBuffers->buffers = (void**) calloc(numBuffers, sizeof(void*));
			txBuffers->numBuffers = numBuffers;
		}
		for (int i = 0; i < numBuffers; i++) {
			switch (MLGetType(stdlink)) {
			case MLTKINT:
				if (!MLGetInteger64(stdlink,
						(mlint64*) &txBuffers->buffers[i])) {
					failWithMsgAndError(stdlink,
							"UHDsetTxStreamerBuffer:intRef:error: ");
					return;
				}
				break;
			case MLTKFUNC:
				buffer = UHDcreateIQbufferEx(&numSamples);
				if (newNumSamples < 0)
					newNumSamples = numSamples;
				else if (numSamples != newNumSamples) {
					failWithClearAndMsg(stdlink, "inconsistent buffer sizes");
					return;
				} else
					txBuffers->buffers[i] = buffer;
			}
		}
		if (newNumSamples >= 0)
			txBuffers->numSamplesPerBuffer = newNumSamples;
		UHDsetTxStreamerBufferFreeAfterUse(bufferRef, freeAfterUse);
	} else
		failWithClearAndMsg(stdlink, "null reference");
}

void UHDdestroyTxStreamerBuffer(mlint64 bufferRef) {
	delete ((UHDtxBuffer*) bufferRef);
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink,
				"UHDdestroyTxStreamerBuffer:result:error: ");
}

void UHDdestroyTxStreamerBufferButKeepIQbuffers(mlint64 bufferRef) {
	UHDtxBuffer *txBuffer = (UHDtxBuffer*) bufferRef;
	int numBuffers = txBuffer->numBuffers;
	size_t numSamplesPerBuffer = txBuffer->numSamplesPerBuffer;
	if (!MLPutFunction(stdlink, "List", numBuffers)) {
		failWithMsgAndError(stdlink,
				"UHDdestroyTxStreamerBufferButKeepIQbuffers:resultHead:error: ");
		return;
	}
	for (int i = 0; i < numBuffers; i++)
		if (!MLPutFunction(stdlink, "UHDiqBuffer", 2)
				|| !MLPutInteger64(stdlink, (mlint64) txBuffer->buffers[i])
				|| !MLPutInteger64(stdlink, numSamplesPerBuffer)) {
			failWithMsgAndError(stdlink,
					"UHDdestroyTxStreamerBufferButKeepIQbuffers:result:error: ");
			return;
		}
	free(txBuffer->buffers);
	txBuffer->buffers = 0;
	delete txBuffer;
}

void UHDqueueTxStreamerBuffer(mlint64 paramsRef) {
	UHDtxStreamerThreadParams_t *params =
			(UHDtxStreamerThreadParams_t*) paramsRef;
	UHDtxBuffer *txBuffer = NULL;
	mlint64 t;
	switch (MLGetType(stdlink)) {
	case MLTKFUNC:
		txBuffer = (UHDtxBuffer*) UHDcreateTxStreamerBufferEx(true);
		if (txBuffer == NULL)
			return;
		break;
	case MLTKINT:
		if (!MLGetInteger64(stdlink, &t)) {
			failWithMsgAndError(stdlink,
					"UHDqueueTxStreamerBuffer:data:error: ");
			return;
		}
		txBuffer = (UHDtxBuffer*) t;
		break;
	default:
		failWithClearAndMsg(stdlink, "buffer data of ref expected");
		return;
	}
	UHDtxBufferListEntry *entry = new UHDtxBufferListEntry(txBuffer);
	if (!MLGetInteger64(stdlink, (mlint64*) &entry->numRepetitions)) {
		failWithMsgAndError(stdlink,
				"UHDqueueTxStreamerBuffer:data:inner:error: ");
		delete entry;
		return;
	}
	const char *shouldBeNull;
	long timeSpecListLength;
	switch (MLGetType(stdlink)) {
	case MLTKFUNC:
		if (!MLCheckFunction(stdlink, "List", &timeSpecListLength)
				|| timeSpecListLength != 2
				|| !MLGetInteger32(stdlink, &entry->timeSpec.fullSeconds)
				|| !MLGetReal64(stdlink, &entry->timeSpec.fractionalSeconds)) {
			failWithMsgAndError(stdlink,
					"UHDqueueTxStreamerBuffer:timeSpec:list:error: ");
			delete entry;
			return;
		}
		entry->_hasTimeSpec = true;
		break;
	case MLTKSYM:
		if (!MLGetSymbol(stdlink, &shouldBeNull)) {
			failWithMsgAndError(stdlink,
					"UHDqueueTxStreamerBuffer:timeSpec:head:error: ");
			delete entry;
			return;
		}
		if (strcmp(shouldBeNull, "Null") != 0) {
			MLReleaseSymbol(stdlink, shouldBeNull);
			failWithMsg(stdlink,
					"UHDqueueTxStreamerBuffer:timeSpec:symbol:shouldBeNull");
			delete entry;
			return;
		}
		MLReleaseSymbol(stdlink, shouldBeNull);
		entry->_hasTimeSpec = false;
		break;
	}
	if (txBuffer->numBuffers != params->streamArguments.n_channels) {
		if (!MLPutFunction(stdlink, "$Failed", 3)
				|| !MLPutString(stdlink, "wrong number of channels")
				|| !MLPutInteger(stdlink, params->streamArguments.n_channels)
				|| !MLPutInteger(stdlink, txBuffer->numBuffers))
			failWithMsgAndError(stdlink,
					"UHDqueueTxStreamerBuffer:wrongNumberOfChannels:error: ");
		delete entry;
		return;
	}
	pthread_mutex_lock((pthread_mutex_t*) &params->txBuffersListMutex);
	if (params->txBuffersListHead == NULL)
		params->txBuffersListHead = entry;
	else
		((UHDtxBufferChainEntry*) params->txBuffersListTail)->setNext(entry);
	params->txBuffersListTail = entry;
	pthread_mutex_unlock((pthread_mutex_t*) &params->txBuffersListMutex);
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink,
				"UHDqueueTxStreamerBuffer:result:error: ");
}

void UHDclearTxStreamerQueue(mlint64 paramsRef) {
	UHDtxStreamerThreadParams_t *params =
			(UHDtxStreamerThreadParams_t*) paramsRef;
	pthread_mutex_lock((pthread_mutex_t*) &params->txBuffersListMutex);
	if (params->txBuffersListHead) {
		((UHDtxBufferChainEntry*) params->txBuffersListHead)->deleteNext();
		delete params->txBuffersListHead;
	}
	params->txBuffersListHead = NULL;
	params->txBuffersListTail = NULL;
	pthread_mutex_unlock((pthread_mutex_t*) &params->txBuffersListMutex);
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink, "UHDclearTxStreamerQueue:result:error: ");
}

void UHDtraverseTxStreamerQueue(UHDtxBufferChainEntry *queueHead,
		std::unordered_set<UHDtxBufferChainEntry*> &entrySet,
		void (*entryAction)(UHDtxBufferChainEntry*, void*),
		void *entryActionParams) {
	if (queueHead && entrySet.find(queueHead) == entrySet.end()) {
		entrySet.insert(queueHead);
		if (entryAction)
			entryAction(queueHead, entryActionParams);
		switch (queueHead->getEntryType()) {
		case UHDtxBufferListEntry::ENTRY_TYPE:
			UHDtraverseTxStreamerQueue(
					((UHDtxBufferListEntry*) queueHead)->next, entrySet,
					entryAction, entryActionParams);
			break;
		}
	}
}

struct UHDtxStreamerQueueSizeParams {
	size_t n;
	bool countRepetitions, countSamples;
};

mlint64 UHDgetTxStreamerQueueSize(mlint64 paramsRef,
		const char *countRepetitions, const char *countSamples) {
	UHDtxStreamerThreadParams_t *params =
			(UHDtxStreamerThreadParams_t*) paramsRef;
	pthread_mutex_lock((pthread_mutex_t*) &params->txBuffersListMutex);
	struct UHDtxStreamerQueueSizeParams queueSizeParams;
	queueSizeParams.n = 0;
	queueSizeParams.countRepetitions = strcmp(countRepetitions, "True") == 0;
	queueSizeParams.countSamples = strcmp(countSamples, "True") == 0;
	std::unordered_set<UHDtxBufferChainEntry*> entriesUnion;
	UHDtraverseTxStreamerQueue(
			(UHDtxBufferChainEntry*) params->txBuffersListHead, entriesUnion,
			[](UHDtxBufferChainEntry *entry, void *x) {
				struct UHDtxStreamerQueueSizeParams *queueSizeParams =
						(struct UHDtxStreamerQueueSizeParams*) x;
				size_t value =
						queueSizeParams->countSamples ?
								entry->txBuffer->numSamplesPerBuffer : 1;
				if (queueSizeParams->countRepetitions
						&& entry->getEntryType()
								== UHDtxBufferListEntry::ENTRY_TYPE) {
					UHDtxBufferListEntry *listEntry =
							(UHDtxBufferListEntry*) entry;
					value *= listEntry->numRepetitions
							- listEntry->repetitionCounter + 1;
				}
				queueSizeParams->n += value;
			},&queueSizeParams);
	pthread_mutex_unlock((pthread_mutex_t*) &params->txBuffersListMutex);
	return queueSizeParams.n;
}

int UHDputTxBufferListEntry(UHDtxBufferListEntry *y) {
	if (y)
		return MLPutFunction(stdlink, "UHDtxBufferListEntry", 4)
				&& MLPutInteger64(stdlink, (mlint64) y)
				&& MLPutInteger64(stdlink, (mlint64) (y->txBuffer))
				&& MLPutFunction(stdlink, "List", 2)
				&& MLPutInteger(stdlink, y->repetitionCounter)
				&& MLPutInteger(stdlink, y->numRepetitions)
				&& (y->hasTimeSpec() ?
						MLPutFunction(stdlink, "List", 2)
								&& MLPutInteger32(stdlink,
										y->timeSpec.fullSeconds)
								&& MLPutReal64(stdlink,
										y->timeSpec.fractionalSeconds) :
						MLPutSymbol(stdlink, "Null"));
	else
		return MLPutSymbol(stdlink, "Null");
}

int UHDputTxBufferChainEntry(UHDtxBufferChainEntry *x) {
	if (x)
		switch (x->getEntryType()) {
		case UHDtxBufferListEntry::ENTRY_TYPE:
			return UHDputTxBufferListEntry((UHDtxBufferListEntry*) x);
		default:
			return MLPutFunction(stdlink, "UHDtxBufferChainEntry", 2)
					&& MLPutInteger64(stdlink, (mlint64) x)
					&& MLPutInteger64(stdlink, (mlint64) (x->txBuffer));
		}
	else
		return MLPutSymbol(stdlink, "Null");
}

void UHDgetTxStreamerQueue(mlint64 paramsRef) {
	UHDtxStreamerThreadParams_t *params =
			(UHDtxStreamerThreadParams_t*) paramsRef;
	pthread_mutex_lock((pthread_mutex_t*) &params->txBuffersListMutex);
	std::unordered_set<UHDtxBufferChainEntry*> entriesUnion;
	std::forward_list<std::pair<UHDtxBufferChainEntry*, UHDtxBufferChainEntry*>> entryVertices;
	int numVertices = 0;
	UHDtraverseTxStreamerQueue(
			(UHDtxBufferChainEntry*) params->txBuffersListHead, entriesUnion, 0,
			0);
	if (!MLPutFunction(stdlink, "List", 2)
			|| !MLPutFunction(stdlink, "List", entriesUnion.size())) {
		pthread_mutex_unlock((pthread_mutex_t*) &params->txBuffersListMutex);
		failWithMsgAndError(stdlink, "UHDgetTxStreamerQueue:result:outer:error: ");
		return;
	}
	for (UHDtxBufferChainEntry *x : entriesUnion) {
		switch (x->getEntryType()) {
		case UHDtxBufferListEntry::ENTRY_TYPE:
			do {
				UHDtxBufferListEntry *y = (UHDtxBufferListEntry*) x;
				if (!UHDputTxBufferListEntry(y)) {
					pthread_mutex_unlock(
							(pthread_mutex_t*) &params->txBuffersListMutex);
					failWithMsgAndError(stdlink,
							"UHDgetTxStreamerQueue:result:nodes:error: ");
					return;
				}
				if (y) {
					entryVertices.push_front(std::make_pair(y, y->next));
					numVertices++;
				}
			} while (0);
			break;
		}
	}
	if (!MLPutFunction(stdlink, "List", numVertices)) {
		pthread_mutex_unlock((pthread_mutex_t*) &params->txBuffersListMutex);
		failWithMsgAndError(stdlink, "UHDgetTxStreamerQueue:result:vertices:error: ");
		return;
	}
	for (std::pair<UHDtxBufferChainEntry*, UHDtxBufferChainEntry*> entryVertex : entryVertices) {
		if (!MLPutFunction(stdlink, "List", 2)
				|| !UHDputTxBufferChainEntry(entryVertex.first)
				|| !UHDputTxBufferChainEntry(entryVertex.second)) {
			pthread_mutex_unlock(
					(pthread_mutex_t*) &params->txBuffersListMutex);
			failWithMsgAndError(stdlink,
					"UHDgetTxStreamerQueue:result:vertex:error: ");
			return;
		}
	}
	pthread_mutex_unlock((pthread_mutex_t*) &params->txBuffersListMutex);
}

void UHDgetTxStreamerQueueTopAndClearQueue(mlint64 paramsRef) {
	UHDtxStreamerThreadParams_t *params =
			(UHDtxStreamerThreadParams_t*) paramsRef;
	pthread_mutex_lock((pthread_mutex_t*) &params->txBuffersListMutex);
	UHDtxBufferChainEntry *top =
			(UHDtxBufferChainEntry*) params->txBuffersListHead;
	if (top) {
		bool oldFreeAfterUse;
		if(top->txBuffer) {
			oldFreeAfterUse = top->txBuffer->freeAfterUse;
			top->txBuffer->freeAfterUse = false;
		}
		if (!UHDputTxStreamerBuffer(top->txBuffer)) {
			top->txBuffer->freeAfterUse = oldFreeAfterUse;
			failWithMsgAndError(stdlink,
					"UHDgetTxStreamerQueueTopAndClearQueue:result:error: ");
		} else {
			top->deleteNext();
			params->txBuffersListTail = top;
			bool repeatLastBuffer = params->repeatLastBuffer;
			params->repeatLastBuffer = false;
			while (params->txBuffersListHead)
				pthread_cond_wait(&params->txBuffersListEmptyCondition,
						&params->txBuffersListMutex);
			params->repeatLastBuffer = repeatLastBuffer;
		}
	} else if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink,
				"UHDgetTxStreamerQueueTopAndClearQueue:result:error: ");
	pthread_mutex_unlock((pthread_mutex_t*) &params->txBuffersListMutex);
}

void UHDgetTxStreamDeviceAndChannelsAndMaxBufferSize(mlint64 paramsRef) {
	UHDtxStreamerThreadParams_t *params =
			(UHDtxStreamerThreadParams_t*) paramsRef;
	size_t maxNumSamples;
	if (uhd_tx_streamer_max_num_samps(params->txStreamer, &maxNumSamples)
			== UHD_ERROR_NONE) {
		if (!MLPutFunction(stdlink, "List", 3)
				|| !MLPutInteger64(stdlink, (mlint64) (params->usrp))
				|| !MLPutInteger64List(stdlink,
						(mlint64*) (params->streamArguments.channel_list),
						params->streamArguments.n_channels)
				|| !MLPutInteger64(stdlink, maxNumSamples))
			failWithMsgAndError(stdlink, "UHDgetTxFrequency:result:error: ");
	} else
		UHDfailWithError(params->usrp);
}

#define exitTxListModeThreadOnError(r,call) do {\
	(r) = (call); \
	if((r) != UHD_ERROR_NONE) { \
		char errorString[UHD_STRING_BUFFER_SIZE]; \
		uhd_get_last_error(errorString, sizeof(errorString)); \
		fprintf(stderr, "error %d in %s line %d: %s, exiting list-mode thread\n", \
				r, __FILE__, __LINE__, errorString); \
		params->isRunning = 0; \
		return NULL;}\
	} while(0);

typedef struct UHDtxListModeThreadParams {
	uhd_usrp_handle usrp;
	size_t numChannels;
	int *channels;
	double dwellTime;
	bool includeSettingInDwellTime;
	double **frequencies;
	long numFrequencyLists;
	double **gains;
	long numGainLists;
	char **gainNames;
	long numGainNames;
	size_t numSteps;
	bool listModeStep;
	bool triggerModeSingle;
	pthread_t txListModeThread;
	pthread_cond_t triggerCondition;
	pthread_mutex_t triggerMutex;
	bool volatile mayRun;
	bool volatile isRunning;
	size_t volatile currentStep;
	uint64_t volatile numRuns;
	size_t volatile pendingTrigger;
	double blankerWaitTime;
	size_t blankerGPIOmainboard;
	int blankerGPIOmask;
	int blankerGPIOvalue;
	char *blankerGPIObank;
} UHDtxListModeThreadParams_t;

void freeUHDtxListModeThreadParams(UHDtxListModeThreadParams_t *params) {
	if(params) {
		if(params->channels)
			free(params->channels);
		if(params->frequencies) {
			for(int i=0;i<params->numFrequencyLists;i++)
				if(params->frequencies[i])
					free(params->frequencies[i]);
			free(params->frequencies);
		}
		if(params->gains) {
			for(int i=0;i<params->numGainLists;i++)
				if(params->gains[i])
					free(params->gains[i]);
			free(params->gains);
		}
		if(params->gainNames) {
			for(int i=0;i<params->numGainNames;i++)
				if(params->gainNames[i])
					free(params->gainNames[i]);
			free(params->gainNames);
		}
		if(params->blankerGPIObank)
			free(params->blankerGPIObank);
		pthread_mutex_destroy(&params->triggerMutex);
		pthread_cond_destroy(&params->triggerCondition);
		free(params);
	}
}

inline void UHDsetTimevalFromTime(struct timeval &val, double time) {
	val.tv_sec = floor(time);
	val.tv_usec = round((time - val.tv_sec) * 1.e6);
}

#define waitUntil(until,t,mayWait) do {\
		gettimeofday(&t, 0);\
		while((mayWait)&&timercmp(&t,&until,<)) {\
			timersub(&until, &t, &t);\
			usleep((t).tv_sec * 1e6l + (t).tv_usec);\
			gettimeofday(&t, 0);\
		}\
	} while(0);

void* UHDtxListModeThread(void *vParams) {
	UHDtxListModeThreadParams_t *params = (UHDtxListModeThreadParams_t*) vParams;
	if (params->numSteps == 0)
		return NULL;
	params->isRunning = 1;
	uhd_error r;
	params->currentStep = 0;
	params->numRuns = 0;
	uhd_tune_request_t tuneReq = { target_freq : 0, rf_freq_policy
			: UHD_TUNE_REQUEST_POLICY_AUTO, rf_freq : 0, dsp_freq_policy
			: UHD_TUNE_REQUEST_POLICY_MANUAL, dsp_freq : 0 };
	uhd_tune_result_t tuneRes;
	struct timeval t0, t1, t2;
	struct timeval dwellTimeVal, blankerWaitTimeVal;
	UHDsetTimevalFromTime(dwellTimeVal,abs(params->dwellTime));
	UHDsetTimevalFromTime(blankerWaitTimeVal,abs(params->blankerWaitTime));
	double *previousFrequencies = 0, *previousGains = 0;
	if (params->frequencies) {
		previousFrequencies = (double*) calloc(params->numChannels,
				sizeof(double));
		for (size_t channel = 0; channel < params->numChannels; channel++)
			exitTxListModeThreadOnError(r,
					uhd_usrp_get_tx_freq(params->usrp, channel,
							&previousFrequencies[channel]));
	}
	if (params->gains) {
		previousGains = (double*) calloc(params->numChannels, sizeof(double));
		for (size_t channel = 0; channel < params->numChannels; channel++)
			exitTxListModeThreadOnError(r,
					uhd_usrp_get_tx_gain(params->usrp, channel,
							params->gainNames ?
									params->gainNames[channel
											% params->numGainNames] :
									"", &previousGains[channel]));
	}
	while (params->mayRun) {
		if (params->triggerModeSingle
				&& (params->listModeStep || params->currentStep == 0)) {
			pthread_mutex_lock(&params->triggerMutex);
			while (params->pendingTrigger == 0 && params->mayRun)
				pthread_cond_wait(&params->triggerCondition,
						&params->triggerMutex);
			if (params->pendingTrigger>0)
				params->pendingTrigger--;
			pthread_mutex_unlock(&params->triggerMutex);
		}
		if (params->mayRun) {
			if (params->includeSettingInDwellTime)
				gettimeofday(&t0, 0);
			if (params->blankerGPIOmask)
				exitTxListModeThreadOnError(r,
						uhd_usrp_set_gpio_attr(params->usrp,
								params->blankerGPIObank, "OUT",
								params->blankerGPIOvalue,
								params->blankerGPIOmask,
								params->blankerGPIOmainboard));
			if (params->gains)
				for (size_t channel = 0; channel < params->numChannels; channel++)
					if (params->gains[channel % params->numGainLists])
						exitTxListModeThreadOnError(r,
								uhd_usrp_set_tx_gain(params->usrp,
										params->gains[channel % params->numGainLists][params->currentStep],
										params->channels[channel],
										params->gainNames ?
												params->gainNames[channel
														% params->numGainNames] :
												""));
			if (params->frequencies)
				for (size_t channel = 0; channel < params->numChannels; channel++)
					if (params->frequencies[channel % params->numFrequencyLists]) {
						tuneReq.target_freq =
								params->frequencies[channel
										% params->numFrequencyLists][params->currentStep];
						exitTxListModeThreadOnError(r,
								uhd_usrp_set_tx_freq(params->usrp, &tuneReq,
										params->channels[channel], &tuneRes));
					}
			if (!params->includeSettingInDwellTime)
				gettimeofday(&t0, 0);
			if(params->blankerWaitTime>0) {
				timeradd(&t0,&blankerWaitTimeVal,&t2);
				waitUntil(t2,t1,params->mayRun);
			}
			if (params->blankerGPIOmask)
				exitTxListModeThreadOnError(r,
						uhd_usrp_set_gpio_attr(params->usrp,
								params->blankerGPIObank, "OUT",
								~(params->blankerGPIOvalue),
								params->blankerGPIOmask,
								params->blankerGPIOmainboard));
			if (params->mayRun) {
				timeradd(&t0, &dwellTimeVal, &t2);
				waitUntil(t2,t1,params->mayRun);
				params->currentStep++;
				if (params->currentStep >= params->numSteps) {
					params->currentStep = 0;
					params->numRuns++;
				}
			}
		}
	}
	if (previousFrequencies) {
		for (size_t channel = 0; channel < params->numChannels; channel++) {
			tuneReq.target_freq = previousFrequencies[channel];
			exitTxListModeThreadOnError(r,
					uhd_usrp_set_tx_freq(params->usrp, &tuneReq,
							params->channels[channel], &tuneRes));
		}
		free(previousFrequencies);
	}
	if (previousGains) {
		for (size_t channel = 0; channel < params->numChannels; channel++)
			exitTxListModeThreadOnError(r,
					uhd_usrp_set_tx_gain(params->usrp, previousGains[channel],
							params->channels[channel],
							params->gainNames ?
									params->gainNames[channel
											% params->numGainNames] :
									""));
	}
	params->isRunning = 0;
	return NULL;
}

void UHDtriggerTxListModeRun(mlint64 paramsRef) {
	UHDtxListModeThreadParams_t *params =
			(UHDtxListModeThreadParams_t*) paramsRef;
	if (params->isRunning) {
		size_t pendingTrigger;
		pthread_mutex_lock(&params->triggerMutex);
		pendingTrigger = ++(params->pendingTrigger);
		pthread_cond_signal(&params->triggerCondition);
		pthread_mutex_unlock(&params->triggerMutex);
		if (pendingTrigger == 0)
			failWithMsg(stdlink, "pendingTrigger wrapped, all triggers lost");
		else if (!MLPutInteger64(stdlink, pendingTrigger))
			failWithMsgAndError(stdlink, "UHDtriggerTxListModeRun:result:error: ");
	} else
		failWithMsg(stdlink, "tx list mode not running");
}

#define max(a,b) (b)<(a)?(a):(b)

double** UHDgetSingleOrMultipleRealListsOfSameOrZeroLength(long *numLists,
		long *numEntries) {
	long num;
	double *x;
	double **r = 0;
	if (!MLCheckFunction(stdlink, "List", &num)) {
		failWithMsgAndError(stdlink,
				"UHDgetSingleOrMultipleRealListsOfSameLength:outer:error: ");
		*numLists = -1;
		*numEntries = -1;
		return 0;
	}
	if(num==0) {
		*numLists = 0;
		*numEntries = 0;
		return 0;
	}
	switch (MLGetType(stdlink)) {
	case MLTKFUNC:
		*numLists = num;
		*numEntries = 0;
		if (*numLists > 0) {
			r = (double**) calloc(num, sizeof(double*));
			for (int i = 0; i < *numLists; i++) {
				if (!MLGetRealList(stdlink, &x, &num)) {
					failWithMsgAndError(stdlink,
							"UHDgetSingleOrMultipleRealListsOfSameLength:inner:error: ");
					*numLists = -1;
					*numEntries = -1;
					return 0;
				}
				if (num > 0) {
					if (*numEntries == 0)
						*numEntries = num;
					else if (*numEntries != num) {
						for (int j = i - 1; j >= 0; j--)
							if (r[j])
								free(r[j]);
						free(r);
						failWithClearAndMsg(stdlink, "lists of unequal length");
						*numLists = -1;
						*numEntries = -1;
						return 0;
					}
					r[i] = (double*) calloc(num, sizeof(double));
					memcpy(r[i], x, sizeof(double) * num);
				}
				MLReleaseReal64List(stdlink, x, num);
			}
		}
		break;
	case MLTKREAL:
		*numLists = 1;
		*numEntries = num;
		r = (double**) calloc(1, sizeof(double*));
		r[0] = (double*)calloc(num, sizeof(double));
		for (int i = 0; i < num; i++)
			if (!MLGetReal(stdlink, &r[0][i])) {
				free(r[0]);
				free(r);
				failWithMsgAndError(stdlink,
						"UHDgetSingleOrMultipleRealListsOfSameLength:singleList:error: ");
				*numLists = -1;
				*numEntries = -1;
				return 0;
			}
		break;
	default:
		free(r);
		failWithClearAndMsg(stdlink, "invalid input");
		*numLists = -1;
		*numEntries = -1;
		return 0;
	}
	return r;
}

void UHDdisableTxListMode(mlint64 paramsRef) {
	UHDtxListModeThreadParams_t *params =
			(UHDtxListModeThreadParams_t*) paramsRef;
	if (!params) {
		failWithMsg(stdlink, "UHDdisableListMode:param:invalidAddress");
		return;
	}
	if (params->isRunning) {
		params->mayRun = 0;
		pthread_cond_signal(&params->triggerCondition);
		int r = pthread_join(params->txListModeThread, NULL);
		if (r) {
			fprintf(stderr, "list mode thread join failed: %d\n", r);
			exit(r);
		}
	} else {
		params->mayRun = 0;
		fprintf(stderr, "list mode thread not started yet or already dead!\n");
	}
	freeUHDtxListModeThreadParams(params);
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink, "UHDdisableListMode:result:error: ");
}

void UHDenableTxListMode(mlint64 usrpRef, double dwellTime,
		const char *includeSettingInDwellTime, const char *listModeStepBool,
		const char *triggerModeSingleBool, double blankerWaitTime,
		int blankerGPIOmainboard, const char *blankerGPIObank,
		int blankerGPIOmask, int blankerGPIOvalue, int *channels,
		long numChannels) {
	long numFrequencies, numGains;
	const char *str;
	UHDtxListModeThreadParams_t *params = (UHDtxListModeThreadParams_t*) calloc(
			1, sizeof(UHDtxListModeThreadParams_t));
	params->usrp = (uhd_usrp_handle) usrpRef;
	params->dwellTime = dwellTime;
	params->includeSettingInDwellTime = strcmp("True",
			includeSettingInDwellTime) == 0;
	params->listModeStep = strcmp("True", listModeStepBool) == 0;
	params->triggerModeSingle = strcmp("True", triggerModeSingleBool) == 0;
	params->blankerWaitTime = blankerWaitTime;
	params->blankerGPIOmainboard = blankerGPIOmainboard;
	params->blankerGPIOmask = blankerGPIOmask;
	params->blankerGPIOvalue = blankerGPIOvalue;
	params->blankerGPIObank = strdup(blankerGPIObank);
	params->numChannels = numChannels;
	if (numChannels) {
		params->channels = (int*) calloc(numChannels, sizeof(int));
		memcpy(params->channels, channels, sizeof(int) * numChannels);
	}
	params->frequencies = UHDgetSingleOrMultipleRealListsOfSameOrZeroLength(
			&params->numFrequencyLists, &numFrequencies);
	if (numFrequencies < 0) {
		freeUHDtxListModeThreadParams(params);
		return;
	}
	params->gains = UHDgetSingleOrMultipleRealListsOfSameOrZeroLength(
			&params->numGainLists, &numGains);
	if (numGains < 0) {
		freeUHDtxListModeThreadParams(params);
		return;
	}
	switch (MLGetType(stdlink)) {
	case MLTKSTR:
		params->numGainNames = 1;
		params->gainNames = (char**) calloc(1, sizeof(char*));
		if (!MLGetString(stdlink, &str)) {
			failWithMsgAndError(stdlink, "UHDenableListMode:gainName:error: ");
			freeUHDtxListModeThreadParams(params);
			return;
		}
		params->gainNames[0] = strdup(str);
		MLReleaseString(stdlink, str);
		break;
	case MLTKFUNC:
		if (!MLCheckFunction(stdlink, "List", &params->numGainNames)) {
			failWithMsgAndError(stdlink, "UHDenableListMode:gainNames:error: ");
			freeUHDtxListModeThreadParams(params);
			return;
		}
		if (params->numGainNames > 0) {
			params->gainNames = (char**) calloc(params->numGainNames,
					sizeof(char*));
			for (int i = 0; i < params->numGainNames; i++) {
				if (!MLGetString(stdlink, &str)) {
					failWithMsgAndError(stdlink,
							"UHDenableListMode:gainName:error: ");
					freeUHDtxListModeThreadParams(params);
					return;
				}
				params->gainNames[i] = strdup(str);
				MLReleaseString(stdlink, str);
			}
		}
		break;
	default:
		failWithClearAndMsg(stdlink, "invalid gainNames");
		freeUHDtxListModeThreadParams(params);
		return;
	}
	if (numFrequencies > 0 && numGains > 0 && numFrequencies != numGains) {
		failWithMsg(stdlink,
				"non-empty frequency and gain lists have to be of same length");
		freeUHDtxListModeThreadParams(params);
		return;
	}
	params->numSteps = max(numFrequencies, numGains);
	params->isRunning = false;
	params->mayRun = true;
	params->currentStep = 0;
	params->pendingTrigger = 0;
	params->numRuns = 0;
	params->triggerCondition = (pthread_cond_t ) PTHREAD_COND_INITIALIZER;
	params->triggerMutex = (pthread_mutex_t ) PTHREAD_MUTEX_INITIALIZER;
	int r = pthread_create(&params->txListModeThread, NULL, UHDtxListModeThread,
			(void*) params);
	if (r) {
		fprintf(stderr, "list-mode thread creation failed: %d\n", r);
		freeUHDtxListModeThreadParams(params);
		failWithMsg(stdlink,
				"UHDenableListMode:could_not_create_list-mode_thread");
		return;
	}
	if (!MLPutInteger64(stdlink, (mlint64) params))
		UHDdisableTxListMode((mlint64) params);
}

void UHDtxListModeRunning(mlint64 paramsRef) {
	UHDtxListModeThreadParams_t *params =
			(UHDtxListModeThreadParams_t*) paramsRef;
	if (!params) {
		failWithMsg(stdlink, "UHDtxListModeRunning:param:invalidAddress");
		return;
	}
	if (!MLPutSymbol(stdlink, params->isRunning ? "True" : "False"))
		failWithMsgAndError(stdlink, "UHDtxListModeRunning:result:error: ");
}

void UHDgetTxListModeCurrentIndex(mlint64 paramsRef) {
	UHDtxListModeThreadParams_t *params =
			(UHDtxListModeThreadParams_t*) paramsRef;
	if (!params) {
		failWithMsg(stdlink, "UHDgetListModeCurrentIndex:param:invalidAddress");
		return;
	}
	if (!MLPutInteger64(stdlink, params->currentStep))
		failWithMsgAndError(stdlink, "UHDgetListModeCurrentIndex:result:error: ");
}

void UHDgetTxListModeChannels(mlint64 paramsRef) {
	UHDtxListModeThreadParams_t *params =
			(UHDtxListModeThreadParams_t*) paramsRef;
	if (!params) {
		failWithMsg(stdlink, "UHDgetListModeChannels:param:invalidAddress");
		return;
	}
	if (!MLPutIntegerList(stdlink, params->channels,params->numChannels))
		failWithMsgAndError(stdlink, "UHDgetListModeChannels:result:error: ");
}

void UHDsetMIMO(mlint64 usrpRef, int mBoard, const char *mimo) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	if (uhd_usrp_set_mimo(usrp, strcmp(mimo, "True") == 0, mBoard)
			== UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "UHDsetMIMO:result:error: ");
	} else
		UHDfailWithError(usrp);
}

void UHDgetMIMO(mlint64 usrpRef, int mBoard) {
	uhd_usrp_handle usrp = (uhd_usrp_handle) usrpRef;
	bool mimo;
	if (uhd_usrp_get_mimo(usrp, mBoard, &mimo) == UHD_ERROR_NONE) {
		if (!MLPutSymbol(stdlink, mimo ? "True" : "False"))
			failWithMsgAndError(stdlink, "UHDgetMIMO:result:error: ");
	} else
		UHDfailWithError(usrp);
}

int main(int argc, char **argv) {
	sargc = argc;
	sargv = argv;
	return MLMain(argc, argv);
}
