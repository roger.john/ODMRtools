ClearAll[ClearWidefieldOdmrFitData,ReadWidefieldOdmrData,
	CreateBfieldFitFunction,CreateWidefieldOdmrFitData,
	CreateWidefieldOdmrImages, SetStandAloneTexFigure,
	InvestigateWidefieldOdmrImages];
Get[FileNameJoin[{HomeDirectory[], "git", "ODMRtools", 
   "ODMRlib.m"}]]; Get[
 FileNameJoin[{HomeDirectory[], "git", "Academic", "SpinMechanics", 
   "SpinMechanics.m"}]];
Get[FileNameJoin[{HomeDirectory[], "git", "Academic", 
    "ImageConstructionFunctions.m"}]];
Options[ReadWidefieldOdmrData]={DataType->"UnsignedInteger32"};
ReadWidefieldOdmrData[file_String,options:OptionsPattern[]]:=
	Module[{f=OpenRead[file,BinaryFormat->True],l,data,
			dataType=OptionValue[DataType]},
		data=If[Length[#[[2]]]>0,#[[2,1]],{}]&@
		Reap@While[(l=BinaryRead[f,dataType])=!=
			EndOfFile, Sow[BinaryReadList[f,dataType,l]]];
	Close[f];
	data];
Options[CreateBfieldFitFunction] = {BfieldEstimate -> 
    4.6*^-3 Normalize[{1, 0.9, 1.1}], WidthEstimate -> 10.*^6, 
   ContrastEstimate -> 0.01, ZeroFieldSplitting -> 2.87*^9, 
   GFactor -> -2.003*ElectronMagneticMoment/PlanckConstant*Tesla*
     Second, MinBackground -> 0};
CreateBfieldFitFunction[OptionsPattern[]] := 
  Module[{Bestimate = OptionValue[BfieldEstimate], 
    wEstimage = OptionValue[WidthEstimate], 
    cEstimate = OptionValue[ContrastEstimate], 
    Dgs = OptionValue[ZeroFieldSplitting], \[Gamma] = 
     OptionValue[GFactor], minBackground = OptionValue[MinBackground],
     f, w, bg, Bx, By, Bz, c, 
    basis = {{0, 1, 1}/2, {1, 0, 1}/2, {1, 1, 0}/2}, 
    offset = {1, 1, 1}/4, vertex, neighbours},
   vertex = Function[x, x[[1 ;; 3]].basis + x[[4]] offset];
   neighbours = 
    Function[
     x, (x + (-1)^x[[4]] Append[-#, 1]) & /@ 
      Prepend[IdentityMatrix[3], {0, 0, 0}]]; 
   Normalize[vertex[#]] & /@ neighbours[{0, 0, 0, 0}] // 
       Function[{Bz, 
             Bp}, {Dgs + (Bp^2 Dgs \[Gamma]^2)/(Dgs^2 - 
                 Bz^2 \[Gamma]^2) + \[Gamma] (-Bz + (Bp^2 \
\[Gamma])/(2 Dgs - 2 Bz \[Gamma])), 
             Dgs + (Bp^2 Dgs \[Gamma]^2)/(Dgs^2 - 
                 Bz^2 \[Gamma]^2) + \[Gamma] (Bz + (Bp^2 \[Gamma])/(2 \
Dgs + 2 Bz \[Gamma]))}][#.{Bx, By, Bz}, 
           Sqrt[Bx^2 + By^2 + Bz^2 - (#.{Bx, By, Bz})^2]] & /@ # & // 
      Flatten // bg (1 - Total[c/((f - #)^2/w^2 + 1) & /@ #]) & // 
    Function[{Bx0, By0, Bz0, w0, c0, minBg}, 
       Function[data, 
        Function[m, 
          If[m >= minBg, 
           NonlinearModelFit[
            data, #, {{Bx, Bx0}, {By, By0}, {Bz, Bz0}, {w, w0}, {c, 
              c0}, {bg, m}}, f]]][Mean[(#1[[2]] &) /@ data]]]][
      Bestimate[[1]], Bestimate[[2]], Bestimate[[3]], wEstimage, 
      cEstimate, minBackground] &];
Options[FitBfieldAgainstSpectrum] = 
  Join[Options[CreateBfieldFitFunction], {}];
FitBfieldAgainstSpectrum[spectrum:{{_,_}..},o:OptionsPattern[]]:=
	Join[ExtractRelevantParametersFromFit[#], {Histogram[#, {.05}], 
        PearsonChiSquareTest[#]} &@#["StandardizedResiduals"]] &@
   CreateBfieldFitFunction[o][spectrum];
Options[CreateWidefieldOdmrImages] = {PlotWidth -> 140 mm, 
   PlotOffset -> {20, 20} mm, ColorBarWidth -> 15 mm, 
   CanvasSize -> {200, 170} mm, LengthScale -> 195/1024, 
   MajorTickSize -> 5 mm,
   NormImageRangeQuantiles -> {0.1, 0.9},
   WidthImageRangeQuantiles -> {0.1, 0.9},
   XimageRangeQuantiles -> {0.1, 0.9},
   YimageRangeQuantiles -> {0.1, 0.9},
   ZimageRangeQuantiles -> {0.1, 0.9},
   ThetaImageRangeQuantiles -> {0.1, 0.9}, 
   ContrastImageRangeQuantiles -> {0.1, 0.9},
   BackgroundImageRangeQuantiles -> {0.1, 0.9}, 
   FrameLabelSpacings -> {{15, 15}, {12, 0}} mm, 
   FrameTickLabelSpacings -> {{1, 1}, {1, 1}} mm, 
   ImageFrameLabel -> {"$\\xi\\si{\\per \\micro m}$", 
     "$\\psi\\si{\\per \\micro m}$"},
   OutputFilePrefix :> NotebookDirectory[]};
CreateWidefieldOdmrImages[suffix_String, OptionsPattern[]] := 
  Module[{set = 
     Function[{var, value}, ClearAll @@ {var <> suffix}; 
      Set[#, value] &@Symbol[var <> suffix]], 
    get = Function[var, Symbol[var <> suffix]], action},
   Monitor[
    action = "calculating image ranges";
    If[Dimensions@get["spectralImageData"] =!= {get["imageHeight"], 
       get["imageWidth"], 6}, 
     Return[$Failed["invalid spectral image data"]]];
    Function[data, 
      Function[{imageName, imageFunction, imageRangeQuantiles}, 
          set[imageName <> "Range", 
           Quantile[imageFunction /@ data, 
            imageRangeQuantiles, {{0, 0}, {0, 
              1}}]]] @@ # & /@ {{"normImage", Norm[#[[1 ;; 3]]] &, 
         OptionValue[NormImageRangeQuantiles]},
        {"widthImage", Abs[#[[4]]] &, 
         OptionValue[WidthImageRangeQuantiles]}, {"xImage", #[[1]] &, 
         OptionValue[XimageRangeQuantiles]},
        {"yImage", #[[2]] &, OptionValue[YimageRangeQuantiles]},
        {"zImage", #[[3]] &, OptionValue[ZimageRangeQuantiles]},
        {"thetaImage", 
         If[{#1, #2} == {0, 0}, 0, 180/\[Pi]*ArcTan[##]] &[
           Norm[#[[1 ;; 2]]], #[[3]]] &, 
         OptionValue[ThetaImageRangeQuantiles]},
        {"contrastImage", #[[5]] &,
		OptionValue[ContrastImageRangeQuantiles]},
        {"backgroundImage", #[[6]] &,
		OptionValue[BackgroundImageRangeQuantiles]}}]@
     DeleteCases[
      Flatten[get["spectralImageData"], 1], {0, 0, 0, 0, 0, 0}];
    Function[{imageName, valueScale, unit},
        action = "rendering " <> imageName;
        Module[{range = get[imageName <> "Range"], 
          scale = OptionValue[LengthScale], 
          plotSize = {1, 
             get["imageHeight"]/get["imageWidth"]} OptionValue[
             PlotWidth], barWidth = OptionValue[ColorBarWidth], 
          majorTickSize = OptionValue[MajorTickSize], 
          plotOffset = OptionValue[PlotOffset], 
          canvasSize = OptionValue[CanvasSize], 
          outputFilePrefix = OptionValue[OutputFilePrefix]},
         set[imageName <> "Rendered", #] &@
          PlotComposition[{{Show[
              ImageApply[List @@ ColorData["SunsetColors"][#] &, #] &@
               ImageAdjust[get[imageName], {0, 0}, range], 
              Frame -> True, 
              PlotRange -> {{0, get["imageWidth"]}, {0, 
                 get["imageHeight"]}}, 
              FrameLabel -> OptionValue[ImageFrameLabel], 
              
              FrameTicks -> (MapAt[#/scale &, #, 
                    1] & /@ # & /@ # & /@ {{CustomTicks[{0, 
                    get["imageHeight"]}*scale, 
                    MajorTickLength -> {0, 
                    majorTickSize/
                    plotSize[[1]]}], {}}, {CustomTicks[{0, 
                    get["imageWidth"]}*scale, 
                    MajorTickLength -> {0, 
                    majorTickSize/plotSize[[2]]}], 
                   CustomTicks[{0, get["imageWidth"]}*scale, 
                    MajorTickLength -> {0, 
                    majorTickSize/plotSize[[2]]}, 
                    MajorTickLabelFunction -> ("" &)]}})], plotOffset,
              plotSize}, {Show[
              ConfocalImageColorLegend[range/valueScale, 
               BarLabel -> 
                StringSwitch[imageName, "normImage", 
                 "$\\text{absolute magnetic flux density}\\si{\\per " \
<> unit <> "}$", ("x" | "y" | "z") ~~ "Image", 
                 "$\\text{magnetic flux density in $" <> 
                  First@Characters[imageName] <> 
                  "$-direction}\\si{\\per " <> unit <> "}$", 
                 "thetaImage", 
                 "$\\text{magnetic flux $z$ tilt}/\\degree$", 
                 "widthImage", 
                 "$\\text{NV linewidth}\\si{\\per " <> unit <> "}$",
		"contrastImage", 
                 "$\\text{contrast}\\si{\\per " <> unit <> "}$",
		"backgroundImage", 
                 "$\\text{background}\\si{\\per " <> unit <> "}$"]],
               FrameTicks -> {{None, 
                 CustomTicks[range/valueScale, 
                  
                  MajorTickLength -> {0, 
                    majorTickSize/barWidth}]}, {None, 
                 None}}], {plotSize[[1]], 0} + plotOffset, {barWidth, 
              plotSize[[2]]}}}, canvasSize, 
           FrameLabelSpacings -> OptionValue[FrameLabelSpacings], 
           FrameTickLabelSpacings -> 
            OptionValue[FrameTickLabelSpacings], 
           FrameLabelPositions -> {{1/2, 1/2}, {1/2, 0}}];
         Print[get[imageName <> "Rendered"]];
         action = "saving rendered " <> imageName;
         SavePlotComposition[get[imageName <> "Rendered"], 
          outputFilePrefix <> suffix <> "_" <> imageName <> "_tex"];
         action = "setting rendered " <> imageName;
         SetStandAloneTexFigure[
          outputFilePrefix <> suffix <> "_" <> imageName <> 
           "_tex.tex", canvasSize[[1]], canvasSize[[2]], 0, 0];
         Print[outputFilePrefix <> suffix <> "_" <> imageName <> ""];
         ]] @@ # & /@ {
	{"normImage", 1*^-3, "mT"},
	{"widthImage", 1*^6, "MHz"}, {"xImage", 1*^-3, "mT"},
	{"yImage", 1*^-3, "mT"},
	{"zImage", 1*^-3, "mT"},
	{"thetaImage", 1, "\[Degree]"},
	{"contrastImage", 0.01, "\\%"},
	{"backgroundImage", 1*^3, "kCounts"}};
    , action]];
ClearWidefieldOdmrFitData[suffix_String] := 
  ClearAll @@ (# <> suffix & /@ 
     Join[{"odmrImages", "odmrSpectra", "imageWidth", "imageHeight", 
       "frequencies", "minBackground", "imageData", "image", "fitBfieldAtPoint",
       "spectralImageData", "normImage", "widthImage", "xImage", 
       "yImage", "zImage", "thetaImage", "contrastImage", "backgroundImage",
	"normImageRange", "widthImageRange", "xImageRange", "yImageRange",
	"zImageRange", "thetaImageRange", "contrastImageRange",
	"backgroundImageRange",	"normImageRendered", "widthImageRendered", 
       "xImageRendered", "yImageRendered", "zImageRendered", 
       "thetaImageRendered", "contrastImageRendered", "backgroundImageRendered"}, 
      ToString[#[[1]]] & /@ Options[CreateBfieldFitFunction]]);
Options[CreateWidefieldOdmrFitData] = 
  Join[{MinBackground :> (Print[
			"please investigate the data and return the minimum background \
applicable for magnetic image fitting"];Dialog[]),
		ImageSize->120*96/25.4,
		MagneticImageFileSuffix->"magneticImage",
		RestoreWrittenFitData->False,PreviewImages->False},
	Options[ReadWidefieldOdmrData],
	Options[FitBfieldAgainstSpectrum],
	Options[CreateWidefieldOdmrImages]];
CreateWidefieldOdmrFitData[file_String, suffix_String, 
   imageDimensions : {_Integer?Positive, _Integer?Positive}, 
   frequencies_List, o : OptionsPattern[]] := 
  Module[{set = 
     Function[{var, value}, ClearAll @@ {var <> suffix}; 
      Set[#, value] &@Symbol[var <> suffix]], 
    get = Function[var, Symbol[var <> suffix]], action, 
    saveFile = 
     file <> "_" <> OptionValue[MagneticImageFileSuffix] <> ".m", 
    abort = False},
   Monitor[
    action = "clearing variables";
    ClearWidefieldOdmrFitData[suffix];
    action = "setting parameters";
    set["imageWidth", imageDimensions[[1]]];
    set["imageHeight", imageDimensions[[2]]];
    set["frequencies", frequencies];
    If[#[[1]] =!= MinBackground, 
       set[ToString@#[[1]], OptionValue[#[[1]]]]] & /@ 
     Options[CreateBfieldFitFunction];
    action = "loading odmr data";
    set["odmrImages", ReadWidefieldOdmrData[file,
			Sequence@@FilterRules[{o},Options@ReadWidefieldOdmrData]]];
    If[Dimensions[get["odmrImages"]] != {Length[frequencies], 
       Times @@ imageDimensions}, 
     Return[$Failed@@{"dimensions mismatch", 
       Dimensions[get["odmrImages"]], {Length[frequencies], 
        Times @@ imageDimensions}}]];
    action = "transposing data";
    set["odmrSpectra", Transpose@get["odmrImages"]];
    action = "creating mean image";
    set["imageData", 
     Partition[Mean /@ get["odmrSpectra"], get["imageWidth"]]];
    action = "rendering mean image";
    set["image", 
     ImageApply[List @@ ColorData["SunsetColors"][#] &, #] &@
      ImageAdjust@Image@get["imageData"]];
    Print[get["image"]];
    action = "creating sum spectrum";
    set["sumSpectrum", 
     ListLinePlot[Transpose[{get["frequencies"], #}], 
        PlotRange -> All, ImageSize -> 120*96/25.4] &@
      Map[Mean, get["odmrImages"]]];
    Print[get["sumSpectrum"]];
	action = "creating spectrum plot function";
	set["plotSpectrumAtPoint",Function[
		ListLinePlot[Transpose[{frequencies,
				Symbol["odmrSpectra"<>suffix][[
					#1+(imageDimensions[[2]]-#2)*imageDimensions[[1]]]]}],
			##3,PlotRange->All]]];
	Print[
		"spectrum plot function available as plotSpectrumAtPoint"<>
			suffix<>"[x,y]"];
	action="creating manual fit function";
	set["fitBfieldAtPoint",Function[
		FitBfieldAgainstSpectrum[
			Transpose[{frequencies,Symbol["odmrSpectra"<>suffix][[
				#1+(imageDimensions[[2]]-#2)*imageDimensions[[1]]]]}],##3, 
			Sequence@@FilterRules[{o},Options@FitBfieldAgainstSpectrum]]]];
    Print[
     "manual fit function available as fitBfieldAtPoint" <> suffix <> 
      "[x,y]"];
    If[OptionValue[RestoreWrittenFitData] === True,
     action = "restoring saved fitting data";
     set["spectralImageData", Get[saveFile]];
     ,
     action = "setting minBackground";
     set["MinBackground", OptionValue[MinBackground]];
     If[get["MinBackground"] === $Aborted, Return[$Aborted]];
     Print[
      "taking minBackground as " <> ToString[get["MinBackground"]]];
     action = "performing magnetic image fitting";
     set["spectralImageData", (Print[
          "magnetic image fitting took " <> ToString[#[[1]]] <> 
           "s"]; #[[2]]) &@
       AbsoluteTiming@
        Module[{n = 0, 
          fitFunction = 
           CreateBfieldFitFunction[
            Sequence @@ 
             Join[Rule[#[[1]], get[ToString[#[[1]]]]] & /@ 
               Options[CreateBfieldFitFunction], 
              FilterRules[{o}, Options@FitBfieldAgainstSpectrum]]]}, 
         SetSharedVariable[n];
         DistributeDefinitions[fitFunction, frequencies]; 
         Monitor[
           ParallelMap[
            If[! abort, 
              Module[{line}, 
               line = Function[data, 
                  Quiet[If[# =!= 
                    Null, #["BestFitParameters"][[All, 2]], {0, 0, 0, 
                    0, 0, 0}] &@
                    fitFunction[
                    Transpose[{frequencies, data}]]]] /@ #; n++; 
               line]] &, 
            Partition[get["odmrSpectra"], 
             imageDimensions[[1]]]], {Button["abort", abort = True], 
            n, ProgressIndicator[n/imageDimensions[[1]]]}] // 
          If[abort, Return[$Aborted], #] &]];
     action = "saving spectral image as " <> saveFile;
     Put[get["spectralImageData"], saveFile]];
    Function[{imageName, imageFunction},
        action = "creating " <> imageName;
        set[imageName, 
         Image@Map[imageFunction, get["spectralImageData"], {2}]]; 
        If[OptionValue[PreviewImages] === True, 
         Print[ImageAdjust@get[imageName]]];
        ] @@ # & /@ {
			{"normImage",Norm[#[[1;;3]]]&},
			{"widthImage",Abs[#[[4]]]&},
			{"xImage",#[[1]]&},
			{"yImage",#[[2]]&},
			{"zImage",#[[3]]&},
			{"thetaImage",If[{#1,#2}=={0,0},0,180/\[Pi]*ArcTan[##]]&[
				Norm[#[[1;;2]]],#[[3]]]&},
			{"contrastImage",#[[5]]&},
			{"backgroundImage",#[[6]]&}};
    , action]//If[MatchQ[#,$Aborted|$Failed|$Aborted[___]|$Failed[___]],#,
		CreateWidefieldOdmrImages[suffix, Sequence @@ 
			FilterRules[{o}, Options@CreateWidefieldOdmrImages]]]&
   ];
Options[InvestigateWidefieldOdmrImages]=
	{ImageSize->240*96/25.4,ImageQuantiles->{0.1,0.9},
		ImagesToDisplay->{"image","normImage","widthImage","xImage","yImage",
			"zImage","thetaImage","contrastImage","backgroundImage"}};
InvestigateWidefieldOdmrImages[suffix_String,OptionsPattern[]]:=
	DynamicModule[{imageName,imageSize=OptionValue[ImageSize],
		p=Round[ImageDimensions[Symbol["image"<>suffix]]/2],
		imageRange={1,#}&/@ImageDimensions[Symbol["image"<>suffix]],
		imageWidth=Symbol["imageWidth"<>suffix],
		imageHeight=Symbol["imageHeight"<>suffix],
		showFit=False,
		imageLowQuantile=OptionValue[ImageQuantiles][[1]],
		imageHighQuantile=OptionValue[ImageQuantiles][[2]],
		imageNames=Select[OptionValue[ImagesToDisplay],
			Head@Symbol[#<>suffix]===Image&]}, 
	Identity[{Dynamic@{SetterBar[Dynamic@imageName,imageNames], 
			Slider[Dynamic@imageLowQuantile],
			Slider[Dynamic@imageHighQuantile],
			p,Checkbox[Dynamic@showFit],"fit"}, 
		ClickPane[
			Dynamic@Show[ImageAdjust[#,{0, 0},
					Quantile[Flatten@ImageData[#],
						{imageLowQuantile,imageHighQuantile},{{0,0},{0,1}}]]&
					@If[imageName!="image",Symbol[imageName<>suffix],
						Image@Symbol["imageData" <> suffix]], 
				Graphics[{Green,
					Line[{{#1,#2[[1]]},{#1,#2[[2]]}}]&[p[[1]],imageRange[[2]]], 
					Line[{{#2[[1]],#1},{#2[[2]],#1}}]&[p[[2]],
						imageRange[[1]]]}], 
				ImageSize->imageSize],(p=Round[#])&], 
		Dynamic@If[showFit,Quiet[Symbol["fitBfieldAtPoint"<>suffix]@@p], 
			Symbol["plotSpectrumAtPoint"<>suffix]@@Append[p,
				ImageSize->imageSize]]}
]];
