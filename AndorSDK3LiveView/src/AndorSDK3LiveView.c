/*
 ============================================================================
 Name        : AndorSDK3LiveView.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <wchar.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <argp.h>
#include <atcore.h>
#include <math.h>
#include <stdint.h>

#define max(a,b) (a<b?b:a)
#define min(a,b) (b<a?b:a)
#define clip(x,a,b) (x<a?a:x>b?b:x)
#define NUM_BUFFERS 3
#define MEM_ALIGN_BOUNDARY 8
#define MIN_TIMEOUT 5
#define DEFAULT_STRING_LENGTH 64
#define NO_COOLING 1000

const char *argp_program_version = "AndorSDK3LiveView 1.0";
const char *argp_program_bug_address = "<roger.john@uni-leipzig>";

static char doc[] =
		"AndorSDK3LiveView - put live image of Andor camera on screen";

static struct argp_option options[] = { { "verbose", 'v', 0, 0,
		"Produce verbose output", 2 }, { "quiet", 'q', 0, 0,
		"Don't produce any output", 2 },
		{ "silent", 's', 0, OPTION_ALIAS, 0, 2 }, { "fullscreen", 'f', 0,
		OPTION_ARG_OPTIONAL, "show image in fullscreen", 1 }, { "noFullscreen",
				'n', 0,
				OPTION_ARG_OPTIONAL, "show image in window", 1 }, { "deviceId",
				'd', "deviceId",
				OPTION_ARG_OPTIONAL, "device id of camera", 0 }, {
				"exposureTime", 'e', "exposureTime", OPTION_ARG_OPTIONAL,
				"exposure time for individual images", 0 }, { "binning", 'b',
				"binning", OPTION_ARG_OPTIONAL,
				"number of pixels to be binned (1, 2, 3, 4 or 8)", 0 }, {
				"width", 'w', "width",
				OPTION_ARG_OPTIONAL, "width of imaging area", 0 }, { "height",
				'h', "height",
				OPTION_ARG_OPTIONAL, "height of imaging area", 0 }, { "xOff",
				'x', "xOff",
				OPTION_ARG_OPTIONAL, "x offset imaging area", 0 }, { "yOff",
				'y', "yOff",
				OPTION_ARG_OPTIONAL, "y offset of imaging area", 0 }, {
				"temperature", 't', "temperature",
				OPTION_ARG_OPTIONAL, "cooler temperature to set", 0 }, { "cool",
				'c', 0, 0, "enable cooler", 0 }, { "nocool", NO_COOLING, 0, 0,
				"disable cooler", 0 }, { 0 } };

typedef struct {
	int silent, verbose, fullscreen, cool;
	int deviceId;
	double exposureTime, temperature;
	int binning, width, height, x, y;
} arguments;

void fillArgumentsWidthDefaultValues(arguments *arguments) {
	arguments->silent = 0;
	arguments->verbose = 0;
	arguments->fullscreen = 0;
	arguments->deviceId = 0;
	arguments->cool = 0;
	arguments->binning = 1;
	arguments->exposureTime = 1.;
	arguments->width = 0;
	arguments->height = 0;
	arguments->x = 1;
	arguments->y = 1;
	arguments->temperature = 0;
}

static error_t parse_opt(int key, char *arg, struct argp_state *state) {
	arguments *arguments = state->input;
	switch (key) {
	case 'q':
	case 's':
		arguments->silent = 1;
		break;
	case 'v':
		arguments->verbose = 1;
		break;
	case 'f':
		arguments->fullscreen = 1;
		break;
	case 'n':
		arguments->fullscreen = 0;
		break;
	case 'c':
		arguments->cool = 1;
		break;
	case NO_COOLING:
		arguments->cool = 0;
		break;
	case 'd':
		if (arg)
			arguments->deviceId = atoi(arg);
		else
			argp_usage(state);
		break;
	case 'b':
		if (arg)
			arguments->binning = atoi(arg);
		else
			argp_usage(state);
		if (arguments->binning != 1 && arguments->binning != 2
				&& arguments->binning != 3 && arguments->binning != 4
				&& arguments->binning != 8)
			argp_usage(state);
		break;
	case 'x':
		if (arg)
			arguments->x = atoi(arg);
		else
			argp_usage(state);
		break;
	case 'y':
		if (arg)
			arguments->y = atoi(arg);
		else
			argp_usage(state);
		break;
	case 'w':
		if (arg)
			arguments->width = atoi(arg);
		else
			argp_usage(state);
		break;
	case 'h':
		if (arg)
			arguments->height = atoi(arg);
		else
			argp_usage(state);
		break;
	case 'e':
		if (arg)
			arguments->exposureTime = atof(arg);
		else
			argp_usage(state);
		break;
	case 't':
		if (arg)
			arguments->temperature = atof(arg);
		else
			argp_usage(state);
		break;
	case ARGP_KEY_ARG:
		if (state->arg_num >= 0)
			/* Too many arguments. */
			argp_usage(state);
		break;
	case ARGP_KEY_END:
		if (state->arg_num < 0)
			/* Not enough arguments. */
			argp_usage(state);
		break;
	default:
		return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

static struct argp argp = { options, parse_opt, 0, doc };

static gboolean delete_event(GtkWidget *widget, GdkEvent *event, gpointer data) {
	return FALSE;
}

/* Another callback */
static void destroy(GtkWidget *widget, gpointer data) {
	gtk_main_quit();
}

GtkWidget *createWindow(arguments *arguments, GtkWidget *image) {
	char windowTitle[1024];
	GtkWidget *window;
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	sprintf(windowTitle, "AndorSDK3LiveView on camera %d", arguments->deviceId);
	gtk_window_set_title(GTK_WINDOW(window), windowTitle);
	g_signal_connect(window, "delete-event", G_CALLBACK (delete_event), NULL);
	g_signal_connect(window, "destroy", G_CALLBACK (destroy), NULL);
	gtk_container_set_border_width(GTK_CONTAINER(window), 0);
	gtk_container_add(GTK_CONTAINER(window), image);
	gtk_window_set_hide_titlebar_when_maximized(GTK_WINDOW(window), TRUE);
	if (arguments->fullscreen) {
		gtk_window_set_decorated(GTK_WINDOW(window), FALSE);
		gtk_window_fullscreen(GTK_WINDOW(window));
	}
	gtk_widget_show(image);
	return window;
}

typedef struct {
	int code;
	const char *name;
} AndorSDKerrorCodeName;

AndorSDKerrorCodeName AndorSDK3ErrorCodeNames[] = {
		{ AT_SUCCESS, "AT_SUCCESS" }, { AT_ERR_NOTINITIALISED,
				"AT_ERR_NOTINITIALISED" }, {
		AT_ERR_NOTIMPLEMENTED, "AT_ERR_NOTIMPLEMENTED" }, { AT_ERR_READONLY,
				"AT_ERR_READONLY" },
		{ AT_ERR_NOTREADABLE, "AT_ERR_NOTREADABLE" }, {
		AT_ERR_NOTWRITABLE, "AT_ERR_NOTWRITABLE" }, { AT_ERR_OUTOFRANGE,
				"AT_ERR_OUTOFRANGE" }, { AT_ERR_INDEXNOTAVAILABLE,
				"AT_ERR_INDEXNOTAVAILABLE" }, { AT_ERR_INDEXNOTIMPLEMENTED,
				"AT_ERR_INDEXNOTIMPLEMENTED" },
		{ AT_ERR_EXCEEDEDMAXSTRINGLENGTH, "AT_ERR_EXCEEDEDMAXSTRINGLENGTH" }, {
		AT_ERR_CONNECTION, "AT_ERR_CONNECTION" }, { AT_ERR_NODATA,
				"AT_ERR_NODATA" }, {
		AT_ERR_INVALIDHANDLE, "AT_ERR_INVALIDHANDLE" }, { AT_ERR_TIMEDOUT,
				"AT_ERR_TIMEDOUT" }, { AT_ERR_BUFFERFULL, "AT_ERR_BUFFERFULL" },
		{
		AT_ERR_INVALIDSIZE, "AT_ERR_INVALIDSIZE" }, { AT_ERR_INVALIDALIGNMENT,
				"AT_ERR_INVALIDALIGNMENT" }, { AT_ERR_COMM, "AT_ERR_COMM" }, {
		AT_ERR_STRINGNOTAVAILABLE, "AT_ERR_STRINGNOTAVAILABLE" }, {
		AT_ERR_STRINGNOTIMPLEMENTED, "AT_ERR_STRINGNOTIMPLEMENTED" }, {
		AT_ERR_NULL_FEATURE, "AT_ERR_NULL_FEATURE" }, { AT_ERR_NULL_HANDLE,
				"AT_ERR_NULL_HANDLE" }, { AT_ERR_NULL_IMPLEMENTED_VAR,
				"AT_ERR_NULL_IMPLEMENTED_VAR" }, { AT_ERR_NULL_READABLE_VAR,
				"AT_ERR_NULL_READABLE_VAR" }, { AT_ERR_NULL_READONLY_VAR,
				"AT_ERR_NULL_READONLY_VAR" }, { AT_ERR_NULL_WRITABLE_VAR,
				"AT_ERR_NULL_WRITABLE_VAR" }, { AT_ERR_NULL_MINVALUE,
				"AT_ERR_NULL_MINVALUE" }, { AT_ERR_NULL_MAXVALUE,
				"AT_ERR_NULL_MAXVALUE" }, { AT_ERR_NULL_VALUE,
				"AT_ERR_NULL_VALUE" }, { AT_ERR_NULL_STRING,
				"AT_ERR_NULL_STRING" }, { AT_ERR_NULL_COUNT_VAR,
				"AT_ERR_NULL_COUNT_VAR" }, { AT_ERR_NULL_ISAVAILABLE_VAR,
				"AT_ERR_NULL_ISAVAILABLE_VAR" }, { AT_ERR_NULL_MAXSTRINGLENGTH,
				"AT_ERR_NULL_MAXSTRINGLENGTH" }, { AT_ERR_NULL_EVCALLBACK,
				"AT_ERR_NULL_EVCALLBACK" }, { AT_ERR_NULL_QUEUE_PTR,
				"AT_ERR_NULL_QUEUE_PTR" }, { AT_ERR_NULL_WAIT_PTR,
				"AT_ERR_NULL_WAIT_PTR" }, { AT_ERR_NULL_PTRSIZE,
				"AT_ERR_NULL_PTRSIZE" }, { AT_ERR_NOMEMORY, "AT_ERR_NOMEMORY" },
		{ AT_ERR_DEVICEINUSE, "AT_ERR_DEVICEINUSE" }, { AT_ERR_DEVICENOTFOUND,
				"AT_ERR_DEVICENOTFOUND" } };

const char *AndorSDK3ErrorName(int errorCode) {
	int n = sizeof(AndorSDK3ErrorCodeNames) / sizeof(AndorSDKerrorCodeName);
	int i = 0;
	while (i < n && errorCode != AndorSDK3ErrorCodeNames[i].code)
		i++;
	if (i < n)
		return AndorSDK3ErrorCodeNames[i].name;
	else
		return "unknown error";
}

#define exitOnError(r,call) (r) = (call); \
	if((r) != AT_SUCCESS) { \
		fprintf(stderr, "error %d in %s line %d: %s\n", r, __FILE__, __LINE__, AndorSDK3ErrorName(r)); \
		exit(r); }

AT_H openCamera(arguments *arguments) {
	AT_H camera = AT_HANDLE_UNINITIALISED;
	AT_64 deviceCount;
	int r;
	exitOnError(r, AT_GetInt(AT_HANDLE_SYSTEM, L"DeviceCount",&deviceCount));
	if (!arguments->silent)
		printf("%lld devices available\n", deviceCount);
	if (arguments->verbose)
		printf("opening device %d...", arguments->deviceId);
	exitOnError(r, AT_Open(arguments->deviceId, &camera));
	if (arguments->verbose) {
		AT_WC name[DEFAULT_STRING_LENGTH], firmware[DEFAULT_STRING_LENGTH];
		exitOnError(r,
				AT_GetString(camera, L"CameraModel", name, DEFAULT_STRING_LENGTH));
		exitOnError(r,
				AT_GetString(camera, L"FirmwareVersion", firmware, DEFAULT_STRING_LENGTH));
		printf("%ls (%ls)\n", name, firmware);
	}
	return camera;
}

void configureCamera(AT_H camera, arguments *arguments) {
	int r;
	AT_64 maxWidth, maxHeight;
	AT_WC binning[DEFAULT_STRING_LENGTH];
	AT_BOOL fullAIOControl;
	swprintf(binning, DEFAULT_STRING_LENGTH, L"%dx%d", arguments->binning,
			arguments->binning);
	if (arguments->verbose)
		printf("setting binning to %ls\n", binning);
	exitOnError(r, AT_SetEnumString(camera, L"AOIBinning", binning));
	if (arguments->verbose)
		printf("setting exposure time to %f...", arguments->exposureTime);
	exitOnError(r,
			AT_SetFloat(camera, L"ExposureTime", arguments->exposureTime));
	if (arguments->verbose) {
		double actualExposureTime;
		exitOnError(r,
				AT_GetFloat(camera, L"ExposureTime", &actualExposureTime));
		printf("actual exposure time is %f\n", arguments->exposureTime);
	} else
		printf("\n");
	if (arguments->verbose)
		printf(
				"setting preamp gain control to 16-bit (low noise & high well capacity)\n");
	exitOnError(r,
			AT_SetEnumString(camera, L"SimplePreAmpGainControl",
					L"16-bit (low noise & high well capacity)"));
	exitOnError(r, AT_GetBool(camera, L"FullAOIControl", &fullAIOControl));
	if (!arguments->silent && !fullAIOControl)
		printf("camera does not have full AIOControl!\n");
	exitOnError(r, AT_GetIntMax(camera, L"AOIWidth", &maxWidth));
	exitOnError(r, AT_GetIntMax(camera, L"AOIHeight", &maxHeight));
	if (arguments->verbose)
		printf("max image size is %lld x %lld\n", maxWidth, maxHeight);
	if (arguments->x < 1 || arguments->x > arguments->binning * maxWidth) {
		arguments->x = clip(arguments->x, 1, arguments->binning * maxWidth);
		if (!arguments->silent)
			printf("x offset adjusted to %d\n", arguments->x);
	}
	if (arguments->y < 1 || arguments->y > arguments->binning * maxHeight) {
		arguments->y = clip(arguments->y, 1, arguments->binning * maxHeight);
		if (!arguments->silent)
			printf("y offset adjusted to %d\n", arguments->y);
	}
	if (arguments->width <= 0
			|| arguments->x + arguments->binning * arguments->width - 1
					> arguments->binning * maxWidth) {
		arguments->width = maxWidth - (arguments->x - 1) / arguments->binning;
		if (!arguments->silent)
			printf("capping image width to %d\n", arguments->width);
	}
	if (arguments->height <= 0
			|| arguments->y + arguments->binning * arguments->height - 1
					> arguments->binning * maxHeight) {
		arguments->height = maxHeight - (arguments->y - 1) / arguments->binning;
		if (!arguments->silent)
			printf("capping image height to %d\n", arguments->height);
	}
	if (arguments->verbose)
		printf("setting AOI to [%d..%d]x[%d..%d]...", arguments->x,
				arguments->x + arguments->width - 1, arguments->y,
				arguments->y + arguments->height - 1);
	exitOnError(r, AT_SetInt(camera, L"AOIWidth", arguments->width));
	exitOnError(r, AT_SetInt(camera, L"AOILeft", arguments->x));
	exitOnError(r, AT_SetInt(camera, L"AOIHeight", arguments->height));
	exitOnError(r, AT_SetInt(camera, L"AOITop", arguments->y));
	if (arguments->verbose || !fullAIOControl) {
		AT_64 actualWidth, actualHeight, actualX, actualY;
		exitOnError(r, AT_GetInt(camera, L"AOIWidth", &actualWidth));
		exitOnError(r, AT_GetInt(camera, L"AOILeft", &actualX));
		exitOnError(r, AT_GetInt(camera, L"AOIHeight", &actualHeight));
		exitOnError(r, AT_GetInt(camera, L"AOITop", &actualY));
		printf("actual AOI is [%lld..%lld]x[%lld..%lld]\n", actualX,
				actualX + actualWidth - 1, actualY, actualY + actualHeight - 1);
	} else
		printf("\n");
	if (arguments->verbose)
		printf("enabling continuous frame capture\n");
	exitOnError(r, AT_SetEnumString(camera, L"CycleMode", L"Continuous"));
	if (arguments->cool) {
		if (arguments->verbose)
			printf("enabling cooler\n");
		exitOnError(r, AT_SetBool(camera, L"SensorCooling", AT_TRUE));
		if (arguments->temperature < 0) {
			AT_64 numAvailableTemperatures;
			exitOnError(r,
					AT_GetEnumCount(camera, L"TemperatureControl",
							&numAvailableTemperatures));
			if (numAvailableTemperatures <= 0) {
				if (!arguments->silent)
					printf("no temperatures to set available\n");
			} else {
				AT_WC *availableTemperatureString = calloc(
				DEFAULT_STRING_LENGTH, sizeof(AT_WC));
				double *availableTemperatures = calloc(numAvailableTemperatures,
						sizeof(double));
				for (int i = 0; i < numAvailableTemperatures; i++) {
					exitOnError(r,
							AT_GetEnumStringByIndex(camera, L"TemperatureControl", i, availableTemperatureString, DEFAULT_STRING_LENGTH));
					availableTemperatures[i] = wcstod(
							availableTemperatureString,
							NULL);
				}
				free(availableTemperatureString);
				double minDeltaT = abs(
						availableTemperatures[0] - arguments->temperature);
				int minDeltaTindex = 0;
				for (int i = 1; i < numAvailableTemperatures; i++) {
					double deltaT = abs(
							availableTemperatures[i] - arguments->temperature);
					if (deltaT < minDeltaT) {
						minDeltaT = deltaT;
						minDeltaTindex = i;
					}
				}
				arguments->temperature = availableTemperatures[minDeltaTindex];
				free(availableTemperatures);
				if (arguments->verbose)
					printf(
							"setting closest matching cooler temperature: %f°C\n",
							arguments->temperature);
				exitOnError(r,
						AT_SetEnumIndex(camera, L"TemperatureControl",
								minDeltaTindex));
			}
		} else if (arguments->verbose) {
			AT_WC targetTemperatureString[DEFAULT_STRING_LENGTH];
			AT_64 targetTemperatureIndex;
			exitOnError(r,
					AT_GetEnumIndex(camera, L"TemperatureControl",
							&targetTemperatureIndex));
			exitOnError(r,
					AT_GetEnumStringByIndex(camera, L"TemperatureControl", targetTemperatureIndex, targetTemperatureString, DEFAULT_STRING_LENGTH));
			printf("target cooler temperature is %ls°C\n",
					targetTemperatureString);
		}
	} else {
		if (arguments->verbose)
			printf("disabling cooler\n");
		exitOnError(r, AT_SetBool(camera, L"SensorCooling", AT_FALSE));
	}
}

typedef struct {
	guchar red;
	guchar green;
	guchar blue
} rgb_pixel;

struct ImageStreamerThreadParams {
	pthread_t thread;
	int mayRun;
	int isRunning;
	int verbose;
	GtkImage *image;
	AT_H camera;
	rgb_pixel *colorFunctionInterPolatationData;
	size_t colorFunctionInterPolatationDataLength;
};

rgb_pixel sunsetColorFunctionInterPolatationData[7] = { { 0, 0, 0 }, { 95, 35,
		129 }, { 201, 66, 69 }, { 250, 115, 13 }, { 255, 174, 33 }, { 255, 225,
		125 }, { 255, 255, 255 } }; // sunset colors :)

static volatile struct ImageStreamerThreadParams globalImageStreamerThreadParams =
		{ 0, 0, 0, 0, 0, 0, sunsetColorFunctionInterPolatationData,
				sizeof(sunsetColorFunctionInterPolatationData)
						/ sizeof(rgb_pixel) };

rgb_pixel colorFunction(double x, rgb_pixel *colorFunctionInterPolatationData,
		size_t colorFunctionInterPolatationDataLength) { //assumes evenly spread support points across 0-1
	int n = colorFunctionInterPolatationDataLength - 1;
	double xn = x * n;
	int l = floorf(xn), r = ceilf(xn);
	if (r <= 0)
		return colorFunctionInterPolatationData[0];
	if (l >= n)
		return colorFunctionInterPolatationData[n];
	if (l == r)
		return colorFunctionInterPolatationData[l];
	rgb_pixel p = { (r - xn) * colorFunctionInterPolatationData[l].red
			+ (xn - l) * colorFunctionInterPolatationData[r].red, (r - xn)
			* colorFunctionInterPolatationData[l].green
			+ (xn - l) * colorFunctionInterPolatationData[r].green, (r - xn)
			* colorFunctionInterPolatationData[l].blue
			+ (xn - l) * colorFunctionInterPolatationData[r].blue };
	return p;
}

typedef struct AndorSDK3BufferEntryStruct {
	AT_U8 *buffer;
	size_t bufferSize;
} AndorSDK3BufferEntry;

#define clockdiffToDouble(a, b)					\
	((a).tv_sec - (b).tv_sec) +					\
	((a).tv_nsec - (b).tv_nsec)*1.e-9
#define timespecToDouble(a)						\
	((a).tv_sec) + ((a).tv_nsec)*1.e-9
#define clockdiffToInt(a, b)					\
	((a).tv_sec - (b).tv_sec)*1000000000ll +	\
	((a).tv_nsec - (b).tv_nsec)
#define timespecToInt(a)						\
	((a).tv_sec)*1000000000ll + ((a).tv_nsec)

void *imageStreamerThread(void *vParams) {
	struct ImageStreamerThreadParams *params =
			(struct ImageStreamerThreadParams*) vParams;
	params->isRunning = TRUE;
	if (params->verbose)
		printf("frame capture thread started\n");
	int r;
	GdkPixbuf *currentBuffer, *hiddenBuffer;
	currentBuffer = gtk_image_get_pixbuf(params->image);
	hiddenBuffer = gdk_pixbuf_copy(currentBuffer);
	guchar *pixels = gdk_pixbuf_get_pixels(hiddenBuffer);
	AT_64 imageWidth, imageHeight, imageSizeInBytes, cameraRowStride;
	double exposureTime;
	exitOnError(r, AT_GetFloat(params->camera, L"ExposureTime", &exposureTime));
	uint timeout = clip(1000*max(MIN_TIMEOUT,2*exposureTime), 0, UINT_MAX);
	AT_64 numTemperatureStati;
	exitOnError(r,
			AT_GetEnumCount(params->camera, L"TemperatureStatus",
					&numTemperatureStati));
	AT_WC **temperatureStati = calloc(numTemperatureStati, sizeof(AT_WC*));
	for (int i = 0; i < numTemperatureStati; i++) {
		temperatureStati[i] = calloc(DEFAULT_STRING_LENGTH, sizeof(AT_WC));
		exitOnError(r,
				AT_GetEnumStringByIndex(params->camera, L"TemperatureStatus",i,temperatureStati[i],DEFAULT_STRING_LENGTH));
	}
	AT_64 temperatureStatusIndex;
	double temperature;
	exitOnError(r, AT_GetInt(params->camera, L"AOIWidth", &imageWidth));
	exitOnError(r, AT_GetInt(params->camera, L"AOIHeight", &imageHeight));
	exitOnError(r,
			AT_GetInt(params->camera, L"ImageSizeBytes", &imageSizeInBytes));
	int imageRowStride = gdk_pixbuf_get_rowstride(hiddenBuffer);
	exitOnError(r, AT_GetInt(params->camera, L"AOIStride", &cameraRowStride));
	AndorSDK3BufferEntry buffers[NUM_BUFFERS];
	if (params->verbose)
		printf("creating buffers\n");
	for (int i = 0; i < NUM_BUFFERS; i++) {
		buffers[i].bufferSize = imageSizeInBytes;
		buffers[i].buffer = aligned_alloc(MEM_ALIGN_BOUNDARY,
				buffers[i].bufferSize * sizeof(AT_U8));
		if (!buffers[i].buffer) {
			fprintf(stderr, "buffer allocation failed!\n");
			exit(1);
		}
		exitOnError(r,
				AT_QueueBuffer(params->camera, buffers[i].buffer,
						buffers[i].bufferSize));
	}
	if (params->verbose) {
		printf("starting continuous frame capture\n");
		setvbuf(stdout, NULL, _IONBF, 0);
	}
	exitOnError(r, AT_Command(params->camera, L"AcquisitionStart"));
	uint64_t frameCounter = 0, totalTime_ms = 0ll;
	struct timespec t1, t2;
	clock_gettime(CLOCK_MONOTONIC, &t2);
	while (params->mayRun) {
		AT_U8 *buffer;
		size_t bufferSize;
		exitOnError(r,
				AT_WaitBuffer(params->camera, &buffer, &bufferSize, timeout));
		frameCounter++;
		uint16_t min = UINT16_MAX, max = 0;
		for (int row = 0; row < imageHeight; row++)
			for (int col = 0; col < imageWidth; col++) {
				uint16_t value = *(uint16_t *) (buffer + row * cameraRowStride
						+ col * 2);
				if (value < min)
					min = value;
				if (value > max)
					max = value;
			}
		double invDelta;
		if (min == max) {
			invDelta = 1;
			if (max < UINT16_MAX)
				max++;
			else
				min--;
		} else
			invDelta = 1. / (max - min);
		for (int row = 0; row < imageHeight; row++)
			for (int col = 0; col < imageWidth; col++) {
				uint16_t value = *(uint16_t *) (buffer + row * cameraRowStride
						+ col * 2);
				*(rgb_pixel *) (pixels + row * imageRowStride + col * 3) =
						colorFunction((value - min) * invDelta,
								params->colorFunctionInterPolatationData,
								params->colorFunctionInterPolatationDataLength);
			}
		exitOnError(r, AT_QueueBuffer(params->camera, buffer, bufferSize));
		GdkPixbuf *temp = hiddenBuffer;
		if (!params->mayRun)
			break;
		gdk_threads_enter();
		hiddenBuffer = gtk_image_get_pixbuf(params->image);
		gtk_image_set_from_pixbuf(params->image, temp);
		pixels = gdk_pixbuf_get_pixels(hiddenBuffer);
//		pixels = gdk_pixbuf_get_pixels(currentBuffer);
		gdk_threads_leave();
		exitOnError(r,
				AT_GetFloat(params->camera, L"SensorTemperature", &temperature));
		exitOnError(r,
				AT_GetEnumIndex(params->camera, L"TemperatureStatus",
						&temperatureStatusIndex));
		t1 = t2;
		clock_gettime(CLOCK_MONOTONIC, &t2);
		double deltaTime_ms = clockdiffToDouble(t2,t1) * 1000;
		totalTime_ms += round(deltaTime_ms);
		if (params->verbose)
			printf(
					"\r\x1b[Kframe %10lld @ %6.2f ms, average %3.4f Hz, temperature %4.1f (%ls), image spans %5d to %5d",
					frameCounter, deltaTime_ms,
					frameCounter / (totalTime_ms / 1000.), temperature,
					temperatureStati[temperatureStatusIndex], min, max);
	}
	if (params->verbose) {
		printf(
				"\r\x1b[Ktotal %10lld frames @ %3.4f Hz, temperature %4.1f°C (%ls)\n",
				frameCounter, frameCounter / (totalTime_ms / 1000.),
				temperature, temperatureStati[temperatureStatusIndex]);
		setvbuf(stdout, NULL, _IOLBF, 0);
		printf("stopping continuous frame capture\n");
	}
	exitOnError(r, AT_Command(params->camera, L"AcquisitionStop"));
	exitOnError(r, AT_Flush(params->camera));
	for (int i = 0; i < NUM_BUFFERS; i++) {
		free(buffers[i].buffer);
		buffers[i].buffer = NULL;
		buffers[i].bufferSize = 0;
	}
	g_object_unref(hiddenBuffer);
	for (int i = 0; i < numTemperatureStati; i++)
		free(temperatureStati[i]);
	free(temperatureStati);
	if (params->verbose)
		printf("frame capture thread exiting\n");
	params->isRunning = FALSE;
	return NULL;
}

void closeCamera(AT_H camera, arguments *arguments) {
	int r;
	if (arguments->cool) {
		if (arguments->verbose)
			printf("disabling cooler\n");
		exitOnError(r, AT_SetBool(camera, L"SensorCooling", AT_FALSE));
	}
	exitOnError(r, AT_Close(camera));
}

int main(int argc, char *argv[]) {
	arguments arguments;
	int r;
	fillArgumentsWidthDefaultValues(&arguments);
	if (!g_thread_supported())
		g_thread_init(NULL);
	gdk_threads_init();
	gdk_threads_enter();
	gtk_init(&argc, &argv);
	argp_parse(&argp, argc, argv, 0, 0, &arguments);
	exitOnError(r, AT_InitialiseLibrary());
	AT_H camera = openCamera(&arguments);
	configureCamera(camera, &arguments);
	GdkPixbuf *imageBuffer = gdk_pixbuf_new(GDK_COLORSPACE_RGB, FALSE, 8,
			arguments.width, arguments.height);
	if (!imageBuffer) {
		fprintf(stderr, "could not allocate image buffer\n");
		exit(1);
	}
	g_assert(gdk_pixbuf_get_width(imageBuffer) == arguments.width);
	g_assert(gdk_pixbuf_get_height(imageBuffer) == arguments.height);
	g_assert(gdk_pixbuf_get_bits_per_sample(imageBuffer) == 8);
	g_assert(gdk_pixbuf_get_has_alpha(imageBuffer) == FALSE);
	g_assert(gdk_pixbuf_get_n_channels(imageBuffer) == 3);
	GtkWidget *image = gtk_image_new_from_pixbuf(imageBuffer);
	GtkWidget *window = createWindow(&arguments, image);
	gtk_widget_show(GTK_WINDOW(window));
	globalImageStreamerThreadParams.verbose = arguments.verbose;
	globalImageStreamerThreadParams.camera = camera;
	globalImageStreamerThreadParams.image = GTK_IMAGE(image);
	globalImageStreamerThreadParams.isRunning = FALSE;
	globalImageStreamerThreadParams.mayRun = TRUE;
	r = pthread_create(&globalImageStreamerThreadParams.thread, NULL,
			imageStreamerThread, (void*) &globalImageStreamerThreadParams);
	if (r) {
		printf("streamer thread creation failed: %d\n", r);
		AT_Close(camera);
		AT_FinaliseLibrary();
		exit(1);
	}
	gtk_main();
	globalImageStreamerThreadParams.mayRun = FALSE;
	gdk_threads_leave();
	r = pthread_join(globalImageStreamerThreadParams.thread, NULL);
	if (r) {
		printf("streamer thread join failed: %d\n", r);
		closeCamera(camera, &arguments);
		AT_FinaliseLibrary();
		exit(1);
	}
	closeCamera(camera, &arguments);
	exitOnError(r, AT_FinaliseLibrary());
	return 0;
}
