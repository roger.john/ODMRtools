/*
 * AndorSDK3Link.c
 *
 *  Created on: 06.07.2017
 *      Author: john
 */

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <math.h>
#include <mathlink.h>
#include <atcore.h>
#include <gtk/gtk.h>
#include "AndorSDK3Link.tmc"

#define ACCUMULATION_BUFFER_STORAGE_BITS 32
#if ACCUMULATION_BUFFER_STORAGE_BITS == 64
#define ACCUMULATION_BUFFER_STORAGE_TYPE uint64_t
#else
#define ACCUMULATION_BUFFER_STORAGE_TYPE uint32_t
#endif

#define max(a,b) (a<b?b:a)
#define min(a,b) (b<a?b:a)
#define clip(x,a,b) (x<a?a:x>b?b:x)
#define NUM_BUFFERS 3
#define MEM_ALIGN_BOUNDARY 8
#define MIN_TIMEOUT 1
#if __WCHAR_MAX__ >= (1<<16)
#define MLGetWString(ml,sp,n) MLGetUTF32String(ml,sp,n)
#define MLReleaseWString(ml,sp,n) MLReleaseUTF32String(ml,sp,n)
#define MLPutWString(ml,sp,n) MLPutUTF32String(ml,sp,n)
#else
#define MLGetWString(ml,sp,n) MLGetUTF16String(ml,sp,n,0)
#define MLReleaseWString(ml,sp,n) MLReleaseUTF16String(ml,sp,n)
#define MLPutWString(ml,sp,n) MLPutUTF16String(ml,sp,n)
#endif

#define MAX_ENUM_STRING_LENGTH 1024
#define MEM_ALIGN_BOUNDARY 8
int sargc;
char **sargv;

typedef struct {
	int code;
	const char *name;
} AndorSDKerrorCodeName;

AndorSDKerrorCodeName AndorSDK3ErrorCodeNames[] = {
		{ AT_SUCCESS, "AT_SUCCESS" }, { AT_ERR_NOTINITIALISED,
				"AT_ERR_NOTINITIALISED" }, {
		AT_ERR_NOTIMPLEMENTED, "AT_ERR_NOTIMPLEMENTED" }, { AT_ERR_READONLY,
				"AT_ERR_READONLY" },
		{ AT_ERR_NOTREADABLE, "AT_ERR_NOTREADABLE" }, {
		AT_ERR_NOTWRITABLE, "AT_ERR_NOTWRITABLE" }, { AT_ERR_OUTOFRANGE,
				"AT_ERR_OUTOFRANGE" }, { AT_ERR_INDEXNOTAVAILABLE,
				"AT_ERR_INDEXNOTAVAILABLE" }, { AT_ERR_INDEXNOTIMPLEMENTED,
				"AT_ERR_INDEXNOTIMPLEMENTED" },
		{ AT_ERR_EXCEEDEDMAXSTRINGLENGTH, "AT_ERR_EXCEEDEDMAXSTRINGLENGTH" }, {
		AT_ERR_CONNECTION, "AT_ERR_CONNECTION" }, { AT_ERR_NODATA,
				"AT_ERR_NODATA" }, {
		AT_ERR_INVALIDHANDLE, "AT_ERR_INVALIDHANDLE" }, { AT_ERR_TIMEDOUT,
				"AT_ERR_TIMEDOUT" }, { AT_ERR_BUFFERFULL, "AT_ERR_BUFFERFULL" },
		{
		AT_ERR_INVALIDSIZE, "AT_ERR_INVALIDSIZE" }, { AT_ERR_INVALIDALIGNMENT,
				"AT_ERR_INVALIDALIGNMENT" }, { AT_ERR_COMM, "AT_ERR_COMM" }, {
		AT_ERR_STRINGNOTAVAILABLE, "AT_ERR_STRINGNOTAVAILABLE" }, {
		AT_ERR_STRINGNOTIMPLEMENTED, "AT_ERR_STRINGNOTIMPLEMENTED" }, {
		AT_ERR_NULL_FEATURE, "AT_ERR_NULL_FEATURE" }, { AT_ERR_NULL_HANDLE,
				"AT_ERR_NULL_HANDLE" }, { AT_ERR_NULL_IMPLEMENTED_VAR,
				"AT_ERR_NULL_IMPLEMENTED_VAR" }, { AT_ERR_NULL_READABLE_VAR,
				"AT_ERR_NULL_READABLE_VAR" }, { AT_ERR_NULL_READONLY_VAR,
				"AT_ERR_NULL_READONLY_VAR" }, { AT_ERR_NULL_WRITABLE_VAR,
				"AT_ERR_NULL_WRITABLE_VAR" }, { AT_ERR_NULL_MINVALUE,
				"AT_ERR_NULL_MINVALUE" }, { AT_ERR_NULL_MAXVALUE,
				"AT_ERR_NULL_MAXVALUE" }, { AT_ERR_NULL_VALUE,
				"AT_ERR_NULL_VALUE" }, { AT_ERR_NULL_STRING,
				"AT_ERR_NULL_STRING" }, { AT_ERR_NULL_COUNT_VAR,
				"AT_ERR_NULL_COUNT_VAR" }, { AT_ERR_NULL_ISAVAILABLE_VAR,
				"AT_ERR_NULL_ISAVAILABLE_VAR" }, { AT_ERR_NULL_MAXSTRINGLENGTH,
				"AT_ERR_NULL_MAXSTRINGLENGTH" }, { AT_ERR_NULL_EVCALLBACK,
				"AT_ERR_NULL_EVCALLBACK" }, { AT_ERR_NULL_QUEUE_PTR,
				"AT_ERR_NULL_QUEUE_PTR" }, { AT_ERR_NULL_WAIT_PTR,
				"AT_ERR_NULL_WAIT_PTR" }, { AT_ERR_NULL_PTRSIZE,
				"AT_ERR_NULL_PTRSIZE" }, { AT_ERR_NOMEMORY, "AT_ERR_NOMEMORY" },
		{ AT_ERR_DEVICEINUSE, "AT_ERR_DEVICEINUSE" }, { AT_ERR_DEVICENOTFOUND,
				"AT_ERR_DEVICENOTFOUND" } };

const char *AndorSDK3ErrorName(int errorCode) {
	int n = sizeof(AndorSDK3ErrorCodeNames) / sizeof(AndorSDKerrorCodeName);
	int i = 0;
	while (i < n && errorCode != AndorSDK3ErrorCodeNames[i].code)
		i++;
	if (i < n)
		return AndorSDK3ErrorCodeNames[i].name;
	else
		return "unknown error";
}

void AndorSDK3FailWithError(int errorCode) {
	if (!MLPutFunction(stdlink, "$Failed", 2)
			|| !MLPutInteger(stdlink, errorCode)
			|| !MLPutString(stdlink, AndorSDK3ErrorName(errorCode)))
		failWithMsgAndError(stdlink, "AndorSDK3FailWithError:result:error: ");
}

void AndorSDK3Open(int cameraIndex) {
	AT_H hndl = AT_HANDLE_UNINITIALISED;
	int r = AT_Open(cameraIndex, &hndl);
	if (r == AT_SUCCESS) {
		if (!MLPutInteger(stdlink, hndl))
			failWithMsgAndError(stdlink, "AndorSDK3Open:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3Close(AT_H hndl) {
	int r = AT_Close(hndl);
	if (r == AT_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "AndorSDK3Close:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

#define AndorSDK3FailOnError(r,call) (r) = (call); \
	if((r) != AT_SUCCESS) { \
		MLReleaseWString(stdlink, featureName, featureNameLength); \
		AndorSDK3FailWithError(r); \
		return; }

void AndorSDK3IsImplemented(AT_H hndl) {
	AT_BOOL b;
	const AT_WC *featureName;
	int featureNameLength;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3IsImplemented:featureName:error: ");
		return;
	}
	int r = AT_IsImplemented(hndl, featureName, &b);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutSymbol(stdlink, b == AT_TRUE ? "True" : "False"))
			failWithMsgAndError(stdlink,
					"AndorSDK3IsImplemented:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3IsReadable(AT_H hndl) {
	AT_BOOL b;
	const AT_WC *featureName;
	int featureNameLength;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink, "AndorSDK3IsReadable:featureName:error: ");
		return;
	}
	int r = AT_IsReadable(hndl, featureName, &b);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutSymbol(stdlink, b == AT_TRUE ? "True" : "False"))
			failWithMsgAndError(stdlink, "AndorSDK3IsReadable:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3IsWritable(AT_H hndl) {
	AT_BOOL b;
	const AT_WC *featureName;
	int featureNameLength;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink, "AndorSDK3IsWritable:featureName:error: ");
		return;
	}
	int r = AT_IsWritable(hndl, featureName, &b);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutSymbol(stdlink, b == AT_TRUE ? "True" : "False"))
			failWithMsgAndError(stdlink, "AndorSDK3IsWritable:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3IsReadOnly(AT_H hndl) {
	AT_BOOL b;
	const AT_WC *featureName;
	int featureNameLength;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink, "AndorSDK3IsReadOnly:featureName:error: ");
		return;
	}
	int r = AT_IsReadOnly(hndl, featureName, &b);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutSymbol(stdlink, b == AT_TRUE ? "True" : "False"))
			failWithMsgAndError(stdlink, "AndorSDK3IsReadOnly:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3SetInt(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	AT_64 value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink, "AndorSDK3SetInt:featureName:error: ");
		return;
	}
	if (!MLGetInteger64(stdlink, &value)) {
		failWithMsgAndError(stdlink, "AndorSDK3SetInt:value:error: ");
		return;
	}
	int r = AT_SetInt(hndl, featureName, value);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "AndorSDK3SetInt:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3GetInt(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	AT_64 value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink, "AndorSDK3GetInt:featureName:error: ");
		return;
	}
	int r = AT_GetInt(hndl, featureName, &value);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutInteger64(stdlink, value))
			failWithMsgAndError(stdlink, "AndorSDK3GetInt:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3GetIntMax(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	AT_64 value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink, "AndorSDK3GetIntMax:featureName:error: ");
		return;
	}
	int r = AT_GetIntMax(hndl, featureName, &value);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutInteger64(stdlink, value))
			failWithMsgAndError(stdlink, "AndorSDK3GetIntMax:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3GetIntMin(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	AT_64 value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink, "AndorSDK3GetIntMin:featureName:error: ");
		return;
	}
	int r = AT_GetIntMin(hndl, featureName, &value);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutInteger64(stdlink, value))
			failWithMsgAndError(stdlink, "AndorSDK3GetIntMin:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3SetFloat(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	double value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink, "AndorSDK3SetFloat:featureName:error: ");
		return;
	}
	if (!MLGetReal64(stdlink, &value)) {
		failWithMsgAndError(stdlink, "AndorSDK3SetFloat:value:error: ");
		return;
	}
	int r = AT_SetFloat(hndl, featureName, value);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "AndorSDK3SetFloat:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3GetFloat(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	double value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink, "AndorSDK3GetFloat:featureName:error: ");
		return;
	}
	int r = AT_GetFloat(hndl, featureName, &value);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutReal64(stdlink, value))
			failWithMsgAndError(stdlink, "AndorSDK3GetFloat:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3GetFloatMax(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	double value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3GetFloatMax:featureName:error: ");
		return;
	}
	int r = AT_GetFloatMax(hndl, featureName, &value);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutReal64(stdlink, value))
			failWithMsgAndError(stdlink, "AndorSDK3GetFloatMax:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3GetFloatMin(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	double value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3GetFloatMin:featureName:error: ");
		return;
	}
	int r = AT_GetFloatMin(hndl, featureName, &value);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutReal64(stdlink, value))
			failWithMsgAndError(stdlink, "AndorSDK3GetFloatMin:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3SetBool(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	const char *value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink, "AndorSDK3SetBool:featureName:error: ");
		return;
	}
	if (!MLGetSymbol(stdlink, &value)) {
		failWithMsgAndError(stdlink, "AndorSDK3SetBool:value:error: ");
		return;
	}
	int r = AT_SetBool(hndl, featureName, strcmp(value, "False") == 0 ? 0 : 1);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "AndorSDK3SetBool:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3GetBool(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	AT_BOOL value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink, "AndorSDK3GetBool:featureName:error: ");
		return;
	}
	int r = AT_GetBool(hndl, featureName, &value);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutSymbol(stdlink, value == AT_FALSE ? "False" : "True"))
			failWithMsgAndError(stdlink, "AndorSDK3GetBool:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3SetEnumerated(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	int value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3SetEnumerated:featureName:error: ");
		return;
	}
	if (!MLGetInteger(stdlink, &value)) {
		failWithMsgAndError(stdlink, "AndorSDK3SetEnumerated:value:error: ");
		return;
	}
	int r = AT_SetEnumerated(hndl, featureName, value);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK3SetEnumerated:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3SetEnumeratedString(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	const AT_WC *value;
	int valueLength;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3SetEnumeratedString:featureName:error: ");
		return;
	}
	if (!MLGetWString(stdlink, &value, &valueLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3SetEnumeratedString:value:error: ");
		return;
	}
	int r = AT_SetEnumerated(hndl, featureName, value);
	MLReleaseWString(stdlink, value, valueLength);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK3SetEnumeratedString:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3GetEnumerated(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	int value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3GetEnumerated:featureName:error: ");
		return;
	}
	int r = AT_GetEnumerated(hndl, featureName, &value);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutInteger(stdlink, value))
			failWithMsgAndError(stdlink,
					"AndorSDK3GetEnumerated:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3GetEnumeratedStringByIndex(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	AT_WC value[MAX_ENUM_STRING_LENGTH];
	int valueLength, index;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3GetEnumeratedStringByIndex:featureName:error: ");
		return;
	}
	if (!MLGetInteger(stdlink, &index)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3GetEnumeratedStringByIndex:index:error: ");
		return;
	}

	int r = AT_GetEnumeratedString(hndl, featureName, index, value,
	MAX_ENUM_STRING_LENGTH);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		valueLength = wcslen(value);
		if (!MLPutWString(stdlink, value, valueLength))
			failWithMsgAndError(stdlink,
					"AndorSDK3GetEnumeratedStringByIndex:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3GetEnumeratedString(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	AT_WC value[MAX_ENUM_STRING_LENGTH];
	int valueLength, index, r, enumCount;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3GetEnumeratedString:featureName:error: ");
		return;
	}
	AndorSDK3FailOnError(r, AT_GetEnumerated(hndl, featureName, &index));
	AndorSDK3FailOnError(r,
			AT_GetEnumeratedString(hndl, featureName, index, value, MAX_ENUM_STRING_LENGTH));
	MLReleaseWString(stdlink, featureName, featureNameLength);
	valueLength = wcslen(value);
	if (!MLPutWString(stdlink, value, valueLength))
		failWithMsgAndError(stdlink,
				"AndorSDK3GetEnumeratedString:result:error: ");
}

void AndorSDK3GetEnumeratedCount(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	int value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3GetEnumeratedCount:featureName:error: ");
		return;
	}
	int r = AT_GetEnumeratedCount(hndl, featureName, &value);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutInteger(stdlink, value))
			failWithMsgAndError(stdlink,
					"AndorSDK3GetEnumeratedCount:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3IsEnumeratedIndexAvailable(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	int index;
	AT_BOOL value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3IsEnumeratedIndexAvailable:featureName:error: ");
		return;
	}
	if (!MLGetInteger(stdlink, &index)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3IsEnumeratedIndexAvailable:index:error: ");
		return;
	}
	int r = AT_IsEnumeratedIndexAvailable(hndl, featureName, index, &value);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutSymbol(stdlink, value == AT_FALSE ? "False" : "True"))
			failWithMsgAndError(stdlink,
					"AndorSDK3IsEnumeratedIndexAvailable:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3IsEnumeratedIndexImplemented(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	int index;
	AT_BOOL value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3IsEnumeratedIndexImplemented:featureName:error: ");
		return;
	}
	if (!MLGetInteger(stdlink, &index)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3IsEnumeratedIndexImplemented:index:error: ");
		return;
	}
	int r = AT_IsEnumeratedIndexImplemented(hndl, featureName, index, &value);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutSymbol(stdlink, value == AT_FALSE ? "False" : "True"))
			failWithMsgAndError(stdlink,
					"AndorSDK3IsEnumeratedIndexImplemented:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3SetEnumIndex(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	int value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3SetEnumIndex:featureName:error: ");
		return;
	}
	if (!MLGetInteger(stdlink, &value)) {
		failWithMsgAndError(stdlink, "AndorSDK3SetEnumIndex:value:error: ");
		return;
	}
	int r = AT_SetEnumIndex(hndl, featureName, value);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK3SetEnumIndex:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3SetEnumString(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	const AT_WC *value;
	int valueLength;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3SetEnumString:featureName:error: ");
		return;
	}
	if (!MLGetWString(stdlink, &value, &valueLength)) {
		failWithMsgAndError(stdlink, "AndorSDK3SetEnumString:value:error: ");
		return;
	}
	int r = AT_SetEnumString(hndl, featureName, value);
	MLReleaseWString(stdlink, value, valueLength);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK3SetEnumString:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3GetEnumIndex(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	int value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3GetEnumIndex:featureName:error: ");
		return;
	}
	int r = AT_GetEnumIndex(hndl, featureName, &value);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutInteger(stdlink, value))
			failWithMsgAndError(stdlink,
					"AndorSDK3GetEnumIndex:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3GetEnumStringByIndex(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	AT_WC value[MAX_ENUM_STRING_LENGTH];
	int valueLength, index;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3GetEnumStringByIndex:featureName:error: ");
		return;
	}
	if (!MLGetInteger(stdlink, &index)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3GetEnumStringByIndex:index:error: ");
		return;
	}
	int r = AT_GetEnumStringByIndex(hndl, featureName, index, value,
	MAX_ENUM_STRING_LENGTH);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		valueLength = wcslen(value);
		if (!MLPutWString(stdlink, value, valueLength))
			failWithMsgAndError(stdlink,
					"AndorSDK3GetEnumStringByIndex:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3GetEnumString(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	AT_WC value[MAX_ENUM_STRING_LENGTH];
	int valueLength, index, r, enumCount;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3GetEnumString:featureName:error: ");
		return;
	}
	AndorSDK3FailOnError(r, AT_GetEnumIndex(hndl, featureName, &index));
	AndorSDK3FailOnError(r,
			AT_GetEnumStringByIndex(hndl, featureName, index, value, MAX_ENUM_STRING_LENGTH));
	MLReleaseWString(stdlink, featureName, featureNameLength);
	valueLength = wcslen(value);
	if (!MLPutWString(stdlink, value, valueLength))
		failWithMsgAndError(stdlink, "AndorSDK3GetEnumString:result:error: ");
}

void AndorSDK3GetEnumCount(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	int value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3GetEnumCount:featureName:error: ");
		return;
	}
	int r = AT_GetEnumCount(hndl, featureName, &value);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutInteger(stdlink, value))
			failWithMsgAndError(stdlink,
					"AndorSDK3GetEnumCount:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3IsEnumIndexAvailable(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	int index;
	AT_BOOL value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3IsEnumIndexAvailable:featureName:error: ");
		return;
	}
	if (!MLGetInteger(stdlink, &index)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3IsEnumIndexAvailable:index:error: ");
		return;
	}
	int r = AT_IsEnumIndexAvailable(hndl, featureName, index, &value);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutSymbol(stdlink, value == AT_FALSE ? "False" : "True"))
			failWithMsgAndError(stdlink,
					"AndorSDK3IsEnumIndexAvailable:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3IsEnumIndexImplemented(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	int index;
	AT_BOOL value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3IsEnumIndexImplemented:featureName:error: ");
		return;
	}
	if (!MLGetInteger(stdlink, &index)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3IsEnumIndexImplemented:index:error: ");
		return;
	}
	int r = AT_IsEnumIndexImplemented(hndl, featureName, index, &value);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutSymbol(stdlink, value == AT_FALSE ? "False" : "True"))
			failWithMsgAndError(stdlink,
					"AndorSDK3IsEnumIndexImplemented:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3SetString(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	const AT_WC *value;
	int valueLength;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink, "AndorSDK3SetString:featureName:error: ");
		return;
	}
	if (!MLGetWString(stdlink, &value, &valueLength)) {
		failWithMsgAndError(stdlink, "AndorSDK3SetString:value:error: ");
		return;
	}
	int r = AT_SetString(hndl, featureName, value);
	MLReleaseWString(stdlink, value, valueLength);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "AndorSDK3SetString:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3GetString(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	AT_WC *value;
	int valueLength;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink, "AndorSDK3GetString:featureName:error: ");
		return;
	}
	int r = AT_GetStringMaxLength(hndl, featureName, &valueLength);
	if (r != AT_SUCCESS) {
		MLReleaseWString(stdlink, featureName, featureNameLength);
		AndorSDK3FailWithError(r);
		return;
	}
	value = calloc(valueLength, sizeof(AT_WC));
	r = AT_GetString(hndl, featureName, value, valueLength);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		valueLength = wcslen(value);
		if (!MLPutWString(stdlink, value, valueLength))
			failWithMsgAndError(stdlink, "AndorSDK3GetString:result:error: ");
	} else
		AndorSDK3FailWithError(r);
	free(value);
}

void AndorSDK3GetStringMaxLength(AT_H hndl) {
	const AT_WC *featureName;
	int featureNameLength;
	int value;
	if (!MLGetWString(stdlink, &featureName, &featureNameLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3GetStringMaxLength:featureName:error: ");
		return;
	}
	int r = AT_GetStringMaxLength(hndl, featureName, &value);
	MLReleaseWString(stdlink, featureName, featureNameLength);
	if (r == AT_SUCCESS) {
		if (!MLPutInteger(stdlink, value))
			failWithMsgAndError(stdlink,
					"AndorSDK3GetStringMaxLength:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3Command(AT_H hndl) {
	const AT_WC *command;
	int commandLength;
	if (!MLGetWString(stdlink, &command, &commandLength)) {
		failWithMsgAndError(stdlink, "AndorSDK3Command:featureName:error: ");
		return;
	}
	int r = AT_Command(hndl, command);
	MLReleaseWString(stdlink, command, commandLength);
	if (r == AT_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "AndorSDK3Command:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

typedef struct AndorSDK3BufferListEntry {
	AT_U8 *buffer;
	size_t bufferSize;
	struct AndorSDK3BufferListEntry *next;
} AndorSDK3BufferListEntry_t;

AndorSDK3BufferListEntry_t *bufferListHead = NULL;

void AndorSDK3QueueBuffer(AT_H hndl, int bufferSize) {
	if (bufferSize <= 0) {
		if (!MLPutInteger(stdlink, 0))
			failWithMsgAndError(stdlink, "AndorSDK3QueueBuffer:result:error: ");
		return;
	}
	if (bufferSize & 1)
		bufferSize += 1;
	AT_U8 *buffer = aligned_alloc(MEM_ALIGN_BOUNDARY,
			bufferSize * sizeof(AT_U8));
	if (!buffer) {
		if (!MLPutFunction(stdlink, "List", 1)
				|| !MLPutSymbol(stdlink, "$Failed")
				|| !MLPutString(stdlink, "buffer allocation failed"))
			failWithMsgAndError(stdlink, "AndorSDK3QueueBuffer:malloc:error: ");
		return;
	}
	AndorSDK3BufferListEntry_t *bufferListEntry = malloc(
			sizeof(AndorSDK3BufferListEntry_t));
	if (!bufferListEntry) {
		if (!MLPutFunction(stdlink, "List", 1)
				|| !MLPutSymbol(stdlink, "$Failed")
				|| !MLPutString(stdlink, "buffer list entry allocation failed"))
			failWithMsgAndError(stdlink,
					"AndorSDK3QueueBuffer:malloc2:error: ");
		return;
	}
	bufferListEntry->buffer = buffer;
	bufferListEntry->bufferSize = bufferSize;
	bufferListEntry->next = bufferListHead;
	bufferListHead = bufferListEntry;
	int r = AT_QueueBuffer(hndl, buffer, bufferSize);
	if (r == AT_SUCCESS) {
		if (!MLPutInteger(stdlink, bufferSize))
			failWithMsgAndError(stdlink, "AndorSDK3QueueBuffer:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3WaitBuffer(AT_H hndl, double timeout) {
	AT_U8 *buffer;
	int bufferSize;
	int r = AT_WaitBuffer(hndl, &buffer, &bufferSize,
			timeout / 1000 > UINT32_MAX ? AT_INFINITE :
			timeout < 0 ? 0 : (unsigned int) (timeout / 1000));
	if (r == AT_SUCCESS) {
		if (buffer && bufferSize > 0) {
			if (!MLPutFunction(stdlink, "List", 2)
					|| !MLPutInteger16List(stdlink, (int16_t*) buffer,
							(bufferSize - 1) / 2 + 1)) {
				failWithMsgAndError(stdlink,
						"AndorSDK3QueueBuffer:result:error: ");
				return;
			}
			r = AT_QueueBuffer(hndl, buffer, bufferSize);
			if (r == AT_SUCCESS) {
				if (!MLPutSymbol(stdlink, "Null"))
					failWithMsgAndError(stdlink,
							"AndorSDK3QueueBuffer:result2:error: ");
			} else
				AndorSDK3FailWithError(r);
		} else {
			if (!MLPutFunction(stdlink, "List", 0))
				failWithMsgAndError(stdlink,
						"AndorSDK3QueueBuffer:emptyResult:error: ");
		}
	} else
		AndorSDK3FailWithError(r);
}

void AndorSDK3Flush(AT_H hndl) {
	int r = AT_Flush(hndl);
	AndorSDK3BufferListEntry_t *s = bufferListHead, *t;
	while (s) {
		t = s->next;
		free(s->buffer);
		free(s);
		s = t;
	}
	bufferListHead = NULL;
	if (r == AT_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "AndorSDK3Flush:result:error: ");
	} else
		AndorSDK3FailWithError(r);
}

/*
 typedef int (AT_EXP_CONV *FeatureCallback)(AT_H Hndl, const AT_WC* Feature, void* Context);
 int AT_EXP_CONV AT_RegisterFeatureCallback(AT_H Hndl, const AT_WC* Feature, FeatureCallback EvCallback, void* Context);
 int AT_EXP_CONV AT_UnregisterFeatureCallback(AT_H Hndl, const AT_WC* Feature, FeatureCallback EvCallback, void* Context);
 */

typedef struct {
	guchar red;
	guchar green;
	guchar blue;
} rgb_pixel;

typedef struct AndorSDK3BufferEntry {
	AT_U8 *buffer;
	size_t bufferSize;
} AndorSDK3BufferEntry_t;

typedef struct AndorSDK3AccumumlationBuffer {
	ACCUMULATION_BUFFER_STORAGE_TYPE *buffer;
	ACCUMULATION_BUFFER_STORAGE_TYPE bufferSize;
	uint64_t accumulations;
	struct AndorSDK3AccumulationBuffer *next;
} AndorSDK3AccumulationBuffer_t;

typedef struct ImageStreamerThreadParams {
	pthread_t imageStreamerThread;
	int mayRun;
	int isRunning;
	int mayAcquire;
	uint timeout_ms;
	GtkWindow *window;
	GtkImage *image;
	AT_H camera;
	AndorSDK3BufferEntry_t *buffers;
	size_t numBuffers;
	rgb_pixel *colorFunctionInterPolationData;
	size_t colorFunctionInterPolationDataLength;
	uint64_t frameCounter;
	pthread_mutex_t currentAccumulationBufferMutex;
	AndorSDK3AccumulationBuffer_t *waitingAccumulationBufferListHead,
			*waitingAccumulationBufferListTail;
	pthread_mutex_t waitingAccumulationBufferListMutex;
	pthread_cond_t waitingAccumulationBufferListEmptyCondition;
	AndorSDK3AccumulationBuffer_t *finishedAccumulationBufferListHead,
			*finishedAccumulationBufferListTail;
	pthread_mutex_t finishedAccumulationBufferListMutex;
	pthread_cond_t finishedAccumulationBufferListNotEmptyCondition;
} ImageStreamerThreadParams_t;

rgb_pixel sunsetColorFunctionInterPolationData[7] = { { 0, 0, 0 },
		{ 95, 35, 129 }, { 201, 66, 69 }, { 250, 115, 13 }, { 255, 174, 33 }, {
				255, 225, 125 }, { 255, 255, 255 } }; // sunset colors :)

static inline rgb_pixel colorFunction(double x,
		rgb_pixel *colorFunctionInterPolationData,
		size_t colorFunctionInterPolationDataLength) { //assumes evenly spread support points across 0-1
	int n = colorFunctionInterPolationDataLength - 1;
	double xn = x * n;
	int l = floorf(xn), r = ceilf(xn);
	if (r <= 0)
		return colorFunctionInterPolationData[0];
	if (l >= n)
		return colorFunctionInterPolationData[n];
	if (l == r)
		return colorFunctionInterPolationData[l];
	rgb_pixel p = { (r - xn) * colorFunctionInterPolationData[l].red
			+ (xn - l) * colorFunctionInterPolationData[r].red, (r - xn)
			* colorFunctionInterPolationData[l].green
			+ (xn - l) * colorFunctionInterPolationData[r].green, (r - xn)
			* colorFunctionInterPolationData[l].blue
			+ (xn - l) * colorFunctionInterPolationData[r].blue };
	return p;
}

#define exitThreadOnError(r,call) (r) = (call); \
	if((r) != AT_SUCCESS) { \
		fprintf(stderr, "error %d in %s line %d: %s, exiting streaming thread\n", r, __FILE__, __LINE__, AndorSDK3ErrorName(r)); \
		params->isRunning = FALSE; \
		return NULL;}

void *imageStreamerThread(void *vParams) {
	ImageStreamerThreadParams_t *params = (ImageStreamerThreadParams_t*) vParams;
	params->isRunning = TRUE;
	int acquisitionRunning = 0;
	int r;
	GdkPixbuf *currentBuffer, *hiddenBuffer;
	currentBuffer = gtk_image_get_pixbuf(params->image);
	hiddenBuffer = gdk_pixbuf_copy(currentBuffer);
	guchar *pixels = gdk_pixbuf_get_pixels(hiddenBuffer);
	rgb_pixel *colorFunctionInterPolationData =
			params->colorFunctionInterPolationData;
	int colorFunctionInterPolationDataLength =
			params->colorFunctionInterPolationDataLength;
	if (!colorFunctionInterPolationData
			|| colorFunctionInterPolationDataLength <= 0) {
		colorFunctionInterPolationData = sunsetColorFunctionInterPolationData;
		colorFunctionInterPolationDataLength =
				sizeof(sunsetColorFunctionInterPolationData)
						/ sizeof(rgb_pixel);
	}
	AT_64 imageWidth, imageHeight, imageSizeInBytes, cameraRowStride;
//	double exposureTime;
//	exitThreadOnError(r,
//			AT_GetFloat(params->camera, L"ExposureTime", &exposureTime));
//	uint timeout = clip(1000*max(MIN_TIMEOUT,2*exposureTime), 0, UINT_MAX);
	exitThreadOnError(r, AT_GetInt(params->camera, L"AOIWidth", &imageWidth));
	exitThreadOnError(r, AT_GetInt(params->camera, L"AOIHeight", &imageHeight));
	exitThreadOnError(r,
			AT_GetInt(params->camera, L"ImageSizeBytes", &imageSizeInBytes));
	int imageRowStride = gdk_pixbuf_get_rowstride(hiddenBuffer);
	exitThreadOnError(r,
			AT_GetInt(params->camera, L"AOIStride", &cameraRowStride));
//	for (int i = 0; i < params->numBuffers; i++)
//		exitThreadOnError(r,
//				AT_QueueBuffer(params->camera, params->buffers[i].buffer,
//						params->buffers[i].bufferSize));
//	int lastCycleMode;
//	exitThreadOnError(r,
//			AT_GetEnumIndex(params->camera, L"CycleMode", &lastCycleMode));
//	exitThreadOnError(r,
//			AT_SetEnumString(params->camera, L"CycleMode", L"Continuous"));
//	exitThreadOnError(r, AT_Command(params->camera, L"AcquisitionStart"));
	uint64_t frameCounter = 0;
	size_t imageSize = imageWidth * imageHeight;
	AndorSDK3AccumulationBuffer_t *currentAccumulationBuffer = NULL;
	setvbuf(stdout, NULL, _IONBF, 0);
	while (params->mayRun) {
		AT_U8 *buffer;
		size_t bufferSize;
		if (params->mayAcquire) {
			if (!acquisitionRunning) {
				for (int i = 0; i < params->numBuffers; i++)
					exitThreadOnError(r,
							AT_QueueBuffer(params->camera,
									params->buffers[i].buffer,
									params->buffers[i].bufferSize));
				exitThreadOnError(r,
						AT_Command(params->camera, L"AcquisitionStart"));
				acquisitionRunning = 1;
			}
			r = AT_WaitBuffer(params->camera, &buffer, &bufferSize,
					params->timeout_ms);
			if (r == AT_ERR_TIMEDOUT) {
				printf("\r\x1b[Ktimeout");
			} else if (r == AT_ERR_NODATA) {
				printf("\r\x1b[Kno data");
			} else if (r != AT_SUCCESS) {
				fprintf(stderr,
						"error %d in %s line %d: %s, exiting streaming thread\n",
						r,
						__FILE__, __LINE__, AndorSDK3ErrorName(r));
				params->isRunning = FALSE;
				return NULL;
			} else {
				frameCounter++;
				uint16_t min = UINT16_MAX, max = 0;
				pthread_mutex_lock(&params->waitingAccumulationBufferListMutex);
				currentAccumulationBuffer =
						params->waitingAccumulationBufferListHead;
				pthread_mutex_unlock(
						&params->waitingAccumulationBufferListMutex);
				if (currentAccumulationBuffer) {
					pthread_mutex_lock(&params->currentAccumulationBufferMutex);
					if (!currentAccumulationBuffer->buffer) {
						currentAccumulationBuffer->bufferSize = imageSize;
						currentAccumulationBuffer->buffer = calloc(imageSize,
								sizeof(ACCUMULATION_BUFFER_STORAGE_TYPE));
					} else if (currentAccumulationBuffer->bufferSize
							< imageSize) {
						free(currentAccumulationBuffer->buffer);
						currentAccumulationBuffer->bufferSize = imageSize;
						currentAccumulationBuffer->buffer = calloc(imageSize,
								sizeof(ACCUMULATION_BUFFER_STORAGE_TYPE));
					}
					for (int row = 0; row < imageHeight; row++)
						for (int col = 0; col < imageWidth; col++) {
							uint16_t value = *(uint16_t *) (buffer
									+ row * cameraRowStride + col * 2);
							if (value < min)
								min = value;
							if (value > max)
								max = value;
							currentAccumulationBuffer->buffer[row * imageWidth
									+ col] += value;
						}
					if (currentAccumulationBuffer->accumulations > 0) {
						currentAccumulationBuffer->accumulations--;
						pthread_mutex_unlock(
								&params->currentAccumulationBufferMutex);
					} else {
						pthread_mutex_lock(
								&params->finishedAccumulationBufferListMutex);
						pthread_mutex_lock(
								&params->waitingAccumulationBufferListMutex);
						params->waitingAccumulationBufferListHead =
								currentAccumulationBuffer->next;
						if (!currentAccumulationBuffer->next) {
							params->waitingAccumulationBufferListTail =
							NULL;
							pthread_cond_signal(
									&params->waitingAccumulationBufferListEmptyCondition);
						}
						currentAccumulationBuffer->next = NULL;
						pthread_mutex_unlock(
								&params->currentAccumulationBufferMutex);
						if (params->finishedAccumulationBufferListTail)
							params->finishedAccumulationBufferListTail->next =
									currentAccumulationBuffer;
						else {
							params->finishedAccumulationBufferListHead =
									currentAccumulationBuffer;
							pthread_cond_signal(
									&params->finishedAccumulationBufferListNotEmptyCondition);
						}
						params->finishedAccumulationBufferListTail =
								currentAccumulationBuffer;
						pthread_mutex_unlock(
								&params->waitingAccumulationBufferListMutex);
						pthread_mutex_unlock(
								&params->finishedAccumulationBufferListMutex);
					}
				} else
					for (int row = 0; row < imageHeight; row++)
						for (int col = 0; col < imageWidth; col++) {
							uint16_t value = *(uint16_t *) (buffer
									+ row * cameraRowStride + col * 2);
							if (value < min)
								min = value;
							if (value > max)
								max = value;
						}
				double invDelta;
				if (min == max) {
					invDelta = 1;
					if (max < UINT16_MAX)
						max++;
					else
						min--;
				} else
					invDelta = 1. / (max - min);
				for (int row = 0; row < imageHeight; row++)
					for (int col = 0; col < imageWidth; col++) {
						uint16_t value = *(uint16_t *) (buffer
								+ row * cameraRowStride + col * 2);
						*(rgb_pixel *) (pixels + row * imageRowStride + col * 3) =
								colorFunction((value - min) * invDelta,
										colorFunctionInterPolationData,
										colorFunctionInterPolationDataLength);
					}
				if (params->mayAcquire)
					exitThreadOnError(r,
							AT_QueueBuffer(params->camera, buffer, bufferSize));
				GdkPixbuf *temp = hiddenBuffer;
				if (!params->mayRun)
					break;
				gdk_threads_enter();
				hiddenBuffer = gtk_image_get_pixbuf(params->image);
				gtk_image_set_from_pixbuf(params->image, temp);
				pixels = gdk_pixbuf_get_pixels(hiddenBuffer);
//				pixels = gdk_pixbuf_get_pixels(currentBuffer);
				gdk_threads_leave();
			}
		} else {
			if (acquisitionRunning) {
				exitThreadOnError(r,
						AT_Command(params->camera, L"AcquisitionStop"));
				exitThreadOnError(r, AT_Flush(params->camera));
				acquisitionRunning = 0;
			}
			usleep(1000 * params->timeout_ms);
		}
	}
	setvbuf(stdout, NULL, _IOLBF, 0);
	if (acquisitionRunning) {
		exitThreadOnError(r, AT_Command(params->camera, L"AcquisitionStop"));
		exitThreadOnError(r, AT_Flush(params->camera));
		acquisitionRunning = 0;
	}
//	exitThreadOnError(r, AT_Command(params->camera, L"AcquisitionStop"));
//	exitThreadOnError(r,
//			AT_SetEnumIndex(params->camera, L"CycleMode", lastCycleMode));
//	exitThreadOnError(r, AT_Flush(params->camera));
	g_object_unref(hiddenBuffer);
	params->isRunning = FALSE;
	return NULL;
}

static volatile ImageStreamerThreadParams_t globalImageStreamerThreadParams = {
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, PTHREAD_MUTEX_INITIALIZER, 0,
		PTHREAD_MUTEX_INITIALIZER, PTHREAD_COND_INITIALIZER, 0,
		PTHREAD_MUTEX_INITIALIZER, PTHREAD_COND_INITIALIZER };

static gboolean delete_event(GtkWidget *widget, GdkEvent *event, gpointer data) {
	return FALSE;
}

/* Another callback */
static void destroyLiveViewWindow(GtkWidget *widget, gpointer data) {
	ImageStreamerThreadParams_t *params = (ImageStreamerThreadParams_t*) data;
	params->mayRun = 0;
	gtk_window_close(GTK_WINDOW(widget));
}

static volatile pthread_t gtkThread;
static volatile gboolean gtk_windowing_available = 0;

void *gtkThreadproc(void *vParams) {
	gdk_threads_enter();
	gtk_main();
	gdk_threads_leave();
	return NULL;
}

#define AndorSDK3StartLiveViewFailOnError(r,call) (r) = (call); \
	if((r) != AT_SUCCESS) { \
		if(colorFunctionInterPolatationDataLength > 0) \
			free(colorFunctionInterPolatationData); \
		char tempString[128]; \
		sprintf(tempString, "error %d in %s line %d: %s\n", r, __FILE__, __LINE__, AndorSDK3ErrorName(r)); \
		failWithMsg( stdlink, tempString); \
		return;}

void AndorSDK3StartLiveView(AT_H camera, int numBuffers,
		const char *windowFullscreenBool, const char *startAcquisitionBool) {
	if (numBuffers < 1) {
		failWithMsg(stdlink, "AndorSDK3StartLiveView:numBuffers:tooSmall");
		return;
	}
	int windowFullscreen = strcmp(windowFullscreenBool, "True") == 0;
	int startAcquisition = strcmp(startAcquisitionBool, "True") == 0;
	int nextArgumentType = MLGetType(stdlink);
	rgb_pixel *colorFunctionInterPolatationData;
	long int colorFunctionInterPolatationDataLength = 0;
	const char *shouldBeNull;
	switch (nextArgumentType) {
	case MLTKFUNC:
		if (!MLCheckFunction(stdlink, "List",
				&colorFunctionInterPolatationDataLength)) {
			failWithMsgAndError(stdlink,
					"AndorSDK3StartLiveView:colorFunctionData:headNotList: ");
			return;
		}
		if (colorFunctionInterPolatationDataLength > 0) {
			colorFunctionInterPolatationData = calloc(
					colorFunctionInterPolatationDataLength, sizeof(rgb_pixel));
			long int shouldBeThree;
			for (int i = 0; i < colorFunctionInterPolatationDataLength; i++) {
				if (!MLCheckFunction(stdlink, "List", &shouldBeThree)
						|| shouldBeThree != 3) {
					free(colorFunctionInterPolatationDataLength);
					failWithMsgAndError(stdlink,
							"AndorSDK3StartLiveView:colorFunctionData:entriesNotTripel: ");
					return;
				}
				for (int j = 0; j < 3; j++) {
					double temp;
					if (!MLGetReal(stdlink, &temp)) {
						free(colorFunctionInterPolatationDataLength);
						failWithMsgAndError(stdlink,
								"AndorSDK3StartLiveView:colorFunctionData:entryComponent:error: ");
						return;
					}
					((guchar*) &colorFunctionInterPolatationData[i])[j] = round(
					clip(temp,0,1) * 255);
				}
			}
		}
		break;
	case MLTKSYM:
		if (!MLGetSymbol(stdlink, &shouldBeNull)) {
			failWithMsgAndError(stdlink,
					"AndorSDK3StartLiveView:colorFunctionData:head:error: ");
			return;
		}
		if (strcmp(shouldBeNull, "Null") != 0) {
			MLReleaseSymbol(stdlink, shouldBeNull);
			failWithMsg(stdlink,
					"AndorSDK3StartLiveView:colorFunctionData:head:symbol:shouldBeNull");
			return;
		}
		MLReleaseSymbol(stdlink, shouldBeNull);
		break;
	case MLTKEND:
		break;
	default:
		failWithMsg(stdlink,
				"AndorSDK3StartLiveView:colorFunctionData:head:shouldNotHappen");
		return;
	}
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	if (threadParams->isRunning) {
		if (colorFunctionInterPolatationDataLength > 0)
			free(colorFunctionInterPolatationData);
		failWithMsg(stdlink, "AndorSDK3StartLiveView:already_running");
		return;
	}
	if (!gtk_windowing_available) {
		if (colorFunctionInterPolatationDataLength > 0)
			free(colorFunctionInterPolatationData);
		failWithMsg(stdlink,
				"AndorSDK3StartLiveView:gtk_windowing_not_available");
		return;
	}
	AT_64 imageWidth, imageHeight, imageSizeInBytes;
	double exposureTime;
	int r;
	AndorSDK3StartLiveViewFailOnError(r,
			AT_GetFloat(camera, L"ExposureTime", &exposureTime));
	AndorSDK3StartLiveViewFailOnError(r,
			AT_GetInt(camera, L"AOIWidth", &imageWidth));
	AndorSDK3StartLiveViewFailOnError(r,
			AT_GetInt(camera, L"AOIHeight", &imageHeight));
	AndorSDK3StartLiveViewFailOnError(r,
			AT_GetInt(camera, L"ImageSizeBytes", &imageSizeInBytes));
	if (imageSizeInBytes & 1) // TODO: can this happen?
		imageSizeInBytes += 1;
	GdkPixbuf *imageBuffer = gdk_pixbuf_new(GDK_COLORSPACE_RGB, FALSE, 8,
			imageWidth, imageHeight);
	if (!imageBuffer) {
		if (colorFunctionInterPolatationDataLength > 0)
			free(colorFunctionInterPolatationData);
		failWithMsg(stdlink, "AndorSDK3StartLiveView:could_not_allocate_image");
		return;
	}
	if (!threadParams->window) {
		gdk_threads_enter();
		GtkWidget *image = gtk_image_new_from_pixbuf(imageBuffer);
		GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
		gtk_window_set_title(GTK_WINDOW(window), "AndorSDK3Link LiveView");
		g_signal_connect(window, "delete-event", G_CALLBACK (delete_event),
				NULL);
		g_signal_connect(window, "destroy", G_CALLBACK (destroyLiveViewWindow),
				threadParams);
		gtk_container_set_border_width(GTK_CONTAINER(window), 0);
		gtk_container_add(GTK_CONTAINER(window), image);
		gtk_window_set_hide_titlebar_when_maximized(GTK_WINDOW(window), TRUE);
		if (windowFullscreen) {
			gtk_window_set_decorated(GTK_WINDOW(window), FALSE);
			gtk_window_fullscreen(GTK_WINDOW(window));
		}
		gtk_widget_show(image);
		gtk_widget_show(GTK_WINDOW(window));
		gdk_threads_leave();
		threadParams->window = GTK_WINDOW(window);
		threadParams->image = GTK_IMAGE(image);
	}
	threadParams->camera = camera;
	threadParams->isRunning = FALSE;
	threadParams->mayRun = TRUE;
	threadParams->mayAcquire = startAcquisition;
	threadParams->timeout_ms = clip(1000*max(MIN_TIMEOUT,2*exposureTime), 0,
			UINT_MAX);
	threadParams->colorFunctionInterPolationData =
			colorFunctionInterPolatationData;
	threadParams->colorFunctionInterPolationDataLength =
			colorFunctionInterPolatationDataLength;
	threadParams->numBuffers = numBuffers;
	threadParams->buffers = calloc(threadParams->numBuffers,
			sizeof(AndorSDK3BufferEntry_t));
	for (int i = 0; i < threadParams->numBuffers; i++) {
		threadParams->buffers[i].bufferSize = imageSizeInBytes;
		threadParams->buffers[i].buffer = aligned_alloc(MEM_ALIGN_BOUNDARY,
				threadParams->buffers[i].bufferSize * sizeof(AT_U8));
		if (!threadParams->buffers[i].buffer) {
			if (colorFunctionInterPolatationDataLength > 0)
				free(colorFunctionInterPolatationData);
			for (int j = 0; j < i; j++)
				free(threadParams->buffers[j].buffer);
			free(threadParams->buffers);
			gdk_threads_enter();
			gtk_window_close(threadParams->window);
			gdk_threads_leave();
			failWithMsg(stdlink, "AndorSDK3StartLiveView:could_not_buffers");
			return;
		}
//		if (startAcquisition)
//			AndorSDK3StartLiveViewFailOnError(r,
//					AT_QueueBuffer(camera, threadParams->buffers[i].buffer,
//							threadParams->buffers[i].bufferSize));
	}
//	if (startAcquisition)
//		AndorSDK3StartLiveViewFailOnError(r,
//				AT_Command(camera, L"AcquisitionStart"));
	r = pthread_create(&threadParams->imageStreamerThread, NULL,
			imageStreamerThread, (void*) threadParams);
	if (r) {
		fprintf(stderr, "streamer thread creation failed: %d\n", r);
		if (colorFunctionInterPolatationDataLength > 0)
			free(colorFunctionInterPolatationData);
		for (int i = 0; i < threadParams->numBuffers; i++)
			free(threadParams->buffers[i].buffer);
		free(threadParams->buffers);
		gdk_threads_enter();
		gtk_window_close(threadParams->window);
		gdk_threads_leave();
		failWithMsg(stdlink,
				"AndorSDK3StartLiveView:could_not_create_streamer_thread");
		return;
	}
	if (!MLPutInteger64(stdlink, (mlint64) threadParams)) {
		failWithMsgAndError(stdlink, "AndorSDK3StartLiveView:result:error: ");
		return;
	}
}

void AndorSDK3StartLiveViewAcquisition(void) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	if (!threadParams) {
		failWithMsg(stdlink,
				"AndorSDK3StartLiveViewAcquisition:param:invalidAddress");
		return;
	}
	threadParams->mayAcquire = 1;
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink,
				"AndorSDK3StartLiveViewAcquisition:result:error: ");
}

void AndorSDK3StopLiveViewAcquisition(void) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	if (!threadParams) {
		failWithMsg(stdlink,
				"AndorSDK3StopLiveViewAcquisition:param:invalidAddress");
		return;
	}
	threadParams->mayAcquire = 0;
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink,
				"AndorSDK3StopLiveViewAcquisition:result:error: ");
}

void AndorSDK3LiveViewAcquisitionRunning(void) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	if (!threadParams) {
		failWithMsg(stdlink,
				"AndorSDK3LiveViewAcquisitionRunning:param:invalidAddress");
		return;
	}
	if (!MLPutSymbol(stdlink, threadParams->mayAcquire ? "True" : "False"))
		failWithMsgAndError(stdlink,
				"AndorSDK3LiveViewAcquisitionRunning:result:error: ");
}

void AndorSDK3GetLiveViewAcquisitionTimeout(void) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	if (!threadParams) {
		failWithMsg(stdlink,
				"AndorSDK3GetLiveViewAcquisitionTimeout:param:invalidAddress");
		return;
	}
	if (!MLPutReal(stdlink, threadParams->timeout_ms / 1.e3))
		failWithMsgAndError(stdlink,
				"AndorSDK3GetLiveViewAcquisitionTimeout:result:error: ");
}

void AndorSDK3SetLiveViewAcquisitionTimeout(double timeout) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	if (!threadParams) {
		failWithMsg(stdlink,
				"AndorSDK3SetLiveViewAcquisitionTimeout:param:invalidAddress");
		return;
	}
	threadParams->timeout_ms = clip(1000 * timeout, 0, UINT_MAX);
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink,
				"AndorSDK3SetLiveViewAcquisitionTimeout:result:error: ");
}

void AndorSDK3StopLiveView(const char *retrieveBuffersString,
		const char *keepWindowOpenString) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	if (!threadParams) {
		failWithMsg(stdlink, "AndorSDK3StopLiveView:param:invalidAddress");
		return;
	}
	int retrieveBuffers = strcmp(retrieveBuffersString, "True") == 0;
	int keepWindowOpen = strcmp(keepWindowOpenString, "True") == 0;
	if (threadParams->isRunning) {
		threadParams->mayRun = 0;
		int r = pthread_join(threadParams->imageStreamerThread, NULL);
		if (r) {
			fprintf(stderr, "streamer thread join failed: %d\n", r);
			exit(r);
		}
	} else {
		fprintf(stderr, "streamer thread already dead!\n");
	}
	if (threadParams->buffers) {
		if (retrieveBuffers) {
			if (!MLPutFunction(stdlink, "List", threadParams->numBuffers))
				failWithMsgAndError(stdlink,
						"AndorSDK3StopLiveView:result:list:error: ");
			else
				for (int i = 0; i < threadParams->numBuffers; i++)
					if (!MLPutInteger16List(stdlink,
							(int16_t*) threadParams->buffers[i].buffer,
							(threadParams->buffers[i].bufferSize - 1) / 2 + 1))
						failWithMsgAndError(stdlink,
								"AndorSDK3StopLiveView:result:list:buffer:error: ");
		} else if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK3StopLiveView:result:error: ");
		for (int i = 0; i < threadParams->numBuffers; i++) {
			if (threadParams->buffers[i].buffer)
				free(threadParams->buffers[i].buffer);
			threadParams->buffers[i].buffer = NULL;
			threadParams->buffers[i].bufferSize = 0;
		}
		free(threadParams->buffers);
		threadParams->buffers = NULL;
	} else if (retrieveBuffers)
		failWithMsg(stdlink,
				"AndorSDK3StopLiveView:retrieveBuffers:noBuffersAvailable");
	if (threadParams->colorFunctionInterPolationData) {
		free(threadParams->colorFunctionInterPolationData);
		threadParams->colorFunctionInterPolationData = NULL;
	}
	if (!keepWindowOpen) {
		gdk_threads_enter();
		gtk_window_close(threadParams->window);
		threadParams->window = NULL;
		threadParams->image = NULL;
		gdk_threads_leave();
	}
}

void AndorSDK3LiveViewRunning(void) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	if (!threadParams) {
		failWithMsg(stdlink, "AndorSDK3LiveViewRunning:param:invalidAddress");
		return;
	}
	if (!MLPutSymbol(stdlink, threadParams->isRunning ? "True" : "False"))
		failWithMsgAndError(stdlink, "AndorSDK3LiveViewRunning:result:error: ");
}

void AndorSDK3GetLiveViewImages(void) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	if (!threadParams) {
		failWithMsg(stdlink, "AndorSDK3GetLiveViewImages:param:invalidAddress");
		return;
	}
	if (threadParams->buffers) {
		if (!MLPutFunction(stdlink, "List", threadParams->numBuffers))
			failWithMsgAndError(stdlink,
					"AndorSDK3GetLiveViewImages:result:list:error: ");
		else
			for (int i = 0; i < threadParams->numBuffers; i++)
				if (!MLPutInteger16List(stdlink,
						(int16_t*) threadParams->buffers[i].buffer,
						(threadParams->buffers[i].bufferSize - 1) / 2 + 1))
					failWithMsgAndError(stdlink,
							"AndorSDK3GetLiveViewImages:result:list:buffer:error: ");
	} else
		failWithMsg(stdlink, "AndorSDK3GetLiveViewImages:noBuffersAvailable");
}

static inline void queueAccumulationBufferEntry(
		ImageStreamerThreadParams_t *threadParams,
		AndorSDK3AccumulationBuffer_t *accBuffer, int locked) {
	if (accBuffer) {
		accBuffer->next = NULL;
		if (!locked)
			pthread_mutex_lock(
					&threadParams->waitingAccumulationBufferListMutex);
		if (threadParams->waitingAccumulationBufferListTail)
			threadParams->waitingAccumulationBufferListTail->next = accBuffer;
		else
			threadParams->waitingAccumulationBufferListHead = accBuffer;
		threadParams->waitingAccumulationBufferListTail = accBuffer;
		if (!locked)
			pthread_mutex_unlock(
					&threadParams->waitingAccumulationBufferListMutex);
	}
}

static inline AndorSDK3AccumulationBuffer_t *unqueueAccumulationBufferEntry(
		ImageStreamerThreadParams_t *threadParams, int locked) {
	if (!locked)
		pthread_mutex_lock(&threadParams->finishedAccumulationBufferListMutex);
	AndorSDK3AccumulationBuffer_t *finishedAccumulationBuffer =
			threadParams->finishedAccumulationBufferListHead;
	if (finishedAccumulationBuffer != NULL) {
		threadParams->finishedAccumulationBufferListHead =
				finishedAccumulationBuffer->next;
		if (threadParams->finishedAccumulationBufferListTail
				== finishedAccumulationBuffer)
			threadParams->finishedAccumulationBufferListTail =
					threadParams->finishedAccumulationBufferListHead;
		finishedAccumulationBuffer->next = NULL;
	}
	if (!locked)
		pthread_mutex_unlock(
				&threadParams->finishedAccumulationBufferListMutex);
	return finishedAccumulationBuffer;
}

#define AndorSDK3FailOnError1(r,call) (r) = (call); \
	if((r) != AT_SUCCESS) { \
		char tempString[128]; \
		sprintf(tempString, "AndorSDK3FailOnError1: error %d in %s line %d: %s\n", \
				r, __FILE__, __LINE__, AndorSDK3ErrorName(r)); \
		failWithMsg( stdlink, tempString); \
		return;}

void AndorSDK3CreateLiveViewAccumulationBuffers(mlint64 *accumulationList,
		int accumulationListSize) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	AT_64 imageWidth, imageHeight;
	int r;
	AndorSDK3FailOnError1(r,
			AT_GetInt(threadParams->camera, L"AOIWidth", &imageWidth));
	AndorSDK3FailOnError1(r,
			AT_GetInt(threadParams->camera, L"AOIHeight", &imageHeight));
	size_t imageSize = imageWidth * imageHeight;
	if (!MLPutFunction(stdlink, "List", accumulationListSize))
		failWithMsgAndError(stdlink,
				"AndorSDK3CreateLiveViewAccumulationBuffers_Ex:result:error: ");
	else
		for (int i = 0; i < accumulationListSize; i++)
			if (accumulationList[i] > 0) {
				AndorSDK3AccumulationBuffer_t *accBuffer = malloc(
						sizeof(AndorSDK3AccumulationBuffer_t));
				accBuffer->accumulations = accumulationList[i] - 1;
				accBuffer->buffer = calloc(imageSize,
						sizeof(ACCUMULATION_BUFFER_STORAGE_TYPE));
				accBuffer->bufferSize = imageSize;
				accBuffer->next = NULL;
				if (!MLPutInteger64(stdlink, (mlint64) accBuffer)) {
					failWithMsgAndError(stdlink,
							"AndorSDK3CreateLiveViewAccumulationBuffers_Ex:result:error: ");
					return;
				}
			} else if (!MLPutSymbol(stdlink, "Null"))
				failWithMsgAndError(stdlink,
						"AndorSDK3CreateLiveViewAccumulationBuffers_Ex:result:error: ");
}

void AndorSDK3QueueLiveViewAccumulationBuffers(mlint64 *accBufferAddresses,
		int accBufferAddressesLength, mlint64 accumulations) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	AndorSDK3AccumulationBuffer_t *newHead = NULL, *newTail = NULL;
	int entries = 0;
	for (int i = 0; i < accBufferAddressesLength; i++)
		if (accBufferAddresses[i]) {
			AndorSDK3AccumulationBuffer_t *accBuffer =
					(AndorSDK3AccumulationBuffer_t*) accBufferAddresses[i];
			if (accBuffer->next) {
				fprintf(stderr, "requeuing still queued buffer!\n");
				accBuffer->next = NULL;
			}
			if (accumulations > 0)
				accBuffer->accumulations = accumulations - 1;
			if (newHead == NULL)
				newHead = accBuffer;
			else
				newTail->next = accBuffer;
			newTail = accBuffer;
			entries++;
		}
	if (newHead != NULL) {
		pthread_mutex_lock(&threadParams->waitingAccumulationBufferListMutex);
		if (threadParams->waitingAccumulationBufferListHead == NULL)
			threadParams->waitingAccumulationBufferListHead = newHead;
		else
			threadParams->waitingAccumulationBufferListTail->next = newHead;
		threadParams->waitingAccumulationBufferListTail = newTail;
		pthread_mutex_unlock(&threadParams->waitingAccumulationBufferListMutex);
	}
	if (!MLPutInteger(stdlink, entries))
		failWithMsgAndError(stdlink,
				"AndorSDK3QueueLiveViewAccumulationBuffers:result:error: ");
}

void AndorSDK3CreateLiveViewAccumulationBuffersFromFile(const char *fileName,
		mlint64 accumulations) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	if (accumulations <= 0)
		if (!MLPutFunction(stdlink, "List", 0)) {
			failWithMsgAndError(stdlink,
					"AndorSDK3CreateLiveViewAccumulationBuffersFromFile:result:error: ");
			return;
		}
	int r;
	MLINK loop = MLLoopbackOpen(stdenv, &r);
	if (r != MLEOK) {
		MLPutFunction(stdlink, "$Failed", 1);
		MLPutString(stdlink, MLErrorString(stdenv, r));
		return;
	}
	FILE *f = fopen(fileName, "rb");
	if (!f)
		if (!MLPutFunction(stdlink, "$Failed", 2)
				|| !MLPutInteger(stdlink, errno)
				|| !MLPutString(stdlink, strerror(errno))) {
			MLClose(loop);
			failWithMsgAndError(stdlink,
					"AndorSDK3CreateLiveViewAccumulationBuffersFromFile:error:error: ");
			return;
		}
	size_t entries = 0;
	while (1) {
		ACCUMULATION_BUFFER_STORAGE_TYPE bufferSize;
		if (fread(&bufferSize, sizeof(ACCUMULATION_BUFFER_STORAGE_TYPE), 1, f)
				!= 1) {
			if (!feof(f)) {
				MLPutString(loop, "could not read buffer size");
				entries++;
			}
			break;
		}
		AndorSDK3AccumulationBuffer_t *accBuffer = malloc(
				sizeof(AndorSDK3AccumulationBuffer_t));
		accBuffer->accumulations = accumulations;
		accBuffer->bufferSize = bufferSize;
		accBuffer->buffer = calloc(bufferSize,
				sizeof(ACCUMULATION_BUFFER_STORAGE_TYPE));
		if (!accBuffer->buffer) {
			char temp[256];
			sprintf(temp, "could not alloc buffer of size %lld",
					accBuffer->bufferSize);
			MLPutString(loop, temp);
			free(accBuffer);
			entries++;
			break;
		}
		size_t pixelsRead = fread(accBuffer->buffer,
				sizeof(ACCUMULATION_BUFFER_STORAGE_TYPE), accBuffer->bufferSize,
				f);
		if (pixelsRead != accBuffer->bufferSize) {
			char temp[256];
			sprintf(temp, "only %lld of %lld pixels were read (ferror=%d)",
					pixelsRead, accBuffer->bufferSize, ferror(f));
			if (!MLPutFunction(loop, "List", 2)
					|| !MLPutInteger64(loop, (mlint64) accBuffer)
					|| !MLPutString(loop, temp)) {
				MLClose(loop);
				fclose(f);
				failWithMsgAndError(stdlink,
						"AndorSDK3CreateLiveViewAccumulationBuffersFromFile:eof:error: ");
				return;
			}
			entries++;
			break;
		}
		if (!MLPutInteger64(loop, (mlint64) accBuffer)) {
			MLClose(loop);
			fclose(f);
			failWithMsgAndError(stdlink,
					"AndorSDK3CreateLiveViewAccumulationBuffersFromFile:bufferedResult:error: ");
			return;
		}
		entries++;
	}
	fclose(f);
	if (!MLPutFunction(stdlink, "List", entries)
			|| !MLTransferToEndOfLoopbackLink(stdlink, loop))
		failWithMsgAndError(stdlink,
				"AndorSDK3CreateLiveViewAccumulationBuffersFromFile:result:transfer:error: ");
	MLClose(loop);
}

void AndorSDK3ResetLiveViewAccumulationBuffers(mlint64 *accBufferAddresses,
		int accBufferAddressesLength) {
	int entries = 0;
	for (int i = 0; i < accBufferAddressesLength; i++)
		if (accBufferAddresses[i]) {
			AndorSDK3AccumulationBuffer_t *accBuffer =
					(AndorSDK3AccumulationBuffer_t*) accBufferAddresses[i];
			if (accBuffer->buffer)
				memset(accBuffer->buffer, 0,
						sizeof(ACCUMULATION_BUFFER_STORAGE_TYPE)
								* accBuffer->bufferSize);
			else if (accBuffer->bufferSize > 0) {
				failWithMsg(stdlink,
						"AndorSDK3ResetLiveViewAccumulationBuffers: null buffer detected");
				return;
			}
			entries++;
		}
	if (!MLPutInteger(stdlink, entries))
		failWithMsgAndError(stdlink,
				"AndorSDK3ResetLiveViewAccumulationBuffers:result:error: ");
}

void AndorSDK3GetAccumulationCounterOfLiveViewAccumulationBuffers(
		mlint64 *accBufferAddresses, int accBufferAddressesLength) {
	if (!MLPutFunction(stdlink, "List", accBufferAddressesLength))
		failWithMsgAndError(stdlink,
				"AndorSDK3GetAccumulationCounterOfLiveViewAccumulationBuffers:result:head:error: ");
	for (int i = 0; i < accBufferAddressesLength; i++)
		if (accBufferAddresses[i]) {
			AndorSDK3AccumulationBuffer_t *accBuffer =
					(AndorSDK3AccumulationBuffer_t*) accBufferAddresses[i];
			if (!MLPutInteger64(stdlink, accBuffer->accumulations + 1))
				failWithMsgAndError(stdlink,
						"AndorSDK3GetAccumulationCounterOfLiveViewAccumulationBuffers:result:error: ");
		} else if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK3GetAccumulationCounterOfLiveViewAccumulationBuffers:result:null:error: ");
}

void AndorSDK3SetAccumulationCounterOfLiveViewAccumulationBuffers(
		mlint64 *accBufferAddresses, int accBufferAddressesLength,
		mlint64 accumulations) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	int entries = 0;
	for (int i = 0; i < accBufferAddressesLength; i++)
		if (accBufferAddresses[i]) {
			AndorSDK3AccumulationBuffer_t *accBuffer =
					(AndorSDK3AccumulationBuffer_t*) accBufferAddresses[i];
			if (accumulations > 0)
				accBuffer->accumulations = accumulations - 1;
			else {
				accBuffer->accumulations = 0;
				pthread_mutex_lock(
						&threadParams->waitingAccumulationBufferListMutex);
				if (threadParams->waitingAccumulationBufferListHead
						== accBuffer) {
					if (accBuffer->next == accBuffer) {
						accBuffer->next = NULL;
						threadParams->waitingAccumulationBufferListHead = NULL;
						threadParams->waitingAccumulationBufferListTail = NULL;
						pthread_mutex_unlock(
								&threadParams->waitingAccumulationBufferListMutex);
						failWithMsg(stdlink,
								"buffer loop detected and destroyed!");
						return;
					}
					threadParams->waitingAccumulationBufferListHead =
							accBuffer->next;
					if (threadParams->waitingAccumulationBufferListTail
							== accBuffer)
						threadParams->waitingAccumulationBufferListTail = NULL;
				} else if (threadParams->waitingAccumulationBufferListHead) {
					AndorSDK3AccumulationBuffer_t *prev =
							threadParams->waitingAccumulationBufferListHead;
					while (prev->next && prev->next != accBuffer)
						prev = prev->next;
					if (prev->next) {
						prev->next = accBuffer->next;
						if (!prev->next)
							threadParams->waitingAccumulationBufferListTail =
									prev;
					}
				}
				pthread_mutex_unlock(
						&threadParams->waitingAccumulationBufferListMutex);

			}
			entries++;
		}
	if (!MLPutInteger(stdlink, entries))
		failWithMsgAndError(stdlink,
				"AndorSDK3SetAccumulationCounterOfLiveViewAccumulationBuffers:result:error: ");
}

void AndorSDK3DestroyLiveViewAccumulationBuffers(mlint64 *accBufferAddresses,
		int accBufferAddressesLength) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	AndorSDK3AccumulationBuffer_t *newHead = NULL, *newTail = NULL;
	int entries = 0;
	for (int i = 0; i < accBufferAddressesLength; i++)
		if (accBufferAddresses[i]) {
			AndorSDK3AccumulationBuffer_t *entry =
					(AndorSDK3AccumulationBuffer_t*) accBufferAddresses[i];
			entry->next = NULL;
			if (entry->buffer)
				free(entry->buffer);
			entry->buffer = NULL;
			free(entry);
			entries++;
		}
	if (!MLPutInteger(stdlink, entries))
		failWithMsgAndError(stdlink,
				"AndorSDK3DestroyLiveViewAccumulationBuffers:result:error: ");
}

void AndorSDK3DumpLiveViewAccumulationBuffersToFile(mlint64 *accBufferAddresses,
		int accBufferAddressesLength, const char *fileName) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	FILE *f = fopen(fileName, "wb");
	if (!f)
		if (!MLPutFunction(stdlink, "$Failed", 2)
				|| !MLPutInteger(stdlink, errno)
				|| !MLPutString(stdlink, strerror(errno))) {
			failWithMsgAndError(stdlink,
					"AndorSDK3DumpLiveViewAccumulationBuffersToFile:error:error: ");
			return;
		}
	size_t recordsWritten = 0;
	for (int i = 0; i < accBufferAddressesLength; i++) {
		AndorSDK3AccumulationBuffer_t *accBuffer =
				(AndorSDK3AccumulationBuffer_t*) accBufferAddresses[i];
		if (fwrite(&accBuffer->bufferSize,
				sizeof(ACCUMULATION_BUFFER_STORAGE_TYPE), 1, f) != 1) {
			failWithMsg(stdlink,
					"AndorSDK3DumpLiveViewAccumulationBuffersToFile: could not write buffer size");
			fclose(f);
			return;
		}
		recordsWritten++;
		size_t pixelsWritten = fwrite(accBuffer->buffer,
				sizeof(ACCUMULATION_BUFFER_STORAGE_TYPE), accBuffer->bufferSize,
				f);
		if (pixelsWritten != accBuffer->bufferSize) {
			char temp[256];
			sprintf(temp,
					"AndorSDK3DumpLiveViewAccumulationBuffersToFile:error: only %lld of %lld pixels were written (ferror=%d)",
					pixelsWritten, accBuffer->bufferSize, ferror(f));
			failWithMsg(stdlink, temp);
			fclose(f);
			return;
		}
		recordsWritten += pixelsWritten;
	}
	fclose(f);
	if (!MLPutInteger64(stdlink,
			sizeof(ACCUMULATION_BUFFER_STORAGE_TYPE) * recordsWritten))
		failWithMsgAndError(stdlink,
				"AndorSDK3DumpLiveViewAccumulationBuffersToFile:result:error: ");
}

void AndorSDK3GetLiveViewAccumulationBufferData(mlint64 accBufferAddress) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	if (accBufferAddress) {
		AndorSDK3AccumulationBuffer_t *accBuffer =
				(AndorSDK3AccumulationBuffer_t*) accBufferAddress;
		if (!
#if ACCUMULATION_BUFFER_STORAGE_BITS == 64
		MLPutInteger64List
#else
		MLPutInteger32List
#endif
		(stdlink, accBuffer->buffer, accBuffer->bufferSize))
			failWithMsgAndError(stdlink,
					"AndorSDK3GetLiveViewAccumulationBufferData:result:error: ");
	} else if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink,
				"AndorSDK3GetLiveViewAccumulationBufferData:result:error: ");
}

void AndorSDK3ExtractPOIformLiveViewAccumulationBuffers(int *poi,
		long poiLength, mlint64 *accBufferAddresses,
		int accBufferAddressesLength) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	if (!MLPutFunction(stdlink, "List", accBufferAddressesLength))
		failWithMsgAndError(stdlink,
				"AndorSDK3ExtractPOIformLiveViewAccumulationBuffers:result:error: ");
	ACCUMULATION_BUFFER_STORAGE_TYPE *poiBuffer = calloc(poiLength,
			sizeof(ACCUMULATION_BUFFER_STORAGE_TYPE));
	for (int i = 0; i < accBufferAddressesLength; i++)
		if (accBufferAddresses[i]) {
			AndorSDK3AccumulationBuffer_t *entry =
					(AndorSDK3AccumulationBuffer_t*) accBufferAddresses[i];
			for (int i = 0; i < poiLength; i++)
				poiBuffer[i] =
						poi[i] >= 0 && poi[i] < entry->bufferSize ?
								entry->buffer[poi[i]] : 0;
			if (!
#if ACCUMULATION_BUFFER_STORAGE_BITS == 64
			MLPutInteger64List
#else
			MLPutInteger32List
#endif
			(stdlink, poiBuffer, poiLength))
				failWithMsgAndError(stdlink,
						"AndorSDK3ExtractPOIformLiveViewAccumulationBuffers:extraction:error: ");
		} else if (!MLPutSymbol(stdlink, "Null")) {
			failWithMsgAndError(stdlink,
					"AndorSDK3ExtractPOIformLiveViewAccumulationBuffers:result:error: ");
			return;
		}
	free(poiBuffer);
}

void AndorSDK3LiveViewAccumulationBufferFinished(void) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	if (!MLPutSymbol(stdlink,
			threadParams->finishedAccumulationBufferListHead != NULL ?
					"True" : "False"))
		failWithMsgAndError(stdlink,
				"AndorSDK3LiveViewAccumulationBufferFinished:result:error: ");
}

void AndorSDK3LiveViewAccumulationBuffersFinished(void) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	if (!MLPutSymbol(stdlink,
			threadParams->waitingAccumulationBufferListHead == NULL ?
					"True" : "False"))
		failWithMsgAndError(stdlink,
				"AndorSDK3LiveViewAccumulationBuffersFinished:result:error: ");
}

void AndorSDK3GetFinishedLiveViewAccumulationBuffers(const char *clearQueueBool) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	int clearQueue = strcmp(clearQueueBool, "True") == 0;
	int r;
	MLINK loop = MLLoopbackOpen(stdenv, &r);
	if (r != MLEOK) {
		MLPutFunction(stdlink, "$Failed", 1);
		MLPutString(stdlink, MLErrorString(stdenv, r));
		return;
	}
	pthread_mutex_lock(&threadParams->finishedAccumulationBufferListMutex);
	AndorSDK3AccumulationBuffer_t *finishedAccumulationBuffer =
			threadParams->finishedAccumulationBufferListHead;
	if (clearQueue) {
		threadParams->finishedAccumulationBufferListHead = NULL;
		threadParams->finishedAccumulationBufferListTail = NULL;
		pthread_mutex_unlock(
				&threadParams->finishedAccumulationBufferListMutex);
	}
	int entries = 0;
	while (finishedAccumulationBuffer) {
		if (!MLPutInteger64(loop, (mlint64) finishedAccumulationBuffer)) {
			failWithMsgAndError(stdlink,
					"AndorSDK3GetFinishedLiveViewAccumulationBuffers:result:error: ");
			MLClose(loop);
			return;
		}
		AndorSDK3AccumulationBuffer_t *next = finishedAccumulationBuffer->next;
		if (clearQueue)
			finishedAccumulationBuffer->next = NULL;
		finishedAccumulationBuffer = next;
		entries++;
	}
	if (!clearQueue)
		pthread_mutex_unlock(
				&threadParams->finishedAccumulationBufferListMutex);
	MLEndPacket(loop);
	if (!MLPutFunction(stdlink, "List", entries)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3GetFinishedLiveViewAccumulationBuffers:result:list:error: ");
		MLClose(loop);
		return;
	}
	if (!MLTransferToEndOfLoopbackLink(stdlink, loop))
		failWithMsgAndError(stdlink,
				"AndorSDK3GetFinishedLiveViewAccumulationBuffers:result:transfer:error: ");
	MLClose(loop);
}

void AndorSDK3GetWaitingLiveViewAccumulationBuffers(const char *clearQueueBool) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	int clearQueue = strcmp(clearQueueBool, "True") == 0;
	int r;
	MLINK loop = MLLoopbackOpen(stdenv, &r);
	if (r != MLEOK) {
		MLPutFunction(stdlink, "$Failed", 1);
		MLPutString(stdlink, MLErrorString(stdenv, r));
		return;
	}
	pthread_mutex_lock(&threadParams->waitingAccumulationBufferListMutex);
	AndorSDK3AccumulationBuffer_t *waitingAccumulationBuffer =
			threadParams->waitingAccumulationBufferListHead;
	if (clearQueue) {
		threadParams->waitingAccumulationBufferListHead = NULL;
		threadParams->waitingAccumulationBufferListTail = NULL;
		pthread_mutex_unlock(&threadParams->waitingAccumulationBufferListMutex);
	}
	int entries = 0;
	while (waitingAccumulationBuffer) {
		if (!MLPutInteger64(loop, (mlint64) waitingAccumulationBuffer)) {
			failWithMsgAndError(stdlink,
					"AndorSDK3GetFinishedLiveViewAccumulationBuffers:result:error: ");
			MLClose(loop);
			return;
		}
		AndorSDK3AccumulationBuffer_t *next = waitingAccumulationBuffer->next;
		if (clearQueue)
			waitingAccumulationBuffer->next = NULL;
		waitingAccumulationBuffer = next;
		entries++;
	}
	if (!clearQueue)
		pthread_mutex_unlock(&threadParams->waitingAccumulationBufferListMutex);
	MLEndPacket(loop);
	if (!MLPutFunction(stdlink, "List", entries)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3GetWaitingLiveViewAccumulationBuffers:result:list:error: ");
		MLClose(loop);
		return;
	}
	if (!MLTransferToEndOfLoopbackLink(stdlink, loop))
		failWithMsgAndError(stdlink,
				"AndorSDK3GetWaitingLiveViewAccumulationBuffers:result:transfer:error: ");
	MLClose(loop);
}

void AndorSDK3WaitForFinishedLiveViewAccumulationBuffer(double timeout) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	pthread_mutex_lock(&threadParams->finishedAccumulationBufferListMutex);
	if (threadParams->finishedAccumulationBufferListHead == NULL) {
		pthread_mutex_lock(&threadParams->waitingAccumulationBufferListMutex);
		if (threadParams->finishedAccumulationBufferListHead == NULL
				&& threadParams->waitingAccumulationBufferListHead == NULL) {
			if (!MLPutSymbol(stdlink, "Null"))
				failWithMsgAndError(stdlink,
						"AndorSDK3WaitForFinishedLiveViewAccumulationBuffer:result:error: ");
			pthread_mutex_unlock(
					&threadParams->waitingAccumulationBufferListMutex);
			pthread_mutex_unlock(
					&threadParams->finishedAccumulationBufferListMutex);
			return;
		}
		pthread_mutex_unlock(&threadParams->waitingAccumulationBufferListMutex);
	}
	if (timeout > 0) {
		struct timespec absTime;
		clock_gettime(CLOCK_REALTIME, &absTime);
		int64_t intTime = floor(timeout);
		int64_t fracTime = (timeout - intTime) * 1.e9;
		absTime.tv_nsec += fracTime;
		if (absTime.tv_nsec >= 1000000000ll) {
			absTime.tv_nsec -= 1000000000ll;
			absTime.tv_sec += intTime + 1;
		} else
			absTime.tv_sec += intTime;
		if (threadParams->finishedAccumulationBufferListHead == NULL)
			pthread_cond_timedwait(
					&threadParams->finishedAccumulationBufferListNotEmptyCondition,
					&threadParams->finishedAccumulationBufferListMutex,
					&absTime);
	} else if (timeout < 0)
		while (threadParams->finishedAccumulationBufferListHead == NULL)
			pthread_cond_wait(
					&threadParams->finishedAccumulationBufferListNotEmptyCondition,
					&threadParams->finishedAccumulationBufferListMutex);
	else if (threadParams->finishedAccumulationBufferListHead == NULL) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK3WaitForFinishedLiveViewAccumulationBuffer:result:error: ");
		pthread_mutex_unlock(
				&threadParams->finishedAccumulationBufferListMutex);
		return;
	}
	AndorSDK3AccumulationBuffer_t *finishedAccumulationBuffer =
			unqueueAccumulationBufferEntry(threadParams, 1);
	pthread_mutex_unlock(&threadParams->finishedAccumulationBufferListMutex);
	if (!MLPutInteger64(stdlink, (mlint64) finishedAccumulationBuffer))
		failWithMsgAndError(stdlink,
				"AndorSDK3WaitForFinishedLiveViewAccumulationBuffer:result:error: ");
}

void AndorSDK3GetCurrentLiveViewAccumulationBufferData(void) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	if (threadParams->waitingAccumulationBufferListHead != NULL) {
		pthread_mutex_lock(&threadParams->currentAccumulationBufferMutex);
		if (!
#if ACCUMULATION_BUFFER_STORAGE_BITS == 64
		MLPutInteger64List
#else
		MLPutInteger32List
#endif
		(stdlink, threadParams->waitingAccumulationBufferListHead->buffer,
				threadParams->waitingAccumulationBufferListHead->bufferSize))
			failWithMsgAndError(stdlink,
					"AndorSDK3FetchCurrentAccumulationBuffer:result:error: ");
		pthread_mutex_unlock(&threadParams->currentAccumulationBufferMutex);
	} else if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink,
				"AndorSDK3FetchCurrentAccumulationBuffer:result:error: ");
}

void AndorSDK3ComputeContrastsOfLiveViewAccumulationBuffers(
		mlint64 *accBufferAddresses, int accBufferAddressesLength) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	if (!MLPutFunction(stdlink, "List", accBufferAddressesLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3ComputeContrastsOfLiveViewAccumulationBuffers:result:error: ");
		return;
	}
	AT_64 imageWidth, imageHeight;
	int r;
	AndorSDK3FailOnError1(r,
			AT_GetInt(threadParams->camera, L"AOIWidth", &imageWidth));
	AndorSDK3FailOnError1(r,
			AT_GetInt(threadParams->camera, L"AOIHeight", &imageHeight));
	for (int i = 0; i < accBufferAddressesLength; i++)
		if (accBufferAddresses[i]) {
			AndorSDK3AccumulationBuffer_t *entry =
					(AndorSDK3AccumulationBuffer_t*) accBufferAddresses[i];
			mlint64 contrastSquareSum, contrastSum, sum;
			ACCUMULATION_BUFFER_STORAGE_TYPE *buffer = entry->buffer;
			ACCUMULATION_BUFFER_STORAGE_TYPE bufferSize = entry->bufferSize;
			if (bufferSize != imageWidth * imageHeight) {
				if (!MLPutString(stdlink, "WrongBufferSize")) {
					failWithMsgAndError(stdlink,
							"AndorSDK3ComputeContrastsOfLiveViewAccumulationBuffers:result:error: ");
					return;
				}
				continue;
			}
#define sqr(a) (a)*(a)
			if (imageWidth <= 0 || imageHeight <= 0) {
				contrastSquareSum = 0;
				sum = 0;
			} else if (imageWidth == 1 && imageHeight == 1) {
				contrastSum = buffer[0];
				contrastSquareSum = sqr(contrastSum);
				sum = buffer[0];
			} else if (imageWidth == 1 || imageHeight == 1) {
				mlint64 x = buffer[1];
				contrastSum = x;
				contrastSquareSum = sqr(x);
				sum = buffer[0];
				for (int i = 1; i < bufferSize - 1; i++) {
					x = -buffer[i - 1] + buffer[i + 1];
					contrastSum += x;
					contrastSquareSum += sqr(x);
					sum += buffer[i];
				}
				x = -buffer[bufferSize - 2];
				contrastSum += x;
				contrastSquareSum += sqr(x);
				sum += buffer[bufferSize - 1];
			} else {
				ACCUMULATION_BUFFER_STORAGE_TYPE *firstRow = &buffer[0],
						*secondRow = &buffer[imageWidth];
				mlint64 x = firstRow[1] + secondRow[0];
				contrastSum = x;
				contrastSquareSum = sqr(x);
				sum = firstRow[0];
				for (int col = 1; col < imageWidth - 1; col++) {
					x = -firstRow[col - 1] + firstRow[col + 1] + secondRow[col];
					contrastSum += x;
					contrastSquareSum += sqr(x);
					sum += firstRow[col];
				}
				x = -firstRow[imageWidth - 2] + secondRow[imageWidth - 1];
				contrastSum += x;
				contrastSquareSum += sqr(x);
				sum += firstRow[imageWidth - 1];
				for (int row = 1; row < imageHeight - 2; row++) {
					ACCUMULATION_BUFFER_STORAGE_TYPE *previousRow = &buffer[(row
							- 1) * imageWidth], *currentRow = &buffer[row
							* imageWidth], *nextRow = &buffer[(row + 1)
							* imageWidth];
					x = -previousRow[0] + nextRow[0] + currentRow[1];
					contrastSum += x;
					contrastSquareSum += sqr(x);
					sum += currentRow[0];
					for (int col = 1; col < imageWidth - 1; col++) {
						x = -currentRow[col - 1] - previousRow[col]
								+ nextRow[col] + currentRow[col + 1];
						contrastSum += x;
						contrastSquareSum += sqr(x);
						sum += currentRow[col];
					}
					x = -currentRow[imageWidth - 2]
							- previousRow[imageWidth - 1]
							+ nextRow[imageWidth - 1];
					contrastSum += x;
					contrastSquareSum += sqr(x);
					sum += currentRow[imageWidth - 1];
				}
				ACCUMULATION_BUFFER_STORAGE_TYPE *penultimateRow =
						&buffer[(imageHeight - 2) * imageWidth], *lastRow =
						&buffer[(imageHeight - 1) * imageWidth];
				x = lastRow[1] - penultimateRow[0];
				contrastSum += x;
				contrastSquareSum += sqr(x);
				sum += lastRow[0];
				for (int col = 1; col < imageWidth - 1; col++) {
					x = -lastRow[col - 1] + penultimateRow[col]
							+ lastRow[col + 1];
					contrastSum += x;
					contrastSquareSum += sqr(x);
					sum += lastRow[col];
				}
				x = lastRow[imageWidth - 2] - penultimateRow[imageWidth - 1];
				contrastSum += x;
				contrastSquareSum += sqr(x);
				sum += lastRow[imageWidth - 1];
			}
			if (!MLPutFunction(stdlink, "List", 3)
					|| !MLPutInteger64(stdlink, contrastSum)
					|| !MLPutInteger64(stdlink, contrastSquareSum)
					|| !MLPutInteger64(stdlink, sum)) {
				failWithMsgAndError(stdlink,
						"AndorSDK3ComputeContrastsOfLiveViewAccumulationBuffers:result:error: ");
				return;
			}
		} else if (!MLPutSymbol(stdlink, "Null")) {
			failWithMsgAndError(stdlink,
					"AndorSDK3ComputeContrastsOfLiveViewAccumulationBuffers:result:error: ");
			return;
		}
}

void AndorSDK3ComputeStatisticsOfLiveViewAccumulationBuffers(
		mlint64 *accBufferAddresses, int accBufferAddressesLength) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	if (!MLPutFunction(stdlink, "List", accBufferAddressesLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3ComputeStatisticsOfLiveViewAccumulationBuffers:result:error: ");
		return;
	}
	AT_64 imageWidth, imageHeight;
	int r;
	AndorSDK3FailOnError1(r,
			AT_GetInt(threadParams->camera, L"AOIWidth", &imageWidth));
	AndorSDK3FailOnError1(r,
			AT_GetInt(threadParams->camera, L"AOIHeight", &imageHeight));
	for (int i = 0; i < accBufferAddressesLength; i++)
		if (accBufferAddresses[i]) {
			AndorSDK3AccumulationBuffer_t *entry =
					(AndorSDK3AccumulationBuffer_t*) accBufferAddresses[i];
			ACCUMULATION_BUFFER_STORAGE_TYPE *buffer = entry->buffer;
			ACCUMULATION_BUFFER_STORAGE_TYPE n = entry->bufferSize;
			if (n != imageWidth * imageHeight) {
				if (!MLPutString(stdlink, "WrongBufferSize")) {
					failWithMsgAndError(stdlink,
							"AndorSDK3ComputeStatisticsOfLiveViewAccumulationBuffers:result:error: ");
					return;
				}
				continue;
			}
			if (n > 1) {
				mlint64 sum = 0, summedSquares = 0;
				for (int i = 0; i < n; i++) {
					sum += buffer[i];
					summedSquares += buffer[i] * buffer[i];
				}
				if (!MLPutFunction(stdlink, "List", 2)
						|| !MLPutReal64(stdlink, ((double) sum) / n)
						|| !MLPutReal64(stdlink,
								(((double) summedSquares)
										- ((double) (sum * sum)) / n)
										/ (n - 1))) {
					failWithMsgAndError(stdlink,
							"AndorSDK3ComputeStatisticsOfLiveViewAccumulationBuffers:result:error: ");
					return;
				}
			} else if (n == 1) {
				if (!MLPutFunction(stdlink, "List", 2)
						|| !MLPutReal64(stdlink, buffer[0])
						|| !MLPutReal64(stdlink, 0)) {
					failWithMsgAndError(stdlink,
							"AndorSDK3ComputeStatisticsOfLiveViewAccumulationBuffers:result:error: ");
					return;
				}
			} else {
				if (!MLPutFunction(stdlink, "List", 2)
						|| !MLPutReal64(stdlink, 0)
						|| !MLPutReal64(stdlink, 0)) {
					failWithMsgAndError(stdlink,
							"AndorSDK3ComputeStatisticsOfLiveViewAccumulationBuffers:result:error: ");
					return;
				}
			}
		} else if (!MLPutSymbol(stdlink, "Null")) {
			failWithMsgAndError(stdlink,
					"AndorSDK3ComputeStatisticsOfLiveViewAccumulationBuffers:result:error: ");
			return;
		}
}

void AndorSDK3Compute33ConvolutionSumOfLiveViewAccumulationBuffers(
		mlint64 *accBufferAddresses, int accBufferAddressesLength) {
	volatile ImageStreamerThreadParams_t *threadParams =
			&globalImageStreamerThreadParams;
	if (!MLPutFunction(stdlink, "List", accBufferAddressesLength)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3ComputeConvolutionSumOfLiveViewAccumulationBuffers:result:error: ");
		return;
	}
	AT_64 imageWidth, imageHeight;
	int r;
	AndorSDK3FailOnError1(r,
			AT_GetInt(threadParams->camera, L"AOIWidth", &imageWidth));
	AndorSDK3FailOnError1(r,
			AT_GetInt(threadParams->camera, L"AOIHeight", &imageHeight));
	double a[3][3], *b;
	int depth, *dims;
	char **bHeads;
	if (!MLGetReal64Array(stdlink, &b, &dims, &bHeads, &depth)) {
		failWithMsgAndError(stdlink,
				"AndorSDK3ComputeConvolutionSumOfLiveViewAccumulationBuffers:result:error: ");
		return;
	}
	if (depth != 2 || dims[0] != 3 || dims[1] != 3) {
		failWithMsg(stdlink,
				"AndorSDK3ComputeConvolutionSumOfLiveViewAccumulationBuffers:a: wrong dimensions");
		return;
	}
	memcpy(a, b, 3 * 3 * sizeof(double));
	MLReleaseReal64Array(stdlink, b, dims, bHeads, depth);
	for (int i = 0; i < accBufferAddressesLength; i++)
		if (accBufferAddresses[i]) {
			AndorSDK3AccumulationBuffer_t *entry =
					(AndorSDK3AccumulationBuffer_t*) accBufferAddresses[i];
			double sum;
			ACCUMULATION_BUFFER_STORAGE_TYPE max;
			ACCUMULATION_BUFFER_STORAGE_TYPE *buffer = entry->buffer;
			ACCUMULATION_BUFFER_STORAGE_TYPE bufferSize = entry->bufferSize;
			if (bufferSize != imageWidth * imageHeight) {
				if (!MLPutString(stdlink, "WrongBufferSize")) {
					failWithMsgAndError(stdlink,
							"AndorSDK3ComputeContrastsOfLiveViewAccumulationBuffers:result:error: ");
					return;
				}
				continue;
			}
			if (imageWidth <= 0 || imageHeight <= 0) {
				sum = 0;
				max = 0;
			} else if (imageWidth == 1 && imageHeight == 1) {
				sum = buffer[0] * a[1][1];
				max = buffer[0];
			} else if (imageWidth == 1 || imageHeight == 1) {
				max = 0;
				for (int i = 1; i < bufferSize; i++)
					if (buffer[i] > max)
						max = buffer[i];
				sum = (buffer[0] * a[1][1] + buffer[1] * a[1][0]) / max;
				for (int i = 1; i < bufferSize - 1; i++)
					sum += (buffer[i - 1] * a[1][2] + buffer[i] * a[1][1]
							+ buffer[i + 1] * a[1][0]) / max;
				sum += (buffer[bufferSize - 2] * a[1][0]
						+ buffer[bufferSize - 1] * a[1][1]) / max;
			} else {
				max = 0;
				for (int i = 1; i < bufferSize; i++)
					if (buffer[i] > max)
						max = buffer[i];
				ACCUMULATION_BUFFER_STORAGE_TYPE *firstRow = &buffer[0],
						*secondRow = &buffer[imageWidth];
				sum = (firstRow[0] * a[1][1] + firstRow[1] * a[1][0]
						+ secondRow[0] * a[0][1] + secondRow[1] * a[0][0])
						/ max; // top-left pixel
				for (int col = 1; col < imageWidth - 1; col++) // top row
					sum += (firstRow[col - 1] * a[1][2]
							+ firstRow[col] * a[1][1]
							+ firstRow[col + 1] * a[1][0]
							+ secondRow[col - 1] * a[0][2]
							+ secondRow[col] * a[0][1]
							+ secondRow[col + 1] * a[0][0]) / max;
				sum += (firstRow[imageWidth - 2] * a[1][2]
						+ firstRow[imageWidth - 1] * a[1][1]
						+ secondRow[imageWidth - 2] * a[0][2]
						+ secondRow[imageWidth - 1] * a[0][1]) / max; // top-right pixel
				for (int row = 1; row < imageHeight - 2; row++) { // inner rows
					ACCUMULATION_BUFFER_STORAGE_TYPE *previousRow = &buffer[(row
							- 1) * imageWidth], *currentRow = &buffer[row
							* imageWidth], *nextRow = &buffer[(row + 1)
							* imageWidth];
					sum += (previousRow[0] * a[2][1] + previousRow[1] * a[2][0]
							+ currentRow[0] * a[1][1] + currentRow[1] * a[1][0]
							+ nextRow[0] * a[0][1] + nextRow[1] * a[0][0]); // left pixel
					for (int col = 1; col < imageWidth - 1; col++) // inner pixels
						sum += (previousRow[col - 1] * a[2][0]
								+ previousRow[col] * a[2][1]
								+ previousRow[col + 1] * a[2][0]
								+ currentRow[col - 1] * a[1][2]
								+ currentRow[col] * a[1][1]
								+ currentRow[col + 1] * a[1][0]
								+ nextRow[col - 1] * a[0][2]
								+ nextRow[col] * a[0][1]
								+ nextRow[col + 1] * a[0][0]) / max;
					sum += (previousRow[imageWidth - 2] * a[2][2]
							+ previousRow[imageWidth - 1] * a[2][1]
							+ currentRow[imageWidth - 2] * a[1][2]
							+ currentRow[imageWidth - 1] * a[1][1]
							+ nextRow[imageWidth - 2] * a[0][2]
							+ nextRow[imageWidth - 1] * a[0][1]) / max; // right pixel
				}
				ACCUMULATION_BUFFER_STORAGE_TYPE *penultimateRow =
						&buffer[(imageHeight - 2) * imageWidth], *lastRow =
						&buffer[(imageHeight - 1) * imageWidth];
				sum += (penultimateRow[0] * a[2][1]
						+ penultimateRow[1] * a[2][0] + lastRow[0] * a[1][1]
						+ lastRow[1] * a[1][0]) / max; // bottom-left pixel
				for (int col = 1; col < imageWidth - 1; col++) // bottom row
					sum += (penultimateRow[col - 1] * a[2][2]
							+ penultimateRow[col] * a[2][1]
							+ penultimateRow[col + 1] * a[2][0]
							+ lastRow[col - 1] * a[1][2]
							+ lastRow[col] * a[1][1]
							+ lastRow[col + 1] * a[1][0]) / max;
				sum += (penultimateRow[imageWidth - 2] * a[2][2]
						+ penultimateRow[imageWidth - 1] * a[2][1]
						+ lastRow[imageWidth - 2] * a[1][2]
						+ lastRow[imageWidth - 1] * a[1][1]) / max; // bottom-rightt pixel
			}
			if (!MLPutFunction(stdlink, "List", 2) || !MLPutReal64(stdlink, sum)
					|| !MLPutInteger64(stdlink, max)) {
				failWithMsgAndError(stdlink,
						"AndorSDK3ComputeContrastsOfLiveViewAccumulationBuffers:result:error: ");
				return;
			}
		} else if (!MLPutSymbol(stdlink, "Null")) {
			failWithMsgAndError(stdlink,
					"AndorSDK3ComputeContrastsOfLiveViewAccumulationBuffers:result:error: ");
			return;
		}
}

int main(int argc, char **argv) {
	if (!g_thread_supported())
		g_thread_init(NULL);
	gdk_threads_init();
	gtk_windowing_available = gtk_init_check(&argc, &argv);
	if (gtk_windowing_available)
		printf("no windowing available, live view will not work\n");
	sargc = argc;
	sargv = argv;
	int r = AT_InitialiseLibrary();
	if (r != AT_SUCCESS) {
		printf("Error: could not initialize AndorSDK3 library: %s",
				AndorSDK3ErrorName(r));
		return r;
	}
	r = pthread_create(&gtkThread, NULL, gtkThreadproc, NULL);
	if (r) {
		fprintf(stderr,
				"error while starting gtk thread (%d), live view will not work\n",
				r);
		gtk_windowing_available = FALSE;
	}
	int s = MLMain(argc, argv);
	gdk_threads_enter();
	gtk_main_quit();
	gdk_threads_leave();
	r = AT_FinaliseLibrary();
	if (r != AT_SUCCESS) {
		printf("Error: could not finalise AndorSDK3 library: %s",
				AndorSDK3ErrorName(r));
		return r;
	}
	return s;
}
