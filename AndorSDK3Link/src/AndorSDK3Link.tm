void AndorSDK3Open(int);

:Begin:
:Function:		AndorSDK3Open
:Pattern:		AndorSDK3Open[cameraIndex_Integer]
:Arguments:		{ cameraIndex }
:ArgumentTypes:	{ Integer }
:ReturnType:	Manual
:End:

void AndorSDK3Close(AT_H);

:Begin:
:Function:		AndorSDK3Close
:Pattern:		AndorSDK3Close[hndl_Integer]
:Arguments:		{ hndl }
:ArgumentTypes:	{ Integer }
:ReturnType:	Manual
:End:

void AndorSDK3IsImplemented(AT_H);

:Begin:
:Function:		AndorSDK3IsImplemented
:Pattern:		AndorSDK3IsImplemented[hndl_Integer, feature_String]
:Arguments:		{ hndl, feature }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3IsReadable(AT_H);

:Begin:
:Function:		AndorSDK3IsReadable
:Pattern:		AndorSDK3IsReadable[hndl_Integer, feature_String]
:Arguments:		{ hndl, feature }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3IsWritable(AT_H);

:Begin:
:Function:		AndorSDK3IsWritable
:Pattern:		AndorSDK3IsWritable[hndl_Integer, feature_String]
:Arguments:		{ hndl, feature }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3IsReadOnly(AT_H);

:Begin:
:Function:		AndorSDK3IsReadOnly
:Pattern:		AndorSDK3IsReadOnly[hndl_Integer, feature_String]
:Arguments:		{ hndl, feature }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3SetInt(AT_H);

:Begin:
:Function:		AndorSDK3SetInt
:Pattern:		AndorSDK3SetInt[hndl_Integer, feature_String, value_Integer]
:Arguments:		{ hndl, feature, value }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3GetInt(AT_H);

:Begin:
:Function:		AndorSDK3GetInt
:Pattern:		AndorSDK3GetInt[hndl_Integer, feature_String]
:Arguments:		{ hndl, feature }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3GetIntMax(AT_H);

:Begin:
:Function:		AndorSDK3GetIntMax
:Pattern:		AndorSDK3GetIntMax[hndl_Integer, feature_String]
:Arguments:		{ hndl, feature }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3GetIntMin(AT_H);

:Begin:
:Function:		AndorSDK3GetIntMin
:Pattern:		AndorSDK3GetIntMin[hndl_Integer, feature_String]
:Arguments:		{ hndl, feature }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3SetFloat(AT_H);

:Begin:
:Function:		AndorSDK3SetFloat
:Pattern:		AndorSDK3SetFloat[hndl_Integer, feature_String, value_Real]
:Arguments:		{ hndl, feature, value }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3GetFloat(AT_H);

:Begin:
:Function:		AndorSDK3GetFloat
:Pattern:		AndorSDK3GetFloat[hndl_Integer, feature_String]
:Arguments:		{ hndl, feature }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3GetFloatMax(AT_H);

:Begin:
:Function:		AndorSDK3GetFloatMax
:Pattern:		AndorSDK3GetFloatMax[hndl_Integer, feature_String]
:Arguments:		{ hndl, feature }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3GetFloatMin(AT_H);

:Begin:
:Function:		AndorSDK3GetFloatMin
:Pattern:		AndorSDK3GetFloatMin[hndl_Integer, feature_String]
:Arguments:		{ hndl, feature }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3SetBool(AT_H);

:Begin:
:Function:		AndorSDK3SetBool
:Pattern:		AndorSDK3SetBool[hndl_Integer, feature_String, value:(True|False)]
:Arguments:		{ hndl, feature, value }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3GetBool(AT_H);

:Begin:
:Function:		AndorSDK3GetBool
:Pattern:		AndorSDK3GetBool[hndl_Integer, feature_String]
:Arguments:		{ hndl, feature }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3SetEnumerated(AT_H);

:Begin:
:Function:		AndorSDK3SetEnumerated
:Pattern:		AndorSDK3SetEnumerated[hndl_Integer, feature_String, value_Integer]
:Arguments:		{ hndl, feature, value }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:


void AndorSDK3SetEnumeratedString(AT_H);

:Begin:
:Function:		AndorSDK3SetEnumeratedString
:Pattern:		AndorSDK3SetEnumeratedString[hndl_Integer, feature_String, value_String]
:Arguments:		{ hndl, feature, value }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3GetEnumerated(AT_H);

:Begin:
:Function:		AndorSDK3GetEnumerated
:Pattern:		AndorSDK3GetEnumerated[hndl_Integer, feature_String]
:Arguments:		{ hndl, feature }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3GetEnumeratedStringByIndex(AT_H);

:Begin:
:Function:		AndorSDK3GetEnumeratedStringByIndex
:Pattern:		AndorSDK3GetEnumeratedStringByIndex[hndl_Integer, feature_String,
					index_Integer]
:Arguments:		{ hndl, feature, index }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3GetEnumeratedString(AT_H);

:Begin:
:Function:		AndorSDK3GetEnumeratedString
:Pattern:		AndorSDK3GetEnumeratedString[hndl_Integer, feature_String]
:Arguments:		{ hndl, feature }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3GetEnumeratedCount(AT_H);

:Begin:
:Function:		AndorSDK3GetEnumeratedCount
:Pattern:		AndorSDK3GetEnumeratedCount[hndl_Integer, feature_String]
:Arguments:		{ hndl, feature }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3IsEnumeratedIndexAvailable(AT_H);

:Begin:
:Function:		AndorSDK3IsEnumeratedIndexAvailable
:Pattern:		AndorSDK3IsEnumeratedIndexAvailable[hndl_Integer, feature_String, index_Integer]
:Arguments:		{ hndl, feature, index }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3IsEnumeratedIndexImplemented(AT_H);

:Begin:
:Function:		AndorSDK3IsEnumeratedIndexImplemented
:Pattern:		AndorSDK3IsEnumeratedIndexImplemented[hndl_Integer, feature_String, index_Integer]
:Arguments:		{ hndl, feature, index }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:


void AndorSDK3SetEnumIndex(AT_H);

:Begin:
:Function:		AndorSDK3SetEnumIndex
:Pattern:		AndorSDK3SetEnumIndex[hndl_Integer, feature_String, value_Integer]
:Arguments:		{ hndl, feature, value }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:


void AndorSDK3SetEnumString(AT_H);

:Begin:
:Function:		AndorSDK3SetEnumString
:Pattern:		AndorSDK3SetEnumString[hndl_Integer, feature_String, value_String]
:Arguments:		{ hndl, feature, value }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3GetEnumIndex(AT_H);

:Begin:
:Function:		AndorSDK3GetEnumIndex
:Pattern:		AndorSDK3GetEnumIndex[hndl_Integer, feature_String]
:Arguments:		{ hndl, feature }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3GetEnumStringByIndex(AT_H);

:Begin:
:Function:		AndorSDK3GetEnumStringByIndex
:Pattern:		AndorSDK3GetEnumStringByIndex[hndl_Integer, feature_String, index_Integer]
:Arguments:		{ hndl, feature, index }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3GetEnumString(AT_H);

:Begin:
:Function:		AndorSDK3GetEnumString
:Pattern:		AndorSDK3GetEnumString[hndl_Integer, feature_String]
:Arguments:		{ hndl, feature }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:


void AndorSDK3GetEnumCount(AT_H);

:Begin:
:Function:		AndorSDK3GetEnumCount
:Pattern:		AndorSDK3GetEnumCount[hndl_Integer, feature_String]
:Arguments:		{ hndl, feature }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3IsEnumIndexAvailable(AT_H);

:Begin:
:Function:		AndorSDK3IsEnumIndexAvailable
:Pattern:		AndorSDK3IsEnumIndexAvailable[hndl_Integer, feature_String, index_Integer]
:Arguments:		{ hndl, feature, index }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3IsEnumIndexImplemented(AT_H);

:Begin:
:Function:		AndorSDK3IsEnumIndexImplemented
:Pattern:		AndorSDK3IsEnumIndexImplemented[hndl_Integer, feature_String, index_Integer]
:Arguments:		{ hndl, feature, index }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3SetString(AT_H);

:Begin:
:Function:		AndorSDK3SetString
:Pattern:		AndorSDK3SetString[hndl_Integer, feature_String, value_String]
:Arguments:		{ hndl, feature, value }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3GetString(AT_H);

:Begin:
:Function:		AndorSDK3GetString
:Pattern:		AndorSDK3GetString[hndl_Integer, feature_String]
:Arguments:		{ hndl, feature }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3GetStringMaxLength(AT_H);

:Begin:
:Function:		AndorSDK3GetStringMaxLength
:Pattern:		AndorSDK3GetStringMaxLength[hndl_Integer, feature_String]
:Arguments:		{ hndl, feature }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3Command(AT_H);

:Begin:
:Function:		AndorSDK3Command
:Pattern:		AndorSDK3Command[hndl_Integer, command_String]
:Arguments:		{ hndl, command }
:ArgumentTypes:	{ Integer, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3QueueBuffer(AT_H,int);

:Begin:
:Function:		AndorSDK3QueueBuffer
:Pattern:		AndorSDK3QueueBuffer[hndl_Integer, bufferSize_Integer]
:Arguments:		{ hndl, bufferSize }
:ArgumentTypes:	{ Integer, Integer }
:ReturnType:	Manual
:End:

void AndorSDK3WaitBuffer(AT_H, double);

:Begin:
:Function:		AndorSDK3WaitBuffer
:Pattern:		AndorSDK3WaitBuffer[hndl_Integer, timeout_Real]
:Arguments:		{ hndl, timeout }
:ArgumentTypes:	{ Integer, Real }
:ReturnType:	Manual
:End:

void AndorSDK3Flush(AT_H);

:Begin:
:Function:		AndorSDK3Flush
:Pattern:		AndorSDK3Flush[hndl_Integer]
:Arguments:		{ hndl }
:ArgumentTypes:	{ Integer }
:ReturnType:	Manual
:End:

void AndorSDK3StartLiveView(AT_H, int numBuffers,
		const char *windowFullscreenBool, const char *startAcquisitionBool);

:Begin:
:Function:		AndorSDK3StartLiveView
:Pattern:		AndorSDK3StartLiveView[hndl_Integer, numBuffers_Integer?Positive,
					makeWindowFullscreen:True|False, startAcquisition:True|False:True,
					colorFunctionInterpolationData:{{_?NumericQ,_?NumericQ,_?NumericQ}...}|Null:Null]
:Arguments:		{ hndl, numBuffers, makeWindowFullscreen, startAcquisition,
					colorFunctionInterpolationData }
:ArgumentTypes:	{ Integer, Integer, Symbol, Symbol, Manual }
:ReturnType:	Manual
:End:

void AndorSDK3StopLiveView(const char *,const char *);

:Begin:
:Function:		AndorSDK3StopLiveView
:Pattern:		AndorSDK3StopLiveView[retrieveBuffers:True|False,
					keepWindowOpen:True|False:False]
:Arguments:		{ retrieveBuffers, keepWindowOpen }
:ArgumentTypes:	{ Symbol, Symbol }
:ReturnType:	Manual
:End:

void AndorSDK3LiveViewRunning(void);

:Begin:
:Function:		AndorSDK3LiveViewRunning
:Pattern:		AndorSDK3LiveViewRunning[]
:Arguments:		{ }
:ArgumentTypes:	{ }
:ReturnType:	Manual
:End:

void AndorSDK3GetLiveViewImages(void);

:Begin:
:Function:		AndorSDK3GetLiveViewImages
:Pattern:		AndorSDK3GetLiveViewImages[]
:Arguments:		{ }
:ArgumentTypes:	{ }
:ReturnType:	Manual
:End:

void AndorSDK3CreateLiveViewAccumulationBuffers(mlint64 *accumulationList,
		int accumulationListSize);

:Begin:
:Function:		AndorSDK3CreateLiveViewAccumulationBuffers
:Pattern:		AndorSDK3CreateLiveViewAccumulationBuffers[
					accumulationList:{___Integer}]
:Arguments:		{ accumulationList }
:ArgumentTypes:	{ Integer64List }
:ReturnType:	Manual
:End:

void AndorSDK3QueueLiveViewAccumulationBuffers(mlint64 *accBufferAddresses,
		int accBufferAddressesLength, mlint64 accumulations);

:Begin:
:Function:		AndorSDK3QueueLiveViewAccumulationBuffers
:Pattern:		AndorSDK3QueueLiveViewAccumulationBuffers[
					accBufferAddresses:{___Integer}, accumulations_Integer:0]
:Arguments:		{ accBufferAddresses, accumulations }
:ArgumentTypes:	{ Integer64List, Integer64 }
:ReturnType:	Manual
:End:

void AndorSDK3CreateLiveViewAccumulationBuffersFromFile(const char *fileName,
	mlint64 accumulations);

:Begin:
:Function:		AndorSDK3CreateLiveViewAccumulationBuffersFromFile
:Pattern:		AndorSDK3CreateLiveViewAccumulationBuffersFromFile[
					fileName_String, accumulations_Integer:1]
:Arguments:		{ fileName, accumulations }
:ArgumentTypes:	{ String, Integer64 }
:ReturnType:	Manual
:End:

void AndorSDK3ResetLiveViewAccumulationBuffers(mlint64 *accBufferAddresses,
		int accBufferAddressesLength);
		
:Begin:
:Function:		AndorSDK3ResetLiveViewAccumulationBuffers
:Pattern:		AndorSDK3ResetLiveViewAccumulationBuffers[
					accBufferAddresses:{___Integer}]
:Arguments:		{ accBufferAddresses }
:ArgumentTypes:	{ Integer64List }
:ReturnType:	Manual
:End:

void AndorSDK3GetAccumulationCounterOfLiveViewAccumulationBuffers(
		mlint64 *accBufferAddresses,int accBufferAddressesLength);
		
:Begin:
:Function:		AndorSDK3GetAccumulationCounterOfLiveViewAccumulationBuffers
:Pattern:		AndorSDK3GetAccumulationCounterOfLiveViewAccumulationBuffers[
					accBufferAddresses:{___Integer}]
:Arguments:		{ accBufferAddresses }
:ArgumentTypes:	{ Integer64List }
:ReturnType:	Manual
:End:

void AndorSDK3SetAccumulationCounterOfLiveViewAccumulationBuffers(
		mlint64 *accBufferAddresses, int accBufferAddressesLength,
		mlint64 accumulations);

:Begin:
:Function:		AndorSDK3SetAccumulationCounterOfLiveViewAccumulationBuffers
:Pattern:		AndorSDK3SetAccumulationCounterOfLiveViewAccumulationBuffers[
					accBufferAddresses:{___Integer}, accumulations_Integer]
:Arguments:		{ accBufferAddresses, accumulations }
:ArgumentTypes:	{ Integer64List, Integer64 }
:ReturnType:	Manual
:End:

void AndorSDK3DestroyLiveViewAccumulationBuffers(mlint64 *accBufferAddresses,
		int accBufferAddressesLength);

:Begin:
:Function:		AndorSDK3DestroyLiveViewAccumulationBuffers
:Pattern:		AndorSDK3DestroyLiveViewAccumulationBuffers[
					accBufferAddresses:{___Integer}]
:Arguments:		{ accBufferAddresses }
:ArgumentTypes:	{ Integer64List }
:ReturnType:	Manual
:End:

void AndorSDK3DumpLiveViewAccumulationBuffersToFile(mlint64 *accBufferAddresses,
		int accBufferAddressesLength, const char *fileName);
		
:Begin:
:Function:		AndorSDK3DumpLiveViewAccumulationBuffersToFile
:Pattern:		AndorSDK3DumpLiveViewAccumulationBuffersToFile[
					accBufferAddresses:{___Integer}, fileName_String]
:Arguments:		{ accBufferAddresses, fileName }
:ArgumentTypes:	{ Integer64List, String }
:ReturnType:	Manual
:End:

void AndorSDK3GetLiveViewAccumulationBufferData(mlint64 accBufferAddress);

:Begin:
:Function:		AndorSDK3GetLiveViewAccumulationBufferData
:Pattern:		AndorSDK3GetLiveViewAccumulationBufferData[
					accBufferAddress_Integer]
:Arguments:		{ accBufferAddress }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void AndorSDK3ExtractPOIformLiveViewAccumulationBuffers(int *poi,
		long poiLength, mlint64 *accBufferAddresses,
		int accBufferAddressesLength);

:Begin:
:Function:		AndorSDK3ExtractPOIformLiveViewAccumulationBuffers
:Pattern:		AndorSDK3ExtractPOIformLiveViewAccumulationBuffers[
					poi:{___Integer}, accBufferAddresses:{___Integer}]
:Arguments:		{ poi, accBufferAddresses }
:ArgumentTypes:	{ IntegerList, Integer64List }
:ReturnType:	Manual
:End:

void AndorSDK3LiveViewAccumulationBufferFinished(void);

:Begin:
:Function:		AndorSDK3LiveViewAccumulationBufferFinished
:Pattern:		AndorSDK3LiveViewAccumulationBufferFinished[]
:Arguments:		{ }
:ArgumentTypes:	{ }
:ReturnType:	Manual
:End:

void AndorSDK3LiveViewAccumulationBuffersFinished(void);

:Begin:
:Function:		AndorSDK3LiveViewAccumulationBuffersFinished
:Pattern:		AndorSDK3LiveViewAccumulationBuffersFinished[]
:Arguments:		{ }
:ArgumentTypes:	{ }
:ReturnType:	Manual
:End:

void AndorSDK3GetFinishedLiveViewAccumulationBuffers(const char *clearQueueBool);

:Begin:
:Function:		AndorSDK3GetFinishedLiveViewAccumulationBuffers
:Pattern:		AndorSDK3GetFinishedLiveViewAccumulationBuffers[
					clearQueue:True|False:True]
:Arguments:		{ clearQueue }
:ArgumentTypes:	{ Symbol }
:ReturnType:	Manual
:End:

void AndorSDK3GetWaitingLiveViewAccumulationBuffers(const char *clearQueueBool);

:Begin:
:Function:		AndorSDK3GetWaitingLiveViewAccumulationBuffers
:Pattern:		AndorSDK3GetWaitingLiveViewAccumulationBuffers[
					clearQueue:True|False:False]
:Arguments:		{ clearQueue }
:ArgumentTypes:	{ Symbol }
:ReturnType:	Manual
:End:

void AndorSDK3WaitForFinishedLiveViewAccumulationBuffer(double);

:Begin:
:Function:		AndorSDK3WaitForFinishedLiveViewAccumulationBuffer
:Pattern:		AndorSDK3WaitForFinishedLiveViewAccumulationBuffer[timeout_Real:-1]
:Arguments:		{ timeout }
:ArgumentTypes:	{ Real }
:ReturnType:	Manual
:End:


void AndorSDK3GetCurrentLiveViewAccumulationBufferData(void);

:Begin:
:Function:		AndorSDK3GetCurrentLiveViewAccumulationBufferData
:Pattern:		AndorSDK3GetCurrentLiveViewAccumulationBufferData[]
:Arguments:		{ }
:ArgumentTypes:	{ }
:ReturnType:	Manual
:End:

void AndorSDK3StartLiveViewAcquisition(void);

:Begin:
:Function:		AndorSDK3StartLiveViewAcquisition
:Pattern:		AndorSDK3StartLiveViewAcquisition[]
:Arguments:		{ }
:ArgumentTypes:	{ }
:ReturnType:	Manual
:End:

void AndorSDK3StopLiveViewAcquisition(void);

:Begin:
:Function:		AndorSDK3StopLiveViewAcquisition
:Pattern:		AndorSDK3StopLiveViewAcquisition[]
:Arguments:		{ }
:ArgumentTypes:	{ }
:ReturnType:	Manual
:End:

void AndorSDK3LiveViewAcquisitionRunning(void);

:Begin:
:Function:		AndorSDK3LiveViewAcquisitionRunning
:Pattern:		AndorSDK3LiveViewAcquisitionRunning[]
:Arguments:		{ }
:ArgumentTypes:	{ }
:ReturnType:	Manual
:End:

void AndorSDK3GetLiveViewAcquisitionTimeout(void);

:Begin:
:Function:		AndorSDK3GetLiveViewAcquisitionTimeout
:Pattern:		AndorSDK3GetLiveViewAcquisitionTimeout[]
:Arguments:		{ }
:ArgumentTypes:	{ }
:ReturnType:	Manual
:End:

void AndorSDK3SetLiveViewAcquisitionTimeout(double timeout);

:Begin:
:Function:		AndorSDK3SetLiveViewAcquisitionTimeout
:Pattern:		AndorSDK3SetLiveViewAcquisitionTimeout[timeout_Real]
:Arguments:		{ timeout }
:ArgumentTypes:	{ Real }
:ReturnType:	Manual
:End:

void AndorSDK3ComputeContrastsOfLiveViewAccumulationBuffers(
		mlint64 *accBufferAddresses, int accBufferAddressesLength);
		
:Begin:
:Function:		AndorSDK3ComputeContrastsOfLiveViewAccumulationBuffers
:Pattern:		AndorSDK3ComputeContrastsOfLiveViewAccumulationBuffers[
					accBufferAddresses:{___Integer}]
:Arguments:		{ accBufferAddresses }
:ArgumentTypes:	{ Integer64List }
:ReturnType:	Manual
:End:

void AndorSDK3ComputeStatisticsOfLiveViewAccumulationBuffers(
		mlint64 *accBufferAddresses, int accBufferAddressesLength);
		
:Begin:
:Function:		AndorSDK3ComputeStatisticsOfLiveViewAccumulationBuffers
:Pattern:		AndorSDK3ComputeStatisticsOfLiveViewAccumulationBuffers[
					accBufferAddresses:{___Integer}]
:Arguments:		{ accBufferAddresses }
:ArgumentTypes:	{ Integer64List }
:ReturnType:	Manual
:End:

void AndorSDK3Compute33ConvolutionSumOfLiveViewAccumulationBuffers(
		mlint64 *accBufferAddresses, int accBufferAddressesLength);
		
:Begin:
:Function:		AndorSDK3Compute33ConvolutionSumOfLiveViewAccumulationBuffers
:Pattern:		AndorSDK3Compute33ConvolutionSumOfLiveViewAccumulationBuffers[
					accBufferAddresses:{___Integer},
					a:{{_?NumericQ, _?NumericQ, _?NumericQ},
						{_?NumericQ, _?NumericQ, _?NumericQ},
						{_?NumericQ, _?NumericQ, _?NumericQ}}]
:Arguments:		{ accBufferAddresses, a }
:ArgumentTypes:	{ Integer64List, Manual }
:ReturnType:	Manual
:End:
