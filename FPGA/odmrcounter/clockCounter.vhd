library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clockCounter is
	port(clk: in std_logic; gate: in std_logic; reset: in std_logic;
		stopped: out std_logic;	start: in unsigned(31 downto 0) := X"00000000";
		count: buffer unsigned(31 downto 0));
end clockCounter;

architecture clockCounter_behaviour of clockCounter is
begin
	stopped <= '1' when count = 0 else '0';
	
	process(clk)
		variable reset_last: std_logic := '0';
	begin
		if(clk'event and clk = '1') then
			if(reset = '0') then
				if(reset_last = '1') then
					if(start > 0) then
						count <= start - 1;
					else
						count <= X"00000000";
					end if;
				elsif(gate = '1' and count > 0) then
					count <= count - 1;
				end if;
			end if;
			reset_last := reset;
		end if;
	end process;
	
end clockCounter_behaviour;
