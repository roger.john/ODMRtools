library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity counter is
	generic(direction: integer := 1; stop: unsigned(31 downto 0) := X"00000000";
		countHold: boolean := false);
	port(input: in std_logic; clk: in std_logic; gate: in std_logic; reset: in std_logic;
		stopped: out std_logic;	start: in unsigned(31 downto 0) := X"00000000";
		count: buffer unsigned(31 downto 0));
end counter;

architecture counter_behaviour of counter is
begin
	stopped <= '1' when count = stop else '0';
	
	process(clk)
		variable reset_last: std_logic := '0';
		variable input_last: std_logic := '0';
		variable count_new: unsigned(31 downto 0);
	begin
		if(clk'event and clk = '1') then
			count_new := count;
			if(reset = '0') then
				if(reset_last = '1') then
					count_new := start;
				end if;
				if((countHold or input_last = '0') and input = '1' and gate = '1'
						and count_new /= stop) then
					if(direction > 0) then
						count_new := count_new + direction;
					elsif(direction < 0) then
						count_new := count_new - (-direction);
					end if;
				end if;
			end if;
			count <= count_new;
			input_last := input;
			reset_last := reset;
		end if;
	end process;
	
end counter_behaviour;
