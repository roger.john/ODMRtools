#include <termios.h>
#include <asm/ioctls.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <linux/serial.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include "MathLinkCommon.h"

#define THORLABS_APT_MESSAGE_HEADER_DATA_PACKET 1<<7
#define THORLABS_APT_MESSAGE_HEADER_SOURCE 0x01
#define THORLABS_APT_MESSAGE_HEADER_DESTINATION 0x50

struct __attribute__((__packed__)) ThorlabsAPTmessageHeader {
	uint16_t id;
	union {
		uint8_t parameters[2];
		uint16_t dataLength;
	};
	uint8_t dst;
	uint8_t src;
};

struct __attribute__((__packed__)) ThorlabsAPThardwareInformation {
	uint32_t serialNumber;
	char modelNumber[8];
	uint16_t type;
	uint8_t firmwareVersion[4];
	char notes[48];
	char emptySpace[12];
	uint16_t hwVersion;
	uint16_t modState;
	uint16_t nChs;
};

struct __attribute__((__packed__)) ThorlabsAPTchannelValue {
	uint16_t channel;
	uint32_t value;
};

struct __attribute__((__packed__)) ThorlabsAPTchannelStatus {
	uint16_t channel;
	uint32_t value;
	uint32_t value2;
	uint32_t statusBits;
};

int openThorlabsAPTcontroller(const char *dev) {
	int fd = open(dev, O_RDWR | O_NOCTTY | O_NDELAY);
	if (fd != -1) {
		fcntl(fd, F_SETFL, 0);
		struct termios options;
		tcgetattr(fd, &options);
		cfsetspeed(&options, B115200);
		cfmakeraw(&options);
		options.c_cflag |= (CLOCAL | CREAD);
		options.c_cflag &= ~CSTOPB;
		//tcsetattr(fd, TCSANOW, &options);
		tcsetattr(fd, TCSAFLUSH, &options);
	} else
		failWithMsg(stdlink, "openThorlabsController:openFailed");
	return fd;
}

int closeThorlabsAPTcontroller(int fd) {
	return close(fd);
}

void readThorlabsResponse(int fd) {
	char buffer[sizeof(struct ThorlabsAPTmessageHeader) + 1024];
	struct ThorlabsAPTmessageHeader *header =
			(struct ThorlabsAPTmessageHeader *) buffer;
	int r = 0;
	while (r < sizeof(struct ThorlabsAPTmessageHeader)) {
		int s = read(fd, buffer + r, sizeof(buffer) - r);
		if (MLAbort) {
			failWithMsg(stdlink, "waited for input");
			return;
		}
		if (s < 0) {
			failWithMsg(stdlink, "readThorlabsResponse:readFailed");
			return;
		}
		r += s;
	}
	int hasData = 0;
	if (header->dst & THORLABS_APT_MESSAGE_HEADER_DATA_PACKET) {
		hasData = 1;
		if (header->dataLength
				<= sizeof(buffer) - sizeof(struct ThorlabsAPTmessageHeader))
			while (r
					< sizeof(struct ThorlabsAPTmessageHeader)
							+ header->dataLength) {
				int s = read(fd, buffer + r, sizeof(buffer) - r);
				if (MLAbort) {
					failWithMsg(stdlink, "waited for further input");
					return;
				}
				if (s < 0) {
					failWithMsg(stdlink, "readThorlabsResponse:readDataFailed");
					return;
				}
				r += s;
			}
		else {
			if (!MLPutFunction(stdlink, "ThorlabsMessageHeader", 2)) {
				failWithMsgAndError(stdlink,
						"readThorlabsResponse:dataHeader:header:error: ");
				return;
			}
			if (!MLPutFunction(stdlink, "BaseForm", 2)
					|| !MLPutInteger(stdlink, header->id)
					|| !MLPutInteger(stdlink, 16)) {
				failWithMsgAndError(stdlink,
						"readThorlabsResponse:dataHeader:id:error: ");
				return;
			}
			if (!MLPutInteger(stdlink, header->dataLength)) {
				failWithMsgAndError(stdlink,
						"readThorlabsResponse:channel:error: ");
				return;
			}
			return;
		}
	}
	switch (header->id) {
	case 0x0006:
		if (!hasData
				|| r
						< sizeof(struct ThorlabsAPTmessageHeader)
								+ sizeof(struct ThorlabsAPThardwareInformation)) {
			if (!MLPutFunction(stdlink, "$Failed", 2)
					|| !MLPutString(stdlink,
							"readThorlabsResponse:hwInfo:wrongLength")
					|| !MLPutInteger(stdlink,
							r - sizeof(struct ThorlabsAPTmessageHeader)))
				failWithMsgAndError(stdlink,
						"readThorlabsResponse:hwInfo:wrongLength:error: ");
			return;
		}
		struct ThorlabsAPThardwareInformation *hwInfo =
				(struct ThorlabsAPThardwareInformation *) (buffer
						+ sizeof(struct ThorlabsAPTmessageHeader));
		if (r
				> sizeof(struct ThorlabsAPTmessageHeader)
						+ sizeof(struct ThorlabsAPThardwareInformation))
			if (!MLPutFunction(stdlink, "List", 2)) {
				failWithMsgAndError(stdlink,
						"readThorlabsResponse:overSizeHeader:error: ");
				return;
			}
		if (!MLPutFunction(stdlink, "ThorlabsHardwareInformation", 8)
				|| !MLPutInteger(stdlink, hwInfo->serialNumber)
				|| !MLPutByteString(stdlink, hwInfo->modelNumber,
						strnlen(hwInfo->modelNumber,
								sizeof(hwInfo->modelNumber)))
				|| !MLPutInteger(stdlink, hwInfo->type)
				|| !MLPutFunction(stdlink, "List", 4)
				|| !MLPutInteger(stdlink, hwInfo->firmwareVersion[2])
				|| !MLPutInteger(stdlink, hwInfo->firmwareVersion[1])
				|| !MLPutInteger(stdlink, hwInfo->firmwareVersion[0])
				|| !MLPutInteger(stdlink, hwInfo->firmwareVersion[3])
				|| !MLPutByteString(stdlink, hwInfo->notes,
						strnlen(hwInfo->notes, sizeof(hwInfo->notes)))
				|| !MLPutInteger(stdlink, hwInfo->hwVersion)
				|| !MLPutInteger(stdlink, hwInfo->modState)
				|| !MLPutInteger(stdlink, hwInfo->nChs)) {
			failWithMsgAndError(stdlink, "readThorlabsResponse:return:error: ");
			return;
		}
		if (r
				> sizeof(struct ThorlabsAPTmessageHeader)
						+ sizeof(struct ThorlabsAPThardwareInformation))
			if (!MLPutFunction(stdlink, "Data", 1)
					|| !MLPutByteString(stdlink,
							buffer + sizeof(struct ThorlabsAPTmessageHeader)
									+ sizeof(struct ThorlabsAPThardwareInformation),
							r - sizeof(struct ThorlabsAPTmessageHeader)
									- sizeof(struct ThorlabsAPThardwareInformation))) {
				failWithMsgAndError(stdlink,
						"readThorlabsResponse:overSizeTrailer:error: ");
				return;
			}
		break;
	case 0x0412:
		if (!hasData
				|| r
						< sizeof(struct ThorlabsAPTmessageHeader)
								+ sizeof(struct ThorlabsAPTchannelValue)) {
			if (!MLPutFunction(stdlink, "$Failed", 2)
					|| !MLPutString(stdlink,
							"readThorlabsResponse:value:wrongLength")
					|| !MLPutInteger(stdlink,
							r - sizeof(struct ThorlabsAPTmessageHeader)))
				failWithMsgAndError(stdlink,
						"readThorlabsResponse:value:wrongLength:error: ");
			return;
		}
		struct ThorlabsAPTchannelValue *channelValue =
				(struct ThorlabsAPTchannelValue *) (buffer
						+ sizeof(struct ThorlabsAPTmessageHeader));
		if (r
				> sizeof(struct ThorlabsAPTmessageHeader)
						+ sizeof(struct ThorlabsAPTchannelValue))
			if (!MLPutFunction(stdlink, "List", 2)) {
				failWithMsgAndError(stdlink,
						"readThorlabsResponse:overSizeHeader:error: ");
				return;
			}
		if (!MLPutFunction(stdlink, "ThorlabsChannelValue", 2)
				|| !MLPutInteger(stdlink, channelValue->channel)
				|| !MLPutInteger(stdlink, channelValue->value)) {
			failWithMsgAndError(stdlink, "readThorlabsResponse:return:error: ");
			return;
		}
		if (r
				> sizeof(struct ThorlabsAPTmessageHeader)
						+ sizeof(struct ThorlabsAPTchannelValue))
			if (!MLPutFunction(stdlink, "Data", 1)
					|| !MLPutByteString(stdlink,
							buffer + sizeof(struct ThorlabsAPTmessageHeader)
									+ sizeof(struct ThorlabsAPTchannelValue),
							r - sizeof(struct ThorlabsAPTmessageHeader)
									- sizeof(struct ThorlabsAPTchannelValue))) {
				failWithMsgAndError(stdlink,
						"readThorlabsResponse:overSizeTrailer:error: ");
				return;
			}
		break;
	case 0x0464:
		if (!hasData
				|| r
						< sizeof(struct ThorlabsAPTmessageHeader)
								+ sizeof(struct ThorlabsAPTchannelStatus)) {
			if (!MLPutFunction(stdlink, "$Failed", 2)
					|| !MLPutString(stdlink,
							"readThorlabsResponse:status:wrongLength")
					|| !MLPutInteger(stdlink,
							r - sizeof(struct ThorlabsAPTmessageHeader)))
				failWithMsgAndError(stdlink,
						"readThorlabsResponse:status:wrongLength:error: ");
			return;
		}
		struct ThorlabsAPTchannelStatus *channelStatus =
				(struct ThorlabsAPTchannelStatus *) (buffer
						+ sizeof(struct ThorlabsAPTmessageHeader));
		if (r
				> sizeof(struct ThorlabsAPTmessageHeader)
						+ sizeof(struct ThorlabsAPTchannelStatus))
			if (!MLPutFunction(stdlink, "List", 2)) {
				failWithMsgAndError(stdlink,
						"readThorlabsResponse:overSizeHeader:error: ");
				return;
			}
		if (!MLPutFunction(stdlink, "ThorlabsChannelStatus", 4)
				|| !MLPutInteger(stdlink, channelStatus->channel)
				|| !MLPutInteger(stdlink, channelStatus->value)
				|| !MLPutInteger(stdlink, channelStatus->value2)
				|| !MLPutInteger(stdlink, channelStatus->statusBits)) {
			failWithMsgAndError(stdlink, "readThorlabsResponse:return:error: ");
			return;
		}
		if (r
				> sizeof(struct ThorlabsAPTmessageHeader)
						+ sizeof(struct ThorlabsAPTchannelStatus))
			if (!MLPutFunction(stdlink, "Data", 1)
					|| !MLPutByteString(stdlink,
							buffer + sizeof(struct ThorlabsAPTmessageHeader)
									+ sizeof(struct ThorlabsAPTchannelStatus),
							r - sizeof(struct ThorlabsAPTmessageHeader)
									- sizeof(struct ThorlabsAPTchannelStatus))) {
				failWithMsgAndError(stdlink,
						"readThorlabsResponse:overSizeTrailer:error: ");
				return;
			}
		break;
	case 0x0444:
		if (!hasData && r == sizeof(struct ThorlabsAPTmessageHeader)) {
			if (!MLPutFunction(stdlink, "ThorlabsChannelHomed", 1)
					|| !MLPutInteger(stdlink, header->parameters[0])) {
				failWithMsgAndError(stdlink,
						"readThorlabsResponse:returnHomed:error: ");
				return;
			}
			break;
		}
	default:
		if (hasData) {
			if (r
					> sizeof(struct ThorlabsAPTmessageHeader)
							+ header->dataLength)
				if (!MLPutFunction(stdlink, "List", 2)) {
					failWithMsgAndError(stdlink,
							"readThorlabsResponse:unknownPacket:overSizeHeader:error: ");
					return;
				}
			if (!MLPutFunction(stdlink, "ThorlabsMessagePacket", 2)
					|| !MLPutFunction(stdlink, "BaseForm", 2)
					|| !MLPutInteger(stdlink, header->id)
					|| !MLPutInteger(stdlink, 16)
					|| !MLPutByteString(stdlink,
							buffer + sizeof(struct ThorlabsAPTmessageHeader),
							header->dataLength)) {
				failWithMsgAndError(stdlink,
						"readThorlabsResponse:unknownPacket:error: ");
				return;
			}
			if (r
					> sizeof(struct ThorlabsAPTmessageHeader)
							+ header->dataLength)
				if (!MLPutFunction(stdlink, "Data", 1)
						|| !MLPutByteString(stdlink,
								buffer + sizeof(struct ThorlabsAPTmessageHeader)
										+ header->dataLength,
								r - sizeof(struct ThorlabsAPTmessageHeader)
										- header->dataLength)) {
					failWithMsgAndError(stdlink,
							"readThorlabsResponse:unknownPacket:overSizeTrailer:error: ");
					return;
				}
		} else {
			if (r > sizeof(struct ThorlabsAPTmessageHeader))
				if (!MLPutFunction(stdlink, "List", 2)) {
					failWithMsgAndError(stdlink,
							"readThorlabsResponse:unknownPacket:overSizeHeader:error: ");
					return;
				}
			if (!MLPutFunction(stdlink, "ThorlabsMessagePacket", 2)
					|| !MLPutFunction(stdlink, "BaseForm", 2)
					|| !MLPutInteger(stdlink, header->id)
					|| !MLPutInteger(stdlink, 16)
					|| !MLPutFunction(stdlink, "List", 2)
					|| !MLPutInteger(stdlink, header->parameters[0])
					|| !MLPutInteger(stdlink, header->parameters[1])) {
				failWithMsgAndError(stdlink,
						"readThorlabsResponse:unknownPacket:error: ");
				return;
			}
			if (r > sizeof(struct ThorlabsAPTmessageHeader))
				if (!MLPutFunction(stdlink, "Data", 1)
						|| !MLPutByteString(stdlink,
								buffer
										+ sizeof(struct ThorlabsAPTmessageHeader),
								r - sizeof(struct ThorlabsAPTmessageHeader))) {
					failWithMsgAndError(stdlink,
							"readThorlabsResponse:unknownPacket:overSizeTrailer:error: ");
					return;
				}
		}
	}
}

void getThorlabsHardwareInformation(int fd) {
	char buffer[sizeof(struct ThorlabsAPTmessageHeader)
			+ sizeof(struct ThorlabsAPThardwareInformation)];
	struct ThorlabsAPTmessageHeader *header =
			(struct ThorlabsAPTmessageHeader *) buffer;
	header->id = 0x0005;
	header->dataLength = 0;
	header->dst = THORLABS_APT_MESSAGE_HEADER_DESTINATION;
	header->src = THORLABS_APT_MESSAGE_HEADER_SOURCE;
	int r = 0;
	while (r < sizeof(struct ThorlabsAPTmessageHeader)) {
		int s = write(fd, buffer + r,
				sizeof(struct ThorlabsAPTmessageHeader) - r);
		if (s < 0) {
			failWithMsg(stdlink,
					"readThorlabsHardwareInformation:requestFailed");
			return;
		}
		r += s;
	}
	if (r != sizeof(struct ThorlabsAPTmessageHeader)) {
		failWithMsg(stdlink, "readThorlabsHardwareInformation:requestFailed");
		return;
	}
	readThorlabsResponse(fd);
}

void getThorlabsChannelEncoder(int fd, int channel) {
	char buffer[sizeof(struct ThorlabsAPTmessageHeader)
			+ sizeof(struct ThorlabsAPTchannelValue)];
	struct ThorlabsAPTmessageHeader *header =
			(struct ThorlabsAPTmessageHeader *) buffer;
	header->id = 0x0411;
	header->parameters[0] = channel;
	header->parameters[1] = 0;
	header->dst = THORLABS_APT_MESSAGE_HEADER_DESTINATION;
	header->src = THORLABS_APT_MESSAGE_HEADER_SOURCE;
	int r = 0;
	while (r < sizeof(struct ThorlabsAPTmessageHeader)) {
		int s = write(fd, buffer + r,
				sizeof(struct ThorlabsAPTmessageHeader) - r);
		if (s < 0) {
			failWithMsg(stdlink, "readThorlabsChannelEncoder:requestFailed");
			return;
		}
		r += s;
	}
	if (r != sizeof(struct ThorlabsAPTmessageHeader)) {
		failWithMsg(stdlink, "readThorlabsChannelEncoder:requestFailed");
		return;
	}
	readThorlabsResponse(fd);
}

void getThorlabsChannelStatus(int fd, int channel) {
	char buffer[sizeof(struct ThorlabsAPTmessageHeader) + 1024];
	struct ThorlabsAPTmessageHeader *header =
			(struct ThorlabsAPTmessageHeader *) buffer;
	int r = 0;
	header->id = 0x0490;
	header->parameters[0] = channel;
	header->parameters[1] = 0;
	header->dst = THORLABS_APT_MESSAGE_HEADER_DESTINATION;
	header->src = THORLABS_APT_MESSAGE_HEADER_SOURCE;
	while (r < sizeof(struct ThorlabsAPTmessageHeader)) {
		int s = write(fd, buffer + r,
				sizeof(struct ThorlabsAPTmessageHeader) - r);
		if (s < 0) {
			failWithMsg(stdlink, "getThorlabsChannelStatus:requestFailed");
			return;
		}
		r += s;
	}
	if (r != sizeof(struct ThorlabsAPTmessageHeader)) {
		failWithMsg(stdlink, "getThorlabsChannelStatus:requestFailed");
		return;
	}
	readThorlabsResponse(fd);
}

void moveThorlabsChannel(int fd, int channel, int newPos, int waitForCompletion) {
	char buffer[sizeof(struct ThorlabsAPTmessageHeader)
			+ sizeof(struct ThorlabsAPTchannelStatus)];
	struct ThorlabsAPTmessageHeader *header =
			(struct ThorlabsAPTmessageHeader *) buffer;
	header->id = 0x0453;
	header->parameters[0] = 6;
	header->parameters[1] = 0;
	header->dst = THORLABS_APT_MESSAGE_HEADER_DESTINATION
			| THORLABS_APT_MESSAGE_HEADER_DATA_PACKET;
	header->src = THORLABS_APT_MESSAGE_HEADER_SOURCE;
	struct ThorlabsAPTchannelValue *channelValue =
			(struct ThorlabsAPTchannelValue *) (buffer
					+ sizeof(struct ThorlabsAPTmessageHeader));
	channelValue->channel = channel;
	channelValue->value = newPos;
	int r = 0;
	while (r < sizeof(struct ThorlabsAPTmessageHeader)) {
		int s = write(fd, buffer + r,
				sizeof(struct ThorlabsAPTmessageHeader)
						+ sizeof(struct ThorlabsAPTchannelValue) - r);
		if (s < 0) {
			failWithMsg(stdlink, "moveThorlabsChannel:requestFailed");
			return;
		}
		r += s;
	}
	if (r
			!= sizeof(struct ThorlabsAPTmessageHeader)
					+ sizeof(struct ThorlabsAPTchannelValue)) {
		failWithMsg(stdlink, "moveThorlabsChannel:requestFailed");
		return;
	}
	if (waitForCompletion)
		readThorlabsResponse(fd);
	else if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink, "moveThorlabsChannel:asyncReturn:error: ");
}

void moveThorlabsChannelSync(int fd, int channel, int newPos) {
	moveThorlabsChannel(fd, channel, newPos, 1);
}

void moveThorlabsChannelAsync(int fd, int channel, int newPos) {
	moveThorlabsChannel(fd, channel, newPos, 0);
}

void homeThorlabsChannel(int fd, int channel, int waitForCompletion) {
	char buffer[sizeof(struct ThorlabsAPTmessageHeader)
			+ sizeof(struct ThorlabsAPTchannelValue)];
	struct ThorlabsAPTmessageHeader *header =
			(struct ThorlabsAPTmessageHeader *) buffer;
	header->id = 0x0443;
	header->parameters[0] = channel;
	header->parameters[1] = 0;
	header->dst = THORLABS_APT_MESSAGE_HEADER_DESTINATION;
	header->src = THORLABS_APT_MESSAGE_HEADER_SOURCE;
	int r = 0;
	while (r < sizeof(struct ThorlabsAPTmessageHeader)) {
		int s = write(fd, buffer + r,
				sizeof(struct ThorlabsAPTmessageHeader) - r);
		if (s < 0) {
			failWithMsg(stdlink, "homeThorlabsChannel:requestFailed");
			return;
		}
		r += s;
	}
	if (r != sizeof(struct ThorlabsAPTmessageHeader)) {
		failWithMsg(stdlink, "homeThorlabsChannel:requestFailed");
		return;
	}
	if (waitForCompletion)
		readThorlabsResponse(fd);
	else if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink, "homeThorlabsChannel:asyncReturn:error: ");
}

void homeThorlabsChannelSync(int fd, int channel) {
	homeThorlabsChannel(fd, channel, 1);
}

void homeThorlabsChannelAsync(int fd, int channel) {
	homeThorlabsChannel(fd, channel, 0);
}
