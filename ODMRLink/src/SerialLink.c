#include <termios.h>
#include <asm/ioctls.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <linux/serial.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include "MathLinkCommon.h"

#define s(x) {x,B##x}
struct {
	int speed;
	int id;
} serialSpeedMap[] = { s(0), s(50), s(75), s(110), s(134), s(200), s(300),
s(600),
s(1200), s(1800), s(2400), s(4800), s(9600), s(19200), s(38400), s(57600),
s(115200), s(230400), s(460800), s(500000), s(576000), s(921600),
s(1000000),
s(1152000), s(1500000), s(2000000), s(2500000), s(3000000), s(3500000),
s(4000000) };
#undef s
int mapSerialSpeed(int speed) {
	if (speed == 4000000)
		return B4000000;
	if (speed == 3500000)
		return B3500000;
	if (speed == 3000000)
		return B3000000;
	if (speed == 2500000)
		return B2500000;
	if (speed == 2000000)
		return B2000000;
	if (speed == 1500000)
		return B1500000;
	if (speed == 1152000)
		return B1152000;
	if (speed == 1000000)
		return B1000000;
	if (speed == 921600)
		return B921600;
	if (speed == 576000)
		return B576000;
	if (speed == 500000)
		return B500000;
	if (speed == 460800)
		return B460800;
	if (speed == 230400)
		return B230400;
	if (speed == 115200)
		return B115200;
	if (speed == 57600)
		return B57600;
	if (speed == 38400)
		return B38400;
	if (speed == 19200)
		return B19200;
	if (speed == 9600)
		return B9600;
	if (speed == 4800)
		return B4800;
	if (speed == 2400)
		return B2400;
	if (speed == 1800)
		return B1800;
	if (speed == 1200)
		return B1200;
	if (speed == 600)
		return B600;
	if (speed == 300)
		return B300;
	if (speed == 200)
		return B200;
	if (speed == 134)
		return B134;
	if (speed == 110)
		return B110;
	if (speed == 75)
		return B75;
	if (speed == 50)
		return B50;
	if (speed == 0)
		return B0;
	return -1;
}
int setCustomSerialSpeed(int fd, int speed) {
	struct serial_struct ser;
	ioctl(fd, TIOCGSERIAL, &ser);
	ser.custom_divisor = ser.baud_base / speed;
	if (!(ser.custom_divisor))
		ser.custom_divisor = 1;
	speed = ser.baud_base / ser.custom_divisor;
	ser.flags &= ~ASYNC_SPD_MASK;
	ser.flags |= ASYNC_SPD_CUST;
	ioctl(fd, TIOCSSERIAL, &ser);
	return speed;
}
void serialOpen(const char *dev, int speed) {
	int fd = open(dev, O_RDWR | O_NOCTTY | O_NDELAY);
	if (fd != -1) {
		fcntl(fd, F_SETFL, 0);
		struct termios options;
		tcgetattr(fd, &options);
		int mappedSpeed = mapSerialSpeed(speed);
		if (mappedSpeed != -1)
			cfsetspeed(&options, mappedSpeed);
		else {
			setCustomSerialSpeed(fd, speed);
			options.c_cflag |= B38400;
		}
		cfmakeraw(&options);
		options.c_cflag |= (CLOCAL | CREAD);
		options.c_cflag &= ~CSTOPB;
		options.c_lflag &= ~ICANON;
		options.c_cc[VTIME] = 0;
		options.c_cc[VMIN] = 1;
		tcsetattr(fd, TCSANOW, &options);
		if (!MLPutInteger(stdlink, fd))
			failWithMsgAndError(stdlink, "serialOpen:return:error: ");
	} else
		failWithMsg(stdlink, "serialOpen:openFailed");
}
void serialSetTimeout(int fd, float timeout) {
	unsigned char timeout_ds =
			timeout < .1f ? 0 :
			timeout > 25.5f ? 255 : (unsigned char) (timeout * 10);
	struct termios options;
	if (!tcgetattr(fd, &options)) {
		options.c_lflag &= ~ICANON;
		options.c_cc[VTIME] = timeout_ds;
		options.c_cc[VMIN] = timeout_ds > 0 ? 0 : 1;
		if (tcsetattr(fd, TCSANOW, &options) < 0)
			failWithMsg(stdlink, "serialSetTimeout:setFailed");
		else if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "serialSetTimeout:return:error: ");
	} else
		failWithMsg(stdlink, "serialSetTimeout:invalidFD");
}
void serialGetTimeout(int fd) {
	struct termios options;
	if (tcgetattr(fd, &options) < 0)
		failWithMsg(stdlink, "serialGetTimeout:invalidFD");
	else if (!MLPutFloat(stdlink, options.c_cc[VTIME] / 10.f))
		failWithMsgAndError(stdlink, "serialGetTimeout:return:error: ");
}
int serialClose(int fd) {
	return close(fd);
}
int serialDataAvail(int fd) {
	int r = 0;
	if (fd != -1)
		if (ioctl(fd, FIONREAD, &r) < 0)
			failWithMsg(stdlink, "serialDataAvail:queryFailed");
	return r;
}
int serialFlushReadBuffer(int fd) {
	return tcflush(fd, TCIFLUSH);
}
int serialFlushWriteBuffer(int fd) {
	return tcflush(fd, TCOFLUSH);
}
int serialFlush(int fd) {
	return tcflush(fd, TCIOFLUSH);
}
void serialReadMin(int fd, int minToRead, int maxToRead) {
	if (maxToRead < 1) {
		MLPutSymbol(stdlink, "Null");
		return;
	}
	unsigned char *buf = malloc(maxToRead);
	int r = 0, s;
	if (fd != -1) {
		do {
			if (MLAbort) {
				if (!MLPutFunction(stdlink, "$Aborted", 1)
						|| !MLPutByteString(stdlink, buf, r))
					failWithMsgAndError(stdlink, "serialReadMin:abort:error: ");
				free(buf);
				return;
			}
			s = read(fd, buf + r, maxToRead - r);
			if (s < 0) {
				if (!MLPutFunction(stdlink, "$Failed", 2)
						|| !MLPutString(stdlink, "readFailed")
						|| !MLPutByteString(stdlink, buf, r))
					failWithMsgAndError(stdlink,
							"serialReadMin:readFailed:error: ");
				free(buf);
				return;
			} else if (s == 0) {
				if (!MLPutFunction(stdlink, "$TimedOut", 1)
						|| !MLPutByteString(stdlink, buf, r))
					failWithMsgAndError(stdlink,
							"serialReadMin:readFailed:error: ");
				free(buf);
				return;
			}
			r += s;
		} while (r < minToRead);
	}
	if (!MLPutByteString(stdlink, buf, r)) {
		free(buf);
		failWithMsgAndError(stdlink, "serialReadMin:result:error: ");
	}
	free(buf);
}
void serialRead(int fd, int maxToRead) {
	serialReadMin(fd, 0, maxToRead);
}
void serialReadUntilC(int fd, const unsigned char *delimiters,
		const int delimitersLength) {
	int bufSize = 4096;
	unsigned char *buf = malloc(bufSize);
	int r = 0, s, i;
	while (1) {
		if (r > 0) {
			for (i = 0; i < delimitersLength; i++)
				if (buf[r - 1] == delimiters[i])
					break;
			if (i < delimitersLength)
				break;
		}
		if (MLAbort) {
			if (!MLPutFunction(stdlink, "$Aborted", 1)
					|| !MLPutByteString(stdlink, buf, r))
				failWithMsgAndError(stdlink, "serialReadUntilC:abort:error: ");
			free(buf);
			return;
		}
		if (r == bufSize) {
			unsigned char *t = malloc(2 * bufSize);
			memcpy(t, buf, bufSize);
			free(buf);
			buf = t;
			bufSize *= 2;
		}
		s = read(fd, buf + r, 1);
		if (s < 0) {
			if (!MLPutFunction(stdlink, "$Failed", 2)
					|| !MLPutString(stdlink, "readFailed")
					|| !MLPutByteString(stdlink, buf, r))
				failWithMsgAndError(stdlink,
						"serialReadUntilC:readFailed:error: ");
			free(buf);
			return;
		} else if (s == 0) {
			if (!MLPutFunction(stdlink, "$TimedOut", 1)
					|| !MLPutByteString(stdlink, buf, r))
				failWithMsgAndError(stdlink,
						"serialReadMin:readFailed:error: ");
			free(buf);
			return;
		}
		r += s;
	}
	if (!MLPutByteString(stdlink, buf, r)) {
		free(buf);
		failWithMsgAndError(stdlink, "serialReadUntilC:result:error: ");
	}
	free(buf);
}
void serialReadUntil(int fd) {
	const unsigned char *delimiters;
	int delimitersLength;
	if (!MLGetByteString(stdlink, &delimiters, &delimitersLength, 0)) {
		failWithMsgAndError(stdlink, "serialReadUntilC:delimiters:error: ");
		return;
	}
	serialReadUntilC(fd, delimiters, delimitersLength);
	MLReleaseByteString(stdlink, delimiters, delimitersLength);
}
void serialReadLine(int fd) {
	serialReadUntilC(fd, (const unsigned char*) "\n", 1);
}
int serialWriteBinary(int fd, const unsigned char *data, int l) {
	int w = 0, s;
	while (w < l) {
		s = write(fd, data + w, l - w);
		if (s < 0)
			return s;
		w += s;
	}
	return w;
}
int serialWrite(int fd) {
	const unsigned char *s;
	int l;
	if (!MLGetByteString(stdlink, &s, &l, 0)) {
		failWithMsgAndError(stdlink, "serialWrite:data:error: ");
		return -1;
	}
	int r = serialWriteBinary(fd, s, l);
	MLReleaseByteString(stdlink, s, l);
	return r;
}

int serialWriteLine(int fd) {
	int r = serialWrite(fd);
	if (r < 0)
		return r;
	int s;
	do {
		s = write(fd, "\r", 1);
	} while (s == 0);
	if (s < 0)
		return s;
	return r + s;
}
int serialWriteLineN(int fd) {
	int r = serialWrite(fd);
	if (r < 0)
		return r;
	int s;
	do {
		s = write(fd, "\n", 1);
	} while (s == 0);
	if (s < 0)
		return s;
	return r + s;
}
void serialWriteAndRead(int fd, int maxToRead) {
	serialWrite(fd);
	serialRead(fd, maxToRead);
}
void serialWriteAndReadMin(int fd, int minToRead, int maxToRead) {
	serialWrite(fd);
	serialReadMin(fd, minToRead, maxToRead);
}
