#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <libusb-1.0/libusb.h>
#include <regex.h>
#include "MathLinkCommon.h"

void libusbSetDebug(mlint64 ctxRef, int level) {
	libusb_set_debug((libusb_context *) ctxRef, level);
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink, "libusbSetDebug:null:error: ");
}

void libusbErrorName(int errorCode) {
	if (!MLPutString(stdlink, libusb_error_name(errorCode)))
		failWithMsgAndError(stdlink, "libusbErrorName:result:error: ");
}

void libusbFailWithError(int errorCode) {
	if (!MLPutFunction(stdlink, "$Failed", 2)
			|| !MLPutInteger(stdlink, errorCode)
			|| !MLPutString(stdlink, libusb_error_name(errorCode)))
		failWithMsgAndError(stdlink, "libusbFailWithError:result:error: ");
}

void libusbInit(void) {
	libusb_context *ctx;
	int r = libusb_init(&ctx);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else {
		if (!MLPutInteger64(stdlink, (mlint64) ctx)) {
			libusb_exit(ctx);
			failWithMsgAndError(stdlink, "libusbInit:result:error: ");
		}
	}
}

void libusbExit(mlint64 ctxRef) {
	libusb_exit((libusb_context *) ctxRef);
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink, "libusbExit:null:error: ");
}

void libusbGetDeviceList(mlint64 ctxRef) {
	libusb_device **list;
	size_t r = libusb_get_device_list((libusb_context *) ctxRef, &list);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else {
		if (!MLPutFunction(stdlink, "List", 2)) {
			libusb_free_device_list(list, 1);
			failWithMsgAndError(stdlink,
					"libusbGetDeviceList:result:list:error: ");
		}
		if (!MLPutInteger64(stdlink, (mlint64) list)) {
			libusb_free_device_list(list, 1);
			failWithMsgAndError(stdlink,
					"libusbGetDeviceList:list:ref:error: ");
		}
		if (!MLPutFunction(stdlink, "List", r)) {
			libusb_free_device_list(list, 1);
			failWithMsgAndError(stdlink,
					"libusbGetDeviceList:result:list:list:error: ");
		}
		for (int i = 0; i < r; i++)
			if (!MLPutInteger64(stdlink, (mlint64) list[i])) {
				libusb_free_device_list(list, 1);
				failWithMsgAndError(stdlink,
						"libusbGetDeviceList:result:list:list:entry:error: ");
				break;
			}
	}
}

void libusbFreeDeviceList(mlint64 listRef, const char *unref) {
	libusb_free_device_list((libusb_device **) listRef,
			strcmp(unref, "False") == 0 ? 0 : 1);
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink, "libusbFreeDeviceList:null:error: ");
}

int libusbGetBusNumber(mlint64 devRef) {
	return libusb_get_bus_number((libusb_device*) devRef);
}

int libusbGetDeviceAddress(mlint64 devRef) {
	return libusb_get_device_address((libusb_device*) devRef);
}

void libusbGetDeviceSpeed(mlint64 devRef) {
	int speedId = libusb_get_device_address((libusb_device*) devRef);
	const char *speedString;
	switch (speedId) {
	case LIBUSB_SPEED_SUPER:
		speedString = "SUPERSPEED";
		break;
	case LIBUSB_SPEED_HIGH:
		speedString = "HIGHSPEED";
		break;
	case LIBUSB_SPEED_FULL:
		speedString = "FULLSPEED";
		break;
	case LIBUSB_SPEED_LOW:
		speedString = "LOWSPEED";
		break;
	case LIBUSB_SPEED_UNKNOWN:
		speedString = "UNKNOWN";
		break;
	default:
		if (!MLPutComposite(stdlink, 1)) {
			failWithMsgAndError(stdlink,
					"libusbGetDeviceSpeed:unknownSpeed:return:error: ");
			return;
		}
		if (!MLPutString(stdlink, "UnknowSpeedIndex")) {
			failWithMsgAndError(stdlink,
					"libusbGetDeviceSpeed:unknownSpeed:head:error: ");
			return;
		}
		if (!MLPutInteger(stdlink, speedId))
			failWithMsgAndError(stdlink,
					"libusbGetDeviceSpeed:unknownSpeed:speedId:error: ");
		return;
	}
	if (!MLPutString(stdlink, speedString)) {
		failWithMsgAndError(stdlink, "libusbGetDeviceSpeed:result:error: ");
		return;
	}
}

void libusbGetMaxPacketSize(mlint64 devRef, int endpoint) {
	int r = libusb_get_max_packet_size((libusb_device*) devRef, endpoint);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else {
		if (!MLPutInteger(stdlink, r))
			failWithMsgAndError(stdlink,
					"libusbGetMaxPacketSize:result:error: ");
	}
}

void libusbGetMaxIsoPacketSize(mlint64 devRef, int endpoint) {
	int r = libusb_get_max_iso_packet_size((libusb_device*) devRef, endpoint);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else {
		if (!MLPutInteger(stdlink, r))
			failWithMsgAndError(stdlink,
					"libusbGetMaxIsoPacketSize:result:error: ");
	}
}

void libusbRefDevice(mlint64 devRef) {
	libusb_ref_device((libusb_device*) devRef);
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink, "libusbRefDevice:return:error: ");
}

void libusbUnrefDevice(mlint64 devRef) {
	libusb_unref_device((libusb_device*) devRef);
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink, "libusbFreeDeviceList:null:error: ");
}

void libusbOpenDevice(mlint64 devRef) {
	libusb_device_handle *devHandle;
	int r = libusb_open((libusb_device*) devRef, &devHandle);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else {
		if (!MLPutInteger64(stdlink, (mlint64) devHandle)) {
			libusb_close(devHandle);
			failWithMsgAndError(stdlink, "libusbOpenDevice:result:error: ");
		}
	}
}

void libusbOpenDeviceWithVidPid(mlint64 ctxRef, int vid, int pid) {
	libusb_device_handle *devHandle = libusb_open_device_with_vid_pid(
			(libusb_context*) ctxRef, vid, pid);
	if (devHandle == NULL) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"libusbOpenDeviceWithVidPid:null:error: ");
	} else {
		if (!MLPutInteger64(stdlink, (mlint64) devHandle)) {
			libusb_close(devHandle);
			failWithMsgAndError(stdlink,
					"libusbOpenDeviceWithVidPid:result:error: ");
		}
	}
}
void libusbGetDeviceBusAndAddressByName(const char *devName) {
	char *usbDev = NULL;
	regex_t rx;
	if (regcomp(&rx, "^/dev/bus/usb/[0-9][0-9][0-9]/[0-9][0-9][0-9]$", 0)) {
		if (!MLPutFunction(stdlink, "$Failed", 1)
				|| !MLPutString(stdlink, "could not create regex")) {
			failWithMsgAndError(stdlink,
					"libusbGetDeviceAddressByName:couldNotCreateRegex:error: ");
			return;
		}
	}
	usbDev = canonicalize_file_name(devName);
	if (usbDev == NULL || regexec(&rx, usbDev, 0, NULL, 0)) {
		if (!MLPutFunction(stdlink, "$Failed", 2)
				|| !MLPutString(stdlink,
						"device file does not point to usb device")
				|| !(usbDev ?
						MLPutString(stdlink, usbDev) :
						MLPutSymbol(stdlink, "Null"))) {
			failWithMsgAndError(stdlink,
					"libusbGetDeviceAddressByName:devIsNotUSB:error: ");
		}
		if (usbDev)
			free(usbDev);
		return;
	}
	usbDev[16] = 0;
	uint8_t bus = atoi(usbDev + 13);
	uint8_t devAddr = atoi(usbDev + 17);
	free(usbDev);
	if (!MLPutFunction(stdlink, "List", 2) || !MLPutInteger(stdlink, bus)
			|| !MLPutInteger(stdlink, devAddr)) {
		failWithMsgAndError(stdlink,
				"libusbGetDeviceAddressByName:return:error: ");
	}
}
void libusbOpenDeviceByAddress(mlint64 ctxRef, int bus, int devAddr) {
	libusb_device **devList;
	size_t r = libusb_get_device_list((libusb_context *) ctxRef, &devList);
	if (devList == NULL || r == 0) {
		if (!MLPutFunction(stdlink, "$Failed", 1)
				|| !MLPutString(stdlink,
						devList ?
								"no usb devices attached to system" :
								"could not get device list")) {
			failWithMsgAndError(stdlink,
					"libusbOpenDeviceByName:deviceListEmpty:error: ");
		}
		if (devList)
			libusb_free_device_list(devList, 1);
		return;
	}
	libusb_device *devRef = NULL;
	for (int i = 0; i < r; i++)
		if (libusb_get_bus_number(devList[i]) == bus
				&& libusb_get_device_address(devList[i]) == devAddr) {
			devRef = devList[i];
			break;
		}
	if (devRef == NULL) {
		if (!MLPutFunction(stdlink, "$Failed", 1)
				|| !MLPutString(stdlink, "device not found")) {
			failWithMsgAndError(stdlink,
					"libusbOpenDeviceByName:deviceNotFound:error: ");
		}
		libusb_free_device_list(devList, 1);
		return;
	}
	libusb_device_handle *devHandle;
	int err = libusb_open((libusb_device*) devRef, &devHandle);
	libusb_free_device_list(devList, 1);
	if (err < LIBUSB_SUCCESS) {
		libusbFailWithError(err);
		return;
	}
	if (!MLPutInteger64(stdlink, (mlint64) devHandle)) {
		libusb_close(devHandle);
		failWithMsgAndError(stdlink, "libusbOpenDevice:result:error: ");
	}
}
void libusbClose(mlint64 devHandleRef) {
	libusb_close((libusb_device_handle*) devHandleRef);
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink, "libusbClose:return:error: ");
}

void libusbGetDevice(mlint64 devHandleRef) {
	libusb_device *dev = libusb_get_device(
			(libusb_device_handle*) devHandleRef);
	if (dev == NULL) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "libusbGetDevice:null:error: ");
	} else {
		if (!MLPutInteger64(stdlink, (mlint64) dev))
			failWithMsgAndError(stdlink, "libusbGetDevice:result:error: ");
	}
}

void libusbGetConfiguration(mlint64 devHandleRef) {
	int config;
	int r = libusb_get_configuration((libusb_device_handle*) devHandleRef,
			&config);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else {
		if (!MLPutInteger(stdlink, config))
			failWithMsgAndError(stdlink,
					"libusbGetConfiguration:result:error: ");
	}
}

void libusbSetConfiguration(mlint64 devHandleRef, int config) {
	int r = libusb_set_configuration((libusb_device_handle*) devHandleRef,
			config);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "libusbSetConfiguration:null:error: ");
	}
}

void libusbClaimInterface(mlint64 devHandleRef, int interfaceNumber) {
	int r = libusb_claim_interface((libusb_device_handle*) devHandleRef,
			interfaceNumber);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "libusbClaimInterface:null:error: ");
	}
}

void libusbReleaseInterface(mlint64 devHandleRef, int interfaceNumber) {
	int r = libusb_release_interface((libusb_device_handle*) devHandleRef,
			interfaceNumber);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "libusbReleaseInterface:null:error: ");
	}
}

void libusbSetInterfaceAltSetting(mlint64 devHandleRef, int interfaceNumber,
		int alternateSetting) {
	int r = libusb_set_interface_alt_setting(
			(libusb_device_handle*) devHandleRef, interfaceNumber,
			alternateSetting);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"libusbSetInterfaceAltSetting:null:error: ");
	}
}

void libusbClearHalt(mlint64 devHandleRef, int endpoint) {
	int r = libusb_clear_halt((libusb_device_handle*) devHandleRef, endpoint);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "libusbClearHalt:null:error: ");
	}
}

void libusbResetDevice(mlint64 devHandleRef) {
	int r = libusb_reset_device((libusb_device_handle*) devHandleRef);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "libusbResetDevice:null:error: ");
	}
}

void libusbKernelDriverActive(mlint64 devHandleRef, int interfaceNumber) {
	int r = libusb_kernel_driver_active((libusb_device_handle*) devHandleRef,
			interfaceNumber);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else {
		if (!MLPutSymbol(stdlink, r == 1 ? "True" : "False"))
			failWithMsgAndError(stdlink,
					"libusbKernelDriverActive:return:error: ");
	}
}

void libusbDetachKernelDriver(mlint64 devHandleRef, int interfaceNumber) {
	int r = libusb_detach_kernel_driver((libusb_device_handle*) devHandleRef,
			interfaceNumber);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"libusbDetachKernelDriver:null:error: ");
	}
}

void libusbAttachKernelDriver(mlint64 devHandleRef, int interfaceNumber) {
	int r = libusb_attach_kernel_driver((libusb_device_handle*) devHandleRef,
			interfaceNumber);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"libusbDetachKernelDriver:null:error: ");
	}
}

void libusbGetCapabilities(void) {
	int err;
	MLINK loop = MLLoopbackOpen(stdenv, &err);
	if (err != MLEOK) {
		MLPutFunction(stdlink, "$Failed", 1);
		MLPutString(stdlink, MLErrorString(stdenv, err));
	} else {
		int c = 0;
		if (libusb_has_capability(LIBUSB_CAP_HAS_CAPABILITY)) {
			MLPutString(loop, "CAP_HAS_CAPABILITY");
			c++;
		}
		if (libusb_has_capability(LIBUSB_CAP_HAS_HOTPLUG)) {
			MLPutString(loop, "CAP_HAS_HOTPLUG");
			c++;
		}
		if (libusb_has_capability(LIBUSB_CAP_HAS_HID_ACCESS)) {
			MLPutString(loop, "CAP_HAS_HID_ACCESS");
			c++;
		}
		if (libusb_has_capability(LIBUSB_CAP_SUPPORTS_DETACH_KERNEL_DRIVER)) {
			MLPutString(loop, "CAP_SUPPORTS_DETACH_KERNEL_DRIVER");
			c++;
		}
		MLEndPacket(loop);
		if (!MLPutFunction(stdlink, "List", c)) {
			failWithMsgAndError(stdlink,
					"libusbGetCapabilities:result:list:error: ");
			MLClose(loop);
			return;
		}
		if (!MLTransferToEndOfLoopbackLink(stdlink, loop))
			failWithMsgAndError(stdlink,
					"libusbGetCapabilities:result:transfer:error: ");
		MLClose(loop);
	}
}

void libusbGetVersion(void) {
	const struct libusb_version *version = libusb_get_version();
	if (!MLPutFunction(stdlink, "List", 6)
			|| !MLPutInteger(stdlink, version->major)
			|| !MLPutInteger(stdlink, version->minor)
			|| !MLPutInteger(stdlink, version->micro)
			|| !MLPutInteger(stdlink, version->nano)
			|| !MLPutString(stdlink, version->rc)
			|| !MLPutString(stdlink, version->describe)) {
		failWithMsgAndError(stdlink, "libusbGetVersion:result:error: ");
		return;
	}
}

const char *libusbDescriptorTypeName(enum libusb_descriptor_type t) {
	switch (t) {
	case LIBUSB_DT_DEVICE:
		return "DT_DEVICE";
	case LIBUSB_DT_CONFIG:
		return "DT_CONFIG";
	case LIBUSB_DT_STRING:
		return "DT_STRING";
	case LIBUSB_DT_INTERFACE:
		return "DT_INTERFACE";
	case LIBUSB_DT_ENDPOINT:
		return "DT_ENDPOINT";
	case LIBUSB_DT_BOS:
		return "DT_BOS";
	case LIBUSB_DT_DEVICE_CAPABILITY:
		return "DT_DEVICE_CAPABILITY";
	case LIBUSB_DT_HID:
		return "DT_HID";
	case LIBUSB_DT_REPORT:
		return "DT_REPORT";
	case LIBUSB_DT_PHYSICAL:
		return "DT_PHYSICAL";
	case LIBUSB_DT_HUB:
		return "DT_HUB";
	case LIBUSB_DT_SUPERSPEED_HUB:
		return "DT_SUPERSPEED_HUB";
	case LIBUSB_DT_SS_ENDPOINT_COMPANION:
		return "DT_SS_ENDPOINT_COMPANION";
	default:
		return "DT_UNKNOWN";
	}
}

const char *libusbClassName(enum libusb_class_code t) {
	switch (t) {
	case LIBUSB_CLASS_PER_INTERFACE:
		return "CLASS_PER_INTERFACE";
	case LIBUSB_CLASS_AUDIO:
		return "CLASS_AUDIO";
	case LIBUSB_CLASS_COMM:
		return "CLASS_COMM";
	case LIBUSB_CLASS_HID:
		return "CLASS_HID";
	case LIBUSB_CLASS_PHYSICAL:
		return "CLASS_PHYSICAL";
	case LIBUSB_CLASS_PRINTER:
		return "CLASS_PRINTER";
	case LIBUSB_CLASS_IMAGE:
		return "CLASS_IMAGE";
//	case LIBUSB_CLASS_PTP:
//		return "CLASS_PTP";
	case LIBUSB_CLASS_MASS_STORAGE:
		return "CLASS_MASS_STORAGE";
	case LIBUSB_CLASS_HUB:
		return "CLASS_HUB";
	case LIBUSB_CLASS_DATA:
		return "CLASS_DATA";
	case LIBUSB_CLASS_SMART_CARD:
		return "CLASS_SMART_CARD";
	case LIBUSB_CLASS_CONTENT_SECURITY:
		return "CLASS_CONTENT_SECURITY";
	case LIBUSB_CLASS_VIDEO:
		return "CLASS_VIDEO";
	case LIBUSB_CLASS_PERSONAL_HEALTHCARE:
		return "CLASS_PERSONAL_HEALTHCARE";
	case LIBUSB_CLASS_DIAGNOSTIC_DEVICE:
		return "CLASS_DIAGNOSTIC_DEVICE";
	case LIBUSB_CLASS_WIRELESS:
		return "CLASS_WIRELESS";
	case LIBUSB_CLASS_APPLICATION:
		return "CLASS_APPLICATION";
	case LIBUSB_CLASS_VENDOR_SPEC:
		return "CLASS_VENDOR_SPEC";
	default:
		return "CLASS_UNKNOWN";
	}
}

int libusbWriteDeviceDescriptorToMathLink(
		const struct libusb_device_descriptor *desc) {
	if (desc == NULL) {
		if (!MLPutSymbol(stdlink, "Null"))
			return 0;
		return 1;
	}
	if (!MLPutFunction(stdlink, "LibUSBDescriptor", 14)
			|| !MLPutInteger(stdlink, desc->bLength)
			|| !MLPutString(stdlink,
					libusbDescriptorTypeName(desc->bDescriptorType)))
		return 0;
	char temp[16];
	sprintf(temp, "USB %x.%02x", (desc->bcdUSB >> 8) & 0xFF,
			desc->bcdUSB & 0xFF);
	if (!MLPutString(stdlink, temp)
			|| !MLPutString(stdlink, libusbClassName(desc->bDeviceClass))
			|| !MLPutInteger(stdlink, desc->bDeviceSubClass)
			|| !MLPutInteger(stdlink, desc->bDeviceProtocol)
			|| !MLPutInteger(stdlink, desc->bMaxPacketSize0))
		return 0;
	sprintf(temp, "%04x", desc->idVendor);
	if (!MLPutString(stdlink, temp))
		return 0;
	sprintf(temp, "%04x", desc->idProduct);
	if (!MLPutString(stdlink, temp))
		return 0;
	sprintf(temp, "%x.%02x", (desc->bcdDevice >> 8) & 0xFF,
			desc->bcdDevice & 0xFF);
	if (!MLPutString(stdlink, temp)
			|| !MLPutInteger(stdlink, desc->iManufacturer)
			|| !MLPutInteger(stdlink, desc->iProduct)
			|| !MLPutInteger(stdlink, desc->iSerialNumber)
			|| !MLPutInteger(stdlink, desc->bNumConfigurations))
		return 0;
	return 1;
}

void libusbGetDeviceDescriptor(mlint64 devRef) {
	struct libusb_device_descriptor desc;
	int r = libusb_get_device_descriptor((libusb_device*) devRef, &desc);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else if (!libusbWriteDeviceDescriptorToMathLink(&desc))
		failWithMsgAndError(stdlink,
				"libusbGetDeviceDescriptor:result:error: ");
}
const char *libusbTransferTypeName(enum libusb_transfer_type t) {
	switch (t) {
	case LIBUSB_TRANSFER_TYPE_CONTROL:
		return "TRANSFER_TYPE_CONTROL";
	case LIBUSB_TRANSFER_TYPE_ISOCHRONOUS:
		return "TRANSFER_TYPE_ISOCHRONOUS";
	case LIBUSB_TRANSFER_TYPE_BULK:
		return "TRANSFER_TYPE_BULK";
	case LIBUSB_TRANSFER_TYPE_INTERRUPT:
		return "TRANSFER_TYPE_INTERRUPT";
	default:
		return "TRANSFER_TYPE_UNKNOWN";
	}
}

const char *libusbIsoSyncTypeName(enum libusb_iso_sync_type t) {
	switch (t) {
	case LIBUSB_ISO_SYNC_TYPE_NONE:
		return "ISO_SYNC_TYPE_NONE";
	case LIBUSB_ISO_SYNC_TYPE_ASYNC:
		return "ISO_SYNC_TYPE_ASYNC";
	case LIBUSB_ISO_SYNC_TYPE_ADAPTIVE:
		return "ISO_SYNC_TYPE_ADAPTIVE";
	case LIBUSB_ISO_SYNC_TYPE_SYNC:
		return "ISO_SYNC_TYPE_SYNC";
	default:
		return "ISO_SYNC_TYPE_UNKNOWN";
	}
}

const char *libusbIsoUsageTypeName(enum libusb_iso_sync_type t) {
	switch (t) {
	case LIBUSB_ISO_USAGE_TYPE_DATA:
		return "ISO_USAGE_TYPE_DATA";
	case LIBUSB_ISO_USAGE_TYPE_FEEDBACK:
		return "ISO_USAGE_TYPE_FEEDBACK";
	case LIBUSB_ISO_USAGE_TYPE_IMPLICIT:
		return "ISO_USAGE_TYPE_IMPLICIT";
	default:
		return "ISO_USAGE_TYPE_UNKNOWN";
	}
}

int libusbWriteEndpointDescriptorToMathLink(
		const struct libusb_endpoint_descriptor *desc) {
	if (desc == NULL) {
		if (!MLPutSymbol(stdlink, "Null"))
			return 0;
		return 1;
	}
	if (!MLPutFunction(stdlink, "LibUSBDescriptor",
			desc->extra_length > 0 ? 9 : 8))
		return 0;
	if (!MLPutInteger(stdlink, desc->bLength))
		return 0;
	if (!MLPutString(stdlink, libusbDescriptorTypeName(desc->bDescriptorType)))
		return 0;
	if (!MLPutInteger(stdlink, desc->bEndpointAddress))
		return 0;
	if (!MLPutFunction(stdlink, "List", 4))
		return 0;
	if (!MLPutString(stdlink,
			libusbTransferTypeName(
					desc->bmAttributes & LIBUSB_TRANSFER_TYPE_MASK)))
		return 0;
	if (!MLPutString(stdlink,
			libusbIsoSyncTypeName(
					(desc->bmAttributes & LIBUSB_ISO_SYNC_TYPE_MASK) >> 2)))
		return 0;
	if (!MLPutString(stdlink,
			libusbIsoSyncTypeName(
					(desc->bmAttributes & LIBUSB_ISO_USAGE_TYPE_MASK) >> 4)))
		return 0;
	if (!MLPutInteger(stdlink,
			(desc->bmAttributes
					& ~(LIBUSB_TRANSFER_TYPE_MASK | LIBUSB_ISO_SYNC_TYPE_MASK
							| LIBUSB_ISO_USAGE_TYPE_MASK)) >> 4))
		return 0;
	if (!MLPutInteger(stdlink, desc->wMaxPacketSize))
		return 0;
	if (!MLPutInteger(stdlink, desc->bInterval))
		return 0;
	if (!MLPutInteger(stdlink, desc->bRefresh))
		return 0;
	if (!MLPutInteger(stdlink, desc->bSynchAddress))
		return 0;
	if (desc->extra_length > 0) {
		if (!MLPutByteString(stdlink, desc->extra, desc->extra_length))
			return 0;
	}
	return 1;
}
int libusbWriteInterfaceDescriptorToMathLink(
		const struct libusb_interface_descriptor *desc) {
	if (desc == NULL) {
		if (!MLPutSymbol(stdlink, "Null"))
			return 0;
		return 1;
	}
	if (!MLPutFunction(stdlink, "LibUSBDescriptor",
			desc->extra_length > 0 ? 11 : 10))
		return 0;
	if (!MLPutInteger(stdlink, desc->bLength))
		return 0;
	if (!MLPutString(stdlink, libusbDescriptorTypeName(desc->bDescriptorType)))
		return 0;
	if (!MLPutInteger(stdlink, desc->bInterfaceNumber))
		return 0;
	if (!MLPutInteger(stdlink, desc->bAlternateSetting))
		return 0;
	if (!MLPutInteger(stdlink, desc->bNumEndpoints))
		return 0;
	if (!MLPutString(stdlink, libusbClassName(desc->bInterfaceClass)))
		return 0;
	if (!MLPutInteger(stdlink, desc->bInterfaceSubClass))
		return 0;
	if (!MLPutInteger(stdlink, desc->bInterfaceProtocol))
		return 0;
	if (!MLPutInteger(stdlink, desc->iInterface))
		return 0;
	if (!MLPutFunction(stdlink, "List", desc->bNumEndpoints))
		return 0;
	for (int i = 0; i < desc->bNumEndpoints; i++)
		if (!libusbWriteEndpointDescriptorToMathLink(&desc->endpoint[i]))
			return 0;
	if (desc->extra_length > 0) {
		if (!MLPutByteString(stdlink, desc->extra, desc->extra_length))
			return 0;
	}
	return 1;
}

int libusbWriteInterfaceToMathLink(const struct libusb_interface *desc) {
	if (desc == NULL) {
		if (!MLPutSymbol(stdlink, "Null"))
			return 0;
		return 1;
	}
	if (!MLPutFunction(stdlink, "LibUSBInterface", 2))
		return 0;
	if (!MLPutFunction(stdlink, "List", desc->num_altsetting))
		return 0;
	for (int i = 0; i < desc->num_altsetting; i++)
		if (!libusbWriteInterfaceDescriptorToMathLink(&desc->altsetting[i]))
			return 0;
	if (!MLPutInteger(stdlink, desc->num_altsetting))
		return 0;
	return 1;
}

int libusbWriteConfigDescriptorToMathLink(
		const struct libusb_config_descriptor *desc) {
	if (desc == NULL) {
		if (!MLPutSymbol(stdlink, "Null"))
			return 0;
		return 1;
	}
	if (!MLPutFunction(stdlink, "LibUSBDescriptor",
			desc->extra_length > 0 ? 10 : 9))
		return 0;
	if (!MLPutInteger(stdlink, desc->bLength))
		return 0;
	if (!MLPutString(stdlink, libusbDescriptorTypeName(desc->bDescriptorType)))
		return 0;
	if (!MLPutInteger(stdlink, desc->wTotalLength))
		return 0;
	if (!MLPutInteger(stdlink, desc->bNumInterfaces))
		return 0;
	if (!MLPutInteger(stdlink, desc->bConfigurationValue))
		return 0;
	if (!MLPutInteger(stdlink, desc->iConfiguration))
		return 0;
	char temp[8];
	sprintf(temp, "0x%02x", desc->bmAttributes);
	if (!MLPutString(stdlink, temp))
		return 0;
	if (!MLPutReal(stdlink, desc->MaxPower * 0.002))
		return 0;
	if (!MLPutFunction(stdlink, "List", desc->bNumInterfaces))
		return 0;
	for (int i = 0; i < desc->bNumInterfaces; i++)
		if (!libusbWriteInterfaceToMathLink(&desc->interface[i]))
			return 0;
	if (desc->extra_length > 0) {
		if (!MLPutByteString(stdlink, desc->extra, desc->extra_length))
			return 0;
	}
	return 1;
}

void libusbGetActiveConfigDescriptor(mlint64 devRef) {
	struct libusb_config_descriptor *config;
	int r = libusb_get_active_config_descriptor((libusb_device*) devRef,
			&config);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else {
		if (!libusbWriteConfigDescriptorToMathLink(config))
			failWithMsgAndError(stdlink,
					"libusbGetActiveConfigDescriptor:result:error: ");
		libusb_free_config_descriptor(config);
	}
}

void libusbGetConfigDescriptor(mlint64 devRef, int configIndex) {
	struct libusb_config_descriptor *config;
	int r = libusb_get_config_descriptor((libusb_device*) devRef, configIndex,
			&config);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else {
		if (!libusbWriteConfigDescriptorToMathLink(config))
			failWithMsgAndError(stdlink,
					"libusbGetConfigDescriptor:result:error: ");
		libusb_free_config_descriptor(config);
	}
}

void libusbGetConfigDescriptorByValue(mlint64 devRef, int configValue) {
	struct libusb_config_descriptor *config;
	int r = libusb_get_config_descriptor_by_value((libusb_device*) devRef,
			configValue, &config);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else {
		if (!libusbWriteConfigDescriptorToMathLink(config))
			failWithMsgAndError(stdlink,
					"libusbGetConfigDescriptorByValue:result:error: ");
		libusb_free_config_descriptor(config);
	}
}

#define LIBUSB_STRING_DESCRIPTOR_LENGTH 1024
void libusbGetStringDescriptorASCII(mlint64 devHandleRef, int descIndex) {
	unsigned char *data = malloc(LIBUSB_STRING_DESCRIPTOR_LENGTH);
	if (data == NULL) {
		failWithMsg(stdlink, "libusbGetStringDescriptorASCII:data:malloc");
		return;
	}
	int r = libusb_get_string_descriptor_ascii(
			(libusb_device_handle*) devHandleRef, descIndex, data,
			LIBUSB_STRING_DESCRIPTOR_LENGTH);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else {
		if (!MLPutByteString(stdlink, data, r))
			failWithMsgAndError(stdlink,
					"libusbGetStringDescriptorASCII:result:error: ");
	}
	free(data);
}

void libusbGetDescriptor(mlint64 devHandleRef, int descType, int descIndex) {
	unsigned char *data = malloc(LIBUSB_STRING_DESCRIPTOR_LENGTH);
	if (data == NULL) {
		failWithMsg(stdlink, "libusbGetDescriptor:data:malloc");
		return;
	}
	int r = libusb_get_descriptor((libusb_device_handle*) devHandleRef,
			descType, descIndex, data,
			LIBUSB_STRING_DESCRIPTOR_LENGTH);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else {
		if (!MLPutByteString(stdlink, data, r))
			failWithMsgAndError(stdlink, "libusbGetDescriptor:result:error: ");
	}
	free(data);
}

void libusbGetStringDescriptor(mlint64 devHandleRef, int descIndex, int langId) {
	unsigned char *data = malloc(LIBUSB_STRING_DESCRIPTOR_LENGTH);
	if (data == NULL) {
		failWithMsg(stdlink, "libusbGetStringDescriptor:data:malloc");
		return;
	}
	int r = libusb_get_string_descriptor((libusb_device_handle*) devHandleRef,
			descIndex, langId, data,
			LIBUSB_STRING_DESCRIPTOR_LENGTH);
	if (r < LIBUSB_SUCCESS)
		libusbFailWithError(r);
	else if (!MLPutUTF16String(stdlink, (unsigned short *) data, r))
		failWithMsgAndError(stdlink,
				"libusbGetStringDescriptor:result:error: ");
	free(data);
}

void libusbControlTransfer(mlint64 devHandleRef, int bmRequestType,
		int bRequest, int wValue, int wIndex, float timeout) {
	int length, r;
	int timeout_ms = (int) (timeout * 1000);
	switch (MLGetType(stdlink)) {
	case MLTKINT:
		bmRequestType |= LIBUSB_ENDPOINT_IN;
		if (!MLGetInteger(stdlink, &length)) {
			failWithMsgAndError(stdlink,
					"libusbControlTransfer:length:error: ");
			return;
		}
		unsigned char *dataToRead = length > 0 ? malloc(length) : NULL;
		if (length > 0 && dataToRead == NULL) {
			failWithMsg(stdlink, "libusbControlTransfer:data:malloc");
			return;
		}
		r = libusb_control_transfer((libusb_device_handle*) devHandleRef,
				bmRequestType, bRequest, wValue, wIndex, dataToRead, length,
				timeout_ms);
		if (r < LIBUSB_SUCCESS)
			libusbFailWithError(r);
		else if ((length > 0 && !MLPutByteString(stdlink, dataToRead, r))
				|| (length == 0 && !MLPutSymbol(stdlink, "Null")))
			failWithMsgAndError(stdlink,
					"libusbControlTransfer:result:error: ");
		if (dataToRead != NULL)
			free(dataToRead);
		break;
	case MLTKSTR:
		bmRequestType &= ~LIBUSB_ENDPOINT_IN;
		const unsigned char *dataToWrite;
		if (!MLGetByteString(stdlink, &dataToWrite, &length, 0)) {
			failWithMsgAndError(stdlink, "libusbControlTransfer:data:error: ");
			return;
		}
		r = libusb_control_transfer((libusb_device_handle*) devHandleRef,
				bmRequestType, bRequest, wValue, wIndex, dataToWrite, length,
				timeout_ms);
		MLReleaseByteString(stdlink, dataToWrite, length);
		if (r < LIBUSB_SUCCESS)
			libusbFailWithError(r);
		else if (!MLPutInteger(stdlink, r))
			failWithMsgAndError(stdlink,
					"libusbControlTransfer:result:error: ");
		break;
	default:
		failWithMsg(stdlink, "libusbControlTransfer:wrongCallFormat");
	}
}

void libusbBulkTransfer(mlint64 devHandleRef, int endpoint, float timeout) {
	int length, transferredLength, r;
	int timeout_ms = (int) (timeout * 1000);
	switch (MLGetType(stdlink)) {
	case MLTKINT:
		endpoint |= LIBUSB_ENDPOINT_IN;
		if (!MLGetInteger(stdlink, &length)) {
			failWithMsgAndError(stdlink, "libusbBulkTransfer:length:error: ");
			return;
		}
		unsigned char *dataToRead = malloc(length);
		if (dataToRead == NULL) {
			failWithMsg(stdlink, "libusbBulkTransfer:dataToRead:malloc");
			return;
		}
		r = libusb_bulk_transfer((libusb_device_handle*) devHandleRef, endpoint,
				dataToRead, length, &transferredLength, timeout_ms);
		if (r < LIBUSB_SUCCESS)
			libusbFailWithError(r);
		else if (!MLPutByteString(stdlink, dataToRead, transferredLength))
			failWithMsgAndError(stdlink, "libusbBulkTransfer:result:error: ");
		free(dataToRead);
		break;
	case MLTKSTR:
		endpoint &= ~LIBUSB_ENDPOINT_IN;
		const unsigned char *dataToWrite;
		if (!MLGetByteString(stdlink, &dataToWrite, &length, 0)) {
			failWithMsgAndError(stdlink,
					"libusbBulkTransfer:dataToWrite:error: ");
			return;
		}
		r = libusb_bulk_transfer((libusb_device_handle*) devHandleRef, endpoint,
				dataToWrite, length, &transferredLength, timeout_ms);
		MLReleaseByteString(stdlink, dataToWrite, length);
		if (r < LIBUSB_SUCCESS)
			libusbFailWithError(r);
		else if (!MLPutInteger(stdlink, transferredLength))
			failWithMsgAndError(stdlink, "libusbBulkTransfer:result:error: ");
		break;
	default:
		failWithMsg(stdlink, "libusbBulkTransfer:wrongCallFormat");
	}
}

void libusbInterruptTransfer(mlint64 devHandleRef, int endpoint, float timeout) {
	int length, transferredLength, r;
	int timeout_ms = (int) (timeout * 1000);
	switch (MLGetType(stdlink)) {
	case MLTKINT:
		endpoint |= LIBUSB_ENDPOINT_IN;
		if (!MLGetInteger(stdlink, &length)) {
			failWithMsgAndError(stdlink,
					"libusbInterruptTransfer:length:error: ");
			return;
		}
		unsigned char *dataToRead = malloc(length);
		if (dataToRead == NULL) {
			failWithMsg(stdlink, "libusbInterruptTransfer:data:malloc");
			return;
		}
		r = libusb_interrupt_transfer((libusb_device_handle*) devHandleRef,
				endpoint, dataToRead, length, &transferredLength, timeout_ms);
		if (r < LIBUSB_SUCCESS)
			libusbFailWithError(r);
		else if (!MLPutByteString(stdlink, dataToRead, transferredLength))
			failWithMsgAndError(stdlink,
					"libusbInterruptTransfer:result:error: ");
		free(dataToRead);
		break;
	case MLTKSTR:
		endpoint &= ~LIBUSB_ENDPOINT_IN;
		const unsigned char *dataToWrite;
		if (!MLGetByteString(stdlink, &dataToWrite, &length, 0)) {
			failWithMsgAndError(stdlink,
					"libusbInterruptTransfer:data:error: ");
			return;
		}
		r = libusb_interrupt_transfer((libusb_device_handle*) devHandleRef,
				endpoint, dataToWrite, length, &transferredLength, timeout_ms);
		MLReleaseByteString(stdlink, dataToWrite, length);
		if (r < LIBUSB_SUCCESS)
			libusbFailWithError(r);
		else if (!MLPutInteger(stdlink, transferredLength))
			failWithMsgAndError(stdlink,
					"libusbInterruptTransfer:result:error: ");
		break;
	default:
		failWithMsg(stdlink, "libusbInterruptTransfer:wrongCallFormat");
	}
}
