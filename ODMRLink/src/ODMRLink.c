//============================================================================
// Name        : ODMRLink.c
// Author      : Roger John <roger.john@uni-leipzig.de>
// Version     :
// Copyright   : Your copyright notice
// Description : Controlling odmr setup from Mathematica
//============================================================================

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <bits/time.h>
#include "MathLinkCommon.h"
#include "ODMRLink.tmc"
#include <Madlib.h>

int sargc;
char **sargv;

#define NUM_COUNTERS 2
int countCounters(int counter, int counts[NUM_COUNTERS]) {
	char buf[4 * NUM_COUNTERS];
	write(counter, "C", 1);
	int l = 0;
	while (l < 4 * NUM_COUNTERS) {
		int s = read(counter, buf + l, 4 * NUM_COUNTERS - l);
		if (s < 0)
			return -1;
		l += s;
	}
	for (int i = 0; i < NUM_COUNTERS; i++)
		counts[i] = *(int*) (buf + i * 4);
	return NUM_COUNTERS;
}
int countCountersAndMoveOn(int counter, int counts[NUM_COUNTERS],
		long countingTime_us, void (*moveFunc)(void*), void *moveArg,
		long settlingTime_us) {
	char buf[4 * NUM_COUNTERS];
	write(counter, "C", 1);
	usleep(countingTime_us);
	struct timespec startTime, endTime;
	clock_gettime(CLOCK_MONOTONIC, &startTime);
	moveFunc(moveArg);
	int l = 0;
	while (l < 4 * NUM_COUNTERS) {
		int s = read(counter, buf + l, 4 * NUM_COUNTERS - l);
		if (s < 0)
			return -1;
		l += s;
	}
	for (int i = 0; i < NUM_COUNTERS; i++)
		counts[i] = *(int*) (buf + i * 4);
	clock_gettime(CLOCK_MONOTONIC, &endTime);
	long additionalWaitTime_us = settlingTime_us
			- (long) (((endTime.tv_sec - startTime.tv_sec) * 1000000000ll
					+ (endTime.tv_nsec - startTime.tv_nsec)) / 1000l);
	if (additionalWaitTime_us > 0)
		usleep(additionalWaitTime_us);
	return NUM_COUNTERS;
}
struct singleAxisMoveArgument {
	int stage;
	int axis;
	double value;
};
void singleAxisMoveFunction(void *arg) {
	struct singleAxisMoveArgument *x = (struct singleAxisMoveArgument*) arg;
	MCL_SingleWriteN(x->value, x->axis, x->stage);
}
struct threeAxesMoveArgument {
	int stage;
	double values[3];
};
void threeAxesMoveFunction(void *arg) {
	struct threeAxesMoveArgument *x = (struct threeAxesMoveArgument*) arg;
	for (int i = 0; i < 3; i++)
		MCL_SingleWriteN(x->values[i], i + 1, x->stage);
}
struct twoAxesMoveArgument {
	int stage;
	int axes[2];
	double values[2];
};
void twoAxesMoveFunction(void *arg) {
	struct twoAxesMoveArgument *x = (struct twoAxesMoveArgument*) arg;
	for (int i = 0; i < 2; i++)
		MCL_SingleWriteN(x->values[i], x->axes[i], x->stage);
}
void odmrScanAxis(int stage, int counter, int xAxis, double xmin, double xmax,
		int nPixels, double y, double z, double iwt, double wt) {
	if (nPixels < 2) {
		MLPutFunction(stdlink, "$Failed", 1);
		MLPutString(stdlink, "nPixelsTooLow");
		return;
	}
	int yAxis = ((xAxis + 1) - 1) % 3 + 1, zAxis = ((xAxis + 2) - 1) % 3 + 1;
	int wt_us = (int) (wt * 1000000);
	int counts[NUM_COUNTERS];
	MCL_SingleWriteN(y, yAxis, stage);
	MCL_SingleWriteN(z, zAxis, stage);
	double step = (xmax - xmin) / (nPixels - 1);
	if (!MLPutFunction(stdlink, "List", nPixels)) {
		failWithMsgAndError(stdlink, "odmrScanLine:result:error: ");
		return;
	}
	for (int pixel = 0; pixel < nPixels; pixel++) {
		double x = xmin + pixel * step;
		MCL_SingleWriteN(x, xAxis, stage);
		if (x == xmin) {
			if (iwt > 0)
				usleep((int) (iwt * 1000000));
		} else if (wt_us > 0)
			usleep(wt_us);
		if (countCounters(counter, counts) < NUM_COUNTERS) {
			failWithMsg(stdlink, "odmrScanLine:count:error: ");
			return;
		}
		if (!MLPutInteger32List(stdlink, counts, NUM_COUNTERS)) {
			failWithMsgAndError(stdlink, "odmrScanLine:result2:error: ");
			return;
		}
	}
}
void odmrScanAxisOpt(int stage, int counter, int xAxis, double xmin,
		double xmax, int nPixels, double y, double z, double iwt, double wt,
		double ct) {
	if (nPixels < 2) {
		MLPutFunction(stdlink, "$Failed", 1);
		MLPutString(stdlink, "nPixelsTooLow");
		return;
	}
	int yAxis = ((xAxis + 1) - 1) % 3 + 1, zAxis = ((xAxis + 2) - 1) % 3 + 1;
	int wt_us = (int) (wt * 1000000);
	int ct_us = (int) (ct * 1000000);
	int counts[NUM_COUNTERS];
	MCL_SingleWriteN(y, yAxis, stage);
	MCL_SingleWriteN(z, zAxis, stage);
	double step = (xmax - xmin) / (nPixels - 1);
	if (!MLPutFunction(stdlink, "List", nPixels)) {
		failWithMsgAndError(stdlink, "odmrScanLineOpt:result:error: ");
		return;
	}
	struct singleAxisMoveArgument ma = { stage, xAxis, xmin };
	MCL_SingleWriteN(xmin, xAxis, stage);
	if (iwt > 0)
		usleep((int) (iwt * 1000000));
	for (int pixel = 1; pixel < nPixels; pixel++) {
		ma.value = xmin + pixel * step;
		if (countCountersAndMoveOn(counter, counts, ct_us,
				singleAxisMoveFunction, &ma, wt_us) < NUM_COUNTERS) {
			failWithMsg(stdlink, "odmrScanLineOpt:countAndMoveOn:error: ");
			return;
		}
		if (!MLPutInteger32List(stdlink, counts, NUM_COUNTERS)) {
			failWithMsgAndError(stdlink, "odmrScanLineOpt:result2:error: ");
			return;
		}
	}
	if (countCounters(counter, counts) < NUM_COUNTERS) {
		failWithMsg(stdlink, "odmrScanLineOpt:count:error: ");
		return;
	}
	if (!MLPutInteger32List(stdlink, counts, NUM_COUNTERS)) {
		failWithMsgAndError(stdlink, "odmrScanLineOpt:result3:error: ");
		return;
	}
}
void odmrScanArea(int stage, int counter, int xAxis, double xmin, double xmax,
		int xPixels, int yAxis, double ymin, double ymax, int yPixels, double z,
		double iwt, double btwt, double wt) {
	if (xPixels < 2 || yPixels < 2) {
		MLPutFunction(stdlink, "$Failed", 1);
		MLPutString(stdlink, "nPixelsTooLow");
		return;
	}
	if (xAxis == yAxis) {
		MLPutFunction(stdlink, "$Failed", 1);
		MLPutString(stdlink, "nullArea");
		return;
	}
	int zAxis;
	for (zAxis = 1; zAxis <= 3; zAxis++)
		if (zAxis != xAxis && zAxis != yAxis)
			break;
	int btwt_us = (int) (btwt * 1000000);
	int wt_us = (int) (wt * 1000000);
	int counts[NUM_COUNTERS];
	MCL_SingleWriteN(z, zAxis, stage);
	double xStep = (xmax - xmin) / (xPixels - 1), yStep = (ymax - ymin)
			/ (yPixels - 1);
	if (!MLPutFunction(stdlink, "List", yPixels)) {
		failWithMsgAndError(stdlink, "odmrScanArea:result:error: ");
		return;
	}
	for (int yPixel = 0; yPixel < yPixels; yPixel++) {
		if (!MLPutFunction(stdlink, "List", xPixels)) {
			failWithMsgAndError(stdlink, "odmrScanArea:result:error: ");
			return;
		}
		double y = ymin + yPixel * yStep;
		MCL_SingleWriteN(y, yAxis, stage);
		for (int xPixel = 0; xPixel < xPixels; xPixel++) {
			double x = xmin + xPixel * xStep;
			MCL_SingleWriteN(x, xAxis, stage);
			if (x == xmin) {
				if (y == ymin && iwt > 0)
					usleep((int) (iwt * 1000000));
				else if (btwt_us > 0)
					usleep(btwt_us);
			} else if (wt_us > 0)
				usleep(wt_us);
			if (countCounters(counter, counts) < NUM_COUNTERS) {
				failWithMsg(stdlink, "odmrScanArea:count:error: ");
				return;
			}
			if (!MLPutInteger32List(stdlink, counts, NUM_COUNTERS)) {
				failWithMsgAndError(stdlink, "odmrScanArea:result2:error: ");
				return;
			}
		}
	}
}
void odmrScanAreaOpt(int stage, int counter, int xAxis, double xmin,
		double xmax, int xPixels, int yAxis, double ymin, double ymax,
		int yPixels, double z, double iwt, double btwt, double wt, double ct) {
	if (xPixels < 2 || yPixels < 2) {
		MLPutFunction(stdlink, "$Failed", 1);
		MLPutString(stdlink, "nPixelsTooLow");
		return;
	}
	if (xAxis == yAxis) {
		MLPutFunction(stdlink, "$Failed", 1);
		MLPutString(stdlink, "nullArea");
		return;
	}
	int zAxis;
	for (zAxis = 1; zAxis <= 3; zAxis++)
		if (zAxis != xAxis && zAxis != yAxis)
			break;
	int btwt_us = (int) (btwt * 1000000);
	int wt_us = (int) (wt * 1000000);
	int ct_us = (int) (ct * 1000000);
	int counts[NUM_COUNTERS];
	MCL_SingleWriteN(z, zAxis, stage);
	double xStep = (xmax - xmin) / (xPixels - 1), yStep = (ymax - ymin)
			/ (yPixels - 1);
	if (!MLPutFunction(stdlink, "List", yPixels)) {
		failWithMsgAndError(stdlink, "odmrScanAreaOpt:result:error: ");
		return;
	}
	struct singleAxisMoveArgument ma = { stage, xAxis, 0 };
	struct twoAxesMoveArgument tma = { stage, { xAxis, yAxis }, { xmin, 0 } };
	for (int yPixel = 0; yPixel < yPixels; yPixel++) {
		if (!MLPutFunction(stdlink, "List", xPixels)) {
			failWithMsgAndError(stdlink, "odmrScanAreaOpt:result:error: ");
			return;
		}
		if (yPixel == 0 && iwt > 0) {
			MCL_SingleWriteN(ymin, yAxis, stage);
			MCL_SingleWriteN(xmin, xAxis, stage);
			if (iwt > 0)
				usleep((int) (iwt * 1000000));
		}
		for (int xPixel = 1; xPixel < xPixels; xPixel++) {
			ma.value = xmin + xPixel * xStep;
			if (countCountersAndMoveOn(counter, counts, ct_us,
					singleAxisMoveFunction, &ma, wt_us) < NUM_COUNTERS) {
				failWithMsg(stdlink, "odmrScanAreaOpt:countAndMoveOn:error: ");
				return;
			}
			if (!MLPutInteger32List(stdlink, counts, NUM_COUNTERS)) {
				failWithMsgAndError(stdlink, "odmrScanAreaOpt:result2:error: ");
				return;
			}
		}
		if (yPixel < yPixels - 1) {
			tma.values[1] = ymin + yPixel * yStep;
			if (countCountersAndMoveOn(counter, counts, ct_us,
					twoAxesMoveFunction, &tma, btwt_us) < NUM_COUNTERS) {
				failWithMsg(stdlink, "odmrScanAreaOpt:countAndMoveOn2:error: ");
				return;
			}
		} else if (countCounters(counter, counts) < NUM_COUNTERS) {
			failWithMsg(stdlink, "odmrScanArea:count:error: ");
			return;
		}
		if (!MLPutInteger32List(stdlink, counts, NUM_COUNTERS)) {
			failWithMsgAndError(stdlink, "odmrScanAreaOpt:result2:error: ");
			return;
		}
	}
}
void odmrScanLine(int stage, int counter, double *startPoint,
		long startPointLength, double *endPoint, long endPointLength,
		int nPixels, double iwt, double wt) {
	if (startPointLength != 3) {
		failWithMsg(stdlink, "odmrScanLine:startPoint:wrongDimension");
		return;
	}
	if (endPointLength != 3) {
		failWithMsg(stdlink, "odmrScanLine:endPoint:wrongDimension");
		return;
	}
	if (nPixels < 2) {
		MLPutFunction(stdlink, "$Failed", 1);
		MLPutString(stdlink, "nPixelsTooLow");
		return;
	}
	int wt_us = (int) (wt * 1000000);
	int counts[NUM_COUNTERS];
	double step[3];
	for (int i = 0; i < 3; i++)
		step[i] = (endPoint[i] - startPoint[i]) / (nPixels - 1);
	if (!MLPutFunction(stdlink, "List", nPixels)) {
		failWithMsgAndError(stdlink, "odmrScanLine:result:error: ");
		return;
	}
	for (int pixel = 0; pixel < nPixels; pixel++) {
		for (int i = 0; i < 3; i++)
			MCL_SingleWriteN(startPoint[i] + pixel * step[i], i + 1, stage);
		if (pixel == 0) {
			if (iwt > 0)
				usleep((int) (iwt * 1000000));
		} else if (wt_us > 0)
			usleep(wt_us);
		if (countCounters(counter, counts) < NUM_COUNTERS) {
			failWithMsg(stdlink, "odmrScanLine:count:error: ");
			return;
		}
		if (!MLPutInteger32List(stdlink, counts, NUM_COUNTERS)) {
			failWithMsgAndError(stdlink, "odmrScanLine:result2:error: ");
			return;
		}
	}
}
void odmrScanLineOpt(int stage, int counter, double *startPoint,
		long startPointLength, double *endPoint, long endPointLength,
		int nPixels, double iwt, double wt, double ct) {
	if (startPointLength != 3) {
		failWithMsg(stdlink, "odmrScanLineOpt:startPoint:wrongDimension");
		return;
	}
	if (endPointLength != 3) {
		failWithMsg(stdlink, "odmrScanLineOpt:endPoint:wrongDimension");
		return;
	}
	if (nPixels < 2) {
		MLPutFunction(stdlink, "$Failed", 1);
		MLPutString(stdlink, "nPixelsTooLow");
		return;
	}
	int wt_us = (int) (wt * 1000000);
	int ct_us = (int) (ct * 1000000);
	int counts[NUM_COUNTERS];
	double step[3];
	for (int i = 0; i < 3; i++)
		step[i] = (endPoint[i] - startPoint[i]) / (nPixels - 1);
	if (!MLPutFunction(stdlink, "List", nPixels)) {
		failWithMsgAndError(stdlink, "odmrScanLineOpt:result:error: ");
		return;
	}
	struct threeAxesMoveArgument ma = { stage, { 0, 0, 0 } };
	for (int i = 0; i < 3; i++)
		MCL_SingleWriteN(startPoint[i], i + 1, stage);
	if (iwt > 0)
		usleep((int) (iwt * 1000000));
	for (int pixel = 1; pixel < nPixels; pixel++) {
		for (int i = 0; i < 3; i++)
			ma.values[i] = startPoint[i] + pixel * step[i];
		if (countCountersAndMoveOn(counter, counts, ct_us,
				threeAxesMoveFunction, &ma, wt_us) < NUM_COUNTERS) {
			failWithMsg(stdlink, "odmrScanLineOpt:countAndMoveOn:error: ");
			return;
		}
		if (!MLPutInteger32List(stdlink, counts, NUM_COUNTERS)) {
			failWithMsgAndError(stdlink, "odmrScanLineOpt:result2:error: ");
			return;
		}
	}
	if (countCounters(counter, counts) < NUM_COUNTERS) {
		failWithMsg(stdlink, "odmrScanLineOpt:count:error: ");
		return;
	}
	if (!MLPutInteger32List(stdlink, counts, NUM_COUNTERS)) {
		failWithMsgAndError(stdlink, "odmrScanLineOpt:result3:error: ");
		return;
	}
}
void odmrScanPoints(int stage, int counter, double iwt, double wt) {
	char **pointListHeads;
	double *pointListData;
	int *pointListDims;
	int pointListDepth;
	if (!MLGetReal64Array(stdlink, &pointListData, &pointListDims,
			&pointListHeads, &pointListDepth)) {
		MLPutFunction(stdlink, "$Failed", 1);
		MLPutString(stdlink, "could not get pointList");
		return;
	}
	if (pointListDepth != 2 || pointListDims[1] != 3) {
		MLReleaseReal64Array(stdlink, pointListData, pointListDims,
				pointListHeads, pointListDepth);
		MLPutFunction(stdlink, "$Failed", 1);
		MLPutString(stdlink, "pointList has wrong format");
		return;
	}
	int wt_us = (int) (wt * 1000000);
	int counts[NUM_COUNTERS];
	if (!MLPutFunction(stdlink, "List", pointListDims[0])) {
		failWithMsgAndError(stdlink, "odmrScanPoints:result:error: ");
		return;
	}
	for (int point = 0; point < pointListDims[0]; point++) {
		for (int axis = 1; axis <= 3; axis++)
			MCL_SingleWriteN(pointListData[point * 3 + axis - 1], axis, stage);
		if (point == 0) {
			if (iwt > 0)
				usleep((int) (iwt * 1000000));
		} else if (wt_us > 0)
			usleep(wt_us);
		if (countCounters(counter, counts) < NUM_COUNTERS) {
			failWithMsg(stdlink, "odmrScanPoints:count:error: ");
			return;
		}
		if (!MLPutInteger32List(stdlink, counts, NUM_COUNTERS)) {
			failWithMsgAndError(stdlink, "odmrScanPoints:result2:error: ");
			return;
		}
	}
}
void odmrScanPointsOpt(int stage, int counter, double iwt, double wt, double ct) {
	char **pointListHeads;
	double *pointListData;
	int *pointListDims;
	int pointListDepth;
	if (!MLGetReal64Array(stdlink, &pointListData, &pointListDims,
			&pointListHeads, &pointListDepth)) {
		MLPutFunction(stdlink, "$Failed", 1);
		MLPutString(stdlink, "could not get pointList");
		return;
	}
	if (pointListDepth != 2 || pointListDims[1] != 3) {
		MLReleaseReal64Array(stdlink, pointListData, pointListDims,
				pointListHeads, pointListDepth);
		MLPutFunction(stdlink, "$Failed", 1);
		MLPutString(stdlink, "pointList has wrong format");
		return;
	}
	int wt_us = (int) (wt * 1000000);
	int ct_us = (int) (ct * 1000000);
	int counts[NUM_COUNTERS];
	if (!MLPutFunction(stdlink, "List", pointListDims[0])) {
		failWithMsgAndError(stdlink, "odmrScanPointsOpt:result:error: ");
		return;
	}
	struct threeAxesMoveArgument ma = { stage, { 0, 0, 0 } };
	for (int axis = 1; axis <= 3; axis++)
		MCL_SingleWriteN(pointListData[axis - 1], axis, stage);
	if (iwt > 0)
		usleep((int) (iwt * 1000000));
	for (int point = 1; point < pointListDims[0]; point++) {
		for (int axis = 1; axis <= 3; axis++)
			ma.values[axis - 1] = pointListData[point * 3 + axis - 1];
		if (countCountersAndMoveOn(counter, counts, ct_us,
				threeAxesMoveFunction, &ma, wt_us) < NUM_COUNTERS) {
			failWithMsg(stdlink, "odmrScanLineOpt:countAndMoveOn:error: ");
			return;
		}
		if (!MLPutInteger32List(stdlink, counts, NUM_COUNTERS)) {
			failWithMsgAndError(stdlink, "odmrScanLineOpt:result2:error: ");
			return;
		}
	}
	if (countCounters(counter, counts) < NUM_COUNTERS) {
		failWithMsg(stdlink, "odmrScanLineOpt:count:error: ");
		return;
	}
	if (!MLPutInteger32List(stdlink, counts, NUM_COUNTERS)) {
		failWithMsgAndError(stdlink, "odmrScanLineOpt:result3:error: ");
		return;
	}

}
void odmrGatherTrace(int counter, int numSamples) {
	if (numSamples < 1) {
		MLPutFunction(stdlink, "List", 0);
		return;
	}
	int counts[NUM_COUNTERS];
	if (!MLPutFunction(stdlink, "List", numSamples)) {
		failWithMsgAndError(stdlink, "odmrGatherTrace:result:error: ");
		return;
	}
	for (int i = 0; i < numSamples; i++) {
		if (countCounters(counter, counts) < NUM_COUNTERS) {
			failWithMsg(stdlink, "odmrGatherTrace:count:error: ");
			return;
		}
		if (!MLPutInteger32List(stdlink, counts, NUM_COUNTERS)) {
			failWithMsgAndError(stdlink, "odmrGatherTrace:result2:error: ");
			return;
		}
	}
}
int chooseCounterValue(int counts[NUM_COUNTERS], int counterId) {
	if (counterId == 0) {
		int r = counts[0];
		for (int i = 1; i < NUM_COUNTERS; i++)
			r += counts[i];
		return r;
	} else if (counterId < 0) {
		int r = counts[0];
		for (int i = 1; i < NUM_COUNTERS; i++)
			if (counts[i] < r)
				r = counts[i];
		return r;
	} else if (counterId > NUM_COUNTERS) {
		int r = counts[0];
		for (int i = 1; i < NUM_COUNTERS; i++)
			if (counts[i] > r)
				r = counts[i];
		return r;
	} else
		return counts[counterId - 1];
}
int countSingleCounter(int counter, int counterId) {
	int counts[NUM_COUNTERS];
	if (countCounters(counter, counts) < NUM_COUNTERS)
		return -1;
	return chooseCounterValue(counts, counterId);
}
void odmrTrackMaximum(int stage, int counter, int counterId, double *x0,
		long x0Length, double *stepSizes, long stepSizesLength, double st,
		double trackingFactor, int numSteps) {
	if (numSteps < 1) {
		MLPutFunction(stdlink, "List", 0);
		return;
	}
	int st_us = (int) (st * 1000000);
	if (x0Length != 3) {
		failWithMsg(stdlink, "odmrTrackMaximum:x0:wrongDimension");
		return;
	}
	if (stepSizesLength != 3) {
		failWithMsg(stdlink, "odmrTrackMaximum:stepSizesLength:wrongDimension");
		return;
	}
	if (!MLPutFunction(stdlink, "List", numSteps)) {
		failWithMsgAndError(stdlink, "odmrTrackMaximum:result:error: ");
		return;
	}
	int m;
	int s[3][2];
	for (int i = 0; i < numSteps; i++) {
		for (int axis = 0; axis < 3; axis++)
			MCL_SingleWriteN(x0[axis], axis + 1, stage);
		if (st_us > 0)
			usleep(st_us);
		if ((m = countSingleCounter(counter, counterId)) < 0) {
			failWithMsg(stdlink, "odmrTrackMaximum:count:error: ");
			return;
		}
		for (int axis = 0; axis < 3; axis++)
			if (stepSizes[axis] > 0)
				for (int d = -1; d <= 1; d += 2) {
					MCL_SingleWriteN(x0[axis] + d * stepSizes[axis], axis + 1,
							stage);
					if (st_us > 0)
						usleep(st_us);
					if ((s[axis][(1 - d) / 2] = countSingleCounter(counter,
							counterId)) < 0) {
						failWithMsg(stdlink, "odmrTrackMaximum:count:error: ");
						return;
					}
					MCL_SingleWriteN(x0[axis], axis + 1, stage);
				}
		for (int axis = 0; axis < 3; axis++)
			if (stepSizes[axis] > 0)
				for (int d = -1; d <= 1; d += 2)
					if (trackingFactor * m < s[axis][(1 - d) / 2]) {
						m = s[axis][(1 - d) / 2];
						x0[axis] += d * stepSizes[axis];
					}
		if (!MLPutFunction(stdlink, "List", 2)) {
			failWithMsgAndError(stdlink, "odmrTrackMaximum:result2:error: ");
			return;
		}
		if (!MLPutReal64List(stdlink, x0, 3)) {
			failWithMsgAndError(stdlink, "odmrTrackMaximum:result3:error: ");
			return;
		}
		if (!MLPutInteger32(stdlink, m)) {
			failWithMsgAndError(stdlink, "odmrTrackMaximum:result4:error: ");
			return;
		}
	}
	for (int axis = 0; axis < 3; axis++)
		MCL_SingleWriteN(x0[axis], axis + 1, stage);
}

struct PhotonCount {
	struct timespec time;
	int counts[NUM_COUNTERS];
};
struct PhotonStreamerThreadParams {
	pthread_t thread;
	int mayRun;
	int isRunning;
	int counter;
	int bufferSize;
	int bufferIndex;
	int bufferWrapped;
	pthread_mutex_t bufferLock;
	struct PhotonCount *buffer;
	struct PhotonCount *spareBuffer;
//	struct timespec startTime;
};

#define clockdiffToDouble(a, b)					\
	((a).tv_sec - (b).tv_sec) +					\
	((a).tv_nsec - (b).tv_nsec)*1.e-9
#define timespecToDouble(a)						\
	((a).tv_sec) + ((a).tv_nsec)*1.e-9
#define clockdiffToInt(a, b)					\
	((a).tv_sec - (b).tv_sec)*1000000000ll +	\
	((a).tv_nsec - (b).tv_nsec)
#define timespecToInt(a)						\
	((a).tv_sec)*1000000000ll + ((a).tv_nsec)

static volatile struct PhotonStreamerThreadParams globalPhotonStreamerThreadParams =
		{ 0, 0, 0, 0, 0, 0, 0, PTHREAD_MUTEX_INITIALIZER, NULL, NULL /*, { 0, 0 } */};

int writeBufferedCountsToBuffer(char buf[4 * NUM_COUNTERS], struct timespec t,
		struct PhotonStreamerThreadParams *photonStreamerThreadParams) {
	pthread_mutex_lock(&photonStreamerThreadParams->bufferLock);
	if (photonStreamerThreadParams->buffer == NULL) {
		photonStreamerThreadParams->buffer = calloc(
				photonStreamerThreadParams->bufferSize,
				sizeof(struct PhotonCount));
		photonStreamerThreadParams->bufferIndex = 0;
	}
	if (photonStreamerThreadParams->buffer == NULL) {
		pthread_mutex_unlock(&photonStreamerThreadParams->bufferLock);
		return 0;
	}
	photonStreamerThreadParams->buffer[photonStreamerThreadParams->bufferIndex].time =
			t;
	for (int i = 0; i < NUM_COUNTERS; i++)
		photonStreamerThreadParams->buffer[photonStreamerThreadParams->bufferIndex].counts[i] =
				*(int*) (buf + i * 4);
	photonStreamerThreadParams->bufferIndex++;
	if (photonStreamerThreadParams->bufferIndex
			>= photonStreamerThreadParams->bufferSize) {
		photonStreamerThreadParams->bufferWrapped = 1;
		photonStreamerThreadParams->bufferIndex = 0;
	}
	pthread_mutex_unlock(&photonStreamerThreadParams->bufferLock);
	return 1;
}
void *odmrPhotonStreamerThread(void *args) {
	struct PhotonStreamerThreadParams *photonStreamerThreadParams =
			(struct PhotonStreamerThreadParams*) args;
	struct timespec t;
	char buf[4 * NUM_COUNTERS];
	//clock_gettime(CLOCK_MONOTONIC, &photonStreamerThreadParams->startTime);
	if (!photonStreamerThreadParams->mayRun
			|| photonStreamerThreadParams->bufferSize <= 0)
		return NULL;
	photonStreamerThreadParams->isRunning = 1;
	int countsRead = 0;
	while (photonStreamerThreadParams->mayRun) {
		clock_gettime(CLOCK_MONOTONIC, &t);
		if (write(photonStreamerThreadParams->counter, "C", 1) < 1) {
			photonStreamerThreadParams->isRunning = 0;
			return NULL;
		}
		if (countsRead
				&& !writeBufferedCountsToBuffer(buf, t,
						photonStreamerThreadParams)) {
			photonStreamerThreadParams->isRunning = 0;
			return NULL;
		}
		int l = 0;
		while (l < 4 * NUM_COUNTERS) {
			int s = read(photonStreamerThreadParams->counter, buf + l,
					4 * NUM_COUNTERS - l);
			if (s < 0) {
				photonStreamerThreadParams->isRunning = 0;
				return NULL;
			}
			l += s;
		}
		countsRead = 1;
	}
	if (countsRead)
		writeBufferedCountsToBuffer(buf, t, photonStreamerThreadParams);
	photonStreamerThreadParams->isRunning = 0;
	return NULL;
}

void odmrStartPhotonStreamerThread(int counter, int bufferSize) {
	if (globalPhotonStreamerThreadParams.thread) {
		if (!MLPutFunction(stdlink, "$Failed", 1)
				|| !MLPutString(stdlink, "streamer still running"))
			failWithMsgAndError(stdlink,
					"odmrStartPhotonStreamerThread:streamer_still_running:error: ");
		return;
	}
	int r;
	globalPhotonStreamerThreadParams.mayRun = 1;
	globalPhotonStreamerThreadParams.counter = counter;
	globalPhotonStreamerThreadParams.bufferSize = bufferSize;
	if (globalPhotonStreamerThreadParams.buffer != NULL) {
		free(globalPhotonStreamerThreadParams.buffer);
		globalPhotonStreamerThreadParams.buffer = NULL;
	}
	if (globalPhotonStreamerThreadParams.spareBuffer != NULL) {
		free(globalPhotonStreamerThreadParams.spareBuffer);
		globalPhotonStreamerThreadParams.spareBuffer = NULL;
	}
//	globalPhotonStreamerThreadParams.startTime.tv_sec = 0;
//	globalPhotonStreamerThreadParams.startTime.tv_nsec = 0;
	r = pthread_create(&globalPhotonStreamerThreadParams.thread, NULL,
			odmrPhotonStreamerThread,
			(void*) &globalPhotonStreamerThreadParams);
	if (r) {
		globalPhotonStreamerThreadParams.thread = 0;
		if (!MLPutFunction(stdlink, "$Failed", 2)
				|| !MLPutString(stdlink, "create_thread failed")
				|| !MLPutInteger(stdlink, r))
			failWithMsgAndError(stdlink,
					"odmrStartPhotonStreamerThread:create_thread_failed:error: ");
	} else {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"odmrStartPhotonStreamerThread:return:error: ");
	}
}

void odmrStopPhotonStreamerThread(double waitTime) {
	if (!globalPhotonStreamerThreadParams.thread) {
		if (!MLPutFunction(stdlink, "$Failed", 1)
				|| !MLPutString(stdlink, "streamer not running"))
			failWithMsgAndError(stdlink,
					"odmrStopPhotonStreamerThread:streamer_not_running:error: ");
		return;
	}
	globalPhotonStreamerThreadParams.mayRun = 0;
	if (globalPhotonStreamerThreadParams.isRunning) {
		if (waitTime != 0) {
			if (waitTime > 0) {
				struct timespec t1, t2;
				clock_gettime(CLOCK_MONOTONIC, &t1);
				while (globalPhotonStreamerThreadParams.isRunning) {
					clock_gettime(CLOCK_MONOTONIC, &t2);
					if (clockdiffToDouble(t2, t1) >= waitTime)
						break;
					sched_yield();
				}
			}
			if (globalPhotonStreamerThreadParams.isRunning) {
				int r = pthread_cancel(globalPhotonStreamerThreadParams.thread);
				if (r) {
					if (!MLPutFunction(stdlink, "$Failed", 2)
							|| !MLPutString(stdlink, "cancel_thread failed")
							|| !MLPutInteger(stdlink, r)) {
						failWithMsgAndError(stdlink,
								"odmrStopPhotonStreamerThread:cancel_thread_failed:error: ");
						return;
					}
				} else {
					globalPhotonStreamerThreadParams.thread = 0;
					globalPhotonStreamerThreadParams.isRunning = 0;
					if (!MLPutFunction(stdlink, "$Canceled", 1)) {
						failWithMsgAndError(stdlink,
								"odmrStopPhotonStreamerThread:cancel_thread_failed:return:error: ");
						return;
					}
				}
			}
		} else {
			int r = pthread_join(globalPhotonStreamerThreadParams.thread, NULL);
			if (r) {
				if (!MLPutFunction(stdlink, "$Failed", 2)
						|| !MLPutString(stdlink, "join_thread failed")
						|| !MLPutInteger(stdlink, r)) {
					failWithMsgAndError(stdlink,
							"odmrStopPhotonStreamerThread:join_thread_failed:error: ");
					return;
				}
			} else {
				globalPhotonStreamerThreadParams.thread = 0;
				globalPhotonStreamerThreadParams.isRunning = 0;
			}
		}
	}
	if (globalPhotonStreamerThreadParams.buffer != NULL
			&& (globalPhotonStreamerThreadParams.bufferIndex > 0
					|| globalPhotonStreamerThreadParams.bufferWrapped)) {
		int transferSize =
				globalPhotonStreamerThreadParams.bufferWrapped ?
						globalPhotonStreamerThreadParams.bufferSize :
						globalPhotonStreamerThreadParams.bufferIndex;
		if (!MLPutFunction(stdlink, "List", transferSize)) {
			failWithMsgAndError(stdlink,
					"odmrStopPhotonStreamerThread:return:list:error: ");
			return;
		}
		for (int i = 0; i < transferSize; i++) {
			if (!MLPutFunction(stdlink, "List", 2) || !MLPutReal(stdlink,
			/*clockdiffToDouble(
			 globalPhotonStreamerThreadParams.buffer[i].time,
			 globalPhotonStreamerThreadParams.startTime)*/
			timespecToDouble(globalPhotonStreamerThreadParams.buffer[i].time))
					|| !MLPutIntegerList(stdlink,
							globalPhotonStreamerThreadParams.buffer[i].counts,
							NUM_COUNTERS)) {
				failWithMsgAndError(stdlink,
						"odmrStopPhotonStreamerThread:return:list:element:error: ");
				return;
			}
		}
	} else if (!MLPutFunction(stdlink, "List", 0))
		failWithMsgAndError(stdlink, "odmrFetchPhotons:return:error: ");
}
void odmrPhotonStreamerThreadRunning(void) {
	if (!MLPutSymbol(stdlink,
			globalPhotonStreamerThreadParams.thread
					&& globalPhotonStreamerThreadParams.isRunning ?
					"True" : "False"))
		failWithMsgAndError(stdlink,
				"odmrPhotonStreamerThreadRunning:return:error: ");
}
void odmrFetchPhotons(void) {
	if (globalPhotonStreamerThreadParams.thread
			&& globalPhotonStreamerThreadParams.isRunning) {
		pthread_mutex_lock(&globalPhotonStreamerThreadParams.bufferLock);
		struct PhotonCount *temp = globalPhotonStreamerThreadParams.buffer;
		int bufferIndex = globalPhotonStreamerThreadParams.bufferIndex;
		int bufferWrapped = globalPhotonStreamerThreadParams.bufferWrapped;
//	struct timespec startTime = globalPhotonStreamerThreadParams.startTime;
		globalPhotonStreamerThreadParams.buffer =
				globalPhotonStreamerThreadParams.spareBuffer;
		globalPhotonStreamerThreadParams.spareBuffer = temp;
		globalPhotonStreamerThreadParams.bufferIndex = 0;
		globalPhotonStreamerThreadParams.bufferWrapped = 0;
		pthread_mutex_unlock(&globalPhotonStreamerThreadParams.bufferLock);
		if (temp != NULL && (bufferIndex > 0 || bufferWrapped)) {
			int transferSize =
					bufferWrapped ?
							globalPhotonStreamerThreadParams.bufferSize :
							bufferIndex;
			if (!MLPutFunction(stdlink, "List", transferSize)) {
				failWithMsgAndError(stdlink,
						"odmrFetchPhotons:return:list:error: ");
				return;
			}
			for (int i = 0; i < transferSize; i++) {
				if (!MLPutFunction(stdlink, "List", 2)
						|| !MLPutReal(stdlink, timespecToDouble(temp[i].time)
						/*clockdiffToDouble(temp[i].time, startTime)*/)
						|| !MLPutIntegerList(stdlink, temp[i].counts,
						NUM_COUNTERS)) {
					failWithMsgAndError(stdlink,
							"odmrFetchPhotons:return:list:element:error: ");
					return;
				}
			}
		} else if (!MLPutFunction(stdlink, "List", 0))
			failWithMsgAndError(stdlink, "odmrFetchPhotons:return:error: ");
	} else if (!MLPutFunction(stdlink, "List", 0))
		failWithMsgAndError(stdlink, "odmrFetchPhotons:return:error: ");
}
double odmrGetClock(void) {
	struct timespec t;
	clock_gettime(CLOCK_MONOTONIC, &t);
	return timespecToDouble(t);
}
double odmrGetClockResolution(void) {
	struct timespec t;
	clock_getres(CLOCK_MONOTONIC, &t);
	return timespecToDouble(t);
}
int main(int argc, char **argv) {
	sargc = argc;
	sargv = argv;
	return MLMain(argc, argv);
}
