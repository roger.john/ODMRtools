#include <asm/ioctls.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include "MathLinkCommon.h"
#include "usbtmc.h"

void usbtmcOpen(const char *dev) {
	int fd = open(dev, O_RDWR);
	if (fd == -1)
		failWithMsg(stdlink, "usbtmcOpen:openFailed");
	else if (!MLPutInteger(stdlink, fd))
		failWithMsgAndError(stdlink, "usbtmcOpen:return:error: ");
}
int usbtmcClose(int fd) {
	return close(fd);
}
void usbtmcGetTimeout(int fd) {
	struct usbtmc_attribute attr;
	attr.attribute = USBTMC_ATTRIB_TIMEOUT;
	int r = ioctl(fd, USBTMC_IOCTL_GET_ATTRIBUTE, &attr);
	if (r >= 0) {
		if (!MLPutFloat(stdlink, attr.value / 1000.f))
			failWithMsgAndError(stdlink, "usbtmcGetTimeout:return:error: ");
	} else
		failWithMsg(stdlink, "usbtmcGetTimeout:invalidIOctl");
}
void usbtmcSetTimeout(int fd, float timeout) {
	struct usbtmc_attribute attr;
	attr.attribute = USBTMC_ATTRIB_TIMEOUT;
	attr.value = (int) (1000 * timeout);
	int r = ioctl(fd, USBTMC_IOCTL_SET_ATTRIBUTE, &attr);
	if (r >= 0) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "usbtmcSetTimeout:return:error: ");
	} else
		failWithMsg(stdlink, "usbtmcSetTimeout:invalidIOctl");
}
void usbtmcCmd(int fd, const char *cmd, int maxToRead) {
	char *cmdLine = malloc(strlen(cmd) + 2);
	sprintf(cmdLine, "%s\n", cmd);
	int r = write(fd, cmdLine, strlen(cmd));
	free(cmdLine);
	if (r < 0) {
		MLPutFunction(stdlink, "$Failed", 1);
		MLPutString(stdlink, "writeFailed");
		return;
	}
	if (maxToRead < 1) {
		MLPutSymbol(stdlink, "Null");
		return;
	}
	unsigned char *buf = malloc(maxToRead);
	r = 0;
	do {
		if (MLAbort) {
			if (!MLPutFunction(stdlink, "$Aborted", 1)
					|| !MLPutByteString(stdlink, buf, r))
				failWithMsgAndError(stdlink, "usbtmcCmd:abort:error: ");
			free(buf);
			return;
		}
		int s = read(fd, buf + r, maxToRead - r);
		if (s < 0) {
			if (!MLPutFunction(stdlink, "$Failed", 2)
					|| !MLPutString(stdlink, "readFailed")
					|| !MLPutByteString(stdlink, buf, r))
				failWithMsgAndError(stdlink, "usbtmcCmd:readFailed:error: ");
			free(buf);
			return;
		}
		r += s;
	} while (r < maxToRead && (r < 1 || buf[r - 1] != '\n'));
	if (!MLPutByteString(stdlink, buf, r)) {
		free(buf);
		failWithMsgAndError(stdlink, "usbtmcCmd:result:error: ");
	}
	free(buf);
}
