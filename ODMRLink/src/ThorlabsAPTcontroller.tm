
int openThorlabsAPTcontroller(const char*);

:Begin:
:Function:       openThorlabsAPTcontroller
:Pattern:        ThorlabsAPTopen[dev_String]
:Arguments:      { dev }
:ArgumentTypes:  { String }
:ReturnType:     Integer
:End:

int closeThorlabsAPTcontroller(int);

:Begin:
:Function:       closeThorlabsAPTcontroller
:Pattern:        ThorlabsAPTclose[fd_Integer]
:Arguments:      { fd }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:

void getThorlabsHardwareInformation(int);

:Begin:
:Function:       getThorlabsHardwareInformation
:Pattern:        ThorlabsAPTgetHardwareInformation[fd_Integer]
:Arguments:      { fd }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void getThorlabsChannelEncoder(int,int);

:Begin:
:Function:       getThorlabsChannelEncoder
:Pattern:        ThorlabsAPTgetChannelEncoder[fd_Integer,channel_Integer]
:Arguments:      { fd, channel }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Manual
:End:

void getThorlabsChannelStatus(int,int);

:Begin:
:Function:       getThorlabsChannelStatus
:Pattern:        ThorlabsAPTgetChannelStatus[fd_Integer,channel_Integer]
:Arguments:      { fd, channel }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Manual
:End:

void readThorlabsResponse(int);

:Begin:
:Function:       readThorlabsResponse
:Pattern:        ThorlabsAPTreadResponse[fd_Integer]
:Arguments:      { fd}
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void moveThorlabsChannelSync(int,int,int);

:Begin:
:Function:       moveThorlabsChannelSync
:Pattern:        ThorlabsAPTmoveChannel[fd_Integer,channel_Integer,newPos_Integer]
:Arguments:      { fd, channel, newPos }
:ArgumentTypes:  { Integer, Integer, Integer }
:ReturnType:     Manual
:End:

void moveThorlabsChannelAsync(int,int,int);

:Begin:
:Function:       moveThorlabsChannelAsync
:Pattern:        ThorlabsAPTmoveChannelAsynchronously[fd_Integer,channel_Integer,newPos_Integer]
:Arguments:      { fd, channel, newPos }
:ArgumentTypes:  { Integer, Integer, Integer }
:ReturnType:     Manual
:End:

void homeThorlabsChannelSync(int,int);

:Begin:
:Function:       homeThorlabsChannelSync
:Pattern:        ThorlabsAPThomeChannel[fd_Integer,channel_Integer]
:Arguments:      { fd, channel }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Manual
:End:

void homeThorlabsChannelAsync(int,int);

:Begin:
:Function:       homeThorlabsChannelAsync
:Pattern:        ThorlabsAPThomeChannelAsynchronously[fd_Integer,channel_Integer]
:Arguments:      { fd, channel }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Manual
:End:
