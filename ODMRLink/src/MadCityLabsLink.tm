
int mclInitHandle(void);

:Begin:
:Function:       mclInitHandle
:Pattern:        MCLInitHandle[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Integer
:End:

int mclInitHandleOrGetExisting(void);

:Begin:
:Function:       mclInitHandle
:Pattern:        MCLInitHandleOrGetExisting[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Integer
:End:

int mclGrabHandle(int type);

:Begin:
:Function:       mclGrabHandle
:Pattern:        MCLGrabHandle[type_Integer]
:Arguments:      { type }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:

int mclGrabHandleOrGetExisting(int type);

:Begin:
:Function:       mclGrabHandleOrGetExisting
:Pattern:        MCLGrabHandleOrGetExisting[type_Integer]
:Arguments:      { type }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:

int mclGrabAllHandles(void);

:Begin:
:Function:       mclGrabAllHandles
:Pattern:        MCLGrabAllHandles[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Integer
:End:

void mclGetAllHandles(void);

:Begin:
:Function:       mclGetAllHandles
:Pattern:        MCLGetAllHandles[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

int mclNumberOfCurrentHandles(void);

:Begin:
:Function:       mclNumberOfCurrentHandles
:Pattern:        MCLNumberOfCurrentHandles[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Integer
:End:

int mclGetHandleBySerial(int serial);

:Begin:
:Function:       mclGetHandleBySerial
:Pattern:        MCLGetHandleBySerial[type_Integer]
:Arguments:      { type }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:

void mclReleaseHandle(int);

:Begin:
:Function:       mclReleaseHandle
:Pattern:        MCLReleaseHandle[handle_Integer]
:Arguments:      { handle }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void mclReleaseAllHandles(void);

:Begin:
:Function:       mclReleaseAllHandles
:Pattern:        MCLReleaseAllHandles[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

void mclDeviceAttached(int, int);

:Begin:
:Function:       mclDeviceAttached
:Pattern:        MCLDeviceAttached[milliseconds_Integer?NonNegative,handle_Integer]
:Arguments:      { milliseconds, handle }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Manual
:End:

double mclGetCalibration(int, int);

:Begin:
:Function:       mclGetCalibration
:Pattern:        MCLGetCalibration[axis_Integer?Positive,handle_Integer]
:Arguments:      { axis, handle }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Real
:End:

void mclGetCalibrationXYZ(int);

:Begin:
:Function:       mclGetCalibrationXYZ
:Pattern:        MCLGetCalibrationXYZ[handle_Integer]
:Arguments:      { handle }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void mclGetFirmwareVersion(int);

:Begin:
:Function:       mclGetFirmwareVersion
:Pattern:        MCLGetFirmwareVersion[handle_Integer]
:Arguments:      { handle }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void mclPrintDeviceInfo(int);

:Begin:
:Function:       mclPrintDeviceInfo
:Pattern:        MCLPrintDeviceInfo[handle_Integer]
:Arguments:      { handle }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

int mclGetSerialNumber(int);

:Begin:
:Function:       mclGetSerialNumber
:Pattern:        MCLGetSerialNumber[handle_Integer]
:Arguments:      { handle }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:

void mclGetProductInfo(int);

:Begin:
:Function:       mclGetProductInfo
:Pattern:        MCLGetProductInfo[handle_Integer]
:Arguments:      { handle }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void mclCorrectDriverVersion(void);

:Begin:
:Function:       mclCorrectDriverVersion
:Pattern:        MCLCorrectDriverVersion[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

void mclGetCommandedPosition(int);

:Begin:
:Function:       mclGetCommandedPosition
:Pattern:        MCLGetCommandedPosition[handle_Integer]
:Arguments:      { handle }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void mclSingleReadN(int, int);

:Begin:
:Function:       mclSingleReadN
:Pattern:        MCLSingleReadN[axis_Integer?Positive,handle_Integer]
:Arguments:      { axis, handle }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Manual
:End:

void mclSingleWriteN(double, int, int);

:Begin:
:Function:       mclSingleWriteN
:Pattern:        MCLSingleWriteN[position_Real, axis_Integer?Positive, handle_Integer]
:Arguments:      { position, axis, handle }
:ArgumentTypes:  { Real, Integer, Integer }
:ReturnType:     Manual
:End:

void mclMonitorN(double, int, int);

:Begin:
:Function:       mclMonitorN
:Pattern:        MCLMonitorN[position_Real, axis_Integer?Positive, handle_Integer]
:Arguments:      { position, axis, handle }
:ArgumentTypes:  { Real, Integer, Integer }
:ReturnType:     Manual
:End:

void mclSingleReadZ(int);

:Begin:
:Function:       mclSingleReadZ
:Pattern:        MCLSingleReadZ[handle_Integer]
:Arguments:      { handle }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void mclSingleWriteZ(double, int);

:Begin:
:Function:       mclSingleWriteZ
:Pattern:        MCLSingleWriteZ[position_Real, handle_Integer]
:Arguments:      { position, handle }
:ArgumentTypes:  { Real, Integer }
:ReturnType:     Manual
:End:

void mclMonitorZ(double, int);

:Begin:
:Function:       mclMonitorZ
:Pattern:        MCLMonitorZ[position_Real, handle_Integer]
:Arguments:      { position, handle }
:ArgumentTypes:  { Real, Integer }
:ReturnType:     Manual
:End:

void mclSingleReadXYZ(int);

:Begin:
:Function:       mclSingleReadXYZ
:Pattern:        MCLSingleReadXYZ[handle_Integer]
:Arguments:      { handle }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void mclSingleWriteXYZ(double*, long, int);

:Begin:
:Function:       mclSingleWriteXYZ
:Pattern:        MCLSingleWriteXYZ[pos_List/;Length[pos]==3&&And@@(NumberQ/@pos),
					 handle_Integer]
:Arguments:      { pos, handle }
:ArgumentTypes:  { RealList, Integer }
:ReturnType:     Manual
:End:

void mclMonitorXYZ(double*, long, int);

:Begin:
:Function:       mclMonitorXYZ
:Pattern:        MCLMonitorXYZ[pos_List/;Length[pos]==3&&And@@(NumberQ/@pos),
					handle_Integer]
:Arguments:      { pos, handle }
:ArgumentTypes:  { RealList, Integer }
:ReturnType:     Manual
:End:

void mclReadWaveFormN(int, int, double, int);

:Begin:
:Function:       mclReadWaveFormN
:Pattern:        MCLReadWaveFormN[axis_Integer?Positive, length_Integer?NonNegative,
					samplePeriodMilliseconds_Real?Positive, handle_Integer]
:Arguments:      { axis, length, samplePeriodMilliseconds, handle }
:ArgumentTypes:  { Integer, Integer, Real, Integer }
:ReturnType:     Manual
:End:

void mclSetupReadWaveFormN(int, int, double, int);

:Begin:
:Function:       mclSetupReadWaveFormN
:Pattern:        MCLSetupReadWaveFormN[axis_Integer?Positive,
					length_Integer?NonNegative,	samplePeriodMilliseconds_Real?Positive,
					handle_Integer]
:Arguments:      { axis, length, samplePeriodMilliseconds, handle }
:ArgumentTypes:  { Integer, Integer, Real, Integer }
:ReturnType:     Manual
:End:

void mclTriggerReadWaveFormN(int, int, int);

:Begin:
:Function:       mclTriggerReadWaveFormN
:Pattern:        MCLTriggerReadWaveFormN[axis_Integer?Positive,
					length_Integer?NonNegative,	handle_Integer]
:Arguments:      { axis, length, handle }
:ArgumentTypes:  { Integer, Integer, Integer }
:ReturnType:     Manual
:End:

void mclLoadWaveFormN(int, double, double*, long, int);

:Begin:
:Function:       mclLoadWaveFormN
:Pattern:        MCLLoadWaveFormN[axis_Integer?Positive,
					samplePeriodMilliseconds_Real?Positive, data_List, handle_Integer]
:Arguments:      { axis, samplePeriodMilliseconds, data, handle }
:ArgumentTypes:  { Integer, Real, RealList, Integer }
:ReturnType:     Manual
:End:

void mclSetupLoadWaveFormN(int, double, double*, long, int);

:Begin:
:Function:       mclSetupLoadWaveFormN
:Pattern:        MCLSetupLoadWaveFormN[axis_Integer?Positive,
					samplePeriodMilliseconds_Real?Positive, data_List, handle_Integer]
:Arguments:      { axis, samplePeriodMilliseconds, data, handle }
:ArgumentTypes:  { Integer, Real, RealList, Integer }
:ReturnType:     Manual
:End:

void mclTriggerLoadWaveFormN(int, int);

:Begin:
:Function:       mclTriggerLoadWaveFormN
:Pattern:        MCLTriggerLoadWaveFormN[axis_Integer?Positive,	handle_Integer]
:Arguments:      { axis, handle }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Manual
:End:

void mclTriggerWaveFormAcquisition(int, int, int);

:Begin:
:Function:       mclTriggerWaveFormAcquisition
:Pattern:        MCLTriggerWaveFormAcquisition[axis_Integer?Positive, length_Integer?NonNegative,
					handle_Integer]
:Arguments:      { axis, length, handle }
:ArgumentTypes:  { Integer, Integer, Integer }
:ReturnType:     Manual
:End:

void mclMultiAxisWaveFormSetup(double*, long, double*, long, double*, long,
	double,	int, int);

:Begin:
:Function:       mclMultiAxisWaveFormSetup
:Pattern:        MCLMultiAxisWaveFormSetup[xData_List, yData_List, zData_List,
					samplePeriodMilliseconds_Real?Positive, iterations_Integer,
					handle_Integer]
:Arguments:      { xData, yData, zData, samplePeriodMilliseconds, iterations, handle }
:ArgumentTypes:  {  RealList, RealList, RealList, Real, Integer, Integer }
:ReturnType:     Manual
:End:

void mclMultiAxisWaveFormTriggerAndRead(int, int, int, int);

:Begin:
:Function:       mclMultiAxisWaveFormTriggerAndRead
:Pattern:        MCLMultiAxisWaveFormTriggerAndRead[xLength_Integer?NonNegative,
					yLength_Integer?NonNegative, zLength_Integer?NonNegative,
					handle_Integer]
:Arguments:      { xLength, yLength, zLength, handle }
:ArgumentTypes:  { Integer, Integer, Integer, Integer }
:ReturnType:     Manual
:End:

void mclMultiAxisWaveFormTrigger(int);

:Begin:
:Function:       mclMultiAxisWaveFormTrigger
:Pattern:        MCLMultiAxisWaveFormTrigger[handle_Integer]
:Arguments:      { handle }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void mclMultiAxisWaveFormRead(int, int, int, int);

:Begin:
:Function:       mclMultiAxisWaveFormRead
:Pattern:        MCLMultiAxisWaveFormRead[xLength_Integer?NonNegative,
					yLength_Integer?NonNegative, zLength_Integer?NonNegative,
					handle_Integer]
:Arguments:      { xLength, yLength, zLength, handle }
:ArgumentTypes:  { Integer, Integer, Integer, Integer }
:ReturnType:     Manual
:End:

void mclMultiAxisWaveFormStop(int);

:Begin:
:Function:       mclMultiAxisWaveFormStop
:Pattern:        MCLMultiAxisWaveFormStop[handle_Integer]
:Arguments:      { handle }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:
