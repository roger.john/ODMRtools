
int gpibOpenDevice(int,int,int);

:Begin:
:Function:       gpibOpenDevice
:Pattern:        GPIBOpenDevice[board_Integer,pad_Integer,sad_Integer]
:Arguments:      { board, pad, sad }
:ArgumentTypes:  { Integer, Integer, Integer }
:ReturnType:     Integer
:End:

int gpibFindDevice(const char*);

:Begin:
:Function:       gpibFindDevice
:Pattern:        GPIBFindDevice[name_String]
:Arguments:      { name }
:ArgumentTypes:  { String }
:ReturnType:     Integer
:End:

int gpibCloseDevice(int);

:Begin:
:Function:       gpibCloseDevice
:Pattern:        GPIBCloseDevice[dev_Integer]
:Arguments:      { dev }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:

int gpibResetDeviceParams(int);

:Begin:
:Function:       gpibResetDeviceParams
:Pattern:        GPIBResetDeviceParams[dev_Integer]
:Arguments:      { dev }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:

int gpibSetEOS(int,int);

:Begin:
:Function:       gpibSetEOS
:Pattern:        GPIBSetEOS[dev_Integer,eos_Integer]
:Arguments:      { dev, eos }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Integer
:End:

int gpibSetEOT(int,int);

:Begin:
:Function:       gpibSetEOT
:Pattern:        GPIBSetEOT[dev_Integer,eot_Integer]
:Arguments:      { dev, eot }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Integer
:End:

int gpibSetTimeout(int,int);

:Begin:
:Function:       gpibSetTimeout
:Pattern:        GPIBSetTimeout[dev_Integer,timeout_Integer]
:Arguments:      { dev, timeout }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Integer
:End:

int gpibWait(int,int);

:Begin:
:Function:       gpibWait
:Pattern:        GPIBWait[dev_Integer,mask_Integer]
:Arguments:      { dev, mask}
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Integer
:End:

int gpibWaitForSRQ(int);

:Begin:
:Function:       gpibWaitForSRQ
:Pattern:        GPIBWaitForSRQ[dev_Integer]
:Arguments:      { dev }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:

int gpibTestSRQ(int);

:Begin:
:Function:       gpibTestSRQ
:Pattern:        GPIBTestSRQ[dev_Integer]
:Arguments:      { dev }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:

int gpibReset(int);

:Begin:
:Function:       gpibReset
:Pattern:        GPIBReset[board_Integer]
:Arguments:      { board }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:

int gpibClearInterface(int);

:Begin:
:Function:       gpibClearInterface
:Pattern:        GPIBClearInterface[board_Integer]
:Arguments:      { board }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:

int gpibClearDevice(int);

:Begin:
:Function:       gpibClearDevice
:Pattern:        GPIBClearDevice[dev_Integer]
:Arguments:      { Manual }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:

int gpibEnableRemoteMode(int);

:Begin:
:Function:       gpibEnableRemoteMode
:Pattern:        GPIBEnableRemoteMode[board_Integer]
:Arguments:      { board }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:

int gpibDisableRemoteMode(int);

:Begin:
:Function:       gpibDisableRemoteMode
:Pattern:        GPIBDisableRemoteMode[board_Integer]
:Arguments:      { board }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:

int gpibStatus(void);

:Begin:
:Function:       gpibStatus
:Pattern:        GPIBStatus[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Integer
:End:

int gpibError(void);

:Begin:
:Function:       gpibError
:Pattern:        GPIBError[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Integer
:End:

int gpibCount(void);

:Begin:
:Function:       gpibCount
:Pattern:        GPIBCount[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Integer
:End:

int gpibSend(int);

:Begin:
:Function:       gpibSend
:Pattern:        GPIBSend[dev_Integer,data_String]
:Arguments:      { dev, data }
:ArgumentTypes:  { Integer, Manual }
:ReturnType:     Integer
:End:

void gpibReceive(int);

:Begin:
:Function:       gpibReceive
:Pattern:        GPIBReceive[dev_Integer,bufferSize_Integer?NonNegative]
:Arguments:      { dev, bufferSize }
:ArgumentTypes:  { Integer, Manual }
:ReturnType:     Manual
:End:
