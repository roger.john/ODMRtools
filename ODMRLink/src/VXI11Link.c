//============================================================================
// Name        : RSIBLink.cpp
// Author      : RJ
// Version     :
// Copyright   : Your copyright notice
// Description : RSIB MathLink interface
//============================================================================

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <math.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <mathlink.h>
#include <rpc/rpc.h>
#include "vxi11.h"
#include "MathLinkCommon.h"

#define	VXI11_DEFAULT_TIMEOUT	10000	/* in ms */
#define	VXI11_READ_TIMEOUT		2000	/* in ms */
#define	VXI11_CLIENT			CLIENT
#define	VXI11_LINK				Create_LinkResp

struct VXI11device {
	VXI11_CLIENT *client;
	VXI11_LINK *link;
};

#define max(a,b) (a>b?a:b)

const char *VXI11mapErrorCode(int errorCode) {
	switch (errorCode) {
	case 0:
		return "no error";
	case 1:
		return "syntax error";
	case 3:
		return "device not accessible";
	case 4:
		return "invalid link identifier";
	case 5:
		return "parameter error";
	case 6:
		return "channel not established";
	case 8:
		return "operation not supported";
	case 9:
		return "out of resources";
	case 11:
		return "device locked by another link";
	case 12:
		return "no lock held by this link";
	case 15:
		return "I/O timeout";
	case 17:
		return "I/O error";
	case 21:
		return "invalid address";
	case 23:
		return "abort";
	case 29:
		return "channel already established";
	default:
		return "unknown error";
	}
}
unsigned long int VXI11convertTimeout(double timeout) {
	return (unsigned long) (
			timeout >= 0 ? 1000 * timeout : VXI11_DEFAULT_TIMEOUT);	// ms
}
void VXI11open(const char *host, const char *dev, double lockTimeout) {
	VXI11_CLIENT *client = clnt_create(host, DEVICE_CORE, DEVICE_CORE_VERSION,
			"tcp");
	if (client == NULL) {
		if (!MLPutFunction(stdlink, "$Failed", 1)
				|| !MLPutString(stdlink,
						clnt_spcreateerror("could not create client")))
			failWithMsg(stdlink, "VXI11open:clientCreateFailed:error: ");
		return;
	}
	Create_LinkParms link_parms;
	link_parms.clientId = (long) client;
	link_parms.lockDevice = 0;
	link_parms.lock_timeout = VXI11convertTimeout(lockTimeout);
	link_parms.device = dev;
	VXI11_LINK *link = (VXI11_LINK *) calloc(1, sizeof(VXI11_LINK));
	if (create_link_1(&link_parms, link, client) != RPC_SUCCESS) {
		if (!MLPutFunction(stdlink, "$Failed", 1)
				|| !MLPutString(stdlink,
						clnt_sperror(client, "could not create link")))
			failWithMsg(stdlink, "VXI11open:linkCreateFailed:error: ");
		clnt_destroy(client);
		return;
	}
	if (!MLPutFunction(stdlink, "VXI11device", 2)
			|| !MLPutInteger64(stdlink, (uint64_t) client)
			|| !MLPutInteger64(stdlink, (uint64_t) link)) {
		Device_Error dev_error;
		memset(&dev_error, 0, sizeof(dev_error));
		destroy_link_1(&link->lid, &dev_error, client);
		clnt_destroy(client);
		failWithMsg(stdlink, "VXI11open:return:error: ");
	}

}
int VXI11parseDevice(struct VXI11device *conn) {
	long int argc;
	if (!conn)
		return -1;
	if (!MLCheckFunction(stdlink, "VXI11device", &argc)) {
		failWithMsg(stdlink, "VXI11parseDevice:head:error: ");
		return -1;
	}
	if (argc == 2) {
		int64_t t;
		if (!MLGetInteger64(stdlink, &t)) {
			failWithMsg(stdlink, "VXI11parseDevice:clientAddress:error: ");
			return -1;
		}
		conn->client = (VXI11_CLIENT *) t;
		if (!MLGetInteger64(stdlink, &t)) {
			failWithMsg(stdlink, "VXI11parseDevice:linkAddress:error: ");
			return -1;
		}
		conn->link = (VXI11_LINK *) t;
		return 0;
	} else {
		if (!MLPutFunction(stdlink, "$Failed", 1)
				|| !MLPutString(stdlink, "device has wrong format"))
			failWithMsg(stdlink, "VXI11parseDevice:header:error: ");
		return -1;
	}
}
void VXI11close(void) {
	struct VXI11device dev;
	if (VXI11parseDevice(&dev) < 0)
		return;
	Device_Error dev_error;
	memset(&dev_error, 0, sizeof(dev_error));
	if (destroy_link_1(&(dev.link->lid), &dev_error, dev.client) != RPC_SUCCESS) {
		if (!MLPutFunction(stdlink, "$Failed", 1)
				|| !MLPutString(stdlink,
						clnt_sperror(dev.client, "could not close link")))
			failWithMsg(stdlink, "VXI11close:linkCloseFailed:error: ");
		clnt_destroy(dev.client);
	} else {
		clnt_destroy(dev.client);
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsg(stdlink, "VXI11close:return:error: ");
	}
}
void VXI11write(void) {
	struct VXI11device dev;
	if (VXI11parseDevice(&dev) < 0)
		return;
	const unsigned char *data;
	int len, r = 0;
	if (!MLGetByteString(stdlink, &data, &len, 0l))
		failWithMsg(stdlink, "VXI11write:data:error: ");
	double ioTimeout, lockTimeout;
	if (!MLGetReal(stdlink, &ioTimeout))
		failWithMsg(stdlink, "VXI11write:ioTimeout:error: ");
	if (!MLGetReal(stdlink, &lockTimeout))
		failWithMsg(stdlink, "VXI11write:lockTimeout:error: ");
	if (len <= 0) {
		MLReleaseByteString(stdlink, data, len);
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsg(stdlink, "VXI11write:return:error: ");
		return;
	}
	unsigned long mtu =
			dev.link->maxRecvSize > 0 ? dev.link->maxRecvSize : 1024;
	Device_WriteParms write_parms;
	write_parms.lid = dev.link->lid;
	write_parms.io_timeout = VXI11convertTimeout(ioTimeout);
	write_parms.lock_timeout = VXI11convertTimeout(lockTimeout);
	Device_WriteResp write_resp;
	while (r < len) {
		memset(&write_resp, 0, sizeof(write_resp));
		if (len - r <= mtu) {
			write_parms.flags = 8;
			write_parms.data.data_len = len - r;
		} else {
			write_parms.flags = 0;
			write_parms.data.data_len = mtu;
		}
		write_parms.data.data_val = data + r;
		if (device_write_1(&write_parms, &write_resp, dev.client)
				!= RPC_SUCCESS) {
			MLReleaseByteString(stdlink, data, len);
			if (!MLPutFunction(stdlink, "$Failed", 1)
					|| !MLPutString(stdlink,
							clnt_sperror(dev.client, "write did not succeed")))
				failWithMsg(stdlink, "VXI11write:writeFailed:error: ");
			return;
		}
		if (write_resp.error != 0) {
			MLReleaseByteString(stdlink, data, len);
			if (!MLPutFunction(stdlink, "$Failed", 1)
					|| !MLPutString(stdlink,
							VXI11mapErrorCode(write_resp.error)))
				failWithMsg(stdlink, "VXI11write:writeFailed:error: ");
			return;
		}
		r += write_resp.size;
	}
	MLReleaseByteString(stdlink, data, len);
	if (!MLPutInteger(stdlink, r))
		failWithMsg(stdlink, "VXI11write:return:error: ");
}
void VXI11read(void) {
#define RCV_END_BIT	0x04	// An end indicator has been read
#define RCV_CHR_BIT	0x02	// A termchr is set in flags and a character which matches termChar is transferred
#define RCV_REQCNT_BIT	0x01	// requestSize bytes have been transferred.  This includes a request size of zero.
	struct VXI11device conn;
	if (VXI11parseDevice(&conn) < 0)
		return;
	int maxSize, r = 0;
	if (!MLGetInteger(stdlink, &maxSize))
		failWithMsg(stdlink, "VXI11read:maxSize:error: ");
	double ioTimeout, lockTimeout;
	if (!MLGetReal(stdlink, &ioTimeout))
		failWithMsg(stdlink, "VXI11read:ioTimeout:error: ");
	if (!MLGetReal(stdlink, &lockTimeout))
		failWithMsg(stdlink, "VXI11read:lockTimeout:error: ");
	Device_ReadParms read_parms;
	Device_ReadResp read_resp;
	read_parms.lid = conn.link->lid;
	read_parms.requestSize = maxSize;
	read_parms.io_timeout = VXI11convertTimeout(ioTimeout);
	read_parms.lock_timeout = VXI11convertTimeout(lockTimeout);
	read_parms.flags = 0;
	read_parms.termChar = 0;
	unsigned char *buffer = malloc(maxSize);
	if (buffer == NULL) {
		if (!MLPutFunction(stdlink, "$Failed", 1)
				|| !MLPutString(stdlink, "could not allocate buffer"))
			failWithMsg(stdlink, "VXI11read:buffer:error: ");
		return;
	}
	while (r < maxSize) {
		memset(&read_resp, 0, sizeof(read_resp));
		read_resp.data.data_val = buffer + r;
		read_parms.requestSize = maxSize - r;
		if (device_read_1(&read_parms, &read_resp, conn.client)
				!= RPC_SUCCESS) {
			if (!MLPutFunction(stdlink, "$Failed", 2)
					|| !MLPutString(stdlink,
							clnt_sperror(conn.client, "read did not succeed"))
					|| !MLPutByteString(stdlink, buffer, r))
				failWithMsg(stdlink, "VXI11read:readFailed:error: ");
			return;
		}
		if (read_resp.error != 0) {
			if (!MLPutFunction(stdlink, "$Failed", 2)
					|| !MLPutString(stdlink, VXI11mapErrorCode(read_resp.error))
					|| !MLPutByteString(stdlink, buffer, r))
				failWithMsg(stdlink, "VXI11read:readFailed:error: ");
			return;
		}
		r += read_resp.data.data_len;
		if (r > maxSize) {// what shall we do in this case? it most likely means data corruption, but if we did not crash yet, we may read
			if (!MLPutFunction(stdlink, "$Failed", 2)
					|| !MLPutString(stdlink,
							"device sent more data than requested")
					|| !MLPutByteString(stdlink, buffer, r))
				failWithMsg(stdlink, "VXI11read:readFailed:error: ");
			return;
		}
		if ((read_resp.reason & RCV_END_BIT)
				|| (read_resp.reason & RCV_CHR_BIT))
			break;
		else if (r == maxSize)
			if (!MLPutFunction(stdlink, "$ProbablyTruncated", 1))
				failWithMsg(stdlink, "VXI11read:return:error: ");
	}
	if (!MLPutByteString(stdlink, buffer, r))
		failWithMsg(stdlink, "VXI11read:return:error: ");
	free(buffer);
}
