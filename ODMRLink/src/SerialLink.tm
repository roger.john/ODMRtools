
void serialOpen(const char*,int);

:Begin:
:Function:       serialOpen
:Pattern:        SerialOpen[dev_String, speed_Integer]
:Arguments:      { dev, speed}
:ArgumentTypes:  { String, Integer }
:ReturnType:     Manual
:End:

void serialSetTimeout(int, float);

:Begin:
:Function:       serialSetTimeout
:Pattern:        SerialSetTimeout[fd_Integer, timeout_Real]
:Arguments:      { fd, timeout}
:ArgumentTypes:  { Integer, Real32 }
:ReturnType:     Manual
:End:

void serialGetTimeout(int);

:Begin:
:Function:       serialGetTimeout
:Pattern:        SerialGetTimeout[fd_Integer]
:Arguments:      { fd }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

int serialClose(int);

:Begin:
:Function:       serialClose
:Pattern:        SerialClose[fd_Integer]
:Arguments:      { fd }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:

int serialFlushReadBuffer(int);

:Begin:
:Function:       serialFlushReadBuffer
:Pattern:        SerialFlushReadBuffer[fd_Integer]
:Arguments:      { fd }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:

int serialFlushWriteBuffer(int);

:Begin:
:Function:       serialFlushWriteBuffer
:Pattern:        SerialFlushWriteBuffer[fd_Integer]
:Arguments:      { fd }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:

int serialFlush(int);

:Begin:
:Function:       serialFlush
:Pattern:        SerialFlush[fd_Integer]
:Arguments:      { fd }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:

int serialWrite(int);

:Begin:
:Function:       serialWrite
:Pattern:        SerialWrite[fd_Integer, data_String]
:Arguments:      { fd, data}
:ArgumentTypes:  { Integer, Manual }
:ReturnType:     Integer
:End:

int serialWriteLine(int);

:Begin:
:Function:       serialWriteLine
:Pattern:        SerialWriteLine[fd_Integer, data_String]
:Arguments:      { fd, data}
:ArgumentTypes:  { Integer, Manual }
:ReturnType:     Integer
:End:

int serialWriteLineN(int);

:Begin:
:Function:       serialWriteLineN
:Pattern:        SerialWriteLineN[fd_Integer, data_String]
:Arguments:      { fd, data}
:ArgumentTypes:  { Integer, Manual }
:ReturnType:     Integer
:End:

void serialWriteAndRead(int, int);

:Begin:
:Function:       serialWriteAndRead
:Pattern:        SerialWriteAndRead[fd_Integer, maxToRead_Integer, data_String]
:Arguments:      { fd, maxToRead, data }
:ArgumentTypes:  { Integer, Integer, Manual }
:ReturnType:     Manual
:End:

void serialWriteAndReadMin(int, int, int);

:Begin:
:Function:       serialWriteAndReadMin
:Pattern:        SerialWriteAndRead[fd_Integer, minToRead_Integer, maxToRead_Integer,
				 data_String]
:Arguments:      { fd, minToRead, maxToRead, data }
:ArgumentTypes:  { Integer, Integer, Integer, Manual }
:ReturnType:     Manual
:End:

void serialRead(int, int);

:Begin:
:Function:       serialRead
:Pattern:        SerialRead[fd_Integer, maxToRead_Integer]
:Arguments:      { fd, maxToRead}
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Manual
:End:

void serialReadMin(int, int, int);

:Begin:
:Function:       serialReadMin
:Pattern:        SerialRead[fd_Integer, minToRead_Integer, maxToRead_Integer]
:Arguments:      { fd, minToRead, maxToRead}
:ArgumentTypes:  { Integer, Integer, Integer }
:ReturnType:     Manual
:End:

void serialReadLine(int);

:Begin:
:Function:       serialReadLine
:Pattern:        SerialReadLine[fd_Integer]
:Arguments:      { fd }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void serialReadUntil(int);

:Begin:
:Function:       serialReadUntil
:Pattern:        SerialRead[fd_Integer, delimiters_String]
:Arguments:      { fd, delimiters }
:ArgumentTypes:  { Integer, Manual }
:ReturnType:     Manual
:End:

int serialDataAvail(int);

:Begin:
:Function:       serialDataAvail
:Pattern:        SerialDataAvail[fd_Integer]
:Arguments:      { fd }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:
