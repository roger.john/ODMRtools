void usbtmcOpen(const char *);

:Begin:
:Function:		 usbtmcOpen
:Pattern:        USBTMCOpen[dev_String]
:Arguments:      { dev }
:ArgumentTypes:  { String }
:ReturnType:     Manual
:End:

void usbtmcGetTimeout(int);

:Begin:
:Function:       usbtmcGetTimeout
:Pattern:        USBTMCGetTimeout[fd_Integer]
:Arguments:      { fd }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void usbtmcSetTimeout(int, float);

:Begin:
:Function:       usbtmcSetTimeout
:Pattern:        USBTMCSetTimeout[fd_Integer, timeout_Real]
:Arguments:      { fd, timeout}
:ArgumentTypes:  { Integer, Real32 }
:ReturnType:     Manual
:End:

int usbtmcClose(int);

:Begin:
:Function:       usbtmcClose
:Pattern:        USBTMCClose[fd_Integer]
:Arguments:      { fd }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:

void usbtmcCmd(int, const char *, int);

:Begin:
:Function:       usbtmcCmd
:Pattern:        USBTMCCmd[fd_Integer, cmd_String, maxToRead_Integer]
:Arguments:      { fd, cmd, maxToRead}
:ArgumentTypes:  { Integer, String, Integer }
:ReturnType:     Manual
:End:

