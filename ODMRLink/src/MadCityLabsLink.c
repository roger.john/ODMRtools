#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "MathLinkCommon.h"
#include <Madlib.h>

const char *mclTranslateErrorCode(int error) {
	if (error >= MCL_SUCCESS)	// no error
		return "MCL_SUCCESS";
	switch (error) {
	case MCL_GENERAL_ERROR:
		return "MCL_GENERAL_ERROR";
	case MCL_DEV_ERROR:
		return "MCL_DEV_ERROR";
	case MCL_DEV_NOT_ATTACHED:
		return "MCL_DEV_NOT_ATTACHED";
	case MCL_USAGE_ERROR:
		return "MCL_USAGE_ERROR";
	case MCL_ARGUMENT_ERROR:
		return "MCL_ARGUMENT_ERROR";
	case MCL_INVALID_AXIS:
		return "MCL_INVALID_AXIS";
	case MCL_INVALID_HANDLE:
		return "MCL_INVALID_HANDLE";
	default:
		return "MCL_UNKNOWN_ERROR";
	}
}

int mclInitHandle() {
	return MCL_InitHandle();
}

int mclInitHandleOrGetExisting() {
	return MCL_InitHandleOrGetExisting();
}

int mclGrabHandle(int type) {
	return MCL_GrabHandle(type);
}

int mclGrabHandleOrGetExisting(int type) {
	return MCL_GrabHandleOrGetExisting(type);
}

int mclGrabAllHandles() {
	return MCL_GrabAllHandles();
}

void mclGetAllHandles() {
	int numberOfHandles = MCL_NumberOfCurrentHandles();
	int *handles = calloc(numberOfHandles, sizeof(int));
	numberOfHandles = MCL_GetAllHandles(handles, numberOfHandles);
	if (!MLPutInteger32List(stdlink, handles, numberOfHandles)) {
		free(handles);
		failWithMsgAndError(stdlink, "mclGetAllHandles:handles:error: ");
		return;
	}
	free(handles);
}

int mclNumberOfCurrentHandles() {
	return MCL_NumberOfCurrentHandles();
}

int mclGetHandleBySerial(int serial) {
	return MCL_GetHandleBySerial(serial);
}

void mclReleaseHandle(int handle) {
	MCL_ReleaseHandle(handle);
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink, "mclReleaseHandle:error: ");
}

void mclReleaseAllHandles() {
	MCL_ReleaseAllHandles();
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink, "mclReleaseAllHandles:error: ");
}

void mclDeviceAttached(int milliseconds, int handle) {
	if (!MLPutSymbol(stdlink,
			MCL_DeviceAttached(milliseconds, handle) ? "True" : "False"))
		failWithMsgAndError(stdlink, "mclDeviceAttached:result:error: ");
}

double mclGetCalibration(int axis, int handle) {
	return MCL_GetCalibration(axis, handle);
}

void mclGetCalibrationXYZ(int handle) {
	double cal[3];
	for (int axis = 1; axis <= 3; axis++)
		cal[axis - 1] = MCL_GetCalibration(axis, handle);
	if (!MLPutReal64List(stdlink, cal, 3))
		failWithMsgAndError(stdlink, "mclGetCalibrationXYZ:cal:error: ");
}

void mclGetFirmwareVersion(int handle) {
	short version, profile;
	int result = MCL_GetFirmwareVersion(&version, &profile, handle);
	if (result < 0) {
		if (!MLPutString(stdlink, mclTranslateErrorCode(result)))
			failWithMsgAndError(stdlink,
					"mclGetFirmwareVersion:result:error: ");
		return;
	}
	if (!MLPutFunction(stdlink, "List", 2)) {
		failWithMsgAndError(stdlink, "mclGetFirmwareVersion:list:head:error: ");
		return;
	}
	if (!MLPutInteger16(stdlink, version)) {
		failWithMsgAndError(stdlink,
				"mclGetFirmwareVersion:list:version:error: ");
		return;
	}
	if (!MLPutInteger16(stdlink, profile)) {
		failWithMsgAndError(stdlink,
				"mclGetFirmwareVersion:list:profile:error: ");
		return;
	}
}

void mclPrintDeviceInfo(int handle) {
	MCL_PrintDeviceInfo(handle);
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsgAndError(stdlink, "mclPrintDeviceInfo:error: ");
}

int mclGetSerialNumber(int handle) {
	return MCL_GetSerialNumber(handle);
}

void mclGetProductInfo(int handle) {
	struct ProductInformation info;
	int result = MCL_GetProductInfo(&info, handle);
	if (result < 0) {
		if (!MLPutString(stdlink, mclTranslateErrorCode(result)))
			failWithMsgAndError(stdlink, "mclGetProductInfo:result:error: ");
		return;
	}
	if (!MLPutFunction(stdlink, "ProductInformation", 6)) {
		failWithMsgAndError(stdlink,
				"mclGetCommandedPosition:info:head:error: ");
		return;
	}
	if (!MLPutInteger16(stdlink, info.axis_bitmap)) {
		failWithMsgAndError(stdlink,
				"mclGetProductInfo:info:axis_bitmap:error: ");
		return;
	}
	if (!MLPutInteger16(stdlink, info.ADC_resolution)) {
		failWithMsgAndError(stdlink,
				"mclGetProductInfo:info:ADC_resolution:error: ");
		return;
	}
	if (!MLPutInteger16(stdlink, info.DAC_resolution)) {
		failWithMsgAndError(stdlink,
				"mclGetProductInfo:info:DAC_resolution:error: ");
		return;
	}
	if (!MLPutInteger16(stdlink, info.Product_id)) {
		failWithMsgAndError(stdlink,
				"mclGetProductInfo:info:Product_id:error: ");
		return;
	}
	if (!MLPutInteger16(stdlink, info.FirmwareVersion)) {
		failWithMsgAndError(stdlink,
				"mclGetProductInfo:list:info:FirmwareVersion:error: ");
		return;
	}
	if (!MLPutInteger16(stdlink, info.FirmwareProfile)) {
		failWithMsgAndError(stdlink,
				"mclGetProductInfo:list:info:FirmwareProfile:error: ");
		return;
	}
}

/* not contained in library
void mclDLLVersion(void) {
	short version, revision;
	MCL_DLLVersion(&version, &revision);
	if (!MLPutFunction(stdlink, "List", 3)) {
		failWithMsgAndError(stdlink, "mclDLLVersion:list:head:error: ");
		return;
	}
	if (!MLPutInteger16(stdlink, version)) {
		failWithMsgAndError(stdlink, "mclDLLVersion:list:version:error: ");
	 	 return;
	}
	if (!MLPutInteger16(stdlink, revision)) {
		failWithMsgAndError(stdlink, "mclDLLVersion:list:revision:error: ");
		return;
	}
}
*/

void mclCorrectDriverVersion() {
	if (!MLPutSymbol(stdlink,
			1 //MCL_CorrectDriverVersion()
			? "True" : "False"))
		failWithMsgAndError(stdlink, "mclCorrectDriverVersion:result:error: ");
}

void mclGetCommandedPosition(int handle) {
	double pos[3];
	int result = MCL_GetCommandedPosition(&pos[0], &pos[1], &pos[2], handle);
	if (result < 0) {
		if (!MLPutString(stdlink, mclTranslateErrorCode(result)))
			failWithMsgAndError(stdlink,
					"mclGetCommandedPosition:result:error: ");
		return;
	}
	if (!MLPutReal64List(stdlink, pos, 3))
		failWithMsgAndError(stdlink, "mclGetCommandedPosition:pos:error: ");
}

void mclSingleReadN(int axis, int handle) {
	double result = MCL_SingleReadN(axis, handle);
	if (result < 0 && result == (int) result) {
		if (!MLPutString(stdlink, mclTranslateErrorCode((int) result))) {
			failWithMsgAndError(stdlink, "mclSingleReadN:error:error: ");
			return;
		}
	} else if (!MLPutReal64(stdlink, result)) {
		failWithMsgAndError(stdlink, "mclSingleReadN:result:error: ");
		return;
	}
}

void mclSingleWriteN(double position, int axis, int handle) {
	int result = MCL_SingleWriteN(position, axis, handle);
	if (!MLPutString(stdlink, mclTranslateErrorCode(result))) {
		failWithMsgAndError(stdlink, "mclSingleWriteN:result:error: ");
		return;
	}
}

void mclMonitorN(double position, int axis, int handle) {
	double result = MCL_MonitorN(position, axis, handle);
	if (result < 0 && result == (int) result) {
		if (!MLPutString(stdlink, mclTranslateErrorCode((int) result))) {
			failWithMsgAndError(stdlink, "mclMonitorN:error:error: ");
			return;
		}
	} else if (!MLPutReal64(stdlink, result)) {
		failWithMsgAndError(stdlink, "mclMonitorN:result:error: ");
		return;
	}
}

void mclSingleReadZ(int handle) {
	double result = MCL_SingleReadZ(handle);
	if (result < 0 && result == (int) result) {
		if (!MLPutString(stdlink, mclTranslateErrorCode((int) result))) {
			failWithMsgAndError(stdlink, "mclSingleReadZ:error:error: ");
			return;
		}
	} else if (!MLPutReal64(stdlink, result)) {
		failWithMsgAndError(stdlink, "mclSingleReadZ:result:error: ");
		return;
	}
}

void mclSingleWriteZ(double position, int handle) {
	int result = MCL_SingleWriteZ(position, handle);
	if (!MLPutString(stdlink, mclTranslateErrorCode(result))) {
		failWithMsgAndError(stdlink, "mclSingleWriteZ:result:error: ");
		return;
	}
}

void mclMonitorZ(double position, int handle) {
	double result = MCL_MonitorZ(position, handle);
	if (result < 0 && result == (int) result) {
		if (!MLPutString(stdlink, mclTranslateErrorCode((int) result))) {
			failWithMsgAndError(stdlink, "mclMonitorZ:error:error: ");
			return;
		}
	} else if (!MLPutReal64(stdlink, result)) {
		failWithMsgAndError(stdlink, "mclMonitorZ:result:error: ");
		return;
	}
}

void mclSingleReadXYZ(int handle) {
	double pos[3];
	for (int axis = 1; axis <= 3; axis++)
		pos[axis - 1] = MCL_SingleReadN(axis, handle);
	for (int i = 0; i < 3; i++)
		if (pos[i] < 0 && pos[i] == (int) pos[i]) {
			if (!MLPutString(stdlink, mclTranslateErrorCode((int) pos[i]))) {
				failWithMsgAndError(stdlink, "mclSingleReadXYZ:error:error: ");
				return;
			}
		}
	if (!MLPutReal64List(stdlink, pos, 3))
		failWithMsgAndError(stdlink, "mclSingleReadXYZ:list:head:error: ");
}

void mclSingleWriteXYZ(double *pos, long posLength, int handle) {
	int result[3];
	if (posLength != 3) {
		if (!MLPutString(stdlink, "MCL_3D_VECTOR_EXPECTED"))
			failWithMsgAndError(stdlink, "mclSingleWriteXYZ:error:error: ");
		return;
	}
	for (int axis = 1; axis <= 3; axis++)
		result[axis - 1] = MCL_SingleWriteN(pos[axis - 1], axis, handle);
	for (int i = 0; i < 3; i++)
		if (result[i] < 0 || i == 2) {
			if (!MLPutString(stdlink, mclTranslateErrorCode(result[i])))
				failWithMsgAndError(stdlink,
						"mclSingleWriteXYZ:result:error: ");
			return;
		}
}

void mclMonitorXYZ(double *pos, long posLength, int handle) {
	if (posLength != 3) {
		if (!MLPutString(stdlink, "MCL_3D_VECTOR_EXPECTED"))
			failWithMsgAndError(stdlink, "mclMonitorXYZ:error:error: ");
		return;
	}
	double readPos[3];
	for (int axis = 1; axis <= 3; axis++)
		readPos[axis - 1] = MCL_MonitorN(pos[axis - 1], axis, handle);
	for (int i = 0; i < 2; i++)
		if (readPos[i] < 0 && readPos[i] == (int) readPos[i]) {
			if (!MLPutString(stdlink, mclTranslateErrorCode((int) readPos[i])))
				failWithMsgAndError(stdlink, "mclMonitorXYZ:result:error: ");
			return;
		}
	if (!MLPutReal64List(stdlink, readPos, 3))
		failWithMsgAndError(stdlink, "mclMonitorXYZ:data:error: ");
}

void mclReadWaveFormN(int axis, int length, double samplePeriodMilliseconds,
		int handle) {
	double *data = calloc(length, sizeof(double));
	int result = MCL_ReadWaveFormN(axis, length, samplePeriodMilliseconds, data,
			handle);
	if (result < 0) {
		free(data);
		if (!MLPutString(stdlink, mclTranslateErrorCode(result)))
			failWithMsgAndError(stdlink, "mclReadWaveFormN:result:error: ");
		return;
	} else if (!MLPutReal64List(stdlink, data, length)) {
		free(data);
		failWithMsgAndError(stdlink, "mclReadWaveFormN:data:error: ");
		return;
	}
	free(data);
}

void mclSetupReadWaveFormN(int axis, int length,
		double samplePeriodMilliseconds, int handle) {
	int result = MCL_Setup_ReadWaveFormN(axis, length, samplePeriodMilliseconds,
			handle);
	if (!MLPutString(stdlink, mclTranslateErrorCode(result)))
		failWithMsgAndError(stdlink, "mclSetupReadWaveFormN:result:error: ");
}

void mclTriggerReadWaveFormN(int axis, int length, int handle) {
	double *data = calloc(length, sizeof(double));
	int result = MCL_Trigger_ReadWaveFormN(axis, length, data, handle);
	if (result < 0) {
		free(data);
		if (!MLPutString(stdlink, mclTranslateErrorCode(result)))
			failWithMsgAndError(stdlink,
					"mclTriggerReadWaveFormN:result:error: ");
		return;
	} else if (!MLPutReal64List(stdlink, data, length)) {
		free(data);
		failWithMsgAndError(stdlink, "mclTriggerReadWaveFormN:data:error: ");
		return;
	}
	free(data);
}

void mclLoadWaveFormN(int axis, double samplePeriodMilliseconds, double *data,
		long dataLength, int handle) {
	int result = MCL_LoadWaveFormN(axis, dataLength, samplePeriodMilliseconds,
			data, handle);
	if (!MLPutString(stdlink, mclTranslateErrorCode(result)))
		failWithMsgAndError(stdlink, "mclLoadWaveFormN:result:error: ");
}

void mclSetupLoadWaveFormN(int axis, double samplePeriodMilliseconds,
		double *data, long dataLength, int handle) {
	int result = MCL_Setup_LoadWaveFormN(axis, dataLength,
			samplePeriodMilliseconds, data, handle);
	if (!MLPutString(stdlink, mclTranslateErrorCode(result)))
		failWithMsgAndError(stdlink, "mclSetupLoadWaveFormN:result:error: ");
}

void mclTriggerLoadWaveFormN(int axis, int handle) {
	int result = MCL_Trigger_LoadWaveFormN(axis, handle);
	if (!MLPutString(stdlink, mclTranslateErrorCode(result)))
		failWithMsgAndError(stdlink, "mclTriggerLoadWaveFormN:result:error: ");
}

void mclTriggerWaveFormAcquisition(int axis, int length, int handle) {
	double *data = calloc(length, sizeof(double));
	int result = MCL_TriggerWaveformAcquisition(axis, length, data, handle);
	if (result < 0) {
		free(data);
		if (!MLPutString(stdlink, mclTranslateErrorCode(result)))
			failWithMsgAndError(stdlink,
					"mclTriggerWaveFormAcquisition:result:error: ");
		return;
	} else if (!MLPutReal64List(stdlink, data, length)) {
		free(data);
		failWithMsgAndError(stdlink,
				"mclTriggerWaveFormAcquisition:data:error: ");
		return;
	}
	free(data);
}

long checkValuesSameAndPositiveOrZero(long x, long y, long z) {
	int t = x;
	if (t < y)
		t = y;
	if (t < z)
		t = z;
	if (t < 0 || (x != 0 && x != t) || (y != 0 && y != t) || (z != 0 && z != t))
		return -1;
	else
		return t;
}

void mclMultiAxisWaveFormSetup(double *x, long xLength, double *y, long yLength,
		double *z, long zLength, double samplePeriodMilliseconds,
		int iterations, int handle) {
	int length = checkValuesSameAndPositiveOrZero(xLength, yLength, zLength);
	if (length < 0) {
		if (!MLPutString(stdlink, "MCL_UNEQUAL_AXIS_LENGTHS"))
			failWithMsgAndError(stdlink,
					"mclMultiAxisWaveFormSetup:error:error: ");
		return;
	}
	int result = MCL_WfmaSetup(xLength == 0 ? 0 : x, yLength == 0 ? 0 : y,
			zLength == 0 ? 0 : z, length, samplePeriodMilliseconds, iterations,
			handle);
	if (!MLPutString(stdlink, mclTranslateErrorCode(result)))
		failWithMsgAndError(stdlink, "mclSetupLoadWaveFormN:result:error: ");
}

void mclMultiAxisWaveFormTriggerAndRead(int xLength, int yLength, int zLength,
		int handle) {
	int length = checkValuesSameAndPositiveOrZero(xLength, yLength, zLength);
	if (length < 0) {
		if (!MLPutString(stdlink, "MCL_UNEQUAL_AXIS_LENGTHS"))
			failWithMsgAndError(stdlink,
					"mclMultiAxisWaveFormTriggerAndRead:error:error: ");
		return;
	}
	double *data = calloc(xLength + yLength + zLength, sizeof(double));
	int result = MCL_WfmaTriggerAndRead(xLength > 0 ? &data[0] : 0,
			yLength > 0 ? &data[xLength] : 0,
			zLength > 0 ? &data[xLength + yLength] : 0, handle);
	if (result < 0) {
		free(data);
		if (!MLPutString(stdlink, mclTranslateErrorCode(result)))
			failWithMsgAndError(stdlink,
					"mclMultiAxisWaveFormTriggerAndRead:result:error: ");
		return;
	}
	if (!MLPutFunction(stdlink, "List", 3)) {
		free(data);
		failWithMsgAndError(stdlink,
				"mclMultiAxisWaveFormTriggerAndRead:list:error: ");
		return;
	}
	if (xLength > 0 ?
			!MLPutReal64List(stdlink, &data[0], xLength) :
			!MLPutFunction(stdlink, "List", 0)) {
		free(data);
		failWithMsgAndError(stdlink,
				"mclMultiAxisWaveFormTriggerAndRead:list:x:error: ");
		return;
	}
	if (yLength > 0 ?
			!MLPutReal64List(stdlink, &data[xLength], yLength) :
			!MLPutFunction(stdlink, "List", 0)) {
		free(data);
		failWithMsgAndError(stdlink,
				"mclMultiAxisWaveFormTriggerAndRead:list:y:error: ");
		return;
	}
	if (zLength > 0 ?
			!MLPutReal64List(stdlink, &data[xLength + yLength], zLength) :
			!MLPutFunction(stdlink, "List", 0)) {
		free(data);
		failWithMsgAndError(stdlink,
				"mclMultiAxisWaveFormTriggerAndRead:list:z:error: ");
		return;
	}
	free(data);
}

void mclMultiAxisWaveFormTrigger(int handle) {
	int result = MCL_WfmaTrigger(handle);
	if (!MLPutString(stdlink, mclTranslateErrorCode(result)))
		failWithMsgAndError(stdlink,
				"mclMultiAxisWaveFormTrigger:result:error: ");
}

void mclMultiAxisWaveFormRead(int xLength, int yLength, int zLength, int handle) {
	int length = checkValuesSameAndPositiveOrZero(xLength, yLength, zLength);
	if (length < 0) {
		if (!MLPutString(stdlink, "MCL_UNEQUAL_AXIS_LENGTHS"))
			failWithMsgAndError(stdlink,
					"mclMultiAxisWaveFormRead:error:error: ");
		return;
	}
	double *data = calloc(xLength + yLength + zLength, sizeof(double));
	int result = MCL_WfmaRead(xLength > 0 ? &data[0] : 0,
			yLength > 0 ? &data[xLength] : 0,
			zLength > 0 ? &data[xLength + yLength] : 0, handle);
	if (result < 0) {
		free(data);
		if (!MLPutString(stdlink, mclTranslateErrorCode(result)))
			failWithMsgAndError(stdlink,
					"mclMultiAxisWaveFormRead:result:error: ");
	}
	if (xLength > 0 ?
			!MLPutReal64List(stdlink, &data[0], xLength) :
			!MLPutFunction(stdlink, "List", 0)) {
		free(data);
		failWithMsgAndError(stdlink, "mclMultiAxisWaveFormRead:list:x:error: ");
		return;
	}
	if (yLength > 0 ?
			!MLPutReal64List(stdlink, &data[xLength], yLength) :
			!MLPutFunction(stdlink, "List", 0)) {
		free(data);
		failWithMsgAndError(stdlink, "mclMultiAxisWaveFormRead:list:y:error: ");
		return;
	}
	if (zLength > 0 ?
			!MLPutReal64List(stdlink, &data[xLength + yLength], zLength) :
			!MLPutFunction(stdlink, "List", 0)) {
		free(data);
		failWithMsgAndError(stdlink, "mclMultiAxisWaveFormRead:list:z:error: ");
		return;
	}
	free(data);
}

void mclMultiAxisWaveFormStop(int handle) {
	int result = MCL_WfmaStop(handle);
	if (!MLPutString(stdlink, mclTranslateErrorCode(result)))
		failWithMsgAndError(stdlink, "mclMultiAxisWaveFormStop:result:error: ");
}
