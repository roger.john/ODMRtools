
void odmrScanAxis(int, int, int, double, double, int, double, double, double, double);

:Begin:
:Function:       odmrScanAxis
:Pattern:        ODMRScanLine[stage_Integer, counter_Integer, xAxis:1|2|3,
					xmin_Real, xmax_Real, nPixels_Integer?Positive, y_Real,
					z_Real, iwt_Real?NonNegative, wt_Real?NonNegative]
:Arguments:      { stage, counter, xAxis, xmin, xmax, nPixels, y, z, iwt, wt }
:ArgumentTypes:  { Integer, Integer, Integer, Real, Real, Integer, Real, Real, Real,
					Real }
:ReturnType:     Manual
:End:

void odmrScanAxisOpt(int, int, int, double, double, int, double, double, double, double,
			double);

:Begin:
:Function:       odmrScanAxisOpt
:Pattern:        ODMRScanLine[stage_Integer, counter_Integer, xAxis:1|2|3,
					xmin_Real, xmax_Real, nPixels_Integer?Positive, y_Real,
					z_Real, iwt_Real?NonNegative, wt_Real?NonNegative, ct_Real?NonNegative]
:Arguments:      { stage, counter, xAxis, xmin, xmax, nPixels, y, z, iwt, wt, ct }
:ArgumentTypes:  { Integer, Integer, Integer, Real, Real, Integer, Real, Real, Real,
					Real, Real }
:ReturnType:     Manual
:End:

void odmrScanArea(int, int, int, double, double, int, int, double, double, int, double,
			double, double, double);

:Begin:
:Function:       odmrScanArea
:Pattern:        ODMRScanArea[stage_Integer, counter_Integer, xAxis:1|2|3,
					xmin_Real, xmax_Real, xPixels_Integer?Positive, yAxis:1|2|3,
					ymin_Real, ymax_Real, yPixels_Integer?Positive,
					z_Real, iwt_Real?NonNegative, btwt_Real?NonNegative, wt_Real?NonNegative]
:Arguments:      { stage, counter, xAxis, xmin, xmax, xPixels, yAxis, ymin, ymax, yPixels,
					z, iwt, btwt, wt }
:ArgumentTypes:  { Integer, Integer, Integer, Real, Real, Integer, Integer, Real, Real,
					Integer, Real, Real, Real, Real }
:ReturnType:     Manual
:End:

void odmrScanAreaOpt(int, int, int, double, double, int, int, double, double, int, double,
			double, double, double, double);

:Begin:
:Function:       odmrScanAreaOpt
:Pattern:        ODMRScanArea[stage_Integer, counter_Integer, xAxis:1|2|3,
					xmin_Real, xmax_Real, xPixels_Integer?Positive, yAxis:1|2|3,
					ymin_Real, ymax_Real, yPixels_Integer?Positive,
					z_Real, iwt_Real?NonNegative, btwt_Real?NonNegative, wt_Real?NonNegative,
					ct_Real?NonNegative]
:Arguments:      { stage, counter, xAxis, xmin, xmax, xPixels, yAxis, ymin, ymax, yPixels,
					z, iwt, btwt, wt, ct }
:ArgumentTypes:  { Integer, Integer, Integer, Real, Real, Integer, Integer, Real, Real,
					Integer, Real, Real, Real, Real, Real }
:ReturnType:     Manual
:End:

void odmrScanLine(int, int, double*, long, double*, long, int, double, double);

:Begin:
:Function:       odmrScanLine
:Pattern:        ODMRScanLine[stage_Integer, counter_Integer,
					startPoint:{_?NumberQ,_?NumberQ,_?NumberQ},
					endPoint:{_?NumberQ,_?NumberQ,_?NumberQ},
					nPixels_Integer?Positive, iwt_Real?NonNegative, wt_Real?NonNegative]
:Arguments:      { stage, counter, startPoint, endPoint, nPixels, iwt, wt }
:ArgumentTypes:  { Integer, Integer, RealList, RealList, Integer, Real,	Real }
:ReturnType:     Manual
:End:

void odmrScanLineOpt(int, int, double*, long, double*, long, int, double, double, double);

:Begin:
:Function:       odmrScanLineOpt
:Pattern:        ODMRScanLine[stage_Integer, counter_Integer,
					startPoint:{_?NumberQ,_?NumberQ,_?NumberQ},
					endPoint:{_?NumberQ,_?NumberQ,_?NumberQ},
					nPixels_Integer?Positive, iwt_Real?NonNegative, wt_Real?NonNegative,
					ct_Real?NonNegative]
:Arguments:      { stage, counter, startPoint, endPoint, nPixels, iwt, wt, ct }
:ArgumentTypes:  { Integer, Integer, RealList, RealList, Integer, Real,	Real, Real }
:ReturnType:     Manual
:End:

void odmrScanPoints(int, int, double, double);

:Begin:
:Function:       odmrScanPoints
:Pattern:        ODMRScanPoints[stage_Integer, counter_Integer,
					iwt_Real?NonNegative, wt_Real?NonNegative,
					pointList:{{_?NumericQ,_?NumericQ,_?NumericQ}..}]
:Arguments:      { stage, counter, iwt, wt }
:ArgumentTypes:  { Integer, Integer, Real,	Real, Manual }
:ReturnType:     Manual
:End:

void odmrScanPointsOpt(int, int, double, double, double);

:Begin:
:Function:       odmrScanPointsOpt
:Pattern:        ODMRScanPoints[stage_Integer, counter_Integer,
					iwt_Real?NonNegative, wt_Real?NonNegative, ct_Real?NonNegative,
					pointList:{{_?NumericQ,_?NumericQ,_?NumericQ}..}]
:Arguments:      { stage, counter, iwt, wt, ct }
:ArgumentTypes:  { Integer, Integer, Real,	Real, Real, Manual }
:ReturnType:     Manual
:End:

void odmrGatherTrace(int, int);

:Begin:
:Function:       odmrGatherTrace
:Pattern:        ODMRGatherTrace[counter_Integer, numSamples_Integer]
:Arguments:      { counter, numSamples }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Manual
:End:
		
void odmrTrackMaximum(int, int, int, double*, long, double*, long, double, double, int);

:Begin:
:Function:       odmrTrackMaximum
:Pattern:        ODMRTrackMaximum[stage_Integer, counter_Integer, counterId_Integer,
					x0:{_Real,_Real,_Real},	stepSizes:{_Real,_Real,_Real},
					trackingFactor_Real, st_Real?NonNegative, numSamples_Integer]
:Arguments:      { stage, counter, counterId, x0, stepSizes, trackingFactor, st,
					numSamples }
:ArgumentTypes:  { Integer, Integer, Integer, RealList, RealList, Real, Real, Integer }
:ReturnType:     Manual
:End:

void odmrStartPhotonStreamerThread(int, int);

:Begin:
:Function:       odmrStartPhotonStreamerThread
:Pattern:        ODMRstartPhotonStreamerThread[counter_Integer,
					bufferSize_Integer?Positive]
:Arguments:      { counter, bufferSize }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Manual
:End:

void odmrStopPhotonStreamerThread(double);

:Begin:
:Function:       odmrStopPhotonStreamerThread
:Pattern:        ODMRstopPhotonStreamerThread[waitTime_Real]
:Arguments:      { waitTime }
:ArgumentTypes:  { Real }
:ReturnType:     Manual
:End:

void odmrPhotonStreamerThreadRunning(void);

:Begin:
:Function:       odmrPhotonStreamerThreadRunning
:Pattern:        ODMRPhotonStreamerThreadRunning[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

void odmrFetchPhotons(void);

:Begin:
:Function:       odmrFetchPhotons
:Pattern:        ODMRFetchPhotons[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

double odmrGetClock(void);

:Begin:
:Function:       odmrGetClock
:Pattern:        ODMRGetClock[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Real
:End:

double odmrGetClockResolution(void);

:Begin:
:Function:       odmrGetClockResolution
:Pattern:        ODMRGetClockResolution[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Real
:End: