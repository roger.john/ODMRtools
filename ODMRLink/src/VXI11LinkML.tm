
void VXI11Open(const char*,const char *dev, double lockTimeout);

:Begin:
:Function:       VXI11open
:Pattern:        VXI11open[host_String, dev_String, lockTimeout_Real]
:Arguments:      { host, dev, lockTimeout }
:ArgumentTypes:  { String, String, Real }
:ReturnType:     Manual
:End:

void VXI11close(void);

:Begin:
:Function:       VXI11close
:Pattern:        VXI11close[conn:VXI11device[_Integer,_Integer]]
:Arguments:      { conn }
:ArgumentTypes:  { Manual }
:ReturnType:     Manual
:End:

void VXI11write(void);

:Begin:
:Function:       VXI11write
:Pattern:        VXI11write[conn:VXI11device[_Integer,_Integer],
					data_String, ioTimeout_Real, lockTimeout_Real]
:Arguments:      { conn, data, ioTimeout, lockTimeout }
:ArgumentTypes:  { Manual }
:ReturnType:     Manual
:End:

void VXI11read(void);

:Begin:
:Function:       VXI11read
:Pattern:        VXI11read[conn:VXI11device[_Integer,_Integer],
					maxSize_Integer?NonNegative,
					ioTimeout_Real, lockTimeout_Real]
:Arguments:      { conn, maxSize, ioTimeout, lockTimeout }
:ArgumentTypes:  { Manual }
:ReturnType:     Manual
:End:
