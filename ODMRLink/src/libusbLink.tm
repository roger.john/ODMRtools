void libusbSetDebug(mlint64,int);

:Begin:
:Function:		libusbSetDebug
:Pattern:		LibUSBSetDebug[ctxRef_Integer,level:(0|1|2|3|4)]
:Arguments:		{ ctxRef, level }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

:Evaluate:	LibUSBSetDebug[level:(0|1|2|3|4)]:=LibUSBSetDebug[0,level];
:Evaluate:	LibUSBSetDebug[Null,level:(0|1|2|3|4)]:=LibUSBSetDebug[0,level];

void libusbInit(void);

:Begin:
:Function:		libusbInit
:Pattern:		LibUSBInit[]
:Arguments:		{ }
:ArgumentTypes:	{ }
:ReturnType:	Manual
:End:

void libusbExit(mlint64);

:Begin:
:Function:		libusbExit
:Pattern:		LibUSBExit[ctxRef_Integer]
:Arguments:		{ ctxRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

:Evaluate:		LibUSBExit[]:=LibUSBExit[0];
:Evaluate:		LibUSBExit[Null]:=LibUSBExit[0];

void libusbGetDeviceList(mlint64);

:Begin:
:Function:		libusbGetDeviceList
:Pattern:		LibUSBGetDeviceList[ctxRef_Integer]
:Arguments:		{ ctxRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

:Evaluate:		LibUSBGetDeviceList[]:=LibUSBGetDeviceList[0];
:Evaluate:		LibUSBGetDeviceList[Null]:=LibUSBGetDeviceList[0];

void libusbFreeDeviceList(mlint64, const char *);

:Begin:
:Function:		libusbFreeDeviceList
:Pattern:		LibUSBFreeDeviceList[listRef_Integer, unref:True|False]
:Arguments:		{ listRef, unref }
:ArgumentTypes:	{ Integer64, Symbol }
:ReturnType:	Manual
:End:

:Evaluate:		libusbFreeDeviceList[listRef_Integer]:=
					LibUSBFreeDeviceList[listRef,True];

int libusbGetBusNumber(mlint64);

:Begin:
:Function:		libusbGetBusNumber
:Pattern:		LibUSBGetBusNumber[devRef_Integer]
:Arguments:		{ devRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Integer
:End:

int libusbGetDeviceAddress(mlint64);

:Begin:
:Function:		libusbGetDeviceAddress
:Pattern:		LibUSBGetDeviceAddress[devRef_Integer]
:Arguments:		{ devRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Integer
:End:
 
void libusbGetDeviceSpeed(mlint64);

:Begin:
:Function:		libusbGetDeviceSpeed
:Pattern:		LibUSBGetDeviceSpeed[devRef_Integer]
:Arguments:		{ devRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void libusbGetMaxPacketSize(mlint64,int);

:Begin:
:Function:		libusbGetMaxPacketSize
:Pattern:		LibUSBGetMaxPacketSize[devRef_Integer,
					endpoint_Integer/;0<=endpoint<2^8]
:Arguments:		{ devRef, endpoint }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:
 
void libusbGetMaxIsoPacketSize(mlint64,int);

:Begin:
:Function:		libusbGetMaxIsoPacketSize
:Pattern:		LibUSBGetMaxIsoPacketSize[devRef_Integer,
					endpoint_Integer/;0<=endpoint<2^8]
:Arguments:		{ devRef, endpoint }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:
 
void libusbRefDevice(mlint64);

:Begin:
:Function:		libusbRefDevice
:Pattern:		LibUSBRefDevice[devRef_Integer]
:Arguments:		{ devRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void libusbUnrefDevice(mlint64);

:Begin:
:Function:		libusbUnrefDevice
:Pattern:		LibUSBUnrefDevice[devRef_Integer]
:Arguments:		{ devRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void libusbOpenDevice(mlint64);

:Begin:
:Function:		libusbOpenDevice
:Pattern:		LibUSBOpenDevice[devRef_Integer]
:Arguments:		{ devRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void libusbOpenDeviceWithVidPid(mlint64,int,int);

:Begin:
:Function:		libusbOpenDeviceWithVidPid
:Pattern:		LibUSBOpenDeviceWithVidPid[ctxRef_Integer,vid_Integer/;0<=vid<2^16,
					pid_Integer/;0<=pid<2^16]
:Arguments:		{ devRef, vid, pid }
:ArgumentTypes:	{ Integer64, Integer, Integer }
:ReturnType:	Manual
:End:
 
:Evaluate:		LibUSBOpenDeviceWithVidPid[Null,
 						vid_Integer/;0<=vid<2^16,pid_Integer/;0<=pid<2^16]:=
 					LibUSBOpenDeviceWithVidPid[0,vid,pid];
:Evaluate:		LibUSBOpenDeviceWithVidPid[vid_Integer/;0<=vid<2^16,
 						pid_Integer/;0<=pid<2^16]:=
 					LibUSBOpenDeviceWithVidPid[0,vid,pid];

void libusbGetDeviceBusAndAddressByName(const char *);

:Begin:
:Function:		libusbGetDeviceBusAndAddressByName
:Pattern:		LibUSBGetDeviceBusAndAddressByName[devName_String]
:Arguments:		{ devName }
:ArgumentTypes:	{ String }
:ReturnType:	Manual
:End:

void libusbOpenDeviceByAddress(mlint64,int,int);

:Begin:
:Function:		libusbOpenDeviceByAddress
:Pattern:		LibUSBOpenDeviceByAddress[ctxRef_Integer,bus_Integer/;0<=bus<2^8,
					devAddr_Integer/;0<=devAddr<2^8]
:Arguments:		{ ctxRef, bus, devAddr }
:ArgumentTypes:	{ Integer64, Integer, Integer }
:ReturnType:	Manual
:End:

:Evaluate:		LibUSBOpenDeviceByAddress[Null,bus_Integer/;0<=bus<2^8,
						devAddr_Integer/;0<=devAddr<2^8]:=
 					LibUSBOpenDeviceByAddress[0,bus,devAddr];
:Evaluate:		LibUSBOpenDeviceByAddress[bus_Integer/;0<=bus<2^8,
						devAddr_Integer/;0<=devAddr<2^8]:=
 					LibUSBOpenDeviceByAddress[0,bus,devAddr];

void libusbClose(mlint64);

:Begin:
:Function:		libusbClose
:Pattern:		LibUSBClose[devHandleRef_Integer]
:Arguments:		{ devHandleRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void libusbGetDevice(mlint64);

:Begin:
:Function:		libusbGetDevice
:Pattern:		LibUSBGetDevice[devHandleRef_Integer]
:Arguments:		{ devHandleRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void libusbGetConfiguration(mlint64);

:Begin:
:Function:		libusbGetConfiguration
:Pattern:		LibUSBGetConfiguration[devHandleRef_Integer]
:Arguments:		{ devHandleRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void libusbSetConfiguration(mlint64,int);

:Begin:
:Function:		libusbSetConfiguration
:Pattern:		LibUSBSetConfiguration[devHandleRef_Integer,config_Integer]
:Arguments:		{ devHandleRef, config }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void libusbClaimInterface(mlint64,int);

:Begin:
:Function:		libusbClaimInterface
:Pattern:		LibUSBClaimInterface[devHandleRef_Integer,interfaceNumber_Integer]
:Arguments:		{ devHandleRef, interfaceNumber }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void libusbReleaseInterface(mlint64,int);

:Begin:
:Function:		libusbReleaseInterface
:Pattern:		LibUSBReleaseInterface[devHandleRef_Integer,interfaceNumber_Integer]
:Arguments:		{ devHandleRef, interfaceNumber }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void libusbSetInterfaceAltSetting(mlint64,int,int);

:Begin:
:Function:		libusbSetInterfaceAltSetting
:Pattern:		LibUSBSetInterfaceAltSetting[devHandleRef_Integer,
					interfaceNumber_Integer,alternateSetting_Integer]
:Arguments:		{ devHandleRef, interfaceNumber, alternateSetting }
:ArgumentTypes:	{ Integer64, Integer, Integer }
:ReturnType:	Manual
:End:

void libusbClearHalt(mlint64,int);

:Begin:
:Function:		libusbClearHalt
:Pattern:		LibUSBClearHalt[devHandleRef_Integer,
					endpoint_Integer/;0<=endpoint<2^8]
:Arguments:		{ devHandleRef, endpoint }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void libusbResetDevice(mlint64);

:Begin:
:Function:		libusbResetDevice
:Pattern:		LibUSBResetDevice[devHandleRef_Integer]
:Arguments:		{ devHandleRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void libusbKernelDriverActive(mlint64,int);

:Begin:
:Function:		libusbKernelDriverActive
:Pattern:		LibUSBKernelDriverActive[devHandleRef_Integer,interfaceNumber_Integer]
:Arguments:		{ devHandleRef, interfaceNumber }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void libusbDetachKernelDriver(mlint64,int);

:Begin:
:Function:		libusbDetachKernelDriver
:Pattern:		LibUSBDetachKernelDriver[devHandleRef_Integer,interfaceNumber_Integer]
:Arguments:		{ devHandleRef, interfaceNumber }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void libusbAttachKernelDriver(mlint64,int);

:Begin:
:Function:		libusbAttachKernelDriver
:Pattern:		LibUSBAttachKernelDriver[devHandleRef_Integer,interfaceNumber_Integer]
:Arguments:		{ devHandleRef, interfaceNumber }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void libusbGetCapabilities(void);

:Begin:
:Function:		libusbGetCapabilities
:Pattern:		LibUSBGetCapabilities[]
:Arguments:		{ }
:ArgumentTypes:	{ }
:ReturnType:	Manual
:End:

void libusbErrorName(int);

:Begin:
:Function:		libusbErrorName
:Pattern:		LibUSBErrorName[errorCode_Integer]
:Arguments:		{ errorCode }
:ArgumentTypes:	{ Integer }
:ReturnType:	Manual
:End:

void libusbGetVersion(void);

:Begin:
:Function:		libusbGetVersion
:Pattern:		LibUSBGetVersion[]
:Arguments:		{ }
:ArgumentTypes:	{ }
:ReturnType:	Manual
:End:

void libusbGetDeviceDescriptor(mlint64);

:Begin:
:Function:		libusbGetDeviceDescriptor
:Pattern:		LibUSBGetDeviceDescriptor[devRef_Integer]
:Arguments:		{ devRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void libusbGetActiveConfigDescriptor(mlint64);

:Begin:
:Function:		libusbGetActiveConfigDescriptor
:Pattern:		LibUSBGetActiveConfigDescriptor[devRef_Integer]
:Arguments:		{ devRef }
:ArgumentTypes:	{ Integer64 }
:ReturnType:	Manual
:End:

void libusbGetConfigDescriptor(mlint64,int);

:Begin:
:Function:		libusbGetConfigDescriptor
:Pattern:		LibUSBGetConfigDescriptor[devRef_Integer,
					configIndex_Integer/;0<=configIndex<2^8]
:Arguments:		{ devRef, configIndex }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void libusbGetConfigDescriptorByValue(mlint64,int);

:Begin:
:Function:		libusbGetConfigDescriptorByValue
:Pattern:		LibUSBGetConfigDescriptorByValue[devRef_Integer,
					configValue_Integer/;0<=configValue<2^8]
:Arguments:		{ devRef, configValue }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void libusbGetStringDescriptorASCII(mlint64,int);

:Begin:
:Function:		libusbGetStringDescriptorASCII
:Pattern:		LibUSBGetStringDescriptorASCII[devHandleRef_Integer,
					descIndex_Integer/;0<=descIndex<2^8]
:Arguments:		{ devRef, descIndex }
:ArgumentTypes:	{ Integer64, Integer }
:ReturnType:	Manual
:End:

void libusbGetDescriptor(mlint64,int,int);

:Begin:
:Function:		libusbGetDescriptor
:Pattern:		LibUSBGetDescriptor[devHandleRef_Integer,
					descType_Integer/;0<=descType<2^8,
					descIndex_Integer/;0<=descIndex<2^8]
:Arguments:		{ devRef, descType, descIndex }
:ArgumentTypes:	{ Integer64, Integer, Integer }
:ReturnType:	Manual
:End:

void libusbGetStringDescriptor(mlint64,int, int);

:Begin:
:Function:		libusbGetStringDescriptor
:Pattern:		LibUSBGetStringDescriptor[devHandleRef_Integer,
					descIndex_Integer/;0<=descIndex<2^8,
					langId_Integer/;0<=langId<2^16]
:Arguments:		{ devRef, descIndex, langId }
:ArgumentTypes:	{ Integer64, Integer, Integer }
:ReturnType:	Manual
:End:

void libusbControlTransfer(mlint64,int,int,int,int,float);

:Begin:
:Function:		libusbControlTransfer
:Pattern:		LibUSBControlTransfer[devHandleRef_Integer,
					bmRequestType_Integer/;0<=bmRequestType<2^8,
					bRequest_Integer/;0<=bRequest<2^8,
					wValue_Integer/;0<=wValue<2^16,
					wIndex_Integer/;0<=wIndex<2^16,
					timeout_Real?NonNegative,
					dataOrLength:_String|(_Integer?(0<=#<2^16&))]
:Arguments:		{ devHandleRef, bmRequestType, bRequest, wValue, wIndex, timeout,
					dataOrLength }
:ArgumentTypes:	{ Integer64, Integer, Integer, Integer, Integer, Real32, Manual }
:ReturnType:	Manual
:End:

void libusbBulkTransfer(mlint64,int,float);

:Begin:
:Function:		libusbBulkTransfer
:Pattern:		LibUSBBulkTransfer[devHandleRef_Integer,
					endpoint_Integer/;0<=endpoint<2^8,
					timeout_Real?NonNegative,
					dataOrLength:_String|_Integer?NonNegative]
:Arguments:		{ devHandleRef, endpoint, timeout, dataOrLength }
:ArgumentTypes:	{ Integer64, Integer, Real32, Manual }
:ReturnType:	Manual
:End:

void libusbInterruptTransfer(mlint64,int,float);

:Begin:
:Function:		libusbInterruptTransfer
:Pattern:		LibUSBInterruptTransfer[devHandleRef_Integer,
					endpoint_Integer/;0<=endpoint<2^8,
					timeout_Real?NonNegative,
					dataOrLength:_String|_Integer?NonNegative]
:Arguments:		{ devHandleRef, endpoint, timeout, dataOrLength }
:ArgumentTypes:	{ Integer64,  Integer, Real32,	Manual }
:ReturnType:	Manual
:End:
