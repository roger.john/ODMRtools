#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <gpib/ib.h>
#include "MathLinkCommon.h"

int gpibOpenDevice(int board, int pad, int sad) {
	return ibdev(board, pad, sad, T3s, 1, 0x0A);
}

int gpibFindDevice(const char *name) {
	return ibfind(name);
}

int gpibCloseDevice(int dev) {
	return ibonl(dev, 0);
}

int gpibResetDeviceParams(int dev) {
	return ibonl(dev, 1);
}

int gpibSetEOS(int dev, int eos) {
	return ibeos(dev, eos);
}

int gpibSetEOT(int dev, int eot) {
	return ibeot(dev, eot);
}

int gpibSetTimeout(int dev, int timeout) {
	return ibtmo(dev, timeout);
}

int gpibWait(int dev, int mask) {
	return ibwait(dev, mask);
}

int gpibWaitForSRQ(int dev) {
	short r;
	WaitSRQ(dev, &r);
	return r;
}

int gpibTestSRQ(int dev) {
	return ibwait(dev, 0) & SRQI ? 1 : 0;
}

int gpibReset(int board) {
	return ibsic(board);
}

int gpibClearInterface(int board) {
	return ibsic(board);
}

int gpibClearDevice(int dev) {
	return ibclr(dev);
}

int gpibTriggerDevice(int dev) {
	return ibtrg(dev);
}

int gpibEnableRemoteMode(int board) {
	return ibsre(board, 1);
}

int gpibDisableRemoteMode(int board) {
	return ibsre(board, 0);
}

int gpibStatus(void) {
	return ThreadIbsta();
}

int gpibError(void) {
	return ThreadIberr();
}

int gpibCount(void) {
	return ThreadIbcnt();
}

int gpibSend(int dev) {
	const unsigned char *data;
	int len;
	if (!MLGetByteString(stdlink, &data, &len, 0)) {
		failWithMsgAndError(stdlink, "gpibSend:data:error: ");
		return 0;
	}
	int r = ibwrt(dev, data, len);
	MLReleaseByteString(stdlink,data,len);
	return r;
}

void gpibReceive(int dev) {
	unsigned char *data;
	int len;
	if (!MLGetInteger(stdlink, &len) || len < 0) {
		failWithMsgAndError(stdlink, "gpibReceive:bufferSize:error: ");
		return;
	}
	data = malloc(len);
	memset(data, 0, len);
	ibrd(dev, data, len);
	len = ThreadIbcnt();
	if (!MLPutByteString(stdlink, data, len)) {
		free(data);
		failWithMsgAndError(stdlink, "gpibReceive:put:error: ");
		return;
	}
	free(data);
}
