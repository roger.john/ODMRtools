#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "MathLinkCommon.h"

void abortWithMsg(MLINK ml, const char *msgId) {
	char err_msg[100];
	sprintf(err_msg, "Message[%s,\"%.76s\"]", msgId, MLErrorMessage(ml));
	MLClearError(ml);
	MLNewPacket(ml);
	MLEvaluate(ml, err_msg);
	MLNextPacket(ml);
	MLNewPacket(ml);
	MLPutSymbol(ml, "$Failed");
}

void failWithMsg(MLINK ml, const char *msg) {
	MLClearError(ml);
	MLNextPacket(ml);
	MLNewPacket(ml);
	if (msg) {
		MLPutFunction(ml, "$Failed", 1);
		MLPutString(ml, msg);
	} else
		MLPutSymbol(ml, "$Failed");
}

void failWithMsgAndError(MLINK ml, const char *msg) {
	if (!msg)
		msg = "";
	char *tmp = malloc(strlen(msg) + 500);
	snprintf(tmp, 1000, "%s%i: %s", msg, MLError(ml), MLErrorMessage(ml));
	MLClearError(ml);
	MLNextPacket(ml);
	MLNewPacket(ml);
	MLPutFunction(ml, "$Failed", 1);
	MLPutString(ml, tmp);
	free(tmp);
}
