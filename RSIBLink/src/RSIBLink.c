//============================================================================
// Name        : RSIBLink.cpp
// Author      : RJ
// Version     :
// Copyright   : Your copyright notice
// Description : RSIB MathLink interface
//============================================================================

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <math.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <mathlink.h>
#include "MathLinkCommon.h"
#include "RSIBLinkML.tmc"

int sendToSocket(int sock, char *buf, int len) {
	int r = 0, s;
	while (r < len) {
		s = send(sock, buf + r, len - r, 0);
		if (s < 0) {
			if (errno == EAGAIN)
				break;
			else
				return s;
		} else if (s == 0)	// timeout
			break;
		r += s;
	}
	return r;
}
int recvFromSocket(int sock, char *buf, int minLen, int maxLen) {
	int r = 0, s;
	while (r < minLen) {
		s = recv(sock, buf + r, maxLen - r, 0);
		if (s < 0) {
			if (errno == EAGAIN)
				break;
			else
				return s;
		} else if (s == 0)	// timeout
			break;
		r += s;
	}
	return r;
}
#define MSG_CMDREP_INCLOMPETE	0x00	// more data to come
#define MSG_HELLO 				0x40	// end this on connect
#define MSG_CMDREP				0x80	// Reply from the instrument
#define MSG_CMD					0x90	// Command to the instrument
#define MSG_SRQ 				0x91	// Query SRQ

struct RSIBconnection {
	int sockfd[2];
};
void RSIBopen(const char *host, int port) {
	char buf[16];
	struct addrinfo *deviceAddrInfo;
	snprintf(buf, sizeof(buf), "%d", port);
	int r = getaddrinfo(host, buf, NULL, &deviceAddrInfo);
	if (r || !deviceAddrInfo) {
		if (!MLPutFunction(stdlink, "$Failed", 2)
				|| !MLPutString(stdlink, "host lookup failed")
				|| !MLPutInteger(stdlink, r))
			failWithMsg(stdlink, "RSIBopen:lockupFailed:error: ");
		return;
	}
	int sockfd1 = socket(AF_INET, SOCK_STREAM, 0), sockfd2;
	if (sockfd1 < 0) {
		if (!MLPutFunction(stdlink, "$Failed", 1)
				|| !MLPutString(stdlink, "could not open socket"))
			failWithMsg(stdlink, "RSIBopen:socketOpenFailed:error: ");
		return;
	}
	r = connect(sockfd1, deviceAddrInfo->ai_addr, deviceAddrInfo->ai_addrlen);
	if (r < 0) {
		close(sockfd1);
		if (!MLPutFunction(stdlink, "$Failed", 2)
				|| !MLPutString(stdlink, "could not connect")
				|| !MLPutInteger(stdlink, r))
			failWithMsg(stdlink, "RSIBopen:connectFailed:error: ");
		return;
	}
	memset(buf, 0, 3);
	buf[3] = MSG_HELLO;
	if ((r = sendToSocket(sockfd1, buf, 4)) < 0) {
		close(sockfd1);
		if (!MLPutFunction(stdlink, "$Failed", 2)
				|| !MLPutString(stdlink, "initial send failed")
				|| !MLPutInteger(stdlink, r))
			failWithMsg(stdlink, "RSIBopen:sendFailed:error: ");
		return;
	}
	if ((r = recvFromSocket(sockfd1, buf, 8, 8)) < 0) {
		close(sockfd1);
		if (!MLPutFunction(stdlink, "$Failed", 2)
				|| !MLPutString(stdlink, "initial receive failed")
				|| !MLPutInteger(stdlink, r))
			failWithMsg(stdlink, "RSIBopen:recvFailed:error: ");
		return;
	}
	sockfd2 = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd2 < 0) {
		close(sockfd1);
		if (!MLPutFunction(stdlink, "$Failed", 1)
				|| !MLPutString(stdlink, "could not open second socket"))
			failWithMsg(stdlink, "RSIBopen:socket2OpenFailed:error: ");
		return;
	}
	r = connect(sockfd2, deviceAddrInfo->ai_addr, deviceAddrInfo->ai_addrlen);
	if (r < 0) {
		close(sockfd1);
		close(sockfd2);
		if (!MLPutFunction(stdlink, "$Failed", 2)
				|| !MLPutString(stdlink, "could not connect second socket")
				|| !MLPutInteger(stdlink, r))
			failWithMsg(stdlink, "RSIBopen:connect2Failed:error: ");
		return;
	}
	if ((r = sendToSocket(sockfd2, buf + 4, 4)) < 0) {
		close(sockfd1);
		close(sockfd2);
		if (!MLPutFunction(stdlink, "$Failed", 2)
				|| !MLPutString(stdlink, "second send failed")
				|| !MLPutInteger(stdlink, r))
			failWithMsg(stdlink, "RSIBopen:send2Failed:error: ");
		return;
	}
	if (!MLPutFunction(stdlink, "RSIBconnection", 2)
			|| !MLPutInteger(stdlink, sockfd1)
			|| !MLPutInteger(stdlink, sockfd2)) {
		close(sockfd1);
		close(sockfd2);
		failWithMsg(stdlink, "RSIBopen:return:error: ");
	}
}
int RSIBparseConnection(struct RSIBconnection *conn) {
	long int argc;
	if (!conn)
		return -1;
	if (!MLCheckFunction(stdlink, "RSIBconnection", &argc)) {
		failWithMsg(stdlink, "RSIBparseConnection:head:error: ");
		return -1;
	}
	if (argc == 2) {
		for (int i = 0; i < 2; i++)
			if (!MLGetInteger(stdlink, &conn->sockfd[i])) {
				failWithMsg(stdlink, "RSIBparseConnection:socketList:error: ");
				return -1;
			}
		return 0;
	} else {
		if (!MLPutFunction(stdlink, "$Failed", 1)
				|| !MLPutString(stdlink, "connection has wrong format"))
			failWithMsg(stdlink, "RSIBparseConnection:header:error: ");
		return -1;
	}
}
void RSIBclose(void) {
	struct RSIBconnection conn;
	if (RSIBparseConnection(&conn) < 0)
		return;
	for (int i = 0; i < 2; i++)
		close(conn.sockfd[i]);
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsg(stdlink, "RSIBclose:return:error: ");
}
void RSIBsetTimeout(void) {
	struct RSIBconnection conn;
	if (RSIBparseConnection(&conn) < 0)
		return;
	double t;
	if (!MLGetReal(stdlink, &t)) {
		failWithMsg(stdlink, "RSIBsetTimeout:timeout:error: ");
		return;
	}
	struct timeval timeout;
	timeout.tv_sec = (long int) round(t);
	timeout.tv_usec = (long int) round((t - round(t)) * 1e6);
	int r;
	if ((r = setsockopt(conn.sockfd[0], SOL_SOCKET, SO_RCVTIMEO,
			(char*) &timeout, sizeof(struct timeval))) < 0) {
		if (!MLPutFunction(stdlink, "$Failed", 2)
				|| !MLPutString(stdlink, "could not set receive timeout")
				|| !MLPutInteger(stdlink, r))
			failWithMsg(stdlink, "RSIBsetTimeout:setRecvTimeoutFailed:error: ");
		return;
	}
	if ((r = setsockopt(conn.sockfd[0], SOL_SOCKET, SO_SNDTIMEO,
			(char*) &timeout, sizeof(struct timeval))) < 0) {
		if (!MLPutFunction(stdlink, "$Failed", 2)
				|| !MLPutString(stdlink, "could not set send timeout")
				|| !MLPutInteger(stdlink, r))
			failWithMsg(stdlink, "RSIBsetTimeout:setSendTimeoutFailed:error: ");
		return;
	}
	if (!MLPutSymbol(stdlink, "Null"))
		failWithMsg(stdlink, "RSIBsetTimeout:return:error: ");
}
void RSIBwrite(void) {
	struct RSIBconnection conn;
	if (RSIBparseConnection(&conn) < 0)
		return;
	const unsigned char *data;
	int len, tag;
	if (!MLGetByteString(stdlink, &data, &len, 0l))
		failWithMsg(stdlink, "RSIBwrite:data:error: ");
	if (!MLGetInteger(stdlink, &tag))
		failWithMsg(stdlink, "RSIBwrite:tag:error: ");
	char *buffer = malloc(7 + len);
	if (!buffer) {
		MLReleaseByteString(stdlink, data, len);
		if (!MLPutFunction(stdlink, "$Failed", 1)
				|| !MLPutString(stdlink, "could not allocate buffer"))
			failWithMsg(stdlink, "RSIBwrite:buffer:error: ");
		return;
	}
	*(uint32_t *) buffer = htonl(len);
	buffer[4] = MSG_CMD;
	buffer[5] = 0x00;
	buffer[6] = tag & 0xFF;
	memcpy(buffer + 7, data, len);
	MLReleaseByteString(stdlink, data, len);
	int r = sendToSocket(conn.sockfd[0], buffer, len + 7);
	free(buffer);
	if (r < 0) {
		if (!MLPutFunction(stdlink, "$Failed", 2)
				|| !MLPutString(stdlink, "write failed")
				|| !MLPutInteger(stdlink, errno))
			failWithMsg(stdlink, "RSIBwrite:writeFailed:error: ");
	} else if (r < len + 7) {
		if (!MLPutFunction(stdlink, "$Failed", 2)
				|| !MLPutString(stdlink, "write timed out")
				|| !MLPutInteger(stdlink, r))
			failWithMsg(stdlink, "RSIBwrite:writeTimedOut:error: ");
	} else {
		if (!MLPutFunction(stdlink, "List", 2) || !MLPutInteger(stdlink, tag)
				|| !MLPutInteger(stdlink, r))
			failWithMsg(stdlink, "RSIBwrite:Return:error: ");
	}
}
struct RSIBresponse {
	uint32_t dataLen;
	char status;
	char zero;
	char tag;
	char *data;
	struct RSIBresponse *next;
};
struct RSIBresponse *RSIBreadResponse(struct RSIBconnection *conn) {
	char header[7];
	int r = recvFromSocket(conn->sockfd[0], header, 7, 7);
	if (r < 0) {
		if (!MLPutFunction(stdlink, "$Failed", 2)
				|| !MLPutString(stdlink, "could not receive header")
				|| !MLPutInteger(stdlink, errno))
			failWithMsg(stdlink,
					"RSIBreadResponse:receiveHeaderFailed:error: ");
		return NULL;
	} else if (r < 7) {
		if (!MLPutFunction(stdlink, "$Failed", 2)
				|| !MLPutString(stdlink, "header receive timed out")
				|| !MLPutInteger(stdlink, r))
			failWithMsg(stdlink,
					"RSIBreadResponse:receiveHeaderTimedOut:error: ");
		return NULL;
	}
	struct RSIBresponse *resp = (struct RSIBresponse *) malloc(
			sizeof(struct RSIBresponse));
	resp->dataLen = ntohl(*(uint32_t *) header);
	resp->status = header[4];
	resp->zero = header[5];
	resp->tag = header[6];
	resp->data = malloc(resp->dataLen);
	resp->next = NULL;
	if (resp->data == NULL) {
		if (!MLPutFunction(stdlink, "$Failed", 1)
				|| !MLPutString(stdlink, "could not allocate buffer"))
			failWithMsg(stdlink, "RSIBreadResponse:buffer:error: ");
		return NULL;
	}
	r = recvFromSocket(conn->sockfd[0], resp->data, resp->dataLen,
			resp->dataLen);
	if (r < 0) {
		free(resp->data);
		free(resp);
		if (!MLPutFunction(stdlink, "$Failed", 2)
				|| !MLPutString(stdlink, "could not receive data")
				|| !MLPutInteger(stdlink, errno))
			failWithMsg(stdlink, "RSIBreadResponse:receiveDataFailed:error: ");
		return NULL;
	} else if (r < resp->dataLen) {
		free(resp->data);
		free(resp);
		if (!MLPutFunction(stdlink, "$Failed", 2)
				|| !MLPutString(stdlink, "data receive timed out")
				|| !MLPutInteger(stdlink, r))
			failWithMsg(stdlink,
					"RSIBreadResponse:receiveDataTimedOut:error: ");
		return NULL;
	}
	return resp;
}
void freeRSIBresponseList(struct RSIBresponse *resp) {
	while (resp != NULL) {
		struct RSIBresponse *t = resp->next;
		if (resp->data != NULL)
			free(resp->data);
		free(resp);
		resp = t;
	}
}
void RSIBread(void) {
	struct RSIBconnection conn;
	if (RSIBparseConnection(&conn) < 0)
		return;
	int tag, r;
	if (!MLGetInteger(stdlink, &tag)) {
		failWithMsg(stdlink, "RSIBwrite:tag:error: ");
		return;
	}
	struct RSIBresponse *respHead, *resp;
	respHead = RSIBreadResponse(&conn);
	if (respHead == NULL)
		return;
	resp = respHead;
	while (resp->status == MSG_CMDREP_INCLOMPETE) {
		resp->next = RSIBreadResponse(&conn);
		if (resp->next == NULL) {
			free(respHead);
			return;
		}
		resp = resp->next;
	}
	uint32_t lenSum = respHead->dataLen;
	resp = respHead;
	while (resp->next != NULL) {
		resp = resp->next;
		lenSum += resp->dataLen;
	}
	unsigned char *buffer = malloc(lenSum);
	if (buffer == NULL) {
		freeRSIBresponseList(respHead);
		if (!MLPutFunction(stdlink, "$Failed", 1)
				|| !MLPutString(stdlink, "could not allocate buffer"))
			failWithMsg(stdlink, "RSIBread:buffer:error: ");
		return;
	}
	memcpy(buffer, respHead->data, respHead->dataLen);
	r = respHead->dataLen;
	resp = respHead;
	while (resp->next != NULL) {
		resp = resp->next;
		memcpy(buffer + r, resp->data, resp->dataLen);
		r += resp->dataLen;
	}
	if (resp->status != (char) (MSG_CMDREP) || resp->zero != 0
			|| resp->tag != (char) (tag & 0xFF)) {
		if (!MLPutFunction(stdlink, "List", 2)
				|| !MLPutFunction(stdlink, "List", 3)
				|| !MLPutInteger(stdlink, resp->status)
				|| !MLPutInteger(stdlink, resp->zero)
				|| !MLPutInteger(stdlink, resp->tag))
			failWithMsg(stdlink, "RSIBread:return:header:error: ");
	}
	if (!MLPutByteString(stdlink, buffer, lenSum))
		failWithMsg(stdlink, "RSIBread:return:error: ");
	free(buffer);
	freeRSIBresponseList(respHead);
}
