/*
 * main.c
 *
 *  Created on: 13.02.2016
 *      Author: rj
 */
#include <stdlib.h>
#include <mathlink.h>

int sargc;
char **sargv;

int main(int argc, char **argv) {
	sargc = argc;
	sargv = argv;
	return MLMain(argc, argv);
}


