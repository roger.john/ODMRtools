
void RSIBOpen(const char*,int);

:Begin:
:Function:       RSIBopen
:Pattern:        RSIBopen[host_String, port_Integer]
:Arguments:      { host, port}
:ArgumentTypes:  { String, Integer }
:ReturnType:     Manual
:End:

void RSIBclose(void);

:Begin:
:Function:       RSIBclose
:Pattern:        RSIBclose[conn:RSIBconnection[_Integer,_Integer]]
:Arguments:      { conn }
:ArgumentTypes:  { Manual }
:ReturnType:     Manual
:End:

void RSIBsetTimeout(void);

:Begin:
:Function:       RSIBsetTimeout
:Pattern:        RSIBsetTimeout[conn:RSIBconnection[_Integer,_Integer],
					t_Real?Positive]
:Arguments:      { conn, t }
:ArgumentTypes:  { Manual }
:ReturnType:     Manual
:End:

void RSIBwrite(void);

:Begin:
:Function:       RSIBwrite
:Pattern:        RSIBwrite[conn:RSIBconnection[_Integer,_Integer],
					data_String, tag_Integer:0]
:Arguments:      { conn, data, tag}
:ArgumentTypes:  { Manual }
:ReturnType:     Manual
:End:

void RSIBread(void);

:Begin:
:Function:       RSIBread
:Pattern:        RSIBread[conn:RSIBconnection[_Integer,_Integer], tag_Integer:0]
:Arguments:      { conn, tag}
:ArgumentTypes:  { Manual }
:ReturnType:     Manual
:End:
