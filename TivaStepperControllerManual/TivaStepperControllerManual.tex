%%This is a very basic article template.
%%There is just one section and two subsections.
\documentclass{article}
\usepackage{cmap}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[babel, german=quotes]{csquotes}
\usepackage{comment}
\usepackage{amsmath}
\usepackage{mathrsfs}
%\usepackage{mathabx}
\usepackage{dsfont}
%\usepackage{mathptmx
\usepackage[scaled=.90]{helvet}
\usepackage{courier}
\usepackage{printlen}
\usepackage{curve2e}
\usepackage{graphicx}
\usepackage{rotating}
\usepackage[T1]{fontenc}
\usepackage{textcomp,gensymb}
%\usepackage[version=3]{mhchem}
\usepackage{upgreek}
\usepackage[separate-uncertainty = false, bracket-numbers = false,
  table-align-uncertainty = false, table-align-exponent=false,
  range-phrase = \ to\ ,list-final-separator = \ and\ ,
  redefine-symbols = true]{siunitx}
\sisetup{
	detect-all,
 	math-micro = {\mathsf{\upmu}},
%  	text-micro = {\textsf{\textmu}}
  	text-micro = {$\mathsf{\upmu}$}
}
\usepackage{hyperref}

%
\begin{document}
\title{Tiva Stepper Controller Manual}

\section{Hardware}
The \href{http://www.ti.com/tool/EK-TM4C1294XL}{TM4C1294 Connected
LaunchPad} is a low cost microcontroller development platform with enough
processing power to be used as a six axis variabel speed stepper
controller. Note that this is a \SI{3.3}{V} controller whereas stepper
drivers have \SI{5}{V} opto-isolated interfaces, thus a level converter
like the 74HCT244 is needed.
Clock rate is \SI{120}{MHz}. Used pins are (external pin names in
parenthesis):
\begin{itemize}
  \item PE0 (X8-6), PE1 (X8-8), PE2 (X8-10), PE3 (X8-12), PE4 (X8-3), PE5
  (X8-11):
  ``direction'' pins for axis 0 to 5.
  \item PL0 (X9-13), PL1 (X9-15), PL2 (X9-17), PL3 (X9-19), PL4 (X9-9), PL5
  (X9-11):
  ``enable'' pins for axis 0 to 5.
  \item PD0 (X9-14), PD2 (X6-3), PA4 (X6-18), PA6 (X8-16), PM4 (X8-18), PM6
  (X7-20): ``step'' pins  for axis 0 to 5.
  \item PD1 (X9-12), PD3 (X8-13), PA5 (X6-20), PA7 (X7-8), PM5 (X8-20), PM7
  (X7-4): ``step-detection'' pins  for axis 0 to 5.
  \item PK0 (X6-10), PK2 (X6-14), PK4 (X7-3), PK6 (X7-17), PM0 (X7-7), PM2
  (X7-11): The lower stopping pins for axis 0 to 5.
  \item PK1 (X6-12), PK3 (X6-16), PK5 (X7-5), PK7 (X7-19), PM1 (X7-9), PM3
  (X9-4): The higher stopping pins for axis 0 to 5.
  \item PP0 (X6-5), PP1 (X6-7), PP2 (X9-20), PP3 (X7-16), PP4 (X6-15), PP5
  (X7-6): Error dectection pins for axis 0 to 5. High means error, pullup by
  default. Short to ground if not needed.
  \item PC4 (X8-5), PC5 (X8-7), PC6 (X8-9), PC7 (X8-15), PB2 (X8-17), PB3
  (X8-19): Inphase encoder pins for axis 0 to 5. Can be left floating or
  grounded if not used.
  \item  PH0 (X7-13), PH1 (X7-15), PH2 (X9-6), PH3 (X9-8), PG0 (X9-7), PG1
  (X7-1): Quadrature encoder pins for axis 0 to 5. Can be left floating or
  grounded if not used.
  \item GND (X6-4, X7-2, X8-4, X9-2): Ground potential.
  \item +3V3 (X6-1, X8-1): \SI{3.3}{V} supply for the stop switches
  \item +5V (X6-2, X8-2): \SI{5}{V} supply for the level converters.
  \textbf{WARNING: DO NOT USE THIS FOR STOPPING SWITCHES!}
\end{itemize}

\section{Communication}
The Tiva Launchpad features usb debug and device ports and an ethernet
interface. The debug interface (``In-Circuit Debug Interface'') also
provides a serial connection with \SI{115200}{bps} that shows up as a
virtual com port on the computer. This interface is currently used as the
command interface for stepping control using the commands described in the
next section. Control over ethernet can also be implemented. On startup,
the controller sends ``Tiva Stepper controller starting...'', and continues
``done<LF>'' if startup initialization succeeds. Afterwards, it will only
send if requested.

\subsection{Commands}
\newcommand{\nt}[1]{\langle \text{#1} \rangle}
\newcommand{\tm}[1]{\text{`#1'}}
\newcommand{\opt}[1]{\left[\ #1 \ \right]}
Each command consists of a command letter, an optional axis designator,
an optional register id, and an optional numeric decimal integer value,
terminated by a carriage-return.
In BNF it looks like this, where $\opt{\cdot}$ means optional and $\vert$
separates alternatives:
\begin{align*}
	\nt{command} &::= \nt{command-letter}\ \opt{\nt{axis}\
	\opt{\nt{register}}\ \opt{\nt{value}}}\ \nt{terminator}\\
	\nt{command-letter} &::= \text{one of}\
		\left\{\tm{!},\tm{R}, \tm{S},
			\tm{a},\tm{g},\tm{h},\tm{j},\tm{p},\tm{r},\tm{s},\tm{t}\right\}\\
	\nt{axis} &::= \text{one of}\ \left\{\tm{0},\ldots,\tm{5}\right\}\\
	\nt{register} &::= \text{one of}\
		\left\{\tm{0},\ldots,\tm{9},\tm{A},\ldots,\tm{K}\right\}\\
	\nt{value} &::= \opt{\tm{+}\ \middle\vert\ \tm{-}}\ \nt{digits}\\
	\nt{digits} &::= \nt{digit}\ \vert\ \nt{digit}\ \nt{digits}\\
	\nt{digit} &::= \text{one of}\ \left\{\tm{0},\ldots,\tm{9}\right\}\\
	\nt{terminator} &::= \tm{<CR>}\ \tm{<LF>}\ \vert\ \tm{<CR>}\ \vert\
		\tm{<LF>}\ \vert\ \tm{<ESC>}
\end{align*}
Answers from the controller are terminated by \tm{<LF>}.
The following commands take the parameters given in parenthesis:
\begin{itemize}
  \item \tm{!}\ (): Stops all motion processes on the controller.
  \item \tm{P}\ (): This command is ignored even when prefixed to other
  commands unless homing is performed.
  \item \tm{R}\ (): Resets the controller.
  \item \tm{S}\	($\nt{axis}$): Immediately stop movements on $\nt{axis}$.
  \item \tm{a}\ ($\nt{axis}$,$\nt{value}$): Move $\nt{axis}$ to the position
  given by $\nt{value}$.
  \item \tm{g}\ ($\nt{axis}$,$\nt{register}$): Get the content of
  $\nt{register}$ on $\nt{axis}$.
  \item \tm{h}\ ($\nt{axis}$,$\nt{value}$): Perform a homing movement on
  $\nt{axis}$. $\nt{value}$ is optional and actually encodes two
  parameters: The sign (also \tm{-0}) gives the direction of the homing
  and defaults to \tm{-}, when no sign is given, searching the lowest
  position.
  The absolute value of $\nt{value}$ indicates whether the axis moves back to its
  previous position after homing if it is not zero.
  Returns either \[ \tm{HOMING\_DELTA: }\ \nt{value} \] where value is the
  difference of the new position to the one before performing homing. If homing
  was aborted by a forced stop (\tm{!}), an error message prefixed with
  $\tm{HOMING\_}$ is returned instead.
  While homing, the follwing single character commands are
  supported without termination:
  \begin{itemize}
    \item \tm{!}: Stops the homing process.
    \item \tm{P}: Get the current position of the currently homed axis.
    \item \tm{t}: Get the status of the controller (see below).
  \end{itemize}
  \itme \tm{j}\ ($\nt{axis}$): Jog $\nt{axis}$ with its currently set jogging
  velocity.
  \item \tm{p}\ ($\nt{axis}$): Get the current position of $\nt{axis}$.
  \item \tm{r}\ ($\nt{axis}$,$\nt{value}$): Move $\nt{axis}$ relatively by
  $\nt{value}$.
  \item \tm{s}\ ($\nt{axis}$,$\nt{register}$,$\nt{value}$): Set the
  content of $\nt{register}$ to $\nt{value}$.
  \item \tm{t}\ (): Get the status of the controller. Returns a terminated
  line of the following form:
  \begin{align*}
	\nt{status} &::= \nt{axis-status}\ \tm{,}\ \nt{axis-status}\
		\tm{,}\ \nt{axis-status}\ \tm{,}\ \nt{axis-status}\\
	\nt{axis-status} &::= \nt{axis-state}\ \nt{axis-direction}\
	\nt{axis-steps-left}\ \nt{axis-stopped}\ \nt{axis-error}\\
	\nt{axis-state} &::= \text{one of}\
	\left\{\tm{A},\tm{C},\tm{D},\tm{E},\tm{P},\tm{c},\tm{d},\tm{s},\tm{?}\right\}\\
	\nt{axis-direction} &::= \opt{\tm{+}\ \middle\vert\ \tm{-}}\\
	\nt{axis-steps-left} &::= \nt{digits}\\
	\nt{axis-stopped} &::= \opt{\tm{!}}\\
	\nt{axis-error} &::= \opt{\tm{\#}}
  \end{align*}
  \begin{itemize}
    \item $\nt{status}$ gives the state of all axes sequentially. 
  	\item The $\nt{axis-state}$ is encoded in the following way:
	  \begin{itemize}
	    \item \tm{s}: The axis is idling, ie. not moving.
	    \item \tm{E}: The axis is enabled, its direction is to be set.
	    \item \tm{P}: The axis is performing the first step of the current
	    move.
	    \item \tm{A}: The axis is accelerating.
	    \item \tm{C}: The axis is cruising at its target velocity.
	    \item \tm{D}: The axis is deccelerating.
	    \item \tm{c}: The axis is cruising the last steps of the current move
	    at its start velocity.
	    \item \tm{d}: The axis is being disabled.
	  \end{itemize}
	\item The $\nt{axis-direction}$ gives its last set direction, \tm{+} for
	increasing position counter, \tm{-} for decreasing.
	\item $\nt{axis-steps-left}$ is the current value of an count-down counter
	that is programmed with the number of steps at every move and counting the
	edges at its corresponding input pin, which is normally attached to step
	pin of the axis. The counter will force-stop the movement of the axis if
	it reaches zero, thus avoiding additional steps if the processor is
	too busy to catch up.
	\item $\nt{axis-stopped}$ indicates if the movement of the axis was
	force-stopped, either by command or by stopping switch.
	\item $\nt{axis-error}$ indicates if the axis has an error. In this case,
	movement of the axis is not possible.
  \end{itemize}
\end{itemize}
\subsection{Registers}
Each axis has its own set of registers. These are
accessible as given by the value in parenthesis in the following listing:
\begin{itemize}
  \item \tm{0}\ (rw): The current position.
  \item \tm{1}\ (rw): The starting or minimal velocity of every move in
  steps per second.
  \item \tm{2}\ (rw): The terminal or maximal velocity in steps per second.
  \item \tm{3}\ (rw): The acceleration in steps per square second.
  \item \tm{4}\ (rw): The home search velocity for coarse homing in steps
  per second.
  \item \tm{5}\ (rw): The home approach velocity for fine homing in steps
  per second.
  \item \tm{6}\ (r): The maximum step period as calculated by the minimal
  veloctiy in clock cycles.
  \item \tm{7}\ (r): The minimum step period as calculated by the maximal
  veloctiy in clock cycles.
  \item \tm{8}\ (r): The acceleration approximation data calculated from
  minimal and maximal velocity as well as acceleration, given back as a
  comma separated list of integers. Positive values indicate the decrease
  of the current step period in clock cylcles after one step, negative the
  number of clock cylcles before a decrement by one. The used value in the list
  is shifted by each doubling of the performed steps. Decceleration is mirrored.
  \item \tm{9}\ (r): The current step period in clock cylces.
  \item \tm{A}\ (r): The current step period change rate in clock cylces.
  \item \tm{B}\ (rw): If set to zero, honour stop switches (default), else
  ignore them. Returns $\nt{value},\nt{value},\nt{value}$ on request, first
  $\nt{value}$ indicating if stop switches are ignored, second and third
  the state of the first and second stop switch.
  \item \tm{C}\ (r): Remaining steps for current movement.
  \item \tm{D}\ (rw): Ignore error if not set to zero, else a high error pin
  will prevent further movements.
  \item \tm{E}\ (rw): the motor is only enabled for move commands if set to
  zero, for all other values it is always enabled.
  \item \tm{F}\ (rw): The delay time between enabling the motor and setting the
  direction in microseconds. Clipped between 0 and $\SI{139.81}{ms}$.
  \item \tm{G}\ (rw): The delay time between setting the
  direction and performing the first step in microseconds. Clipped between 0 and
  $\SI{139.81}{ms}$..
  \item \tm{H}\ (rw): The delay time between performing the last step and
  disabling the motor in microseconds. Clipped between 0 and $\SI{139.81}{ms}$.
  \item \tm{I}\ (rw): The current value of the associated encoder. This value
  is intended for external position verification and not used for movements.
  \item \tm{J}\ (rw): The target jogging velocity. If it is changed while the
  axis is in jog mode, it will change its velocity accordingly. 0 makes it
  deccelerate and stop.
  \item \tm{K}\ {r}: The current stepping velocity.
\end{itemize}

\end{document}
