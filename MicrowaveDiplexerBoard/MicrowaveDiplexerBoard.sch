EESchema Schematic File Version 4
LIBS:MicrowaveDiplexerBoard-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR0101
U 1 1 5AFE7D32
P 5600 3200
F 0 "#PWR0101" H 5600 2950 50  0001 C CNN
F 1 "GND" H 5605 3027 50  0000 C CNN
F 2 "" H 5600 3200 50  0001 C CNN
F 3 "" H 5600 3200 50  0001 C CNN
	1    5600 3200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J1
U 1 1 5AFE8975
P 2800 2450
F 0 "J1" H 2730 2688 50  0000 C CNN
F 1 "Conn_Coaxial" H 2730 2597 50  0000 C CNN
F 2 "Connect:SMA_THT_Jack_Straight" H 2800 2450 50  0001 C CNN
F 3 " ~" H 2800 2450 50  0001 C CNN
	1    2800 2450
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J2
U 1 1 5AFE8CA8
P 4950 2050
F 0 "J2" H 5049 2026 50  0000 L CNN
F 1 "Conn_Coaxial" H 5049 1935 50  0000 L CNN
F 2 "Connect:SMA_THT_Jack_Straight" H 4950 2050 50  0001 C CNN
F 3 " ~" H 4950 2050 50  0001 C CNN
	1    4950 2050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J3
U 1 1 5AFE8D5E
P 4950 2900
F 0 "J3" H 5049 2876 50  0000 L CNN
F 1 "Conn_Coaxial" H 5049 2785 50  0000 L CNN
F 2 "Connect:SMA_THT_Jack_Straight" H 4950 2900 50  0001 C CNN
F 3 " ~" H 4950 2900 50  0001 C CNN
	1    4950 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5AFE8F8A
P 3350 2200
F 0 "C2" H 3465 2246 50  0000 L CNN
F 1 "C" H 3465 2155 50  0000 L CNN
F 2 "MicrowaveDiplexerBoard:SMD_0402" H 3388 2050 50  0001 C CNN
F 3 "~" H 3350 2200 50  0001 C CNN
	1    3350 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5AFE90EC
P 3750 2200
F 0 "C4" H 3865 2246 50  0000 L CNN
F 1 "C" H 3865 2155 50  0000 L CNN
F 2 "MicrowaveDiplexerBoard:SMD_0402" H 3788 2050 50  0001 C CNN
F 3 "~" H 3750 2200 50  0001 C CNN
	1    3750 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5AFE9149
P 4150 2200
F 0 "C6" H 4265 2246 50  0000 L CNN
F 1 "C" H 4265 2155 50  0000 L CNN
F 2 "MicrowaveDiplexerBoard:SMD_0402" H 4188 2050 50  0001 C CNN
F 3 "~" H 4150 2200 50  0001 C CNN
	1    4150 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5AFE9183
P 4550 2200
F 0 "C8" H 4665 2246 50  0000 L CNN
F 1 "C" H 4665 2155 50  0000 L CNN
F 2 "MicrowaveDiplexerBoard:SMD_0402" H 4588 2050 50  0001 C CNN
F 3 "~" H 4550 2200 50  0001 C CNN
	1    4550 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5AFE91E8
P 3150 2900
F 0 "C1" V 2898 2900 50  0000 C CNN
F 1 "C" V 2989 2900 50  0000 C CNN
F 2 "MicrowaveDiplexerBoard:SMD_0402" H 3188 2750 50  0001 C CNN
F 3 "~" H 3150 2900 50  0001 C CNN
	1    3150 2900
	0    1    1    0   
$EndComp
$Comp
L Device:C C3
U 1 1 5AFE9347
P 3550 2900
F 0 "C3" V 3298 2900 50  0000 C CNN
F 1 "C" V 3389 2900 50  0000 C CNN
F 2 "MicrowaveDiplexerBoard:SMD_0402" H 3588 2750 50  0001 C CNN
F 3 "~" H 3550 2900 50  0001 C CNN
	1    3550 2900
	0    1    1    0   
$EndComp
$Comp
L Device:C C5
U 1 1 5AFE9378
P 3950 2900
F 0 "C5" V 3698 2900 50  0000 C CNN
F 1 "C" V 3789 2900 50  0000 C CNN
F 2 "MicrowaveDiplexerBoard:SMD_0402" H 3988 2750 50  0001 C CNN
F 3 "~" H 3950 2900 50  0001 C CNN
	1    3950 2900
	0    1    1    0   
$EndComp
$Comp
L Device:C C7
U 1 1 5AFE93AB
P 4350 2900
F 0 "C7" V 4098 2900 50  0000 C CNN
F 1 "C" V 4189 2900 50  0000 C CNN
F 2 "MicrowaveDiplexerBoard:SMD_0402" H 4388 2750 50  0001 C CNN
F 3 "~" H 4350 2900 50  0001 C CNN
	1    4350 2900
	0    1    1    0   
$EndComp
$Comp
L Device:L L2
U 1 1 5AFE9B43
P 3350 3050
F 0 "L2" H 3403 3096 50  0000 L CNN
F 1 "L" H 3403 3005 50  0000 L CNN
F 2 "MicrowaveDiplexerBoard:SMD_0402" H 3350 3050 50  0001 C CNN
F 3 "~" H 3350 3050 50  0001 C CNN
	1    3350 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L4
U 1 1 5AFE9C3F
P 3750 3050
F 0 "L4" H 3803 3096 50  0000 L CNN
F 1 "L" H 3803 3005 50  0000 L CNN
F 2 "MicrowaveDiplexerBoard:SMD_0402" H 3750 3050 50  0001 C CNN
F 3 "~" H 3750 3050 50  0001 C CNN
	1    3750 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L6
U 1 1 5AFE9C8E
P 4150 3050
F 0 "L6" H 4203 3096 50  0000 L CNN
F 1 "L" H 4203 3005 50  0000 L CNN
F 2 "MicrowaveDiplexerBoard:SMD_0402" H 4150 3050 50  0001 C CNN
F 3 "~" H 4150 3050 50  0001 C CNN
	1    4150 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L8
U 1 1 5AFE9CE0
P 4550 3050
F 0 "L8" H 4603 3096 50  0000 L CNN
F 1 "L" H 4603 3005 50  0000 L CNN
F 2 "MicrowaveDiplexerBoard:SMD_0402" H 4550 3050 50  0001 C CNN
F 3 "~" H 4550 3050 50  0001 C CNN
	1    4550 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L1
U 1 1 5AFE9D35
P 3150 2050
F 0 "L1" V 3340 2050 50  0000 C CNN
F 1 "L" V 3249 2050 50  0000 C CNN
F 2 "MicrowaveDiplexerBoard:SMD_0402" H 3150 2050 50  0001 C CNN
F 3 "~" H 3150 2050 50  0001 C CNN
	1    3150 2050
	0    -1   -1   0   
$EndComp
$Comp
L Device:L L3
U 1 1 5AFE9F3C
P 3550 2050
F 0 "L3" V 3740 2050 50  0000 C CNN
F 1 "L" V 3649 2050 50  0000 C CNN
F 2 "MicrowaveDiplexerBoard:SMD_0402" H 3550 2050 50  0001 C CNN
F 3 "~" H 3550 2050 50  0001 C CNN
	1    3550 2050
	0    -1   -1   0   
$EndComp
$Comp
L Device:L L5
U 1 1 5AFE9F73
P 3950 2050
F 0 "L5" V 4140 2050 50  0000 C CNN
F 1 "L" V 4049 2050 50  0000 C CNN
F 2 "MicrowaveDiplexerBoard:SMD_0402" H 3950 2050 50  0001 C CNN
F 3 "~" H 3950 2050 50  0001 C CNN
	1    3950 2050
	0    -1   -1   0   
$EndComp
$Comp
L Device:L L7
U 1 1 5AFE9FA4
P 4350 2050
F 0 "L7" V 4540 2050 50  0000 C CNN
F 1 "L" V 4449 2050 50  0000 C CNN
F 2 "MicrowaveDiplexerBoard:SMD_0402" H 4350 2050 50  0001 C CNN
F 3 "~" H 4350 2050 50  0001 C CNN
	1    4350 2050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3000 2050 3000 2450
Wire Wire Line
	3000 2450 3000 2900
Connection ~ 3000 2450
Wire Wire Line
	3300 2050 3350 2050
Wire Wire Line
	3350 2050 3400 2050
Connection ~ 3350 2050
Wire Wire Line
	3750 2050 3700 2050
Wire Wire Line
	3800 2050 3750 2050
Connection ~ 3750 2050
Wire Wire Line
	4100 2050 4150 2050
Wire Wire Line
	4200 2050 4150 2050
Connection ~ 4150 2050
Wire Wire Line
	4500 2050 4550 2050
Wire Wire Line
	4550 2050 4750 2050
Connection ~ 4550 2050
Wire Wire Line
	4750 2900 4550 2900
Wire Wire Line
	4500 2900 4550 2900
Connection ~ 4550 2900
Wire Wire Line
	4200 2900 4150 2900
Wire Wire Line
	4150 2900 4100 2900
Connection ~ 4150 2900
Wire Wire Line
	3800 2900 3750 2900
Wire Wire Line
	3750 2900 3700 2900
Connection ~ 3750 2900
Wire Wire Line
	3400 2900 3350 2900
Wire Wire Line
	3350 2900 3300 2900
Connection ~ 3350 2900
Wire Wire Line
	3350 3200 3750 3200
Wire Wire Line
	3750 3200 4150 3200
Connection ~ 3750 3200
Wire Wire Line
	4150 3200 4550 3200
Connection ~ 4150 3200
Wire Wire Line
	4550 3200 4950 3200
Wire Wire Line
	4950 3200 4950 3100
Connection ~ 4550 3200
Wire Wire Line
	3350 3200 2800 3200
Wire Wire Line
	2800 3200 2800 2650
Connection ~ 3350 3200
Wire Wire Line
	3350 2350 3750 2350
Wire Wire Line
	4150 2350 3750 2350
Connection ~ 3750 2350
Wire Wire Line
	4150 2350 4550 2350
Connection ~ 4150 2350
Wire Wire Line
	4550 2350 4950 2350
Wire Wire Line
	4950 2350 4950 2250
Connection ~ 4550 2350
Wire Wire Line
	5600 2350 5600 3200
Wire Wire Line
	5600 3200 4950 3200
Connection ~ 4950 3200
Connection ~ 5600 3200
Wire Wire Line
	4950 2350 5600 2350
Connection ~ 4950 2350
$EndSCHEMATC
