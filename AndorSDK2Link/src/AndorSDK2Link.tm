char *AndorSDK2ErrorName(int);
:Begin:
:Function:		AndorSDK2ErrorName
:Pattern:		AndorSDK2ErrorName[errorCode_Integer]
:Arguments:		{ errorCode }
:ArgumentTypes:	{ Integer }
:ReturnType:	String
:End:

void AndorSDK2FailWithError(int);
:Begin:
:Function:		AndorSDK2FailWithError
:Pattern:		AndorSDK2FailWithError[errorCode_Integer]
:Arguments:		{ errorCode }
:ArgumentTypes:	{ Integer }
:ReturnType:	Manual
:End:

void AndorSDK2GetCosmicRayFilter();
:Begin:
:Function:		AndorSDK2GetCosmicRayFilter
:Pattern:		AndorSDK2GetCosmicRayFilter[]
:Arguments:		{  }
:ArgumentTypes:	{  }
:ReturnType:	Manual
:End:

void AndorSDK2SetCosmicRayFilter(const char *);
:Begin:
:Function:		AndorSDK2SetCosmicRayFilter
:Pattern:		AndorSDK2SetCosmicRayFilter[filterState_String]
:Arguments:		{ filterState }
:ArgumentTypes:	{ String }
:ReturnType:	Manual
:End:

void AndorSDK2Initialize(const char *);
:Begin:
:Function:		AndorSDK2Initialize
:Pattern:		AndorSDK2Initialize[dir_String]
:Arguments:		{ dir }
:ArgumentTypes:	{ String }
:ReturnType:	Manual
:End:

void AndorSDK2Shutdown();
:Begin:
:Function:		AndorSDK2Shutdown
:Pattern:		AndorSDK2Shutdown[]
:Arguments:		{  }
:ArgumentTypes:	{  }
:ReturnType:	Manual
:End:

void AndorSDK2GetDetectorSize();
:Begin:
:Function:		AndorSDK2GetDetectorSize
:Pattern:		AndorSDK2GetDetectorSize[]
:Arguments:		{  }
:ArgumentTypes:	{  }
:ReturnType:	Manual
:End:

void AndorSDK2GetHardwareVersion();
:Begin:
:Function:		AndorSDK2GetHardwareVersion
:Pattern:		AndorSDK2GetHardwareVersion[]
:Arguments:		{  }
:ArgumentTypes:	{  }
:ReturnType:	Manual
:End:

void AndorSDK2GetSoftwareVersion();
:Begin:
:Function:		AndorSDK2GetSoftwareVersion
:Pattern:		AndorSDK2GetSoftwareVersion[]
:Arguments:		{  }
:ArgumentTypes:	{  }
:ReturnType:	Manual
:End:

void AndorSDK2GetHeadModel();
:Begin:
:Function:		AndorSDK2GetHeadModel
:Pattern:		AndorSDK2GetHeadModel[]
:Arguments:		{  }
:ArgumentTypes:	{  }
:ReturnType:	Manual
:End:

void AndorSDK2GetNumberADChannels();
:Begin:
:Function:		AndorSDK2GetNumberADChannels
:Pattern:		AndorSDK2GetNumberADChannels[]
:Arguments:		{  }
:ArgumentTypes:	{  }
:ReturnType:	Manual
:End:

void AndorSDK2SetADChannel(int);
:Begin:
:Function:		AndorSDK2SetADChannel
:Pattern:		AndorSDK2SetADChannel[channel_Integer]
:Arguments:		{ channel }
:ArgumentTypes:	{ Integer }
:ReturnType:	Manual
:End:

void AndorSDK2GetBitDepth(int);
:Begin:
:Function:		AndorSDK2GetBitDepth
:Pattern:		AndorSDK2GetBitDepth[channel_Integer]
:Arguments:		{ channel }
:ArgumentTypes:	{ Integer }
:ReturnType:	Manual
:End:

void AndorSDK2GetHSSpeeds(int, int);
:Begin:
:Function:		AndorSDK2GetHSSpeeds
:Pattern:		AndorSDK2GetHSSpeeds[adChannel_Integer, type_Integer]
:Arguments:		{ adChannel, type }
:ArgumentTypes:	{ Integer, Integer }
:ReturnType:	Manual
:End:

void AndorSDK2SetHSSpeed(int, int);
:Begin:
:Function:		AndorSDK2SetHSSpeed
:Pattern:		AndorSDK2SetHSSpeed[typ_Integer, index_Integer]
:Arguments:		{ typ, index }
:ArgumentTypes:	{ Integer, Integer }
:ReturnType:	Manual
:End:

void AndorSDK2GetVSSpeeds();
:Begin:
:Function:		AndorSDK2GetVSSpeeds
:Pattern:		AndorSDK2GetVSSpeeds[]
:Arguments:		{  }
:ArgumentTypes:	{  }
:ReturnType:	Manual
:End:

void AndorSDK2GetFastestRecommendedVSSpeed();
:Begin:
:Function:		AndorSDK2GetFastestRecommendedVSSpeed
:Pattern:		AndorSDK2GetFastestRecommendedVSSpeed[]
:Arguments:		{  }
:ArgumentTypes:	{  }
:ReturnType:	Manual
:End:

void AndorSDK2SetVSSpeed(int);
:Begin:
:Function:		AndorSDK2SetVSSpeed
:Pattern:		AndorSDK2SetVSSpeed[index_Integer]
:Arguments:		{ index }
:ArgumentTypes:	{ Integer }
:ReturnType:	Manual
:End:

void AndorSDK2SetExposureTime(double);
:Begin:
:Function:		AndorSDK2SetExposureTime
:Pattern:		AndorSDK2SetExposureTime[time_Real]
:Arguments:		{ time }
:ArgumentTypes:	{ Real }
:ReturnType:	Manual
:End:

void AndorSDK2SetNumberAccumulations(int);
:Begin:
:Function:		AndorSDK2SetNumberAccumulations
:Pattern:		AndorSDK2SetNumberAccumulations[nAcc_Integer]
:Arguments:		{ nAcc }
:ArgumentTypes:	{ Integer }
:ReturnType:	Manual
:End:

void AndorSDK2SetAccumulationCycleTime(double);
:Begin:
:Function:		AndorSDK2SetAccumulationCycleTime
:Pattern:		AndorSDK2SetAccumulationCycleTime[time_Real]
:Arguments:		{ time }
:ArgumentTypes:	{ Real }
:ReturnType:	Manual
:End:

void AndorSDK2SetNumberKinetics(int);
:Begin:
:Function:		AndorSDK2SetNumberKinetics
:Pattern:		AndorSDK2SetNumberKinetics[nKin_Integer]
:Arguments:		{ nKin }
:ArgumentTypes:	{ Integer }
:ReturnType:	Manual
:End:

void AndorSDK2SetKineticCycleTime(double);
:Begin:
:Function:		AndorSDK2SetKineticCycleTime
:Pattern:		AndorSDK2SetKineticCycleTime[time_Real]
:Arguments:		{ time }
:ArgumentTypes:	{ Real }
:ReturnType:	Manual
:End:

void AndorSDK2SetSingleTrack(int, int);
:Begin:
:Function:		AndorSDK2SetSingleTrack
:Pattern:		AndorSDK2SetSingleTrack[centre_Integer, height_Integer]
:Arguments:		{ centre, height }
:ArgumentTypes:	{ Integer, Integer }
:ReturnType:	Manual
:End:

void AndorSDK2SetSingleTrackHBin(int);
:Begin:
:Function:		AndorSDK2SetSingleTrackHBin
:Pattern:		AndorSDK2SetSingleTrackHBin[bin_Integer]
:Arguments:		{ bin }
:ArgumentTypes:	{ Integer }
:ReturnType:	Manual
:End:

void AndorSDK2SetMultiTrack(int, int, int);
:Begin:
:Function:		AndorSDK2SetMultiTrack
:Pattern:		AndorSDK2SetMultiTrack[nTracks_Integer, height_Integer, offset_Integer]
:Arguments:		{ nTracks, height, offset }
:ArgumentTypes:	{ Integer, Integer, Integer }
:ReturnType:	Manual
:End:

void AndorSDK2SetMultiTrackHBin(int);
:Begin:
:Function:		AndorSDK2SetMultiTrackHBin
:Pattern:		AndorSDK2SetMultiTrackHBin[bin_Integer]
:Arguments:		{ bin }
:ArgumentTypes:	{ Integer }
:ReturnType:	Manual
:End:

void AndorSDK2SetMultiTrackHRange(int, int);
:Begin:
:Function:		AndorSDK2SetMultiTrackHRange
:Pattern:		AndorSDK2SetMultiTrackHRange[iStart_Integer, iEnd_Integer]
:Arguments:		{ iStart, iEnd }
:ArgumentTypes:	{ Integer, Integer }
:ReturnType:	Manual
:End:

void AndorSDK2SetImage(int, int, int, int, int, int);
:Begin:
:Function:		AndorSDK2SetImage
:Pattern:		AndorSDK2SetImage[hbin_Integer, vbin_Integer, hstart_Integer, hend_Integer, vstart_Integer, vend_Integer]
:Arguments:		{ hbin, vbin, hstart, hend, vstart, vend }
:ArgumentTypes:	{ Integer, Integer, Integer, Integer, Integer, Integer }
:ReturnType:	Manual
:End:

void AndorSDK2GetAcquistionTimings();
:Begin:
:Function:		AndorSDK2GetAcquistionTimings
:Pattern:		AndorSDK2GetAcquistionTimings[]
:Arguments:		{  }
:ArgumentTypes:	{  }
:ReturnType:	Manual
:End:

void AndorSDK2SetAcquisitionMode(int);
:Begin:
:Function:		AndorSDK2SetAcquisitionMode
:Pattern:		AndorSDK2SetAcquisitionMode[mode_Integer]
:Arguments:		{ mode }
:ArgumentTypes:	{ Integer }
:ReturnType:	Manual
:End:

void AndorSDK2SetReadMode(int);
:Begin:
:Function:		AndorSDK2SetReadMode
:Pattern:		AndorSDK2SetReadMode[mode_Integer]
:Arguments:		{ mode }
:ArgumentTypes:	{ Integer }
:ReturnType:	Manual
:End:

void AndorSDK2SetTriggerMode(int);
:Begin:
:Function:		AndorSDK2SetTriggerMode
:Pattern:		AndorSDK2SetTriggerMode[mode_Integer]
:Arguments:		{ mode }
:ArgumentTypes:	{ Integer }
:ReturnType:	Manual
:End:

void AndorSDK2SetShutter(int, int, double, double);
:Begin:
:Function:		AndorSDK2SetShutter
:Pattern:		AndorSDK2SetShutter[typ_Integer, mode_Integer, closingtime_Real, openingtime_Real]
:Arguments:		{ typ, mode, closingtime, openingtime }
:ArgumentTypes:	{ Integer, Integer, Real, Real }
:ReturnType:	Manual
:End:

void AndorSDK2StartAcquisition();
:Begin:
:Function:		AndorSDK2StartAcquisition
:Pattern:		AndorSDK2StartAcquisition[]
:Arguments:		{  }
:ArgumentTypes:	{  }
:ReturnType:	Manual
:End:

void AndorSDK2AbortAcquisition();
:Begin:
:Function:		AndorSDK2AbortAcquisition
:Pattern:		AndorSDK2AbortAcquisition[]
:Arguments:		{  }
:ArgumentTypes:	{  }
:ReturnType:	Manual
:End:

void AndorSDK2GetAcquiredData(int);
:Begin:
:Function:		AndorSDK2GetAcquiredData
:Pattern:		AndorSDK2GetAcquiredData[nPixels_Integer]
:Arguments:		{ nPixels }
:ArgumentTypes:	{ Integer }
:ReturnType:	Manual
:End:

void AndorSDK2GetStatus();
:Begin:
:Function:		AndorSDK2GetStatus
:Pattern:		AndorSDK2GetStatus[]
:Arguments:		{  }
:ArgumentTypes:	{  }
:ReturnType:	Manual
:End:

void AndorSDK2GetAcquisitionProgress();
:Begin:
:Function:		AndorSDK2GetAcquisitionProgress
:Pattern:		AndorSDK2GetAcquisitionProgress[]
:Arguments:		{  }
:ArgumentTypes:	{  }
:ReturnType:	Manual
:End:

void AndorSDK2SetTemperature(double);
:Begin:
:Function:		AndorSDK2SetTemperature
:Pattern:		AndorSDK2SetTemperature[temp_Real]
:Arguments:		{ temp }
:ArgumentTypes:	{ Real }
:ReturnType:	Manual
:End:

void AndorSDK2GetTemperatureRange();
:Begin:
:Function:		AndorSDK2GetTemperatureRange
:Pattern:		AndorSDK2GetTemperatureRange[]
:Arguments:		{  }
:ArgumentTypes:	{  }
:ReturnType:	Manual
:End:

void AndorSDK2GetTemperature();
:Begin:
:Function:		AndorSDK2GetTemperature
:Pattern:		AndorSDK2GetTemperature[]
:Arguments:		{  }
:ArgumentTypes:	{  }
:ReturnType:	Manual
:End:

void AndorSDK2SetCooler(int);
:Begin:
:Function:		AndorSDK2SetCooler
:Pattern:		AndorSDK2SetCooler[state_Integer]
:Arguments:		{ state }
:ArgumentTypes:	{ Integer }
:ReturnType:	Manual
:End:

void AndorSDK2CoolerON();
:Begin:
:Function:		AndorSDK2CoolerON
:Pattern:		AndorSDK2CoolerON[]
:Arguments:		{  }
:ArgumentTypes:	{  }
:ReturnType:	Manual
:End:

void AndorSDK2CoolerOFF();
:Begin:
:Function:		AndorSDK2CoolerOFF
:Pattern:		AndorSDK2CoolerOFF[]
:Arguments:		{  }
:ArgumentTypes:	{  }
:ReturnType:	Manual
:End:

void AndorSDK2IsCoolerOn();
:Begin:
:Function:		AndorSDK2IsCoolerOn
:Pattern:		AndorSDK2IsCoolerOn[]
:Arguments:		{  }
:ArgumentTypes:	{  }
:ReturnType:	Manual
:End:

void AndorSDK2GetCapabilities();
:Begin:
:Function:		AndorSDK2GetCapabilities
:Pattern:		AndorSDK2GetCapabilities[]
:Arguments:		{  }
:ArgumentTypes:	{  }
:ReturnType:	Manual
:End: