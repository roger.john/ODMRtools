/*
 ============================================================================
 Name        : TimeHarpLink.c
 Author      : Roger John
 Version     :
 Copyright   :
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <mathlink.h>
#ifdef __linux__
#include <atmcdLXd.h>
#define MAX_PATH 255
#elif _WIN32
#include <windows.h>
#include <ATMCD32D.H>
#endif
#include "AndorSDK2Link.tmc"

int sargc;
char **sargv;

void abortWithMsg(MLINK ml, const char *msgId) {
	char err_msg[100];
	sprintf(err_msg, "Message[%s,\"%.76s\"]", msgId, MLErrorMessage(ml));
	MLClearError(ml);
	MLNewPacket(ml);
	MLEvaluate(ml, err_msg);
	MLNextPacket(ml);
	MLNewPacket(ml);
	MLPutSymbol(ml, "$Failed");
}

void failWithMsg(MLINK ml, const char *msg) {
	MLClearError(ml);
	MLNextPacket(ml);
	MLNewPacket(ml);
	if (msg) {
		MLPutFunction(ml, "$Failed", 1);
		MLPutString(ml, msg);
	} else
		MLPutSymbol(ml, "$Failed");
}

void failWithMsgAndError(MLINK ml, const char *msg) {
	if (!msg)
		msg = "";
	char *tmp = malloc(strlen(msg) + 500);
	sprintf(tmp, "%s%i: %s", msg, MLError(ml), MLErrorMessage(ml));
	MLClearError(ml);
	MLNextPacket(ml);
	MLNewPacket(ml);
	MLPutFunction(ml, "$Failed", 1);
	MLPutString(ml, tmp);
	free(tmp);
}
struct AndorErrorCodeName {
	int code;
	const char *name;
};
struct AndorErrorCodeName AndorErrorCodeNames[] = {
		{ 20001, "DRV_ERROR_CODES" }, { 20002, "DRV_SUCCESS" }, { 20003,
				"DRV_VXDNOTINSTALLED" }, { 20004, "DRV_ERROR_SCAN" }, { 20005,
				"DRV_ERROR_CHECK_SUM" }, { 20006, "DRV_ERROR_FILELOAD" }, {
				20007, "DRV_UNKNOWN_FUNCTION" },
		{ 20008, "DRV_ERROR_VXD_INIT" }, { 20009, "DRV_ERROR_ADDRESS" }, {
				20010, "DRV_ERROR_PAGELOCK" },
		{ 20011, "DRV_ERROR_PAGEUNLOCK" }, { 20012, "DRV_ERROR_BOARDTEST" }, {
				20013, "DRV_ERROR_ACK" }, { 20014, "DRV_ERROR_UP_FIFO" }, {
				20015, "DRV_ERROR_PATTERN" },
		{ 20017, "DRV_ACQUISITION_ERRORS" }, { 20018, "DRV_ACQ_BUFFER" }, {
				20019, "DRV_ACQ_DOWNFIFO_FULL" }, { 20020,
				"DRV_PROC_UNKONWN_INSTRUCTION" },
		{ 20021, "DRV_ILLEGAL_OP_CODE" }, { 20022, "DRV_KINETIC_TIME_NOT_MET" },
		{ 20023, "DRV_ACCUM_TIME_NOT_MET" }, { 20024, "DRV_NO_NEW_DATA" }, {
				20025, "DRV_PCI_DMA_FAIL" }, { 20026, "DRV_SPOOLERROR" }, {
				20027, "DRV_SPOOLSETUPERROR" }, { 20028,
				"DRV_FILESIZELIMITERROR" }, { 20029, "DRV_ERROR_FILESAVE" }, {
				20033, "DRV_TEMPERATURE_CODES" },
		{ 20034, "DRV_TEMPERATURE_OFF" }, { 20035,
				"DRV_TEMPERATURE_NOT_STABILIZED" }, { 20036,
				"DRV_TEMPERATURE_STABILIZED" }, { 20037,
				"DRV_TEMPERATURE_NOT_REACHED" }, { 20038,
				"DRV_TEMPERATURE_OUT_RANGE" }, { 20039,
				"DRV_TEMPERATURE_NOT_SUPPORTED" }, { 20040,
				"DRV_TEMPERATURE_DRIFT" }, { 20033, "DRV_TEMP_CODES" }, { 20034,
				"DRV_TEMP_OFF" }, { 20035, "DRV_TEMP_NOT_STABILIZED" }, { 20036,
				"DRV_TEMP_STABILIZED" }, { 20037, "DRV_TEMP_NOT_REACHED" }, {
				20038, "DRV_TEMP_OUT_RANGE" },
		{ 20039, "DRV_TEMP_NOT_SUPPORTED" }, { 20040, "DRV_TEMP_DRIFT" }, {
				20049, "DRV_GENERAL_ERRORS" }, { 20050, "DRV_INVALID_AUX" }, {
				20051, "DRV_COF_NOTLOADED" }, { 20052, "DRV_FPGAPROG" }, {
				20053, "DRV_FLEXERROR" }, { 20054, "DRV_GPIBERROR" }, { 20055,
				"DRV_EEPROMVERSIONERROR" }, { 20064, "DRV_DATATYPE" }, { 20065,
				"DRV_DRIVER_ERRORS" }, { 20066, "DRV_P1INVALID" }, { 20067,
				"DRV_P2INVALID" }, { 20068, "DRV_P3INVALID" }, { 20069,
				"DRV_P4INVALID" }, { 20070, "DRV_INIERROR" }, { 20071,
				"DRV_COFERROR" }, { 20072, "DRV_ACQUIRING" }, { 20073,
				"DRV_IDLE" }, { 20074, "DRV_TEMPCYCLE" }, { 20075,
				"DRV_NOT_INITIALIZED" }, { 20076, "DRV_P5INVALID" }, { 20077,
				"DRV_P6INVALID" }, { 20078, "DRV_INVALID_MODE" }, { 20079,
				"DRV_INVALID_FILTER" }, { 20080, "DRV_I2CERRORS" }, { 20081,
				"DRV_I2CDEVNOTFOUND" }, { 20082, "DRV_I2CTIMEOUT" }, { 20083,
				"DRV_P7INVALID" }, { 20084, "DRV_P8INVALID" }, { 20085,
				"DRV_P9INVALID" }, { 20086, "DRV_P10INVALID" }, { 20087,
				"DRV_P11INVALID" }, { 20089, "DRV_USBERROR" }, { 20090,
				"DRV_IOCERROR" }, { 20091, "DRV_VRMVERSIONERROR" }, { 20092,
				"DRV_GATESTEPERROR" }, { 20093,
				"DRV_USB_INTERRUPT_ENDPOINT_ERROR" }, { 20094,
				"DRV_RANDOM_TRACK_ERROR" },
		{ 20095, "DRV_INVALID_TRIGGER_MODE" }, { 20096,
				"DRV_LOAD_FIRMWARE_ERROR" },
		{ 20097, "DRV_DIVIDE_BY_ZERO_ERROR" }, { 20098,
				"DRV_INVALID_RINGEXPOSURES" }, { 20099, "DRV_BINNING_ERROR" }, {
				20100, "DRV_INVALID_AMPLIFIER" }, { 20101,
				"DRV_INVALID_COUNTCONVERT_MODE" },
		{ 20990, "DRV_ERROR_NOCAMERA" }, { 20991, "DRV_NOT_SUPPORTED" }, {
				20992, "DRV_NOT_AVAILABLE" }, { 20115, "DRV_ERROR_MAP" }, {
				20116, "DRV_ERROR_UNMAP" }, { 20117, "DRV_ERROR_MDL" }, { 20118,
				"DRV_ERROR_UNMDL" }, { 20119, "DRV_ERROR_BUFFSIZE" }, { 20121,
				"DRV_ERROR_NOHANDLE" }, { 20130, "DRV_GATING_NOT_AVAILABLE" }, {
				20131, "DRV_FPGA_VOLTAGE_ERROR" }, { 20150, "DRV_OW_CMD_FAIL" },
		{ 20151, "DRV_OWMEMORY_BAD_ADDR" },
		{ 20152, "DRV_OWCMD_NOT_AVAILABLE" }, { 20153, "DRV_OW_NO_SLAVES" }, {
				20154, "DRV_OW_NOT_INITIALIZED" }, { 20155,
				"DRV_OW_ERROR_SLAVE_NUM" }, { 20156, "DRV_MSTIMINGS_ERROR" }, {
				20173, "DRV_OA_NULL_ERROR" },
		{ 20174, "DRV_OA_PARSE_DTD_ERROR" }, { 20175,
				"DRV_OA_DTD_VALIDATE_ERROR" }, { 20176,
				"DRV_OA_FILE_ACCESS_ERROR" }, { 20177,
				"DRV_OA_FILE_DOES_NOT_EXIST" }, { 20178,
				"DRV_OA_XML_INVALID_OR_NOT_FOUND_ERROR" }, { 20179,
				"DRV_OA_PRESET_FILE_NOT_LOADED" }, { 20180,
				"DRV_OA_USER_FILE_NOT_LOADED" }, { 20181,
				"DRV_OA_PRESET_AND_USER_FILE_NOT_LOADED" }, { 20182,
				"DRV_OA_INVALID_FILE" }, { 20183,
				"DRV_OA_FILE_HAS_BEEN_MODIFIED" },
		{ 20184, "DRV_OA_BUFFER_FULL" },
		{ 20185, "DRV_OA_INVALID_STRING_LENGTH" }, { 20186,
				"DRV_OA_INVALID_CHARS_IN_NAME" }, { 20187,
				"DRV_OA_INVALID_NAMING" }, { 20188, "DRV_OA_GET_CAMERA_ERROR" },
		{ 20189, "DRV_OA_MODE_ALREADY_EXISTS" }, { 20190,
				"DRV_OA_STRINGS_NOT_EQUAL" }, { 20191, "DRV_OA_NO_USER_DATA" },
		{ 20192, "DRV_OA_VALUE_NOT_SUPPORTED" }, { 20193,
				"DRV_OA_MODE_DOES_NOT_EXIST" }, { 20194,
				"DRV_OA_CAMERA_NOT_SUPPORTED" }, { 20195,
				"DRV_OA_FAILED_TO_GET_MODE" },
		{ 20211, "DRV_PROCESSING_FAILED" } };
char *AndorSDK2ErrorName(int errorCode) {
	int n = sizeof(AndorErrorCodeNames) / sizeof(struct AndorErrorCodeName);
	int i = 0;
	while (i < n && errorCode != AndorErrorCodeNames[i].code)
		i++;
	if (i < n)
		return AndorErrorCodeNames[i].name;
	else
		return "unknown error";
}
void AndorSDK2FailWithError(int errorCode) {
	if (!MLPutFunction(stdlink, "$Failed", 2)
			|| !MLPutInteger(stdlink, errorCode)
			|| !MLPutString(stdlink, AndorSDK2ErrorName(errorCode)))
		failWithMsgAndError(stdlink, "AndorSDK2FailWithError:result:error: ");
}
void AndorSDK2GetCosmicRayFilter(void) {
	int filterMode;
	int r = GetFilterMode(&filterMode);
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, filterMode == 2 ? "True" : "False"))
			failWithMsgAndError(stdlink,
					"AndoSDK2LinkGetCosmicRayFilter:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2SetCosmicRayFilter(const char *filterState) {
	int r = SetFilterMode(strcmp(filterState, "False") == 0 ? 0 : 2);
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndoSDK2LinkSetCosmicRayFilter:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}

void AndorSDK2Initialize(const char *dir) {
	int r = Initialize(dir);
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "AndorSDK2Initialize:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2Shutdown(void) {
	int r = ShutDown();
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "AndorSDK2Shutdown:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2GetDetectorSize(void) {
	int x, y;
	int r = GetDetector(&x, &y);
	if (r == DRV_SUCCESS) {
		if (!MLPutFunction(stdlink, "List", 2) || !MLPutInteger(stdlink, x)
				|| !MLPutInteger(stdlink, y))
			failWithMsgAndError(stdlink,
					"AndorSDK2GetDetectorSize:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2GetHardwareVersion(void) {
	int PCB, Decode, dummy1, dummy2, CameraFirmwareVersion, CameraFirmwareBuild;
	int r = GetHardwareVersion(&PCB, &Decode, &dummy1, &dummy2,
			&CameraFirmwareVersion, &CameraFirmwareBuild);
	if (r == DRV_SUCCESS) {
		if (!MLPutFunction(stdlink, "List", 6) || !MLPutInteger(stdlink, PCB)
				|| !MLPutInteger(stdlink, Decode)
				|| !MLPutInteger(stdlink, dummy1)
				|| !MLPutInteger(stdlink, dummy2)
				|| !MLPutInteger(stdlink, CameraFirmwareVersion)
				|| !MLPutInteger(stdlink, CameraFirmwareBuild))
			failWithMsgAndError(stdlink,
					"AndorSDK2GetHardwareVersion:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2GetSoftwareVersion(void) {
	int eprom, cofFile, vxdRev, vxdVer, dllRev, dllVer;
	int r = GetSoftwareVersion(&eprom, &cofFile, &vxdRev, &vxdVer, &dllRev,
			&dllVer);
	if (r == DRV_SUCCESS) {
		if (!MLPutFunction(stdlink, "List", 6) || !MLPutInteger(stdlink, eprom)
				|| !MLPutInteger(stdlink, cofFile)
				|| !MLPutInteger(stdlink, vxdRev)
				|| !MLPutInteger(stdlink, vxdVer)
				|| !MLPutInteger(stdlink, dllRev)
				|| !MLPutInteger(stdlink, dllVer))
			failWithMsgAndError(stdlink,
					"AndorSDK2GetSoftwareVersion:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2GetHeadModel(void) {
	char name[MAX_PATH];
	int r = GetHeadModel(name);
	if (r == DRV_SUCCESS) {
		if (!MLPutString(stdlink, name))
			failWithMsgAndError(stdlink,
					"AndorSDK2GetHeadModel:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2GetNumberADChannels(void) {
	int n;
	int r = GetNumberADChannels(&n);
	if (r == DRV_SUCCESS) {
		if (!MLPutInteger(stdlink, n))
			failWithMsgAndError(stdlink,
					"AndorSDK2GetNumberADChannels:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2SetADChannel(int channel) {
	int r = SetADChannel(channel);
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK2SetADChannel:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2GetBitDepth(int channel) {
	int depth;
	int r = GetBitDepth(channel, &depth);
	if (r == DRV_SUCCESS) {
		if (!MLPutInteger(stdlink, depth))
			failWithMsgAndError(stdlink, "AndorSDK2GetBitDepth:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2GetHSSpeeds(int adChannel, int type) {
	int nSpeeds;
	int r = GetNumberHSSpeeds(adChannel, type, &nSpeeds);
	if (r != DRV_SUCCESS) {
		AndorSDK2FailWithError(r);
		return;
	}
	if (!MLPutFunction(stdlink, "List", nSpeeds))
		failWithMsgAndError(stdlink, "AndorSDK2GetVSSpeeds:result:error: ");
	for (int i = 0; i < nSpeeds; i++) {
		float tmp;
		r = GetHSSpeed(adChannel, type, i, &tmp);
		if (r != DRV_SUCCESS) {
			AndorSDK2FailWithError(r);
			return;
		}
		if (!MLPutReal(stdlink, tmp))
			failWithMsgAndError(stdlink,
					"AndorSDK2GetVSSpeeds:result:entry:error: ");
	}
}
void AndorSDK2SetHSSpeed(int typ, int index) {
	int r = SetHSSpeed(typ, index);
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "AndorSDK2SetHSSpeed:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2GetVSSpeeds(void) {
	int nSpeeds;
	int r = GetNumberVSSpeeds(&nSpeeds);
	if (r != DRV_SUCCESS) {
		AndorSDK2FailWithError(r);
		return;
	}
	if (!MLPutFunction(stdlink, "List", nSpeeds))
		failWithMsgAndError(stdlink, "AndorSDK2GetVSSpeeds:result:error: ");
	for (int i = 0; i < nSpeeds; i++) {
		float tmp;
		r = GetVSSpeed(i, &tmp);
		if (r != DRV_SUCCESS) {
			AndorSDK2FailWithError(r);
			return;
		}
		if (!MLPutReal(stdlink, tmp))
			failWithMsgAndError(stdlink,
					"AndorSDK2GetVSSpeeds:result:entry:error: ");
	}
}
void AndorSDK2GetFastestRecommendedVSSpeed(void) {
	int index;
	float speed;
	int r = GetFastestRecommendedVSSpeed(&index, &speed);
	if (r == DRV_SUCCESS) {
		if (!MLPutFunction(stdlink, "List", 2) || !MLPutInteger(stdlink, index)
				|| !MLPutReal(stdlink, speed))
			failWithMsgAndError(stdlink,
					"AndorSDK2GetFastestRecommendedVSSpeed:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2SetVSSpeed(int index) {
	int r = SetVSSpeed(index);
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "AndorSDK2SetVSSpeed:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2SetExposureTime(double time) {
	int r = SetExposureTime(time);
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK2SetExposureTime:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2SetNumberAccumulations(int nAcc) {
	int r = SetNumberAccumulations(nAcc);
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK2SetNumberAccumulations:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2SetAccumulationCycleTime(double time) {
	int r = SetAccumulationCycleTime((float) time);
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK2SetAccumulationCycletime:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2SetNumberKinetics(int nKin) {
	int r = SetNumberKinetics(nKin);
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK2SetNumberKinetics:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2SetKineticCycleTime(double time) {
	int r = SetKineticCycleTime((float) time);
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK2SetKineticCycletime:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2SetSingleTrack(int centre, int height) {
	int r = SetSingleTrack(centre, height);
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK2SetSingleTrack:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2SetSingleTrackHBin(int bin) {
	int r = SetSingleTrackHBin(bin);
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK2SetSingleTrackBin:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2SetMultiTrack(int nTracks, int height, int offset) {
	int bottom, gap;
	int r = SetMultiTrack(nTracks, height, offset, &bottom, &gap);
	if (r == DRV_SUCCESS) {
		if (!MLPutFunction(stdlink, "List", 2) || !MLPutInteger(stdlink, bottom)
				|| !MLPutReal(stdlink, gap))
			failWithMsgAndError(stdlink,
					"AndorSDK2SetMultiTrack:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2SetMultiTrackHBin(int bin) {
	int r = SetMultiTrackHBin(bin);
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK2SetMultiTrackBin:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2SetMultiTrackHRange(int iStart, int iEnd) {
	int r = SetMultiTrackHRange(iStart, iEnd);
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK2SetMultiTrackRange:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2SetImage(int hbin, int vbin, int hstart, int hend, int vstart, int vend) {
	int r = SetImage(hbin, vbin, hstart, hend, vstart, vend);
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "AndorSDK2SetImage:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2GetAcquistionTimings(void) {
	float exposure, accumulate, kinetic;
	int r = GetAcquisitionTimings(&exposure, &accumulate, &kinetic);
	if (r == DRV_SUCCESS) {
		if (!MLPutFunction(stdlink, "List", 3) || !MLPutReal(stdlink, exposure)
				|| !MLPutReal(stdlink, accumulate)
				|| !MLPutReal(stdlink, kinetic))
			failWithMsgAndError(stdlink,
					"AndorSDK2GetAcquistionTimings:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2SetAcquisitionMode(int mode) {
	int r = SetAcquisitionMode(mode);
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK2SetAcquisitionMode:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2SetReadMode(int mode) {
	int r = SetReadMode(mode);
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "AndorSDK2SetReadMode:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2SetTriggerMode(int mode) {
	int r = SetTriggerMode(mode);
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK2SetTriggerMode:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2SetShutter(int typ, int mode, double closingtime, double openingtime) {
	int r = SetShutter(typ, mode, (int) round(closingtime * 1000),
			(int) round(openingtime * 1000));
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "AndorSDK2SetShutter:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2StartAcquisition(void) {
	int r = StartAcquisition();
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK2StartAcquisition:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2AbortAcquisition(void) {
	int r = AbortAcquisition();
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK2AbortAcquisition:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2GetAcquiredData(int nPixels) {
	int *data = calloc(nPixels, sizeof(int));
	if (!data) {
		if (!MLPutFunction(stdlink, "$Failed", 1)
				|| !MLPutString(stdlink, "could not allocate buffer"))
			failWithMsgAndError(stdlink,
					"AndorSDK2GetAcquiredData:buffer:error: ");
		return;
	}
	int r = GetAcquiredData(data, nPixels);
	if (r == DRV_SUCCESS) {
		if (!MLPutInteger32List(stdlink, data, nPixels))
			failWithMsgAndError(stdlink,
					"AndorSDK2GetAcquiredData:result:error: ");
	} else
		AndorSDK2FailWithError(r);
	free(data);
}
void AndorSDK2GetStatus(void) {
	int status;
	int r = GetStatus(&status);
	if (r == DRV_SUCCESS) {
		if (!MLPutFunction(stdlink, "List", 2) || !MLPutInteger(stdlink, status)
				|| !MLPutString(stdlink, AndorSDK2ErrorName(status)))
			failWithMsgAndError(stdlink, "AndorSDK2GetStatus:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2GetAcquisitionProgress(void) {
	long acc, series;
	int r = GetAcquisitionProgress(&acc, &series);
	if (r == DRV_SUCCESS) {
		if (!MLPutFunction(stdlink, "List", 2) || !MLPutInteger(stdlink, acc)
				|| !MLPutInteger(stdlink, series))
			failWithMsgAndError(stdlink,
					"AndorSDK2GetTemperatureRange:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2SetTemperature(double temp) {
	int r = SetTemperature((int) round(10 * temp));
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink,
					"AndorSDK2SetTemperature:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2GetTemperatureRange(void) {
	int minTemp, maxTemp;
	int r = GetTemperatureRange(&minTemp, &maxTemp);
	if (r == DRV_SUCCESS) {
		if (!MLPutFunction(stdlink, "List", 2)
				|| !MLPutReal(stdlink, minTemp / 10.0)
				|| !MLPutReal(stdlink, maxTemp / 10.0))
			failWithMsgAndError(stdlink,
					"AndorSDK2GetTemperatureRange:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2GetTemperature(void) {
	float temp;
	int r = GetTemperature(&temp);
	if (r == DRV_SUCCESS) {
		if (!MLPutReal(stdlink, temp))
			failWithMsgAndError(stdlink,
					"AndorSDK2GetTemperature:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2SetCooler(int state) {
	int r = state ? CoolerON() : CoolerOFF();
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, "Null"))
			failWithMsgAndError(stdlink, "AndorSDK2SetCooler:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
void AndorSDK2CoolerON(void) {
	AndorSDK2SetCooler(1);
}
void AndorSDK2CoolerOFF(void) {
	AndorSDK2SetCooler(0);
}
void AndorSDK2IsCoolerOn(void) {
	int status;
	int r = IsCoolerOn(&status);
	if (r == DRV_SUCCESS) {
		if (!MLPutSymbol(stdlink, status == 1 ? "True" : "False"))
			failWithMsgAndError(stdlink, "AndorSDK2IsCoolerOn:result:error: ");
	} else
		AndorSDK2FailWithError(r);
}
int decodeBitCaps(MLINK ml, unsigned long int value, const char **desc,
		int descLength) {
	int i = 0, c = 0;
	while (value != 0 && i < descLength) {
		if (value & 1) {
			c++;
			if (!MLPutString(ml, desc[i]))
				return -c;
		}
		value >>= 1;
		i++;
	}
	return c;
}
#define STRING_ARRAY_LENGTH(x) sizeof(x)/sizeof(char*)
const char *AndorAquisitionModes[] = { "AC_ACQMODE_SINGLE", "AC_ACQMODE_VIDEO",
		"AC_ACQMODE_ACCUMULATE", "AC_ACQMODE_KINETIC",
		"AC_ACQMODE_FRAMETRANSFER", "AC_ACQMODE_FASTKINETICS",
		"AC_ACQMODE_OVERLAP" };
const char *AndorReadModes[] = { "AC_READMODE_FULLIMAGE",
		"AC_READMODE_SUBIMAGE", "AC_READMODE_SINGLETRACK", "AC_READMODE_FVB",
		"AC_READMODE_MULTITRACK", "AC_READMODE_RANDOMTRACK",
		"AC_READMODE_MULTITRACKSCAN" };
const char *AndorFTReadModes[] = { "AC_FT_READMODE_FULLIMAGE",
		"AC_FT_READMODE_SUBIMAGE", "AC_FT_READMODE_SINGLETRACK",
		"AC_FT_READMODE_FVB", "AC_FT_READMODE_MULTITRACK",
		"AC_FT_READMODE_RANDOMTRACK", "AC_FT_READMODE_MULTITRACKSCAN" };
const char *AndorTriggerModes[] = { "AC_TRIGGERMODE_INTERNAL",
		"AC_TRIGGERMODE_EXTERNAL", "AC_TRIGGERMODE_EXTERNAL_FVB_EM",
		"AC_TRIGGERMODE_CONTINUOUS", "AC_TRIGGERMODE_EXTERNALSTART",
		"AC_TRIGGERMODE_EXTERNALEXPOSURE", "AC_TRIGGERMODE_INVERTED 0x40",
		"AC_TRIGGERMODE_EXTERNAL_CHARGESHIFTING" };
const char *AndorCameraTypes[] = { "AC_CAMERATYPE_PDA", "AC_CAMERATYPE_IXON",
		"AC_CAMERATYPE_ICCD", "AC_CAMERATYPE_EMCCD", "AC_CAMERATYPE_CCD",
		"AC_CAMERATYPE_ISTAR", "AC_CAMERATYPE_VIDEO", "AC_CAMERATYPE_IDUS",
		"AC_CAMERATYPE_NEWTON", "AC_CAMERATYPE_SURCAM", "AC_CAMERATYPE_USBICCD",
		"AC_CAMERATYPE_LUCA", "AC_CAMERATYPE_RESERVED", "AC_CAMERATYPE_IKON",
		"AC_CAMERATYPE_INGAAS", "AC_CAMERATYPE_IVAC",
		"AC_CAMERATYPE_UNPROGRAMMED", "AC_CAMERATYPE_CLARA",
		"AC_CAMERATYPE_USBISTAR", "AC_CAMERATYPE_SIMCAM", "AC_CAMERATYPE_NEO",
		"AC_CAMERATYPE_IXONULTRA", "AC_CAMERATYPE_VOLMOS",
		"AC_CAMERATYPE_IVAC_CCD", "AC_CAMERATYPE_ASPEN", "AC_CAMERATYPE_ASCENT",
		"AC_CAMERATYPE_ALTA", "AC_CAMERATYPE_ALTAF", "AC_CAMERATYPE_IKONXL",
		"AC_CAMERATYPE_RES1" };
const char *AndorPixelModes[] = { "AC_PIXELMODE_8BIT", "AC_PIXELMODE_14BIT",
		"AC_PIXELMODE_16BIT", "AC_PIXELMODE_32BIT" };
const char *AndorPixelColorModels[] = { "AC_PIXELMODE_MONO", "AC_PIXELMODE_RGB",
		"AC_PIXELMODE_CMY" };
const char *AndorSetFunctions[] = { "AC_SETFUNCTION_VREADOUT",
		"AC_SETFUNCTION_HREADOUT", "AC_SETFUNCTION_TEMPERATURE",
		"AC_SETFUNCTION_MCPGAIN", "AC_SETFUNCTION_EMCCDGAIN",
		"AC_SETFUNCTION_BASELINECLAMP", "AC_SETFUNCTION_VSAMPLITUDE",
		"AC_SETFUNCTION_HIGHCAPACITY", "AC_SETFUNCTION_BASELINEOFFSET",
		"AC_SETFUNCTION_PREAMPGAIN", "AC_SETFUNCTION_CROPMODE",
		"AC_SETFUNCTION_DMAPARAMETERS", "AC_SETFUNCTION_HORIZONTALBIN",
		"AC_SETFUNCTION_MULTITRACKHRANGE", "AC_SETFUNCTION_RANDOMTRACKNOGAPS",
		"AC_SETFUNCTION_EMADVANCED", "AC_SETFUNCTION_GATEMODE",
		"AC_SETFUNCTION_DDGTIMES", "AC_SETFUNCTION_IOC",
		"AC_SETFUNCTION_INTELLIGATE", "AC_SETFUNCTION_INSERTION_DELAY",
		"AC_SETFUNCTION_GATESTEP", "AC_SETFUNCTION_TRIGGERTERMINATION",
		"AC_SETFUNCTION_EXTENDEDNIR", "AC_SETFUNCTION_SPOOLTHREADCOUNT",
		"AC_SETFUNCTION_REGISTERPACK", "AC_SETFUNCTION_PRESCANS",
		"AC_SETFUNCTION_GATEWIDTHSTEP", "AC_SETFUNCTION_EXTENDED_CROP_MODE",
		"AC_SETFUNCTION_SUPERKINETICS", "AC_SETFUNCTION_TIMESCAN" };
const char *AndorGetFunctions[] = { "AC_GETFUNCTION_TEMPERATURE",
		"AC_GETFUNCTION_TARGETTEMPERATURE", "AC_GETFUNCTION_TEMPERATURERANGE",
		"AC_GETFUNCTION_DETECTORSIZE", "AC_GETFUNCTION_MCPGAIN",
		"AC_GETFUNCTION_EMCCDGAIN", "AC_GETFUNCTION_HVFLAG",
		"AC_GETFUNCTION_GATEMODE", "AC_GETFUNCTION_DDGTIMES",
		"AC_GETFUNCTION_IOC", "AC_GETFUNCTION_INTELLIGATE",
		"AC_GETFUNCTION_INSERTION_DELAY", "AC_GETFUNCTION_GATESTEP",
		"AC_GETFUNCTION_PHOSPHORSTATUS", "AC_GETFUNCTION_MCPGAINTABLE",
		"AC_GETFUNCTION_BASELINECLAMP", "AC_GETFUNCTION_GATEWIDTHSTEP" };
const char *AndorFeatures[] = { "AC_FEATURES_POLLING", "AC_FEATURES_EVENTS",
		"AC_FEATURES_SPOOLING", "AC_FEATURES_SHUTTER", "AC_FEATURES_SHUTTEREX",
		"AC_FEATURES_EXTERNAL_I2C", "AC_FEATURES_SATURATIONEVENT",
		"AC_FEATURES_FANCONTROL", "AC_FEATURES_MIDFANCONTROL",
		"AC_FEATURES_TEMPERATUREDURINGACQUISITION",
		"AC_FEATURES_KEEPCLEANCONTROL", "AC_FEATURES_DDGLITE",
		"AC_FEATURES_FTEXTERNALEXPOSURE", "AC_FEATURES_KINETICEXTERNALEXPOSURE",
		"AC_FEATURES_DACCONTROL", "AC_FEATURES_METADATA",
		"AC_FEATURES_IOCONTROL", "AC_FEATURES_PHOTONCOUNTING",
		"AC_FEATURES_COUNTCONVERT", "AC_FEATURES_DUALMODE",
		"AC_FEATURES_OPTACQUIRE", "AC_FEATURES_REALTIMESPURIOUSNOISEFILTER",
		"AC_FEATURES_POSTPROCESSSPURIOUSNOISEFILTER",
		"AC_FEATURES_DUALPREAMPGAIN", "AC_FEATURES_DEFECT_CORRECTION",
		"AC_FEATURES_STARTOFEXPOSURE_EVENT", "AC_FEATURES_ENDOFEXPOSURE_EVENT",
		"AC_FEATURES_CAMERALINK", "AC_FEATURES_FIFOFULL_EVENT",
		"AC_FEATURES_SENSOR_PORT_CONFIGURATION",
		"AC_FEATURES_SENSOR_COMPENSATION", "AC_FEATURES_IRIG_SUPPORT" };
const char *AndorEMGainCapabilities[] = { "AC_EMGAIN_8BIT", "AC_EMGAIN_12BIT",
		"AC_EMGAIN_LINEAR12", "AC_EMGAIN_REAL12" };
void AndorSDK2GetCapabilities(void) {
	int r;
	MLINK loop = MLLoopbackOpen(stdenv, &r);
	if (r != MLEOK) {
		MLPutFunction(stdlink, "$Failed", 1);
		MLPutString(stdlink, MLErrorString(stdenv, r));
	} else {
		int c = 0, d;
		AndorCapabilities caps;
		caps.ulSize = sizeof(AndorCapabilities);
		r = GetCapabilities(&caps);
		if ((d = decodeBitCaps(loop, caps.ulAcqModes, AndorAquisitionModes,
				STRING_ARRAY_LENGTH(AndorAquisitionModes))) < 0) {
			failWithMsgAndError(stdlink,
					"AndorSDK2GetCapabilities:result:AquisitionModes:error: ");
			MLClose(loop);
			return;
		} else
			c += d;
		if ((d = decodeBitCaps(loop, caps.ulReadModes, AndorReadModes,
				STRING_ARRAY_LENGTH(AndorReadModes))) < 0) {
			failWithMsgAndError(stdlink,
					"AndorSDK2GetCapabilities:result:ReadModes:error: ");
			MLClose(loop);
			return;
		} else
			c += d;
		if ((d = decodeBitCaps(loop, caps.ulFTReadModes, AndorFTReadModes,
				STRING_ARRAY_LENGTH(AndorFTReadModes))) < 0) {
			failWithMsgAndError(stdlink,
					"AndorSDK2GetCapabilities:result:ReadModes:error: ");
			MLClose(loop);
			return;
		} else
			c += d;
		if (caps.ulCameraType < STRING_ARRAY_LENGTH(AndorCameraTypes)) {
			if (!MLPutString(loop, AndorCameraTypes[caps.ulCameraType])) {
				failWithMsgAndError(stdlink,
						"AndorSDK2GetCapabilities:result:CameraType:error: ");
				MLClose(loop);
				return;
			}
		} else if (!MLPutString(loop, "AC_CAMERATYPE_RESERVED")) {
			failWithMsgAndError(stdlink,
					"AndorSDK2GetCapabilities:result:ReservedCameraType:error: ");
			MLClose(loop);
			return;
		}
		c++;
		if ((d = decodeBitCaps(loop, caps.ulPixelMode, AndorPixelModes,
				STRING_ARRAY_LENGTH(AndorPixelModes))) < 0) {
			failWithMsgAndError(stdlink,
					"AndorSDK2GetCapabilities:result:AndorPixelModes:error: ");
			MLClose(loop);
			return;
		} else
			c += d;
		if ((caps.ulPixelMode >> 16)
				< STRING_ARRAY_LENGTH(AndorPixelColorModels)) {
			if (!MLPutString(loop, AndorCameraTypes[caps.ulPixelMode >> 16])) {
				failWithMsgAndError(stdlink,
						"AndorSDK2GetCapabilities:result:PixelColorModel:error: ");
				MLClose(loop);
				return;
			}
		} else if (!MLPutString(loop, "AC_PIXELMODE_RESERVED")) {
			failWithMsgAndError(stdlink,
					"AndorSDK2GetCapabilities:result:ReservedPixelColorModel:error: ");
			MLClose(loop);
			return;
		}
		c++;
		if ((d = decodeBitCaps(loop, caps.ulSetFunctions, AndorSetFunctions,
				STRING_ARRAY_LENGTH(AndorSetFunctions))) < 0) {
			failWithMsgAndError(stdlink,
					"AndorSDK2GetCapabilities:result:SetFunctions:error: ");
			MLClose(loop);
			return;
		} else
			c += d;
		if ((d = decodeBitCaps(loop, caps.ulGetFunctions, AndorGetFunctions,
				STRING_ARRAY_LENGTH(AndorGetFunctions))) < 0) {
			failWithMsgAndError(stdlink,
					"AndorSDK2GetCapabilities:result:GetFunctions:error: ");
			MLClose(loop);
			return;
		} else
			c += d;
		if ((d = decodeBitCaps(loop, caps.ulFeatures, AndorFeatures,
				STRING_ARRAY_LENGTH(AndorFeatures))) < 0) {
			failWithMsgAndError(stdlink,
					"AndorSDK2GetCapabilities:result:Features:error: ");
			MLClose(loop);
			return;
		} else
			c += d;
		if ((d = decodeBitCaps(loop, caps.ulEMGainCapability,
				AndorEMGainCapabilities,
				STRING_ARRAY_LENGTH(AndorEMGainCapabilities))) < 0) {
			failWithMsgAndError(stdlink,
					"AndorSDK2GetCapabilities:result:EMGainCapabilities:error: ");
			MLClose(loop);
			return;
		} else
			c += d;
		if (!MLPutFunction(loop, "Rule", 2)
				|| !MLPutString(loop, "AC_PCI_MAX_SPEED_HZ")
				|| !MLPutInteger(loop, caps.ulPCICard)) {
			failWithMsgAndError(stdlink,
					"AndorSDK2GetCapabilities:result:EMGainCapabilities:error: ");
			MLClose(loop);
			return;
		} else
			c++;
		MLEndPacket(loop);
		if (!MLPutFunction(stdlink, "List", c)) {
			failWithMsgAndError(stdlink,
					"AndorSDK2GetCapabilities:result:list:error: ");
			MLClose(loop);
			return;
		}
		if (!MLTransferToEndOfLoopbackLink(stdlink, loop))
			failWithMsgAndError(stdlink,
					"AndorSDK2GetCapabilities:result:transfer:error: ");
		MLClose(loop);
	}

}
/*

 */

int main(int argc, char **argv) {
	sargc = argc;
	sargv = argv;
	return MLMain(argc, argv);
}

